.. index:: Installation

============
Installation
============

You have 2 options to install the CMS, either through composer or by downloading the latest stable release from our website https://www.continut.org

Composer
========

Download the latest stable version of Conținut CMS from https://www.bitbucket.org

Make sure Composer is already installed on your machine, then run::

   composer install

This will install the project's dependencies along with the "dev" tools: codeception, selenium and php code sniffer, which is great to run in a development machine.

If you plan to use it in production, then simply run::

   composer install --no-dev

Manual download
===============

Download the latest stable version from https://www.continut.org. This stable version already comes bundled with the necessary libraries.

Running the project
===================

You will need to be running an *apache* or *nginx* server, along with MySQL, sqLite or PostgreSQL.

The minimal PHP version supported is 5.6, and you will need the following PHP extensions: *@todo list* so make sure to install them

Define a VirtualHost for your project and access it or if you copied the project in the root folder of your local webserver, go to http://localhost/install.php to start the installation process

Make sure you follow all the installation steps, and once you're done with the installation you can safely remove the "install.php" file and the "Install" folder.

Docker image
============

If you prefer to work with an already configured docker image, then just grab the latest docker image from *@todo docker hub link*

Run the docker image, cd into the project folder located inside */var/www/continutcms* and make sure you run the following command to
update the project to it's latest version::

   git pull

Add this line to your hosts file::

   continut.dev 192.168.1.100

The CMS is already installed and configured and you can access it at the following address http://continut.dev
