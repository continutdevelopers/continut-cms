========
Features
========

The following is a list of features of Conținut CMS. Those that are in beta or planned for a future version are marked as such

+--------------------------------------------+---------+-------+----------+
|Feature name                                | Stable  | Beta  | Planned  |
+============================================+=========+=======+==========+
|Multidomain support                         | X       |       |          |
+--------------------------------------------+---------+-------+----------+
|Multilanguage support for pages and content | X       |       |          |
|elements                                    |         |       |          |
+--------------------------------------------+---------+-------+----------+
|Recursive content elements                  | X       |       |          |
+--------------------------------------------+---------+-------+----------+
|Backend editing with drag & drop support    | X       |       |          |
|for content elements and containers         |         |       |          |
+--------------------------------------------+---------+-------+----------+
|Themes and extensions support               | X       |       |          |
+--------------------------------------------+---------+-------+----------+
|Global, domain and language configurations  | X       |       |          |
+--------------------------------------------+---------+-------+----------+
|Fullpage cache, using an integrated file    | X       |       |          |
|cache, redis cache or APCU cache            |         |       |          |
+--------------------------------------------+---------+-------+----------+
|Headless support                            |         | X     |          |
+--------------------------------------------+---------+-------+----------+
|Backend users UAC system                    |         | X     |          |
+--------------------------------------------+---------+-------+----------+
|Realtime frontend editor (based on angular) |         |       | x        |
+--------------------------------------------+---------+-------+----------+
|Partial cache per content element           |         |       | x        |
+--------------------------------------------+---------+-------+----------+
|Frontend users login and UAC system         |         |       | x        |
+--------------------------------------------+---------+-------+----------+