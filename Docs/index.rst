.. _index:

========================================
Welcome to Conținut CMS's documentation!
========================================

Conţinut CMS aims to be an easy to use and easy to extend low to midlevel CMS system with builtin multidomain,
multilanguage and responsive support. Different Backend and Frontend layouts as well as different containers can be
created and inserted recursively on any page allowing you to create that ultimate design you always wanted to.
Elements in the backend can be moved using drag and drop and the entire backend is responsive, so you can start
modifying your website wherever you are and whatever device you are using. Every part of the system has been created
in such a way as to ease development and to make it easy to extend almost any part of the system. Plugin support is
added for any content element and all the parts of the CMS use MVC to show content and bind things together. Lookup
our extended documentation pages and you will not regret your choice.

The full features list is available here: @link

Introduction:
============

.. toctree::
   :numbered:
   :maxdepth: 2

   introduction/features.rst
   introduction/installation.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

