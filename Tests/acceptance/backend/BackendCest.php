<?php

/**
 * Backend functionality tests are defined here
 *
 * Class BackendCest
 */
class BackendCest
{
    /**
     * Store cookie
     *
     * @var null
     */
    private $cookie = null;

    // @TODO : this should come from configuration so that it is not hardcoded
    const ADMIN_PATH = '/admin/';

    /**
     * Can we access the administration page?
     *
     * @param AcceptanceTester $I
     */
    public function isLoginPageDisplayed(AcceptanceTester $I)
    {
        $I->amOnPage(self::ADMIN_PATH);
        $I->see('Conținut CMS');
    }

    /**
     * Can we connect to the backend?
     * @TODO : username and password should come from the command line, maybe
     *
     * @param AcceptanceTester $I
     */
    public function canLogin(AcceptanceTester $I)
    {
        $I->am('backend user');
        $I->wantTo('login to the Admin page');
        $I->amOnPage(self::ADMIN_PATH);
        $I->fillField('cms_username','admin');
        $I->fillField('cms_password','admin');
        $I->click('.submit');
        $I->seeElement('#main_toolbar');
        $this->cookie = $I->grabCookie('ContinutCMS_Backend');
    }

    /**
     * @param AcceptanceTester $I
     * @depends canLogin
     */
    public function checkLanguageSwitch(AcceptanceTester $I)
    {
        $I->setCookie( 'ContinutCMS_Backend', $this->cookie );
        $I->wantTo('check backend user can change admin language');
        $I->amOnPage(self::ADMIN_PATH . 'Backend/User/profile');
        //$I->expect('the user not to use Romanian yet');
        //$I->cantSee('Administrare profil');
        $I->wantTo('set current language to Romanian');
        $I->submitForm(
            '#save_profile',
            array('data' => array('language' => 'ro_RO')),
            '.btn-primary'
        );
        $I->see('Administrare profil');
    }
}
