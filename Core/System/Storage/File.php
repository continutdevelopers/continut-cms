<?php
/**
 * This file is part of the Conținut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 01.08.2015 @ 13:53
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Storage;

class File
{
    /**
     * Filename without extension
     *
     * @var string
     */
    protected $name;

    /**
     * Filename with extension
     *
     * @var string
     */
    protected $fullname;

    /**
     * @var int
     */
    protected $size;

    /**
     * @var string
     */
    protected $extension;

    /**
     * @var string
     */
    protected $relativePath;

    /**
     * @var string
     */
    protected $relativeFilename;

    /**
     * @var string
     */
    protected $absolutePath;

    /**
     * @var string
     */
    protected $absoluteFilename;

    /**
     * @var int Timestamp with the creation date of the node
     */
    protected $creationDate;

    /**
     * @var int Timestamp with the last date the node was modified
     */
    protected $modificationDate;

    /**
     * Reference id in sys_files after it has been indexed
     *
     * @var int
     */
    protected $id;

    /**
     * Width and Height, in pixels, for images only
     *
     * @var array
     */
    protected $dimensions;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     *
     * @return $this
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     *
     * @return $this
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @return string
     */
    public function getRelativePath()
    {
        return $this->relativePath;
    }

    /**
     * @param string $relativePath
     *
     * @return $this
     */
    public function setRelativePath($relativePath)
    {
        $this->relativePath = $relativePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getRelativeFilename()
    {
        return $this->relativeFilename;
    }

    /**
     * @param string $relativeFilename
     *
     * @return $this
     */
    public function setRelativeFilename($relativeFilename)
    {
        $this->relativeFilename = $relativeFilename;

        return $this;
    }

    /**
     * @return string
     */
    public function getAbsolutePath()
    {
        return $this->absolutePath;
    }

    /**
     * @param string $absolutePath
     *
     * @return $this
     */
    public function setAbsolutePath($absolutePath)
    {
        $this->absolutePath = $absolutePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getAbsoluteFilename()
    {
        return $this->absoluteFilename;
    }

    /**
     * @param string $absoluteFilename
     *
     * @return $this
     */
    public function setAbsoluteFilename($absoluteFilename)
    {
        $this->absoluteFilename = $absoluteFilename;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param int $creationDate
     *
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * @param int $modificationDate
     *
     * @return $this
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return array
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param array $dimensions
     *
     * @return $this
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;

        return $this;
    }
}
