<?php
/**
 * This file is part of the Conținut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 01.08.2015 @ 13:41
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Storage;

use Continut\Core\Utility;

/**
 * Class LocalStorage
 *
 * Local filesystem file storage
 * It only deals with files and folders INSIDE "/Media/"
 *
 * @package Continut\Core\System\Storage
 */
class LocalStorage implements StorageInterface
{

    const MEDIA_DIRECTORY = 'Media';

    /**
     * Get root directory of the storage
     *
     * @return string
     */
    public function getRoot()
    {
        return self::MEDIA_DIRECTORY;
    }

    /**
     * Return all files found inside a directory
     *
     * @param string $path Path where to look for files
     * @param string $searchFor Filter the list on files containing this value
     *
     * @return array
     */
    public function getFiles($path = '', $searchFor = '')
    {
        $paths         = $this->getValidPath($path);
        $existingFiles = new \FilesystemIterator($paths['absolutePath']);

        $fileObjects = [];

        foreach ($existingFiles as $existingFile) {
            try {
                if ($existingFile->isFile()) {
                    $fileObject = $this->setFileInfo($existingFile);

                    // if searching for files, only show those that contain our keyword
                    if (mb_strlen($searchFor) == 0 ||
                        (
                            mb_strlen(trim($searchFor)) > 0 &&
                            mb_strpos($fileObject->getFullname(), $searchFor) !== false
                        )
                    ) {
                        $fileObjects[] = $fileObject;
                    }
                }
            } catch (\RuntimeException $e) {
                // TODO: Add log message regarding an invalid file (probably the file name is in a different encoding)
                // and/or has special symbols
            }
        }

        return $fileObjects;
    }

    /**
     * Returns all folders inside $path
     *
     * @param string $path
     *
     * @return array
     */
    public function getFolders($path = '')
    {
        $paths           = $this->getValidPath($path);
        $existingFolders = new \FilesystemIterator($paths['absolutePath']);

        $folderObjects = [];

        foreach ($existingFolders as $existingFolder) {
            try {
                if ($existingFolder->getType() == 'dir') {
                    $folderObject = Utility::createInstance('Continut\Core\System\Storage\Folder');

                    $folderObject->setName($existingFolder->getFilename());
                    $folderObject->setAbsolutePath($existingFolder->getPathname());
                    $folderObject->setRelativePath($paths['relativePath'] . DS . $existingFolder->getFilename());

                    // count files and folders inside this folder
                    $subfiles = new \FilesystemIterator(
                        $folderObject->getAbsolutePath(),
                        \FilesystemIterator::SKIP_DOTS
                    );
                    foreach ($subfiles as $sub) {
                        if ($sub->getType() == 'dir') {
                            $folderObject->setCountFolders($folderObject->getCountFolders() + 1);
                        } else {
                            $folderObject->setCountFiles($folderObject->getCountFiles() + 1);
                        }
                    }
                    $folderObjects[] = $folderObject;
                }
            } catch (\RuntimeException $e) {
                // TODO: Add log message regarding an invalid folder (probably folder name is in a different encoding)
            }
        }

        return $folderObjects;
    }

    /**
     * Attempts to create a directory
     *
     * @param string $folder Folder to create
     * @param string $path   Inside which path?
     *
     * @return bool
     */
    public function createFolder($folder, $path)
    {
        $paths = $this->getValidPath($path);

        return mkdir($paths['absolutePath'] . DS . $folder);
    }

    /**
     * @param string $identifier
     *
     * @return \Continut\Core\System\Storage\File
     */
    public function getFileInfo($identifier)
    {
        // files are url encoded
        $file = urldecode($identifier);
        // make sure the path is valid and is inside our getRoot() path
        $paths = $this->getValidPath($file);
        // return the absolutePath to this file
        $file = $paths['absolutePath'];
        // and get it's information
        $splFile      = new \SplFileInfo($file);
        $fileObject   = $this->setFileInfo($splFile);

        return $fileObject;
    }

    /**
     * @param \SplFileInfo $splFile
     *
     * @return \Continut\Core\System\Storage\File
     */
    protected function setFileInfo($splFile)
    {
        $fileObject = Utility::createInstance('Continut\Core\System\Storage\File');

        $relativePath     = str_replace(__ROOTCMS__, '', $splFile->getPath());
        $fileRelativePath = $relativePath . DS . $splFile->getFilename();

        $fileObject
            ->setName($splFile->getBasename('.' . $splFile->getExtension()))
            ->setExtension(strtoupper($splFile->getExtension()))
            ->setRelativePath($relativePath)
            ->setRelativeFilename($fileRelativePath)
            ->setAbsolutePath($splFile->getPath())
            ->setAbsoluteFilename($splFile->getPathname())
            ->setFullname($splFile->getFilename())
            ->setSize($splFile->getSize())
            ->setCreationDate($splFile->getCTime())
            ->setModificationDate($splFile->getMTime());

        // for images, also grab their dimensions
        $imageInfo = @getimagesize($fileObject->getAbsoluteFilename());
        if (is_array($imageInfo)) {
            $fileObject->setDimensions(['w' => $imageInfo[0], 'h' => $imageInfo[1]]);
        }

        return $fileObject;
    }

    /**
     * Validates a path and removes invalid characters
     *
     * @param $path
     *
     * @return array
     */
    protected function getValidPath($path)
    {
        $basePath = realpath(__ROOTCMS__ . DS . $this->getRoot());
        $userPath = realpath(__ROOTCMS__ . DS . $path);

        // if the path is invalid or if someone tries directory traversal then return the root path
        // else return the relative path
        if ($userPath === false || strpos($userPath, $basePath) !== 0) {
            return ['relativePath' => $this->getRoot(), 'absolutePath' => $basePath];
        } else {
            return ['relativePath' => $path, 'absolutePath' => $userPath];
        }
    }
}
