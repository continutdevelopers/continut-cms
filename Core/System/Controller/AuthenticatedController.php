<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 27.04.2015 @ 22:55
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Controller;

use Continut\Core\Utility;

class AuthenticatedController extends ActionController
{
    /**
     * Check if the BE user is still connected, otherwise redirect him to the login page
     */
    public function __construct()
    {
        parent::__construct();
        if (!$this->isConnected()) {
            $url = Utility::helper('Url')->linkToPath('admin', ['_controller' => 'Login', '_action' => 'index']);
            if ($this->getRequest()->isAjax()) {
                header('Continut-Redirect: ' . $url);
            } else {
                $this->redirect($url);
            }
        }
    }
}
