<?php
/**
 * This file is part of the Conținut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoș <radu.mogos@pixelplant.ch>
 * Date: 01.04.2015 @ 21:30
 * Project: Conținut CMS
 */

namespace Continut\Core\System\Controller;

use Continut\Core\Utility;

/**
 * Backend Controller base class. Always extends AuthenticatedController since anything in the Backend
 * is only accessible if a user is connected
 *
 * @package Continut\Core\System\Controller
 */
class BackendController extends AuthenticatedController
{
    /**
     * Backend constructor
     */
    public function __construct()
    {
        parent::__construct();
        // set the default layout
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
        // and the application scope
        $this->setScope('Backend');
        // make sure the $backendUser variable is always callable inside any Backend view or partial
        $this->getView()->assign('backendUser', $this->getUser());
    }
}
