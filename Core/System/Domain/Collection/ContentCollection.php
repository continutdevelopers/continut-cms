<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 04.04.2015 @ 13:02
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class ContentCollection extends BaseCollection
{
    /**
     * Set tablename and element class
     */
    public function __construct()
    {
        $this->tablename    = 'sys_content';
        $this->elementClass = 'Continut\Core\System\Domain\Model\Content';
    }

    /**
     * Build a json tree of pages, specifically useful for JSON consuming javascript plugins
     *
     * @return array
     */
    public function buildJsonTree()
    {
        $children    = [];
        $children[0] = [];

        // add a root child node, that allows us to select the page
        // this is a virtual node, as it does not exist in the actual page content
        // used for the moment only for the FEEditor
        /*$rootChild           = new \stdClass();
        $rootChild->id       = '9999';
        $rootChild->parentId = '0';
        $rootChild->label    = 'Root node';
        $rootChild->text     = 'Root';
        $rootChild->icon     = 'fa fa-sitemap';
        $rootChild->status   = 'normal';

        $children[0][] = $rootChild;*/

        foreach ($this->getAll() as $item) {
            $data           = new \stdClass();
            $data->id       = $item->getId();
            $data->parentId = $item->getParentId();
            $data->label    = $item->getTitle() . " [id: $data->id]";
            $data->text     = $item->getTitle();
            $data->status   = 'normal';
            $data->type     = $item->getType();
            $data->value    = $item->getValue();
            switch ($item->getType()) {
                case 'content':
                    $data->icon = 'fa fa-file-text';
                    break;
                case 'plugin':
                    $data->icon = 'fa fa-list-alt';
                    break;
                case 'container':
                    $data->icon = 'fa fa-columns';
                    break;
                default:
                    $data->icon = 'fa fa-file-text';
            }
            if (!$item->getIsVisible()) {
                $data->status = 'hidden-frontend';
            }
            $children[ $data->parentId ][] = $data;
        }

        foreach ($children as $child) {
            foreach ($child as $data) {
                if (isset($children[ $data->id ])) {
                    $data->children = $children[ $data->id ];
                } else {
                    $data->children = [];
                }
            }
        }

        $tree = [];
        if (sizeof($children) > 0) {
            $tree = reset($children);
        }

        return $tree;
    }

    /**
     * Returns the entire tree for a leaf with a certain id
     *
     * @param int $id
     *
     * @return mixed
     */
    public function findChildrenForId($id)
    {
        $tree = $this->buildTree();

        return $this->browseChildren($tree, $id);
    }

    /**
     * Returns all the content elements for a certain page id
     *
     * @param int  $pageId      Id of the page to fetch content elements from
     * @param int  $domainUrlId Id of the language/domainUrl to filter on
     * @param bool $deleted     Should we return deleted records too?
     *
     * @return $this
     */
    public function findAllForPage($pageId, $domainUrlId, $deleted = false)
    {
        return $this->where(
            'page_id = :page_id AND domain_url_id = :domain_url_id AND is_deleted = :deleted ORDER BY sorting ASC',
            [
                ':page_id'       => $pageId,
                ':deleted'       => $deleted,
                ':domain_url_id' => $domainUrlId
            ]
        );
    }

    /**
     * Return the content elements for a certain page
     *
     * @param int $pageId    Page id to fetch elements for
     * @param int $isDeleted Should we fetch deleted content elements?
     * @param int $isVisible Should we fetch also hidden content elements?
     *
     * @return $this
     */
    public function findVisibleForPage($pageId, $isDeleted = 0, $isVisible = 1)
    {
        return $this->where(
            'page_id = :page_id AND is_deleted = :is_deleted AND is_visible = :is_visible ORDER BY sorting ASC',
            [
                ':page_id'    => $pageId,
                ':is_visible' => $isVisible,
                ':is_deleted' => $isDeleted
            ]
        );
    }

    /**
     * Build a tree of content elements from this collection
     *
     * @return null
     */
    public function buildTree()
    {
        // Build content tree
        $children = [];

        foreach ($this->getAll() as $item) {
            $children[ $item->getParentId() ][] = $item;
        }

        foreach ($this->getAll() as $item) {
            if (isset($children[ $item->getId() ])) {
                $item->children = $children[ $item->getId() ];
            } else {
                $item->children = [];
            }
        }

        $tree = null;
        if (isset($children[0])) {
            $tree = $children[0];
        }

        return $tree;
    }

    /**
     * Called recursively by findChildrenForId
     *
     * @param $elements
     * @param $id
     *
     * @return mixed
     */
    protected function browseChildren($elements, $id)
    {
        if ($elements) {
            foreach ($elements as $child) {
                if ($child->getId() == $id) {
                    return $child;
                } else {
                    $this->browseChildren($child->children, $id);
                }
            }
        }

        return null;
    }

    /**
     * Return the active element with this id
     * Please note that "hidden" elements (is_visible = 0) can still be displayed in the backend so we need
     * to make sure to include them in our results
     *
     * @param int $id
     *
     * @return \Continut\Extensions\System\Backend\Classes\Domain\Model\BackendContent|null
     */
    public function findActiveById($id)
    {
        $records = $this->where(
            'is_deleted = 0 AND id = :id LIMIT 1',
            [
                'id' => $id
            ]
        );

        if ($records->getFirst()) {
            return $records->getFirst();
        }

        return null;
    }

    /**
     * Update the sorting order of the element
     *
     * @param int $pageId
     * @param int $columnId
     * @param int $parentId
     * @param int $sorting
     */
    public function updateOrder($pageId, $columnId, $parentId, $sorting)
    {
        $this
            ->reset()
            ->where(
                'page_id = :page_id AND column_id = :column_id AND parent_id = :parent_id AND ' .
                'is_deleted = 0 AND sorting >= :sorting',
                [
                    'page_id'   => $pageId,
                    'column_id' => $columnId,
                    'parent_id' => $parentId,
                    'sorting'   => $sorting
                ]
            );

        foreach ($this->getAll() as $element) {
            $element->setSorting($element->getSorting() + 1);
        }

        $this->save();
    }

    /**
     * Return the maximum "sorting" value for this page and column, for visible and non deleted records
     *
     * @param int $pageId
     * @param int $columnId
     *
     * @return int Maximum sorting value found
     */
    public function getLastSortingFor($pageId, $columnId)
    {
        $records = $this->where(
            'is_deleted = 0 AND page_id = :page_id AND column_id = :column_id ORDER BY sorting DESC LIMIT 1',
            [
                'page_id'   => $pageId,
                'column_id' => $columnId
            ]
        );

        if ($records->getFirst()) {
            return (int)$records->getFirst()->getSorting();
        }

        return 0;
    }
}
