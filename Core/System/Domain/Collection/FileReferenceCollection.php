<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 27.04.2015 @ 22:30
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class FileReferenceCollection extends BaseCollection
{

    /**
     * Set tablename and element class
     */
    public function __construct()
    {
        $this->tablename    = 'sys_file_references';
        $this->elementClass = 'Continut\Core\System\Domain\Model\FileReference';
    }

    /**
     * @param int    $fileId
     * @param string $tablename
     *
     * @return $this
     */
    public function findByFileIdAndTablename($fileId, $tablename)
    {
        return $this->where(
            'is_deleted = 0 AND is_visible = 1 AND file_id = :file_id AND tablename LIKE :tablename',
            [
                'file_id'   => $fileId,
                'tablename' => $tablename
            ]
        );
    }

    /**
     * Returns the file references with these ids
     *
     * @param array $ids
     *
     * @return $this
     */
    public function findWithIds($ids)
    {
        if (empty($ids)) {
            return $this;
        }

        // IN conditions are a bit tricky in PDO, so we need to create a variable for each value
        // and map it to a new array
        $variables = [];
        $values    = [];
        foreach ($ids as $key => $id) {
            $variableName            = ':_value_in_' . $key;
            $variables[]             = $variableName;
            $values[ $variableName ] = $id;
        }

        $idsString = implode(',', $variables);

        return $this->where("is_deleted = 0 AND id IN ($idsString)", $values);
    }
}
