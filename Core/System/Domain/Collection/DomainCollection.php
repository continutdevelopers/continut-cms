<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 04.04.2015 @ 12:39
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class DomainCollection extends BaseCollection
{

    /**
     * Set tablename and element class for this collection
     */
    public function __construct()
    {
        $this->tablename    = 'sys_domains';
        $this->elementClass = 'Continut\Core\System\Domain\Model\Domain';
    }

    /**
     * Fetches all domains which are set as visible
     *
     * @return $this
     */
    public function whereVisible()
    {
        return $this->where('is_visible = 1 AND is_deleted = 0 ORDER BY sorting ASC');
    }

    /**
     * Grab all deleted or non deleted domains
     *
     * @param bool $deleted
     *
     * @return $this
     */
    public function whereDeleted($deleted)
    {
        return $this->where('is_deleted = :condition', ['condition' => $deleted]);
    }

    /**
     * Filter on all the domains that have at least 1 domainUrl/language assigned
     *
     * @return $this
     */
    public function whereHasLanguages()
    {
        return $this->sql(
            'SELECT sys_domains.* FROM sys_domains LEFT JOIN sys_domain_urls ' .
            'ON sys_domains.id=sys_domain_urls.domain_id WHERE sys_domain_urls.domain_id IS NOT NULL ' .
            'AND sys_domains.is_visible =:is_visible '.
            'GROUP BY sys_domains.id, sys_domains.title, sys_domains.is_visible, sys_domains.sorting '.
            'ORDER BY sys_domains.sorting ASC',
            [
                'is_visible' => 1
            ]
        );
    }
}
