<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 27.04.2015 @ 22:30
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;
use Continut\Core\System\Domain\Model\BaseModel;

class FileCollection extends BaseCollection
{

    /**
     * Set tablename and element class
     */
    public function __construct()
    {
        $this->tablename    = 'sys_files';
        $this->elementClass = 'Continut\Core\System\Domain\Model\File';
    }

    /**
     * Return a File object based on it's linked reference
     *
     * @param int $referenceId Reference id in sys_file_references table
     *
     * @return BaseModel
     */
    public function findFromReference($referenceId)
    {
        return $this->sql(
            'SELECT sf.* FROM sys_files sf LEFT JOIN sys_file_references sfr ON sf.id=sfr.file_id ' .
            'WHERE sfr.id = :reference_id LIMIT 1',
            [
                'reference_id' => $referenceId
            ]
        )->getFirst();
    }
}
