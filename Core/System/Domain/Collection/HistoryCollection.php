<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.05.2017 @ 23:15
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class HistoryCollection extends BaseCollection
{
    /**
     * Set tablename and element class
     */
    public function __construct()
    {
        $this->tablename    = 'sys_history';
        $this->elementClass = 'Continut\Core\System\Domain\Model\History';
    }
}
