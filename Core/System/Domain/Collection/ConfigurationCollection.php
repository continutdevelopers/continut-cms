<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 04.04.2015 @ 12:39
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;
use Continut\Core\System\Domain\Model\BaseModel;

class ConfigurationCollection extends BaseCollection
{
    /**
     * Set tablename and element class for this collection
     */
    public function __construct()
    {
        $this->tablename    = 'sys_configuration';
        $this->elementClass = 'Continut\Core\System\Domain\Model\Configuration';
    }

    /**
     * Return the configuration stored for a certain domain language
     *
     * First get "Global [domain_id = 0, language_id = 0]" config, then "Domain" config [domain_id = currentDomain,
     * language_id = 0] and finally get "Language" config [domain_id = currentDomain, language_id = currentLanguage]
     *
     * @param int $domainId
     * @param int $languageId
     *
     * @return $this
     */
    public function whereDomainAndLanguage($domainId, $languageId)
    {
        return $this->where(
            '(domain_id = 0 AND language_id = 0) OR (domain_id = :domain_id AND language_id = 0) OR '.
            '(domain_id = :domain_id AND language_id = :language_id) ORDER BY domain_id ASC, language_id ASC',
            [
                'domain_id'   => $domainId,
                'language_id' => $languageId
            ]
        );
    }

    /**
     * Get all configuration settings only for the global scope
     *
     * @return $this
     */
    public function whereGlobalScope()
    {
        return $this->where('domain_id = 0 AND language_id = 0 ORDER BY domain_id ASC, language_id ASC');
    }

    /**
     * Get all configuration settings only for the domain scope
     *
     * @param int $domainId
     *
     * @return $this
     */
    public function whereDomainScope($domainId)
    {
        return $this->where(
            '(domain_id = :domain_id AND language_id = 0) ORDER BY domain_id ASC, language_id ASC',
            [
                'domain_id' => $domainId
            ]
        );
    }

    /**
     * Get all configuration settings only for the language scope
     *
     * @param int $languageId
     *
     * @return $this
     */
    public function whereLanguageScope($languageId)
    {
        return $this->where(
            'language_id = :language_id ORDER BY domain_id ASC, language_id ASC',
            [
                'language_id' => $languageId
            ]
        );
    }

    /**
     * @param int    $domainId
     * @param int    $languageId
     * @param string $reference
     *
     * @return BaseModel
     */
    public function findWithReference($domainId, $languageId, $reference)
    {
        return $this->where(
            'domain_id = :domain_id AND language_id = :language_id AND reference LIKE :reference LIMIT 1',
            [
                'domain_id'   => $domainId,
                'language_id' => $languageId,
                'reference'   => $reference
            ]
        )->getFirst();
    }

    /**
     * @param int    $domainId
     * @param int    $languageId
     * @param array  $references
     *
     * @return $this
     */
    public function findWithReferences($domainId, $languageId, $references)
    {
        $values = [
            'domain_id'   => $domainId,
            'language_id' => $languageId
        ];

        $binds = [];
        foreach ($references as $key => $reference) {
            $bind          = 'reference_' . $key;
            $binds[]       = ':' . $bind;
            $values[$bind] = $reference;
        }

        return $this->where(
            'domain_id = :domain_id AND language_id = :language_id AND reference IN (' . implode(',', $binds) . ')',
            $values
        );
    }

    /**
     * @param int    $domainId
     * @param string $reference
     *
     * @return null
     */
    public function findWithReferenceForUrl($domainId, $reference)
    {
        return $this->where(
            '((domain_id = 0 AND language_id = 0) OR (domain_id = :domain_id AND language_id = 0)) ' .
            'AND reference LIKE :reference ORDER BY domain_id DESC LIMIT 1',
            [
                'domain_id'   => $domainId,
                'reference'   => $reference
            ]
        )->getFirst();
    }
}
