<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 08.08.2015 @ 15:11
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class DomainUrlCollection extends BaseCollection
{
    /**
     * Set tablename and element class for this collection
     */
    public function __construct()
    {
        $this->tablename    = 'sys_domain_urls';
        $this->elementClass = 'Continut\Core\System\Domain\Model\DomainUrl';
    }

    /**
     * @param bool   $addEmpty   Should an initial empty value be added?
     * @param string $emptyTitle If so, what title should be shown, if any
     *
     * @return array
     */
    public function toSimplifiedArray($addEmpty = false, $emptyTitle = '')
    {
        $data = [];
        // Too much EU patriotism, I know. still waiting for a globe fontawesome icon to replace it
        if ($addEmpty) {
            $data = [0 => ['title' => $emptyTitle, 'flag' => 'eu']];
        }
        foreach ($this->getAll() as $language) {
            $data[] = [
                'id'         => $language->getId(),
                'title'      => $language->getTitle(),
                'flag'       => $language->getFlag(),
                'is_default' => $language->getIsDefault()
            ];
        }

        return $data;
    }

    /**
     * Fetches all languages (domainUrls) for a certain domain
     *
     * @param int $domainId
     * @param bool $deleted Include deleted languages in the result?
     * @param string $orderBy
     *
     * @return $this
     */
    public function whereDomain($domainId, $deleted = false, $orderBy = 'sorting ASC')
    {
        return $this->where(
            'domain_id = :domain_id AND is_deleted = :deleted ORDER BY :order_by',
            [
                'domain_id' => $domainId,
                'deleted'   => $deleted,
                'order_by'  => $orderBy
            ]
        );
    }

    /**
     * Fetches the language specified by id and makes sure it also belongs to the right domain
     *
     * @param int $domainId
     * @param int $languageId
     *
     * @return $this
     */
    public function whereDomainAndLanguage($domainId, $languageId)
    {
        return $this->where(
            'domain_id = :domain_id AND id = :id ORDER BY sorting ASC',
            ['domain_id' => $domainId, 'id' => $languageId]
        );
    }

    /**
     * Find domainUrls by their url and code
     *
     * @param string $url
     * @param string $code
     *
     * @return $this
     */
    public function findByUrlAndCode($url, $code = '')
    {
        $domainUrls = $this->where(
            '(url = :url AND code = :code) OR (url = :url AND code = :empty) ORDER BY code DESC',
            [
                'url'   => $url,
                'code'  => $code,
                'empty' => ''
            ]
        );
        return $domainUrls;
    }

    /**
     * Check if we already have some is_default=1 languages for this domain
     *
     * @param int $domainId Domain id to check for
     *
     * @return int
     */
    public function countExistingDefault($domainId)
    {
        return $this->findDefaultLanguage($domainId)->count();
    }

    /**
     * Get default language of domain_id
     *
     * @param int $domainId
     *
     * @return $this
     */
    public function findDefaultLanguage($domainId)
    {
        return $this->where('domain_id = :domain_id AND is_default = 1 LIMIT 2', [':domain_id' => $domainId]);
    }
}
