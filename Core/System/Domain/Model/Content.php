<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 03.04.2015 @ 20:38
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

use Continut\Core\System\Tools\ErrorException;
use Continut\Core\Utility;

class Content extends BaseModel
{
    /**
     * @var string value of the content element
     */
    protected $value;

    /**
     * @var string type of content element
     */
    protected $type;

    /**
     * @var int The id of this element's parent
     */
    protected $parentId = 0;

    /**
     * @var bool Is the content element visible?
     */
    protected $isVisible;

    /**
     * @var bool Is the content element deleted?
     */
    protected $isDeleted;

    /**
     * @var int Column id
     */
    protected $columnId;

    /**
     * @var int Id of the page where the element is stored
     */
    protected $pageId;

    /**
     * @var int The reference id, if this is a reference content element
     */
    protected $referenceId;

    /**
     * @var int Field used for the sorting order of content elements
     */
    protected $sorting;

    /**
     * @var \Continut\Core\System\View\BaseLayout Link to the parent layout
     */
    protected $layout;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int Creation date+time (unix timestamp)
     */
    protected $createdAt;

    /**
     * @var int Last modified date+time (unix timestamp)
     */
    protected $modifiedAt;

    /**
     * @var int Language id
     */
    protected $domainUrlId;

    /**
     * Datamapper for this model
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'page_id'       => $this->pageId,
            'type'          => $this->type,
            'title'         => $this->title,
            'column_id'     => $this->columnId,
            'parent_id'     => $this->parentId,
            'domain_url_id' => $this->domainUrlId,
            'value'         => $this->value,
            'is_deleted'    => $this->isDeleted,
            'is_visible'    => $this->isVisible,
            'sorting'       => $this->sorting,
            'modified_at'   => $this->modifiedAt,
            'created_at'    => $this->createdAt,
            'reference_id'  => $this->referenceId
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * @return int Get the parent id of this content element
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set parent id
     *
     * @param int $parentId
     *
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     *
     * @return $this
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * @return int
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param int $referenceId
     *
     * @return $this
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return int Get id of column where content is stored
     */
    public function getColumnId()
    {
        return $this->columnId;
    }

    /**
     * Set column id
     *
     * @param $columnId
     *
     * @return $this
     */
    public function setColumnId($columnId)
    {
        $this->columnId = $columnId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Gets the element's serialized values. Do a json_decode after retrieving them
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the element's serialized values
     *
     * @param string $value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param \Continut\Core\System\View\BaseLayout $layout
     *
     * @return $this
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return \Continut\Core\System\View\BaseLayout
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @return mixed
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param mixed $isVisible
     *
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @param \Continut\Core\System\View\BaseView $view
     *
     * @return \Continut\Core\System\View\BaseView
     */
    public function prepareView($view)
    {
        // pass on the current record to the view
        $view->assign('record', $this);

        return $view;
    }

    /**
     * @param mixed $elements
     */
    public function render($elements = null)
    {
    }

    /**
     * Generated the data-continut-cms-* properties for frontend editing
     *
     * @return string
     */
    public function frontendEditor()
    {
        return sprintf(
            'data-continut-cms-id="%s" data-continut-cms-type="%s" data-continut-cms-page="%s"',
            $this->getId(),
            $this->getType(),
            $this->getPageId()
        );
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param int $pageId
     *
     * @return $this
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param int $modifiedAt
     *
     * @return $this
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getDomainUrlId()
    {
        return $this->domainUrlId;
    }

    /**
     * @param int $domainUrlId
     *
     * @return $this
     */
    public function setDomainUrlId($domainUrlId)
    {
        $this->domainUrlId = $domainUrlId;

        return $this;
    }

    /**
     * Place the current element on another page, in a certain column id, before, after or inside another element
     *
     * @param int $pageId
     * @param int $targetId Container or Content element id after, before or inside which we'll add our current element
     * @param int $targetColumnId
     * @param string $placement 'before', 'after' or 'inside'
     *
     * @throws ErrorException
     */
    public function changePlacement($pageId, $targetId, $targetColumnId, $placement)
    {
        $contentCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\ContentCollection');

        $targetElement = $contentCollection->findActiveById($targetId);

        // if we need to position it INSIDE a container, it means it's the last content element
        // so just get the last sorting id and increment it by one
        if ($placement == 'inside') {
            $lastSortingId = $contentCollection->getLastSortingFor($pageId, $targetColumnId);
            if ($targetElement) {
                $this->setParentId($targetElement->getId());
            }
            $this->setColumnId($targetColumnId);
            $this->setSorting($lastSortingId + 1);
        }

        if ($placement == 'before' || $placement == 'after') {
            if ($targetElement) {
                // update the new element's sorting
                $newSortingValue = ($placement == 'before') ?
                    $targetElement->getSorting() :
                    $targetElement->getSorting() + 1;
                $this
                    ->setSorting($newSortingValue)
                    ->setParentId($targetElement->getParentId());
                // and the 'sorting' for all those elements AFTER it
                $contentCollection
                    ->updateOrder(
                        $pageId,
                        $targetColumnId,
                        $targetElement->getParentId(),
                        $newSortingValue
                    );
            } else {
                throw new ErrorException(
                    'The target element of your placement does not exist anymore',
                    810000
                );
            }
        }

        $contentCollection
            ->reset()
            ->add($this)
            ->save();
    }
}
