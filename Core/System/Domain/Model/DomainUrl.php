<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 08.08.2015 @ 14:58
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

use Continut\Core\Utility;
use Respect\Validation\Validator as v;

class DomainUrl extends BaseModel
{
    /**
     * @var bool
     */
    protected $isAlias;

    /**
     * @var int
     */
    protected $parentId;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var int
     */
    protected $domainId;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var int
     */
    protected $sorting;

    /**
     * @var string ISO2 code used for the flag
     */
    protected $flag;

    /**
     * @var string Locale used by this domain url
     */
    protected $locale;

    /**
     * @var \Continut\Core\System\Domain\Model\Domain
     */
    protected $domain = null;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var bool
     */
    protected $isDefault;

    /**
     * @var bool
     */
    protected $isDeleted;

    /**
     * Array of \Continut\Core\System\Domain\Model\DomainUrl
     * @var array
     */
    protected $aliases = [];

    /**
     * Simple datamapper used for the database
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'is_alias'   => $this->isAlias,
            'is_default' => $this->isDefault,
            'parent_id'  => $this->parentId,
            'domain_id'  => $this->domainId,
            'sorting'    => $this->sorting,
            'locale'     => $this->locale,
            'flag'       => $this->flag,
            'url'        => $this->url,
            'title'      => $this->title,
            'code'       => $this->code,
            'is_deleted' => $this->isDeleted
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * Validation rules for the data
     *
     * @return array
     */
    public function dataValidation()
    {
        // add our custom validation rules
        v::with('Continut\\Extensions\\System\\Backend\\Classes\\Validation\\Rules\\');

        return [
            'title'      => v::length(3, 200),
            'url'        => v::noWhitespace()->length(1, 200),
            'locale'     => v::noWhitespace()->length(2, 40),
            'is_default' => v::isDefaultDomain($this->getId(), $this->getDomainId())
        ];
    }

    /**
     * @return bool
     */
    public function getIsAlias()
    {
        return $this->isAlias;
    }

    /**
     * @param bool $isAlias
     */
    public function setIsAlias($isAlias)
    {
        $this->isAlias = $isAlias;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * If a code is defined, return the url AND the code
     *
     * @return string
     */
    public function getCompleteUrl()
    {
        if ($this->getCode()) {
            return $this->getUrl() . '/' . $this->getCode();
        }

        return $this->getUrl();
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * @param string $flag
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * @param int $domainId
     */
    public function setDomainId($domainId)
    {
        $this->domainId = $domainId;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return \Continut\Core\System\Domain\Model\Domain
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function getDomain()
    {
        if ($this->domain == null) {
            $this->domain = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
                ->findById($this->domainId);
        }

        return $this->domain;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        if (empty($this->aliases)) {
            $this->aliases = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                ->findByParentId($this->id)
                ->getAll();
        }

        return $this->aliases;
    }

    /**
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

    /**
     * Returns the Homepage page for the current domain
     *
     * @return mixed
     */
    public function getHomepage()
    {
        return Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
            ->findWithIdOrSlug(0, '');
    }
}
