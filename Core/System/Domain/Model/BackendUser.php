<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 27.04.2015 @ 22:27
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

use Continut\Core\Utility;

class BackendUser extends User
{
    /**
     * @var string Fullname of backend user
     */
    protected $name;

    /**
     * @var string Language for this backend user
     */
    protected $language;

    /**
     * @var string Id of a sys_file_reference or list of ids
     */
    protected $image;

    /**
     * @var string Timezone of this backend user
     */
    protected $timezone;

    /**
     * @var FileReference
     */
    protected $imageReference = null;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Simple datamapper used for the database
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'name'     => $this->name,
            'image'    => $this->image,
            'language' => $this->language,
            'timezone' => $this->timezone
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return FileReference
     */
    public function getImageReference()
    {
        if (is_null($this->imageReference)) {
            $collection = Utility::createInstance('Continut\Core\System\Domain\Collection\FileReferenceCollection');
            $this->imageReference = $collection->findById($this->getImage());
        }

        return $this->imageReference;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     *
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Save FrontendUser data
     */
    public function save()
    {
        $collection = Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserCollection');
        $collection->add($this)->save();
    }
}
