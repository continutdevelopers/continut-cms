<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.05.2017 @ 23:12
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

class History extends BaseModel
{
    /**
     * @var int
     */
    protected $recordId;

    /**
     * @var string
     */
    protected $recordTable;

    /**
     * @var int
     */
    protected $modifiedBy;

    /**
     * @var string
     */
    protected $data;

    /**
     * @return int
     */
    public function getRecordId()
    {
        return $this->recordId;
    }

    /**
     * @param int $recordId
     */
    public function setRecordId($recordId)
    {
        $this->recordId = $recordId;
    }

    /**
     * @return string
     */
    public function getRecordTable()
    {
        return $this->recordTable;
    }

    /**
     * @param string $recordTable
     */
    public function setRecordTable($recordTable)
    {
        $this->recordTable = $recordTable;
    }

    /**
     * @return int
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * @param int $modifiedBy
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}
