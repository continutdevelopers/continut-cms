<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 05.04.2015 @ 12:40
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

use Continut\Core\Utility;
use Respect\Validation\Validator as v;

class Page extends BaseModel
{
    /**
     * @var string Page title
     */
    protected $title = 'Unnamed page';

    /**
     * @var \Continut\Core\System\Domain\Model\Page
     */
    protected $parent;

    /**
     * @var int Parent page id
     */
    protected $parentId;

    /**
     * @var bool Is our Page visible in the Frontend?
     */
    protected $isVisible = true;

    /**
     * @var bool Is our page shown in frontend menus?
     */
    protected $isInMenu = true;

    /**
     * @var bool Has our Page been deleted?
     */
    protected $isDeleted = false;

    /**
     * @var int The id of the domain url this page belongs to
     */
    protected $domainUrlId = 0;

    /**
     * @var string Layout for this page
     */
    protected $layout;

    /**
     * @var bool Are templates inherited recursively or not?
     */
    protected $isLayoutRecursive;

    /**
     * @var string Frontend cached layout path for this page
     */
    protected $frontendLayout;

    /**
     * @var string Backend cached layout path for this page
     */
    protected $backendLayout;

    /**
     * @var string Cached path used for breadcrumb (List of comma separated values of parent ids)
     */
    protected $cachedPath;

    /**
     * @var string Page slug
     */
    protected $slug;

    /**
     * @var string
     */
    protected $fullpageSlug;

    /**
     * @var int Sorting order
     */
    protected $sorting;

    /**
     * @var string
     */
    protected $metaKeywords;

    /**
     * @var string
     */
    protected $metaDescription;

    /**
     * @var int
     */
    protected $originalId;

    /**
     * @var \Continut\Core\System\Domain\Model\Page Original page, if this is a translated one
     */
    protected $original;

    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var bool Can our page be cached or should it always be dynamic?
     */
    protected $isCachable = false;

    /**
     * @var \Continut\Core\System\Domain\Model\DomainUrl
     */
    protected $domainUrl = null;

    /**
     * Protocol used on this page [http, https]
     *
     * @var string
     */
    protected $protocol = 'http';

    /**
     * Simple datamapper used for the database
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'parent_id'           => $this->parentId,
            'title'               => $this->title,
            'slug'                => $this->slug,
            'fullpage_slug'       => $this->fullpageSlug,
            'cached_path'         => $this->cachedPath,
            'domain_url_id'       => $this->domainUrlId,
            'is_deleted'          => $this->isDeleted,
            'is_in_menu'          => $this->isInMenu,
            'is_visible'          => $this->isVisible,
            'layout'              => $this->layout,
            'is_layout_recursive' => $this->isLayoutRecursive,
            'frontend_layout'     => $this->frontendLayout,
            'backend_layout'      => $this->backendLayout,
            'original_id'         => $this->originalId,
            'sorting'             => $this->sorting,
            'meta_keywords'       => $this->metaKeywords,
            'meta_description'    => $this->metaDescription,
            'start_date'          => $this->startDate,
            'end_date'            => $this->endDate,
            'is_cachable'         => $this->isCachable
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * Validation rules for the Page model
     *
     * @return array
     */
    public function dataValidation()
    {
        return [
            'title' => v::notEmpty(),
            'slug'  => v::notEmpty()
        ];
    }

    /**
     * @return bool
     */
    public function getIsInMenu()
    {
        return $this->isInMenu;
    }

    /**
     * @param bool $isInMenu
     *
     * @return $this
     */
    public function setIsInMenu($isInMenu)
    {
        $this->isInMenu = $isInMenu;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        // set also the fullpage_slug
        $parentPagesIds =
            Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                ->cachedBreadcrumb($this->getParentId());

        $this->fullpageSlug = '';
        if ($parentPagesIds) {
            foreach (array_reverse($parentPagesIds) as $pageId) {
                $pageSlug           = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                    ->findById($pageId)
                    ->getSlug();
                $this->fullpageSlug .= $pageSlug . '/';
            }
        }
        // append the current slug to the entire path
        $this->fullpageSlug .= $this->slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullpageSlug()
    {
        return $this->fullpageSlug;
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     *
     * @return $this
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     *
     * @return $this
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     *
     * @return $this
     */
    public function setSorting($sorting)
    {
        if ($sorting < 0) {
            $sorting = 0;
        }
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     *
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return int
     */
    public function getDomainUrlId()
    {
        return $this->domainUrlId;
    }

    /**
     * @param int $domainUrlId
     *
     * @return $this
     */
    public function setDomainUrlId($domainUrlId)
    {
        $this->domainUrlId = $domainUrlId;

        return $this;
    }

    /**
     * @return DomainUrl
     */
    public function getDomainUrl()
    {
        if ($this->domainUrl == null) {
            $this->domainUrl = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                ->findById($this->getDomainUrlId());
        }

        return $this->domainUrl;
    }

    /**
     * Return page title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set page title
     *
     * @param $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get parent PageModel
     *
     * @return mixed
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function getParent()
    {
        if ($this->parentId) {
            if (empty($this->parent)) {
                $this->parent = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                    ->findById($this->getParentId());
            }
        }

        return $this->parent;
    }

    /**
     * Set this page's parent
     *
     * @param \Continut\Core\System\Domain\Model\Page $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param $parentId
     *
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Merges different values from the original page to the translated one
     * Settings like the frontend or backend layout to use are only specified on the original page, and thus
     * need to be re-added to the translated one too
     *
     * @return $this
     */
    public function mergeOriginal()
    {
        // When merging the translated page with the data from the original one, it's best if we ignore the layout
        // as this gives us the posibility to use different layouts for translated pages
        /**if ($this->originalId > 0) {
         * $originalPage = $this->getOriginal();
         * $this->setBackendLayout($originalPage->getBackendLayout());
         * $this->setFrontendLayout($originalPage->getFrontendLayout());
         * }*/

        // @TODO: Add additional merging data that is language specific

        return $this;
    }

    /**
     * @return Page
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function getOriginal()
    {
        if ($this->originalId) {
            if (empty($this->original)) {
                $this->original = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                    ->findById($this->getOriginalId());
            }
        }

        return $this->original;
    }

    /**
     * @return int
     */
    public function getOriginalId()
    {
        return $this->originalId;
    }

    /**
     * @param int $originalId
     *
     * @return $this
     */
    public function setOriginalId($originalId)
    {
        $this->originalId = $originalId;

        return $this;
    }

    /**
     * Layout to be used in the Backend preview
     *
     * @return string
     */
    public function getBackendLayout()
    {
        return $this->backendLayout;
    }

    /**
     * @param string $backendLayout
     *
     * @return $this
     */
    public function setBackendLayout($backendLayout)
    {
        $this->backendLayout = $backendLayout;

        return $this;
    }

    /**
     * Layout to be used in the frontend
     *
     * @return string
     */
    public function getFrontendLayout()
    {
        return $this->frontendLayout;
    }

    /**
     * Sets the layout to be used in the frontend
     *
     * @param $frontendLayout
     *
     * @return $this
     */
    public function setFrontendLayout($frontendLayout)
    {
        $this->frontendLayout = $frontendLayout;

        return $this;
    }

    /**
     * Layout is stored in the DB in the form ExtensionName.layoutName so this method extracts
     * the extension name from layout value (the first part before the '.' dot)
     *
     * @return string
     */
    public function getLayoutExtension()
    {
        if ($this->getLayout()) {
            return mb_substr($this->getLayout(), 0, mb_strpos($this->getLayout(), '.'));
        }

        return '';
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     *
     * @return $this
     */
    public function setLayout($layout)
    {
        // set the layout identifier
        $this->layout = $layout;

        $extensionName = mb_substr($layout, 0, mb_strpos($layout, '.'));
        $settings      = Utility::getExtensionSettings($extensionName);
        $layoutId      = mb_substr($layout, mb_strlen($extensionName) + 1);

        // also set the BE and FE cached layout files
        if (isset($settings['ui']['layout'][ $layoutId ])) {
            $this->setBackendLayout($settings['ui']['layout'][ $layoutId ]['backendFile']);
            $this->setFrontendLayout($settings['ui']['layout'][ $layoutId ]['frontendFile']);
        }

        return $this;
    }

    /**
     * Returns comma separated list of breadcrumb page ids
     *
     * @return string
     */
    public function getCachedPath()
    {
        return $this->cachedPath;
    }

    /**
     * @param string $cachedPath
     *
     * @return $this
     */
    public function setCachedPath($cachedPath)
    {
        $this->cachedPath = $cachedPath;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsLayoutRecursive()
    {
        return $this->isLayoutRecursive;
    }

    /**
     * @param bool $isLayoutRecursive
     *
     * @return $this
     */
    public function setIsLayoutRecursive($isLayoutRecursive)
    {
        $this->isLayoutRecursive = $isLayoutRecursive;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return $this
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return $this
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsCachable()
    {
        return $this->isCachable;
    }

    /**
     * @param bool $isCachable
     *
     * @return $this
     */
    public function setIsCachable($isCachable)
    {
        $this->isCachable = $isCachable;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     *
     * @return $this
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * @param $pageId
     *
     * @TODO Add method that automatically sets the sorting
     */
    public function moveBeforePageId($pageId)
    {
    }

    /**
     * @return string
     */
    public function getCacheKey()
    {
        return 'sys_pages_' . $this->getId();
    }

    /**
     * Is this page assigned to the default language or is it a translated one?
     * Pages that belong to the default language can be translated but cannot have any translations
     *
     * @return bool
     */
    public function hasDefaultLanguage()
    {
        return $this->getDomainUrl()->getIsDefault();
    }

    /**
     * Returns a formatted text to use as display in the filter text
     * The method returns either the page title, or it's breadcrumb if it has one
     * The final text is limited to 100 characters, and always displays the last 100 characters to improve readability
     *
     * @return string
     */
    public function formatForFilter()
    {
        if ($this->getCachedPath()) {
            $text = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                    ->reset()
                    ->whereBreadcrumb($this->getCachedPath())
                    ->implodeAll(' \\ ', 'title') . ' \\ ' . $this->fetchFromField('title');
        } else {
            $text = $this->fetchFromField('title');
        }
        $text .= ' (' . $this->getId() . ')';

        // if the displayed text is too long, crop it so that we can see the final values
        if (mb_strlen($text) > 100) {
            $text = '...' . mb_substr($text, -100);
        }

        return $text;
    }

    /**
     * Set is_deleted = 1 for this page and all it's children
     */
    public function deleteRecursively()
    {
        $collection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');

        $this->setIsDeleted(true);

        $collection
            ->add($this)
            ->save();

        $this->deletePageAndChildren();
    }

    protected function deletePageAndChildren()
    {
        $collection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $childPages = $collection->findByParentId($this->getId());

        foreach ($childPages->getAll() as $page) {
            $page->deleteRecursively();
        }
    }
}
