<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 17.01.2016 @ 12:40
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

class File extends BaseModel
{
    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $fullpath;

    /**
     * @var int
     */
    protected $filesize;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var string
     */
    protected $mime;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $modifiedAt;

    /**
     * @var int mount id
     */
    protected $mountId;

    /**
     * Simple datamapper used for the database
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'fullpath'    => $this->fullpath,
            'filename'    => $this->filename,
            'filesize'    => $this->filesize,
            'location'    => $this->location,
            'mime'        => $this->mime,
            'created_at'  => $this->createdAt,
            'modified_at' => $this->modifiedAt,
            'mount_id'    => $this->mountId
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return int
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * @param int $filesize
     *
     * @return $this
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Return the file's relative path
     *
     * @return string
     */
    public function getRelativePath()
    {
        return $this->location . DS . $this->filename;
    }

    /**
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param string $mime
     *
     * @return $this
     */
    public function setMime($mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     *
     * @return $this
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get the file's extension, in lowercase
     *
     * @return string
     */
    public function getExtension()
    {
        return strtolower(pathinfo($this->getFilename(), PATHINFO_EXTENSION));
    }

    /**
     * @return string
     */
    public function getFullpath()
    {
        return $this->fullpath;
    }

    /**
     * @param string $fullpath
     *
     * @return $this
     */
    public function setFullpath($fullpath)
    {
        $this->fullpath = $fullpath;

        return $this;
    }
}
