<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 07.04.2015 @ 22:46
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Domain\Model;

use Continut\Core\Utility;
use Respect\Validation\Validator as v;

class Domain extends BaseModel
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var bool
     */
    protected $isVisible;

    /**
     * @var bool
     */
    protected $isDeleted;

    /**
     * @var int
     */
    protected $sorting;

    /**
     * Cached array containing the list of domainUrl/language objects for this domain
     *
     * @var array
     */
    protected $domainUrls;

    /**
     * Cached array containing only the domainUrl/language ids for this domain
     *
     * @var array
     */
    protected $domainUrlIds;

    /**
     * @var DomainUrl
     */
    protected $defaultLanguage = null;

    /**
     * Simple datamapper used for the database
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'title'      => $this->title,
            'is_visible' => $this->isVisible,
            'is_deleted' => $this->isDeleted,
            'sorting'    => $this->sorting
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * Validation rules for the data
     *
     * @return array
     */
    public function dataValidation()
    {
        return [
            'title'      => v::length(3, 200),
            'is_visible' => v::notBlank()
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get all the languages defined for this domain
     *
     * @return array
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function getDomainUrls()
    {
        if ($this->domainUrls == null) {
            $this->domainUrls = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                ->whereDomain($this->id)->getAll();
        }

        return $this->domainUrls;
    }

    /**
     * Return an array with all the codes defined for this domain
     * The key stores the domainUrl/language id and the value it's "code" column
     *
     * @return array
     */
    public function getDomainUrlIds()
    {
        if ($this->domainUrlIds == null) {
            $this->domainUrlIds = [];
            foreach ($this->getDomainUrls() as $domainUrl) {
                $this->domainUrlIds[$domainUrl->getId()] = $domainUrl->getCode();
            }
        }

        return $this->domainUrlIds;
    }

    /**
     * Returns all the "codes" defined for all of this domain's languages
     *
     * @return array
     */
    public function getDomainUrlCodes()
    {
        $domainUrls = $this->getDomainUrls();

        $codes = [];
        if ($domainUrls) {
            foreach ($domainUrls as $url) {
                if (mb_strlen(trim($url->getCode())) > 0) {
                    $codes[] = $url->getCode();
                }
            }
        }

        return $codes;
    }

    /**
     * Get the default language of this domain
     *
     * @return DomainUrl
     */
    public function getDefaultLanguage()
    {
        if ($this->defaultLanguage == null) {
            $this->defaultLanguage = Utility::createInstance(
                'Continut\Core\System\Domain\Collection\DomainUrlCollection'
            )->findDefaultLanguage($this->id)->getFirst();
        }

        return $this->defaultLanguage;
    }
}
