<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 17.05.2015 @ 10:31
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

class Folder
{
    /**
     * Recursively delete a folder and the files it contains
     *
     * @param $folderPath
     */
    public function delete($folderPath)
    {
        if (is_dir($folderPath)) {
            $objects = scandir($folderPath);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($folderPath . DS . $object)) {
                        $this->delete($folderPath . DS . $object);
                    } else {
                        unlink($folderPath . DS . $object);
                    }
                }
            }
            rmdir($folderPath);
        }
    }
}
