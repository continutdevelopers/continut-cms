<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 17.05.2015 @ 10:31
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

use Continut\Core\Utility;

/**
 * Class Form
 *
 * The Form class is a helper class that can be used to generate different form elements for
 * your form, whenever your inputs are NOT linked to a model/BaseModel instance
 *
 * If you need to link them to a model and have automatic validation/error messages displayed, use
 * the FormObject helper instead
 *
 * @package Continut\Core\System\Helpers
 */
class Form
{
    /**
     * Field prefix used in the backend form
     *
     * @var string
     */
    protected $prefix = 'data';

    /**
     * When in repeater mode we need to adjust the field names to allow
     * storage for array elements
     *
     * @var bool
     */
    protected $repeaterMode = false;

    /**
     * Repeater element's name, used as an id in the html tag
     *
     * @var string
     */
    protected $repeaterName = 'repeater';

    /**
     * Counter for repeater elements
     *
     * @var int
     */
    protected $repeaterElementId = 0;

    /**
     * Sets up the input's name accordingly
     *
     * @param string $name Name of the input
     *
     * @return string Formatted name of the input, with prefix and optional square brackets
     */
    protected function setFieldName($name)
    {
        // you can also pass config keys as input names, in which case the slash needs to be replaced
        $name      = str_replace('/', '_', $name);
        $fieldName = $this->prefix . "[$name]";

        if ($this->repeaterMode) {
            $fieldName = $this->prefix . "[{$this->repeaterName}][$this->repeaterElementId][$name]";
        }

        // if the field name contains a square bracket, it means it's an array
        // so we need to set it up accordingly
        if (mb_strpos($name, '[')) {
            // extract only the field name
            $name      = mb_substr($name, 0, mb_strpos($name, '['));
            $fieldName = $this->prefix . "[$name][]";
        }

        return $fieldName;
    }

    /**
     * Sets the id attribute of the input
     *
     * @param string $name
     *
     * @return string
     */
    protected function setFieldId($name)
    {
        return 'field_' . str_replace(['[', ']'], ['_', ''], $this->setFieldName($name));
    }

    /**
     * Set the <label> tag
     *
     * @param string $name
     * @param string $label
     *
     * @return string
     */
    protected function setFieldLabel($name, $label)
    {
        if ($label) {
            $id = $this->setFieldId($name);

            return "<label class=\"control-label\" for='$id'>$label</label>";
        }

        return '';
    }

    /**
     * Wizard helper used to generate a hidden input
     *
     * @param string $name
     * @param string $value
     *
     * @return string
     */
    public function hiddenField($name, $value)
    {
        $fieldName = $this->setFieldName($name);
        $fieldId   = $this->setFieldId($name);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/hiddenField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'      => $name,
                'value'     => $value,
                'fieldName' => $fieldName,
                'fieldId'   => $fieldId
            ]
        );

        return $view->render();
    }

    /**
     * Wizard helper used to generate a checkbox field
     *
     * @param string $name          Input name
     * @param string $label         Input label
     * @param mixed  $values        One value or array if you need to create a checkbox group
     * @param mixed  $selectedValue Selected value by default
     *
     * @return string
     */
    public function checkboxField($name, $label, $values, $selectedValue = null)
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $html = '';
        if (is_array($values)) {
            $fieldName = $fieldName . '[]';
            foreach ($values as $text => $value) {
                $id   = $fieldId . '_' . $value;
                $html .= <<<HER
                    <input id="$id" type="text" value="$value" name="$fieldName"/> $text
HER;
            }
        } else {
            $html = <<<HER
                <div class="checkbox">
                $fieldLabel
                <input id="$fieldId" class="form-control" type="checkbox" value="$selectedValue" name="$fieldName"/>
                </div>
HER;
        }

        return $html;
    }

    /**
     * Shows a simple DateTime field with a label in a wizard
     *
     * @param string $name  Input name
     * @param string $label Input label
     * @param string $value Default value
     *
     * @return string
     */
    public function dateTimeField($name, $label, $value = '')
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/dateTimeField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'label'      => $label,
                'value'      => $value,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel
            ]
        );

        return $view->render();
    }

    /**
     * Wizard helper method to show a simple text field with a label
     *
     * @param string $name    Input name
     * @param string $label   Input label
     * @param string $value   Input default value
     * @param array  $options Additional parameters to pass onto the wizard (like "prefix", etc)
     *
     * @return string
     */
    public function textField($name, $label = '', $value = '', $options = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/textField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'label'      => $label,
                'value'      => $value,
                'options'    => $options,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel
            ]
        );

        return $view->render();
    }

    /**
     * Wizard helper method to show a simple text field with a label
     *
     * @param string $name      Input name
     * @param string $label     Input label
     * @param string $value     Input default value
     * @param array  $arguments Additional parameters to pass onto the wizard (like "prefix", etc)
     *
     * @return string
     */
    public function mediaField($name, $label = '', $value = '', $arguments = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/mediaField', 'Backend', 'Backend', 'Helper'));

        $fileReferenceCollection = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\FileReferenceCollection'
        );
        // if the field holds multiple images, their ids will be separated by commas
        if (mb_strlen(trim($value)) == 0) {
            $values = [];
        } else {
            $values = explode(',', $value);
        }
        $fileReferenceCollection->findWithIds($values);
        if (!isset($arguments['maxFiles'])) {
            $arguments['maxFiles'] = 1;
        }

        $view->assignMultiple(
            [
                'name'           => $name,
                'label'          => $label,
                'value'          => $value,
                'fileReferences' => $fileReferenceCollection->getAll(),
                'arguments'      => $arguments,
                'fieldName'      => $fieldName,
                'fieldId'        => $fieldId,
                'fieldLabel'     => $fieldLabel
            ]
        );

        return $view->render();
    }

    /**
     * Simple textarea field
     *
     * @param string $name
     * @param string $label
     * @param mixed $value
     *
     * @return string
     */
    public function textareaField($name, $label, $value)
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $html = <<<HER
        <div class="form-group">
            $fieldLabel
            <textarea id="$fieldId" name="$fieldName" class="form-control" rows="5">$value</textarea>
        </div>
HER;

        return $html;
    }

    /**
     * RTE field
     *
     * @param string $name
     * @param string $label
     * @param mixed $value
     *
     * @return string
     */
    public function rteField($name, $label, $value)
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/rteField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'value'      => $value,
                'label'      => $label,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel
            ]
        );

        return $view->render();
    }

    /**
     * Select field
     *
     * @param string $name
     * @param string $label
     * @param array  $values
     * @param mixed  $value
     * @param array  $options
     *
     * @return string
     */
    public function selectField($name, $label, $values, $value = null, $options = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        /* if $values is an multiarray then it means we have optgroup definitions
         *
         * Example to generate a simple list of options
         * $values = ["value1" => "label 1", "value2" => "label 2"];
         *
         * And with multiarray for optgroups
         * $values = ["optgroup label" => ["value1" => "label1", "value2" => "label 2"]];
        */

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/selectField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple([
            'values'     => $values,
            'value'      => $value,
            'fieldLabel' => $fieldLabel,
            'fieldId'    => $fieldId,
            'fieldName'  => $fieldName,
            'options'    => $options
        ]);

        return $view->render();
    }

    /**
     * Shows a multiselect filter input that allows you to select 1 or more items from a table
     *
     * @param string $name
     * @param string $label
     * @param mixed  $value
     * @param array  $filters
     *
     * @return mixed
     */
    public function linkWizardField($name, $label, $value, $filters = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/linkWizardField', 'Backend', 'Backend', 'Helper'));

        $filter = Utility::createInstance('Continut\Extensions\System\Backend\Classes\Controller\FilterController');
        $selectedValues = null;
        if (isset($filters['type'])) {
            $selectedValues = $filter->getSelected($filters['type'], [$value], $filters['collection']);
        }

        $view->assignMultiple(
            [
                'name'       => $name,
                'value'      => $value,
                'label'      => $label,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel,
                'filters'    => $filters,
                'selectedValues' => $selectedValues
            ]
        );

        return $view->render();
    }

    /**
     * @param string $name
     * @param string $label
     * @param array  $values
     * @param string $template
     * @param string $extensionName
     * @param int    $limit
     * @param string $type
     *
     * @return string
     */
    public function repeaterField($name, $label, $values, $template, $extensionName, $limit = 100, $type = 'accordion')
    {
        // This view holds the template with all the fields you want to repeat
        $fieldsView = Utility::createInstance('Continut\Core\System\View\BaseView');
        $fieldsView->setTemplate(Utility::getResourcePath($template, $extensionName, 'Backend', 'Template'));

        // this is the repeater's view, that will repeat the view you specified above
        $repeaterView = Utility::createInstance('Continut\Core\System\View\BaseView');
        $repeaterView->setTemplate(
            Utility::getResourcePath('Form/repeater' . ucfirst($type), 'Backend', 'Backend', 'Helper')
        );

        $repeaterView->assignMultiple(
            [
                'fieldsView' => $fieldsView,
                'name'       => $name,
                'label'      => $label,
                'values'     => $values,
                'template'   => $template,
                'extension'  => $extensionName,
                'limit'      => $limit
            ]
        );

        return $repeaterView->render();
    }

    public function setRepeaterMode($enabled)
    {
        $this->repeaterMode = $enabled;
    }

    public function setRepeaterName($name)
    {
        $this->repeaterName = $name;
    }

    public function setRepeaterElementId($id)
    {
        $this->repeaterElementId = $id;
    }
}
