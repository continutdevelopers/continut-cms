<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 13.04.2015 @ 22:06
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

use Continut\Core\System\Tools\ErrorException;
use Continut\Core\Utility;

class Image
{
    const SUPPORTED_IMAGES = ['gif', 'png', 'jpg', 'jpeg'];

    /**
     * Resize an image and return it's url
     *
     * @param int|string $image  Image name or image id from sys_file_reference to resize with path and file extension
     * @param int        $width  Image width to resize to
     * @param int        $height Image height to resize to
     * @param string     $prefix Prefix to use for the cached image, by default it is "resize"
     *
     * @return string The fullpath of the newly generated image
     */
    public function resize($image, $width, $height, $prefix = 'resize')
    {
        $fileParts = $this->getImageOrReference($image, $width, $height, $prefix);
        if ($fileParts === false) {
            return '';
        }

        $resizedImage         = $fileParts['resizedImage'];
        $resizedImageAbsolute = $fileParts['resizedImageAbsolute'];
        $absoluteFilename     = $fileParts['absoluteFilename'];

        if (file_exists($resizedImageAbsolute)) {
            return $resizedImage;
        }
        if (!file_exists($absoluteFilename)) {
            return '';
        }

        Utility::getImageManager()
            ->make($absoluteFilename)
            ->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save($resizedImageAbsolute);

        return $resizedImage;
    }

    /**
     * Crop an image and return it's url
     *
     * @param int|string $image  Image name or image id from sys_file_reference to resize with path and file extension
     * @param int        $width  Image width to resize to
     * @param int        $height Image height to resize to
     * @param string     $prefix Prefix to use for the cached image, by default it is "resize"
     *
     * @return string The fullpath of the newly generated image
     */
    public function crop($image, $width, $height, $prefix = 'crop')
    {
        if (is_null($image)) {
            return '';
        }
        $fileParts = $this->getImageOrReference($image, $width, $height, $prefix);
        if ($fileParts === false) {
            return '';
        }

        $resizedImage         = $fileParts['resizedImage'];
        $resizedImageAbsolute = $fileParts['resizedImageAbsolute'];
        $absoluteFilename     = $fileParts['absoluteFilename'];

        if (file_exists($resizedImageAbsolute)) {
            return $resizedImage;
        }
        if (!file_exists($absoluteFilename)) {
            return '';
        }

        Utility::getImageManager()
            ->make($absoluteFilename)
            ->fit($width, $height)
            ->save($resizedImageAbsolute);

        return $resizedImage;
    }

    /**
     * Fetches the absolute path of an image or of a file reference from sys_file_reference
     *
     * @param int|string $image
     * @param int        $width
     * @param int        $height
     * @param string     $prefix
     *
     * @throws ErrorException
     *
     * @return array|false The absolute filename's path and the absolute filename path for it's resized version or
     *                     false if invalid
     */
    protected function getImageOrReference($image, $width, $height, $prefix)
    {
        // image can be either an id (from sys_file_reference) or a string, meaning a relative path to an image
        if (is_array($image)) {
            return false;
        }
        $imageId = (int)$image;
        if ($imageId > 0) {
            $file = Utility::createInstance(
                'Continut\Core\System\Domain\Collection\FileCollection'
            )->findFromReference($imageId);
            if ($file) {
                $image = $file->getRelativePath();
            } else {
                throw new ErrorException(
                    'The original file of this file reference (' . $imageId . ') no longer exists'
                );
            }
        }
        $extension            = pathinfo($image, PATHINFO_EXTENSION);
        $hashFile             = md5($image . $width . $height);
        $resizedImage         = 'Cache/Temp/Images/' . $prefix . '_' . $hashFile . '.' . $extension;
        $resizedImageAbsolute = __ROOTCMS__ . DS . $resizedImage;
        $absoluteFilename     = __ROOTCMS__ . DS . $image;

        return [
            'resizedImage'         => $resizedImage,
            'absoluteFilename'     => $absoluteFilename,
            'resizedImageAbsolute' => $resizedImageAbsolute
        ];
    }

    /**
     * Return the fullpath to a file stored in the "Public" folder of an extension
     *
     * @param string $filename
     * @param string $extensionName
     *
     * @return string
     */
    public function getPath($filename, $extensionName)
    {
        return '/' . Utility::getAssetPath($filename, $extensionName);
    }

    /**
     * Checks if the filename has an image extension. Only used for image types that CAN be resized by the gd library
     *
     * @param $filename
     *
     * @return bool
     */
    public function isImage($filename)
    {
        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

        return in_array($ext, self::SUPPORTED_IMAGES);
    }
}
