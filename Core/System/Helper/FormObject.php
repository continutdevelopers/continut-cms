<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 17.05.2015 @ 10:31
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

use Continut\Core\System\Domain\Model\BaseModel;
use Continut\Core\Utility;

/**
 * Class FormObject
 *
 * The FormObject class is a helper class that can be used to generate different form elements for
 * BaseModel objects, and it includes validation and display of error messages
 *
 * So, as a rule of thumb...
 * Whenever you need to show form inputs for a model, use the FormObject view helper
 * When you need to show form inputs not linked to a model, use the Form view helper instead
 *
 * @package Continut\Core\System\Helpers
 */
class FormObject
{
    /**
     * @var string Field prefix used in the backend form
     */
    protected $prefix = 'data';

    /**
     * Sets the id attribute of the input
     *
     * @param string $name
     *
     * @return string
     */
    protected function setFieldId($name)
    {
        return 'field_' . str_replace(['[', ']'], ['_', ''], $this->setFieldName($name));
    }

    /**
     * Sets up the input's name accordingly
     *
     * @param string $name Name of the input
     *
     * @return string Formatted name of the input, with prefix and optional square brackets
     */
    protected function setFieldName($name)
    {
        $fieldName = $this->prefix . "[$name]";
        // if the field name contains a square bracket, it means it's an array
        // so we need to set it up accordingly
        if (mb_strpos($name, '[')) {
            // extract only the field name
            $name      = mb_substr($name, 0, mb_strpos($name, "["));
            $fieldName = $this->prefix . "[$name][]";
        }

        return $fieldName;
    }

    /**
     * Set the <label> tag
     *
     * @param string $name
     * @param string $label
     *
     * @return string
     */
    protected function setFieldLabel($name, $label)
    {
        if ($label) {
            // we do not set the id if the field $name is an array
            if (strpos($name, '[')) {
                return "<label class=\"control-label\">$label</label>";
            } else {
                return "<label class=\"control-label\" for='field_$name'>$label</label>";
            }
        }

        return '';
    }

    /**
     * Set the error messages and error classes on the wrapping divs
     *
     * @param BaseModel $model
     * @param string    $columnName
     *
     * @return array Returns the error block with the messages and the error css class to use
     */
    protected function setErrors($model, $columnName)
    {
        $errorsClass = '';
        $errorsBlock = '';

        if ($model->hasValidationErrors($columnName)) {
            $errorsClass   = 'has-error';
            $errorMessages = '';
            foreach ($model->getValidationErrors($columnName) as $error) {
                $errorMessages .= "<p>$error</p>";
            }
            $errorsBlock = <<<HER
                <span class="help-block">$errorMessages</span>
HER;
        }

        return ['errorsBlock' => $errorsBlock, 'errorsClass' => $errorsClass];
    }

    /**
     * Renders static text, no inputs
     *
     * @param string $label
     * @param mixed  $value
     *
     * @return string
     */
    public function info($label, $value)
    {
        return '<div class="form-group">
                    <label class="control-label">' . $label . '</label>
                    <p class="form-control-static">' . $value . '</p>
                </div>';
    }

    /**
     * Wizard helper used to generate a hidden input
     *
     * @param BaseModel $model
     * @param string    $name
     * @param string    $value
     *
     * @return string
     */
    public function hiddenField($model, $name, $value = '')
    {
        $fieldName = $this->setFieldName($name);

        // if no default value is set, get the one from the model
        if (!$value) {
            $value = $model->fetchFromField($name);
        }
        $html = <<<HER
			<input id="field_$name" type="hidden" class="form-control" value="$value" name="$fieldName"/>
HER;

        return $html;
    }

    /**
     * Wizard helper used to generate a checkbox field
     *
     * @param BaseModel $model
     * @param string    $name  Input name
     * @param string    $label Input label
     * @param mixed     $value Checkbox value
     *
     * @return string
     */
    public function checkboxField($model, $name, $label, $value)
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $errors     = $this->setErrors($model, $name);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/checkboxField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'value'      => $value,
                'label'      => $label,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel,
                'errors'     => $errors
            ]
        );

        return $view->render();
    }

    /**
     * Shows a simple DateTime field with a label in a wizard
     *
     * @param BaseModel $model Model object
     * @param string    $name  Input name
     * @param string    $label Input label
     * @param string    $value Default value
     * @param array     $options Additional options
     *
     * @return string
     */
    public function dateTimeField($model, $name, $label, $value = null, $options = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $errors     = $this->setErrors($model, $name);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/dateTimeField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'value'      => $value,
                'options'    => $options,
                'label'      => $label,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel,
                'errors'     => $errors
            ]
        );

        return $view->render();
    }

    /**
     * Wizard helper method to show a simple text field with a label
     *
     * @param BaseModel $model   Model object
     * @param string    $name    Input name
     * @param string    $label   Input label
     * @param string    $value   Input default value
     * @param array     $options Additional parameters to pass onto the wizard (like "prefix", etc)
     *
     * @return string
     */
    public function textField($model, $name, $label = '', $value = '', $options = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $fieldId    = $this->setFieldId($name);
        $errors     = $this->setErrors($model, $name);

        // if no default value is set, get the one from the model
        if (!$value) {
            $value = $model->fetchFromField($name);
        }

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/textField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'label'      => $label,
                'value'      => $value,
                'options'    => $options,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel,
                'errors'     => $errors,
            ]
        );

        return $view->render();
    }

    /**
     * Wizard helper method to show a simple text field with a label
     *
     * @param BaseModel $model Model object
     * @param string $name      Input name
     * @param string $label     Input label
     * @param string $value     Input default value
     * @param array  $options Additional parameters to pass onto the wizard (like "prefix", etc)
     *
     * @return string
     */
    public function mediaField($model, $name, $label = '', $value = '', $options = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $errors     = $this->setErrors($model, $name);

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/mediaField', 'Backend', 'Backend', 'Helper'));

        $fileReferenceCollection = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\FileReferenceCollection'
        );
        // if the field holds multiple images, their ids will be separated by commas
        if (mb_strlen(trim($value)) == 0) {
            $values = [];
        } else {
            $values = explode(',', $value);
        }
        $fileReferenceCollection->findWithIds($values);
        if (!isset($options['maxFiles'])) {
            $options['maxFiles'] = 1;
        }

        $view->assignMultiple(
            [
                'name'           => $name,
                'label'          => $label,
                'value'          => $value,
                'fileReferences' => $fileReferenceCollection->getAll(),
                'options'        => $options,
                'fieldName'      => $fieldName,
                'fieldId'        => $fieldId,
                'fieldLabel'     => $fieldLabel,
                'errors'         => $errors
            ]
        );

        return $view->render();
    }

    /**
     * Simple textarea field
     *
     * @param BaseModel $model Model object
     * @param string    $name
     * @param string    $label
     * @param string    $value
     *
     * @return string
     */
    public function textareaField($model, $name, $label, $value = '')
    {
        $fieldName  = $this->setFieldName($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $errors     = $this->setErrors($model, $name);

        // if no default value is set, get the one from the model
        if (!$value) {
            $value = $model->fetchFromField($name);
        }

        $html = <<<HER
        <div class="form-group {$errors['errorsClass']}">
			$fieldLabel
			<textarea id="field_$name" name="$fieldName" class="form-control" rows="5">$value</textarea>
			{$errors['errorsBlock']}
		</div>
HER;

        return $html;
    }

    /**
     * RTE field
     *
     * @param BaseModel $model Model object
     * @param string    $name
     * @param string    $label
     * @param string    $value
     *
     * @return string
     */
    public function rteField($model, $name, $label, $value = '')
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $errors     = $this->setErrors($model, $name);

        if (!$value) {
            $value = $model->fetchFromField($name);
        }

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/rteField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple(
            [
                'name'       => $name,
                'value'      => $value,
                'label'      => $label,
                'fieldName'  => $fieldName,
                'fieldId'    => $fieldId,
                'fieldLabel' => $fieldLabel,
                'errors'     => $errors
            ]
        );

        return $view->render();
    }

    /**
     * Select field
     *
     * @param BaseModel $model   Model object
     * @param string    $name    Input name
     * @param string    $label   Input label
     * @param array     $values  Select dropdown values
     * @param mixed     $value   Selected value
     * @param array     $options Additional options to configure the input
     *
     * @return string
     */
    public function selectField($model, $name, $label, $values, $value = null, $options = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        // if no default value is set, get the one from the model
        if (!$value) {
            $value = $model->fetchFromField($name);
        }

        /* if $values is an multiarray then it means we have optgroup definitions
         *
         * Example to generate a simple list of options
         * $values = ["value1" => "label 1", "value2" => "label 2"];
         *
         * And with multiarray for optgroups
         * $values = ["optgroup label" => ["value1" => "label1", "value2" => "label 2"]];
        */

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/selectField', 'Backend', 'Backend', 'Helper'));

        $view->assignMultiple([
            'values'     => $values,
            'value'      => $value,
            'fieldLabel' => $fieldLabel,
            'fieldId'    => $fieldId,
            'fieldName'  => $fieldName,
            'options'    => $options
        ]);

        return $view->render();
    }

    /**
     * Shows a multiselect filter input that allows you to select 1 or more items from a table
     *
     * @param BaseModel $model Model object
     * @param string    $name
     * @param string    $label
     * @param mixed     $value
     * @param array     $filters
     *
     * @return mixed
     */
    public function linkWizardField($model, $name, $label, $value, $filters = [])
    {
        $fieldName  = $this->setFieldName($name);
        $fieldId    = $this->setFieldId($name);
        $fieldLabel = $this->setFieldLabel($name, $label);
        $errors     = $this->setErrors($model, $name);

        if (!$value) {
            $value = $model->fetchFromField($name);
        }

        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('Form/linkWizardField', 'Backend', 'Backend', 'Helper'));

        $filter = Utility::createInstance('Continut\Extensions\System\Backend\Classes\Controller\FilterController');
        $selectedValues = null;
        if (isset($filters['type'])) {
            $selectedValues = $filter->getSelected($filters['type'], [$value]);
        }

        $view->assignMultiple(
            [
                'name'           => $name,
                'value'          => $value,
                'label'          => $label,
                'fieldName'      => $fieldName,
                'fieldId'        => $fieldId,
                'fieldLabel'     => $fieldLabel,
                'filters'        => $filters,
                'errors'         => $errors,
                'selectedValues' => $selectedValues,
            ]
        );

        return $view->render();
    }
}
