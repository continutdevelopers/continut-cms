<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 15.04.2017 @ 17:23
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

use Continut\Core\Utility;

/**
 * Class DateTime
 *
 * The DateTime class is a helper class that can be used to handle date/time based on current locale
 *
 * @package Continut\Core\System\Helpers
 */
class DateTime
{
    /**
     * Date and DateTime format constants
     */
    const DATE_FORMAT_DEFAULT = 'locale.date.format.default';
    const DATE_FORMAT_SHORT = 'locale.date.format.short';
    const DATE_FORMAT_MEDIUM = 'locale.date.format.medium';
    const DATE_FORMAT_LONG = 'locale.date.format.long';
    const DATE_FORMAT_FULL = 'locale.date.format.full';

    const DATETIME_FORMAT_DEFAULT = 'locale.datetime.format.default';
    const DATETIME_FORMAT_SHORT = 'locale.datetime.format.short';
    const DATETIME_FORMAT_MEDIUM = 'locale.datetime.format.medium';
    const DATETIME_FORMAT_LONG = 'locale.datetime.format.long';
    const DATETIME_FORMAT_FULL = 'locale.datetime.format.full';

    /**
     * @var \DateTimeZone
     */
    protected $defaultTimezone;

    public function __construct()
    {
        // by default we store our datetimes as UTC then convert them to the appropriate timezone
        $this->defaultTimezone = new \DateTimeZone('UTC');
    }

    /**
     * Format a timestamp to a date using the specified format and the locale
     *
     * @param int|string $timestamp Timestamp or MySQL DateTime formatted string (Y-m-d H:i:s)
     * @param string     $pattern   Custom date format pattern, if required, see DATETIME_FORMAT constants in this class
     * @param string     $locale    Custom locale, if not uses the one defined in the system
     *
     * @return string
     */
    public function format($timestamp, $pattern = self::DATETIME_FORMAT_LONG, $locale = '')
    {
        // if no timestamp is passed, exit
        if (empty($timestamp)) {
            return '';
        }

        // Get the locale for this domain/session or use the one defined in configuration.php
        if (empty($locale)) {
            $locale = Utility::getConfiguration('System/Locale');
        }

        // Get the timezone configured for this domain/session, if not use UTC by default
        $timezone = new \DateTimeZone(Utility::getConfiguration('System/Timezone'));
        if (empty($timezone)) {
            $timezone = $this->defaultTimezone;
        }
        if (filter_var($timestamp, FILTER_VALIDATE_INT) === false) {
            // we assume that in this case $timestamp is already in MySQL Datetime format
            $timestamp = strtotime($timestamp);
        }

        $dateTime = new \DateTime();
        $dateTime
            ->setTimestamp($timestamp)
            ->setTimezone($timezone);

        return $dateTime->format(Utility::helper('Localization')->translate($pattern));

        /*$formatter = new \IntlDateFormatter(
            $locale,
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::SHORT,
            $timezone,
            \IntlDateFormatter::GREGORIAN,
            $pattern
        );

        return $formatter->format($dateTime->getTimestamp());*/
    }

    /**
     * Gets the list of all the available timezones on this system and PHP version
     * The list contains the timezone identifier and the UTC hour difference for each timezone
     *
     * @return array
     */
    public function getAllTimezones()
    {
        // Get all available timezones on this system
        $allSupportedTimezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);

        // Calculate their offset based on UTC time
        $timezoneOffsets = [];
        foreach ($allSupportedTimezones as $timezone) {
            $specificTimezone             = new \DateTimeZone($timezone);
            $timezoneOffsets[ $timezone ] = $specificTimezone->getOffset(new \DateTime);
        }

        // Sort timezones by offset
        asort($timezoneOffsets);

        // Generate the pretty list with UTC difference in hours
        $timezoneList = [];
        foreach ($timezoneOffsets as $timezone => $offset) {
            $offset_prefix   = $offset < 0 ? '-' : '+';
            $offsetFormatted = gmdate('H:i', abs($offset));

            $prettyOffset = "UTC${offset_prefix}${offsetFormatted}";

            $timezoneList[ $timezone ] = "(${prettyOffset}) $timezone";
        }

        return $timezoneList;
    }

    /**
     * @param string        $date     Date and time in a string format
     * @param \DateTimeZone $timezone
     * @param string        $format
     *
     * @return null|int Timestamp or null if $date is invalid
     */
    public function dateTimeToUTCTimestamp($date, $timezone, $format = self::DATETIME_FORMAT_LONG)
    {
        $utcDate = null;
        if (!empty($date)) {
            $formattedDate = \DateTime::createFromFormat(
                Utility::helper('Localization')->translate($format),
                $date
            );
            $formattedDate->setTimezone($timezone);
            // store date as UTC
            $utcDate = $formattedDate->getTimestamp() - $formattedDate->getOffset();
        }

        return $utcDate;
    }
}
