<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 13.04.2015 @ 22:07
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

use Continut\Core\System\Tools\Exception;
use Continut\Core\Utility;

class Url
{
    /**
     * Generates a link to a path
     *
     * @param string $path   Path name
     * @param array  $params path params
     *
     * @return string
     */
    public function linkToPath($path, $params = array())
    {
        // only in the Frontend, add the "html" postfix to urls, if no format is defined
        // eg: /en/page/some-page becomes /en/page/some-page.html
        if (Utility::getApplicationScope() == Utility::SCOPE_FRONTEND) {
            if (!isset($params['_format'])) {
                $params['_format'] = 'html';
            }
        }

        return Utility::getRequest()->getUrlGenerator()->generate($path, $params);
    }

    /**
     * Frontend link to a page by using it's id
     *
     * @param int $pageId id of the page to link to
     *
     * @return string
     */
    public function linkToPageId($pageId)
    {
        $params = ['id' => $pageId];

        return $this->linkToPath('page_id', $params);
    }

    /**
     * @return string
     */
    public function linkToHome()
    {
        $homepage = Utility::getSite()->getDomainUrl()->getHomepage();
        return $this->linkToSlug($homepage);
    }

    /**
     * Frontend link to a certaing slug
     * //TODO Add additional parameters, language, etc
     *
     * @param      $page
     * @param bool $absolute Return just the slug or an absolute url and the slug?
     *
     * @return mixed
     */
    public function linkToSlug($page, $absolute = false)
    {
        // check if the page has a code or not, so we know which symfony route to use
        // Utility::getSite()->getDomain()->getDomainUrlIds() returns an array that holds the domain_url 'id' as
        // it's key and the domain_url's 'code' as it's value
        if (Utility::getSite()->getDomain()->getDomainUrlIds()[ $page->getDomainUrlId() ]) {
            $link = $this->linkToPath(
                'page_slug',
                [
                    'slug' => $page->getFullpageSlug(),
                    'code' => Utility::getSite()->getDomain()->getDomainUrlIds()[ $page->getDomainUrlId() ]
                ]
            );
        } else {
            $link = $this->linkToPath(
                'page_slug_no_code',
                [
                    'slug' => $page->getFullpageSlug()
                ]
            );
        }

        if ($absolute) {
            $link = $page->getProtocol() . '://' . $page->getDomainUrl()->getUrl() . $link;
        }

        return $link;
    }

    /**
     * Backend link to menu method
     *
     * @param array $settings Menu settings from configuration.json
     *
     * @throws Exception
     *
     * @return string
     */
    public function linkToMenu($settings)
    {
        $params = isset($settings['params']) ? $settings['params'] : [];
        if (isset($settings['path'])) {
            return $this->linkToPath($settings['path'], $params);
        }

        return '';
    }
}
