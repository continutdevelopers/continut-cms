<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 17.05.2015 @ 10:31
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Helper;

use Continut\Core\Utility;

/**
 * Class FormSettings
 *
 * The Form class is a helper class that can be used to generate different form elements for
 * your form, whenever your inputs are NOT linked to a model/BaseModel instance
 *
 * If you need to link them to a model and have automatic validation/error messages displayed, use
 * the FormObject helper instead
 *
 * @package Continut\Core\System\Helpers
 */
class FormSettings extends Form
{
    /**
     * Wizard helper method to show a simple text field with a label
     *
     * @param string $name    Input name
     * @param string $label   Input label
     * @param string $value   Input default value
     * @param array  $options Additional parameters to pass onto the wizard (like "prefix", etc)
     *
     * @return string
     */
    public function textField($name, $label = '', $value = '', $options = [])
    {
        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('FormSettings/wrapper', 'Backend', 'Backend', 'Helper'));

        $label       = Utility::helper('Localization')->translate($label);
        $actualValue = $value;
        $visibility  = $this->checkVisibility($options['visibility']);
        $parentValue = $this->getParentConfigValue($name);
        $disabled    = (is_null($value) && Utility::getSession()->get('configurationSite') != '0');
        if ($disabled) {
            $options['disabled'] = true;
            $value               = $parentValue;
        }
        $input = parent::textField($name, $label, $value, $options);

        $view->assignMultiple(
            [
                'name'        => $name,
                'fieldName'   => $this->setFieldName($name),
                'deleteField' => $this->getDeleteFieldName($name),
                'input'       => $input,
                'fieldId'     => $this->setFieldId($name),
                'value'       => $actualValue,
                'parentValue' => $parentValue,
                'visibility'  => $visibility
            ]
        );

        return $view->render();
    }

    /**
     * Select field
     *
     * @param string $name
     * @param string $label
     * @param array  $values
     * @param mixed  $value
     * @param array  $options
     *
     * @return string
     */
    public function selectField($name, $label, $values, $value = null, $options = [])
    {
        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath('FormSettings/wrapper', 'Backend', 'Backend', 'Helper'));

        $label       = Utility::helper('Localization')->translate($label);
        $actualValue = $value;
        $visibility  = $this->checkVisibility($options['visibility']);
        $parentValue = $this->getParentConfigValue($name);
        $disabled    = ($value == null && Utility::getSession()->get('configurationSite') != '0');
        if ($disabled) {
            $options['disabled'] = true;
            $value               = $parentValue;
        }
        $input = parent::selectField($name, $label, $values, $value, $options);

        $view->assignMultiple(
            [
                'name'        => $name,
                'input'       => $input,
                'fieldName'   => $this->setFieldName($name),
                'deleteField' => $this->getDeleteFieldName($name),
                'fieldId'     => $this->setFieldId($name),
                'value'       => $actualValue,
                'parentValue' => $parentValue,
                'visibility'  => $visibility
            ]
        );

        return $view->render();
    }

    /**
     * Return visibility levels for this input
     *
     * @param string $visibility CSV list of visibility levels
     *
     * @return array
     */
    public function checkVisibility($visibility)
    {
        $visibilitySettings = array_map('trim', explode(',', $visibility));

        $scope = 'global';
        if (Utility::getSession()->has('configurationSite')) {
            $currentScope = explode('_', Utility::getSession()->get('configurationSite'));
            if (sizeof($currentScope) > 1) {
                $scope = $currentScope[0];
            }
            // else we're in global scope, which is already set
        }
        $minimalVisibility = 'global';
        if (in_array('domain', $visibilitySettings)) {
            $minimalVisibility = 'domain';
        }
        if (in_array('url', $visibilitySettings)) {
            $minimalVisibility = 'url';
        }

        return ['minimal' => $minimalVisibility, 'scope' => $scope];
    }

    /**
     * Get the first configuration value you find for this key
     *
     * @param string $name
     *
     * @return null|string
     */
    public function getParentConfigValue($name)
    {
        $configCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\ConfigurationCollection');

        $item = null;
        if (Utility::getSession()->has('configurationSite')) {
            $currentScope = explode('_', Utility::getSession()->get('configurationSite'));
            if (sizeof($currentScope) > 1) {
                if ($currentScope[0] == 'domain') {
                    // for domains get the global config, if any
                    $item = $configCollection->findWithReference(0, 0, $name);
                }
                if ($currentScope[0] == 'url') {
                    $languageId = (int)$currentScope[1];
                    $domainUrl  = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                        ->findById($languageId);
                    // for the url, get the domain config
                    $item = $configCollection->findWithReferenceForUrl($domainUrl->getDomainId(), $name);
                }
            }
            if ($item) {
                return $item->getValue();
            }

            return null;
        } else {
            return null;
        }
    }

    /**
     * Generate input name for deleting configuration
     *
     * @param string $name
     *
     * @return string
     */
    public function getDeleteFieldName($name)
    {
        $name = str_replace('/', '_', $name);
        return $this->prefix . '[delete][' . $name . ']';
    }
}
