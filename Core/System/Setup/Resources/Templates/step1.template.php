<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $this->__('setup.title')?></title>

        <link rel="stylesheet" type="text/css" href="/Core/System/Setup/Resources/Assets/install.css" />
        <script type="text/javascript" src="/Extensions/System/Backend/Resources/Public/JavaScript/jquery/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="/Core/System/Setup/Resources/Assets/install.js"></script>
    </head>
    <body>
        <div class="lines clear">
            <div class="line theme1"></div>
            <div class="line theme2"></div>
            <div class="line theme3"></div>
            <div class="line theme4"></div>
            <div class="line theme5"></div>
        </div>
        <div id="main" class="main">
            <p class="text-center title">
                <img src="/Extensions/System/Backend/Resources/Public/Images/logo_negru.svg" height="50" alt="<?= $this->__('setup.title') ?>" />
                <br/><?= $this->__('setup.title')?>
            </p>
            <div class="panel panel-login" id="steps_container">
                <div class="panel-heading">
                    <?= $this->__('setup.step1.title') ?>
                </div>
                <div class="panel panel-body">
                    <form method="post" class="form login" action="install.php?step=2">
                        <fieldset>
                            <legend><?= $this->__('setup.step1.changeLanguage')?></legend>
                            <div class="field">
                                <select name="locale" id="locale" class="text">
                                    <?php foreach ($locales as $localeKey => $localeLabel): ?>
                                        <option <?= (isset($params['locale']) && $params['locale'] == $localeKey) ? 'selected="selected"' : ''; ?> value="<?= $localeKey ?>"><?= $localeLabel ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </fieldset>
                        <?= $this->__('setup.step1.description') ?>
                        <p><input type="submit" class="button submit" value="<?= $this->__('setup.general.next') ?>" /></p>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>