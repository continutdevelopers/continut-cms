<div class="panel-heading">
    <?= $this->__('setup.step4.title') ?>
</div>
<div class="panel panel-body">
    <form method="post" class="form login" action="install.php?step=6">
        <input type="hidden" name="params[db_user]" value="<?= $params['db_user'] ?>" />
        <input type="hidden" name="params[db_pass]" value="<?= $params['db_pass'] ?>" />
        <input type="hidden" name="params[db_host]" value="<?= $params['db_host'] ?>" />
        <input type="hidden" name="params[db_driver]" value="<?= $params['db_driver'] ?>" />
        <?php if (sizeof($databases) > 0): ?>
        <fieldset>
            <legend><?= $this->__('setup.step4.useExistingDatabase') ?></legend>
            <p><?= $this->__('setup.step4.existingDatabaseOverwrite') ?></p>
            <div class="field">
                <select name="db_name" id="db_name" class="text">
                    <?php foreach ($databases as $database): ?>
                        <option <?= (isset($params['db']) && $params['db'] == $database) ? 'selected="selected"' : ''; ?> value="<?= $database ?>"><?= $database ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </fieldset>
        <?php else: ?>
            <p class="text-danger"><?= $this->__('setup.step4.noDatabase') ?></p>
        <?php endif; ?>
        <fieldset>
            <legend><?= $this->__('setup.step4.createDatabase') ?></legend>
            <div class="field">
                <input name="db_name_new" id="db_name_new" type="text" class="text" value="<?= isset($params['db_new']) ? $params['db_new'] : ''; ?>" placeholder="<?= $this->__('setup.step4.databaseName')?>"/>
            </div>
        </fieldset>
        <?php if (isset($params['error'])): ?>
            <p class="text-danger"><?= $params['error'] ?></p>
        <?php endif; ?>
        <fieldset>
            <legend><?= $this->__('setup.step4.adminAccount') ?></legend>
            <p><small><?= $this->__('setup.step4.credentialRequirements') ?></small></p>
            <div class="field">
                <input name="admin_user" id="admin_user" type="text" class="text" value="<?= isset($params['admin_user']) ? $params['admin_user'] : ''; ?>" placeholder="<?= $this->__('setup.step4.adminUsername')?>"/>
            </div>
            <div class="field">
                <input name="admin_pass" id="admin_pass" type="password" class="text" value="<?= isset($params['admin_pass']) ? $params['admin_pass'] : ''; ?>" placeholder="<?= $this->__('setup.step4.adminPassword')?>"/>
            </div>
            <div class="field">
                <label for="admin_locale"><?= $this->__('setup.step4.adminLocale') ?></label>
                <select name="admin_locale" id="admin_locale" class="text">
                    <?php foreach ($locales as $localeKey => $localeLabel): ?>
                        <option <?= (isset($params['admin_locale']) && $params['admin_locale'] == $localeKey) ? 'selected="selected"' : ''; ?> value="<?= $localeKey ?>"><?= $localeLabel ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="field">
                <label for="admin_timezone"><?= $this->__('setup.step4.adminTimezone') ?></label>
                <select name="admin_timezone" id="admin_timezone" class="text">
                    <?php foreach ($timezones as $timezoneKey => $timezoneLabel): ?>
                        <option <?= (isset($params['admin_timezone']) && $params['admin_timezone'] == $timezoneKey) ? 'selected="selected"' : ''; ?> value="<?= $timezoneKey ?>"><?= $timezoneLabel ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </fieldset>
        <p><input type="submit" class="button submit" value="<?= $this->__('setup.general.next') ?>" /></p>
    </form>
</div>