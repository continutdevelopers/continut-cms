<div class="panel-heading">
    <?= $this->__('setup.step2.title') ?>
</div>
<div class="panel panel-body">
    <form method="post" class="form login" action="install.php?step=3">
        <?php if ($phpVersionOk): ?>
            <p class="text-success"><img src="/Core/System/Setup/Resources/Images/check.svg" alt="" /> <?= $this->__('setup.step2.phpVersionOk', ['version' => $phpVersion]) ?></p>
        <?php else: ?>
            <p class="text-danger"><img src="/Core/System/Setup/Resources/Images/ban.svg" alt="" /> <?= $this->__('setup.step2.phpVersionNotOk') ?></p>
        <?php endif; ?>
        <?php if (sizeof($missingExtensions) > 0): ?>
            <p><?= $this->__('setup.step2.extensionsMissing') ?></p>
            <ol>
                <?php foreach ($missingExtensions as $extension): ?>
                <li class="text-danger"><?= $extension; ?></li>
                <?php endforeach; ?>
            </ol>
        <?php else: ?>
            <p class="text-success"><img src="/Core/System/Setup/Resources/Images/check.svg" alt="" /> <?= $this->__('setup.step2.extensionsOk') ?></p>
        <?php endif; ?>
        <?php if (sizeof($availablePdoDrivers) > 0): ?>
            <p class="text-success"><img src="/Core/System/Setup/Resources/Images/check.svg" alt="" /> <?= $this->__('setup.step2.driversOk', ['drivers' => implode(', ', $availablePdoDrivers)]) ?></p>
        <?php else: ?>
            <p class="text-danger"><img src="/Core/System/Setup/Resources/Images/ban.svg" alt="" /> <?= $this->__('setup.step2.driversMissing') ?></p>
        <?php endif; ?>
        <?php if ($canInstall): ?>
            <p><input type="submit" class="button submit" value="<?= $this->__('setup.general.next') ?>" /></p>
        <?php else: ?>
            <p><?= $this->__('setup.step2.retry') ?></p>
            <p><input type="submit" class="button error" data-action="install.php?step=2" value="<?= $this->__('setup.step2.recheckSystem') ?>" /></p>
        <?php endif; ?>
    </form>
</div>