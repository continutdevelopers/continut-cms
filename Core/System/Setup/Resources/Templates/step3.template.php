<div class="panel-heading">
    <?= $this->__('setup.step3.title') ?>
</div>
<div class="panel panel-body">
    <form method="post" class="form login" action="install.php?step=4">
        <?= $this->__('setup.step3.description') ?>
        <fieldset>
            <legend><?= $this->__('setup.step3.configurationHeader') ?></legend>
            <?php if (sizeof($availablePdoDrivers) > 0): ?>
            <div class="field">
                <select name="params[db_driver]" id="db_driver" class="text">
                    <?php foreach ($availablePdoDrivers as $driver): ?>
                    <option <?= (isset($params['db_driver']) && $params['db_driver'] == $driver) ? 'selected="selected"' : ''; ?> value="<?= $driver ?>"><?= $driver ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div id="db_fields">
                <div class="field">
                    <input name="params[db_host]" id="db_host" type="text" class="text" value="<?= isset($params['db_host']) ? $params['db_host'] : '127.0.0.1'; ?>" placeholder="<?= $this->__('setup.step3.databaseHost')?>"/>
                </div>
                <div class="field">
                    <input name="params[db_user]" id="db_user" type="text" class="text" value="<?= isset($params['db_user']) ? $params['db_user'] : 'vagrant'; ?>" placeholder="<?= $this->__('setup.step3.databaseUser')?>"/>
                </div>
                <div class="field">
                    <input name="params[db_pass]" id="db_pass" type="password" class="password" value="<?= isset($params['db_pass']) ? $params['db_pass'] : 'vagrant'; ?>" placeholder="<?= $this->__('setup.step3.databasePassword')?>"/>
                </div>
                <?php if (isset($params['error'])): ?>
                    <p class="text-danger"><?= $this->__('setup.step3.errorConnection', ['error' => $params['error']])?></p>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <div class="field">
                <?php if (sizeof($availablePdoDrivers) > 0): ?>
                    <input type="submit" class="button submit" value="<?= $this->__('setup.general.next') ?>" />
                <?php else: ?>
                    <?= $this->__('setup.step3.missingPdoDrivers') ?>
                    <input type="submit" class="button error" data-action="install.php?step=3" value="<?= $this->__('setup.step2.recheckSystem') ?>" />
                <?php endif; ?>
            </div>
        </fieldset>
    </form>
</div>