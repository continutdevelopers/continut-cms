<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 18.02.2017 @ 18:17
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Setup;

use Continut\Core\Utility;

class Database
{
    protected $db;

    protected $host;

    protected $handler;

    public function __construct($hostAndPort, $username, $password)
    {
        if (Utility::getDatabase()) {
            $this->handler = Utility::getDatabase();
        } else {
            try {
                $this->handler = new \PDO(
                    $hostAndPort,
                    $username,
                    $password,
                    [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']
                );
            } catch (\PDOException $e) {
                throw new \Exception(
                    'Cannot connect to the database. Please check username, password and host',
                    20000001
                );
            }
        }
    }
}
