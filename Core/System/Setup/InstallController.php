<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 18.02.2017 @ 18:17
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Setup;

use Continut\Core\System\View\BaseView;
use Continut\Core\Utility;

/**
 * Class Installer - contains the code necessary to install the CMS for the first time
 *
 * @package Continut\Core
 */
class InstallController
{
    /**
     * List of required PHP extensions by the CMS, checked during install
     * @TODO : check if imagick is required
     * @var array
     */
    protected $requiredExtensionsList = [
        'PDO',
        'Reflection',
        'bcmath',
        'ctype',
        'date',
        'exif',
        'fileinfo',
        'filter',
        'gd',
        'iconv',
        'intl',
        'json',
        'mbstring',
        'pcre',
        'posix',
        'session',
        'spl',
        'standard',
        'tokenizer',
        'curl',
        'dom',
        'xml'
    ];

    protected $locale = 'en_US';

    /**
     * Initialization method called before each step
     *
     * @param int $step
     *
     * @return BaseView
     */
    public function init($step = 1)
    {
        session_start();

        $step = (int)$step;

        if (isset($_POST['locale'])) {
            $_SESSION['locale'] = $_POST['locale'];
        }
        $this->locale = (isset($_SESSION['locale'])) ? $_SESSION['locale'] : 'en_US';

        $rootPath = __ROOTCMS__ . DS . 'Core' . DS . 'System' . DS . 'Setup';
        $labelsPath = $rootPath . DS . 'Languages';
        Utility::helper('Localization')->loadLabelsFromPath($this->locale, $labelsPath);

        $view = new BaseView();
        $view->setTemplate($rootPath . DS . 'Resources' . DS . 'Templates' . DS . 'step' . $step . '.template.php');

        return $view;
    }

    /**
     * Step 1 of the installer
     *
     * @return string
     */
    public function step1()
    {
        $view = $this->init(1);

        $params['locale'] = $this->locale;

        $view->assign('locales', Utility::helper('Locale')->locales());
        $view->assign('params', $params);

        return $view->render();
    }

    /**
     * Step 2 - check if all system requirements are met
     *
     * @return string
     */
    public function step2()
    {
        $view = $this->init(2);

        $missingExtensions = [];
        foreach ($this->requiredExtensionsList as $extension) {
            if (!extension_loaded($extension)) {
                $missingExtensions[] = $extension;
            }
        }

        $availablePdoDrivers = [];
        if (extension_loaded('pdo')) {
            $availablePdoDrivers = \PDO::getAvailableDrivers();
        }
        $phpVersionOk = version_compare(PHP_VERSION, '5.5.0', '>=');
        $canInstall   = ($phpVersionOk && sizeof($missingExtensions) == 0 && sizeof($availablePdoDrivers) > 0);

        $view->assignMultiple(
            [
                'phpVersion'          => PHP_VERSION,
                'phpVersionOk'        => $phpVersionOk,
                'missingExtensions'   => $missingExtensions,
                'canInstall'          => $canInstall,
                'availablePdoDrivers' => $availablePdoDrivers
            ]
        );

        return $view->render();
    }

    /**
     * Step 3 - connect to an existing database or create a new one
     *
     * @param array $params
     *
     * @return string
     */
    public function step3($params = [])
    {
        $view = $this->init(3);

        // get all loaded PDO drivers
        $availablePdoDrivers = \PDO::getAvailableDrivers();

        $view->assignMultiple(
            [
                'availablePdoDrivers' => $availablePdoDrivers,
                'params'              => $params
            ]
        );

        return $view->render();
    }

    /**
     * Step 4 is an intermediary step that tests the database connection and redirects accordingly
     * @return string
     */
    public function step4()
    {
        $params = $_POST['params'];
        $params['error'] = '';

        $driver = $params['db_driver'];
        $host   = $params['db_host'];
        $pass   = $params['db_pass'];
        $user   = $params['db_user'];
        //$file   = $params['db_file'];

        try {
            // the DSN for sqlite points to a file, we will take care of it in step 5
            if ($driver != 'sqlite') {
                new \Pdo("$driver:host=$host", $user, $pass);
            }

            return $this->step5($params);
        } catch (\PDOException $e) {
            $params['error'] = $e->getMessage();

            return $this->step3($params);
        }
    }

    /**
     * Create or use an existing database/sqlite file
     *
     * @param array $params
     *
     * @return string
     */
    public function step5($params)
    {
        $view = $this->init(5);

        $driver = $params['db_driver'];
        $host   = $params['db_host'];
        $pass   = $params['db_pass'];
        $user   = $params['db_user'];

        $queryDatabases = [
            'mysql' => 'SELECT schema_name FROM information_schema.schemata WHERE ' .
                       'schema_name NOT IN (\'information_schema\',\'performance_schema\',\'mysql\')',
            'pgsql' => 'SELECT datname FROM pg_database'
        ];

        try {
            $databases = [];
            if ($driver != 'sqlite') {
                $dbh = new \Pdo("$driver:host=$host", $user, $pass);
                $dbs = $dbh->query($queryDatabases[ $driver ]);
                if ($dbs) {
                    while (($db = $dbs->fetchColumn(0)) !== false) {
                        $databases[] = $db;
                    }
                }
            }
        } catch (\PDOException $e) {
            $params['error'] = $e->getMessage();
            $this->step4($params);
        }

        $view->assignMultiple(
            [
                'databases' => $databases,
                'params'    => $params,
                'locales'   => Utility::helper('Locale')->locales(),
                'timezones' => Utility::helper('Locale')->timezones()
            ]
        );

        return $view->render();
    }

    /**
     * Create the database and the Extensions/configuration.php file
     *
     * @return string
     */
    public function step6()
    {
        $view = $this->init(6);

        $params = $_POST['params'];

        $driver = $params['db_driver'];
        $host   = $params['db_host'];
        $pass   = $params['db_pass'];
        $user   = $params['db_user'];
        $locale = $params['admin_locale'];
        $adminU = $params['admin_user'];
        $adminP = $params['admin_pass'];
        $zone   = $params['admin_timezone'];

        $existingDatabase = isset($_POST['db_name']) ? $_POST['db_name'] : '';
        $newDatabase      = isset($_POST['db_name_new']) ? $_POST['db_name_new'] : '';

        if (strlen(trim($newDatabase)) > 0) {
            try {
                $dbh = new \Pdo("$driver:host=$host", $user, $pass);
                $dbh->exec("CREATE DATABASE IF NOT EXISTS `$newDatabase`; USE `$newDatabase`;");
                $params['db_name'] = $newDatabase;
                $db = $newDatabase;
            } catch (\PDOException $e) {
                $params['error'] = $e->getMessage();

                return $this->step5($params);
            }
        } else {
            try {
                $dbh = new \Pdo("$driver:host=$host;dbname=$existingDatabase", $user, $pass);
                $dbh->exec("USE `$existingDatabase`");
                $params['db_name'] = $existingDatabase;
                $db = $existingDatabase;
            } catch (\PDOException $e) {
                $params['error'] = $e->getMessage();

                return $this->step5($params);
            }
        }

        $config = <<<'HER'
<?php

$config = [
    "Development" => [
        "Database" => [
            "Connection" => "$driver:host=$host;dbname=$db",
            "Username"   => "$user",
            "Password"   => "$pass"
        ],
        "System" => [
            "Locale" => "$locale",
            "Timezone" => "$zone",
            "Debug" => [
                "Enabled" => false
            ],
            "Minify"  => [
                "Html"       => false,
                "Css"        => false,
                "JavaScript" => false
            ],
            "Cache"  => [
                "Handler" => "Continut\\Core\\System\\Cache\\FileCache"
            ],
            "Logger" => [
                "Handler" => "Apix\\Log\\Logger\\File",
                "Target"  => __ROOTCMS__ . "/Var/Log/app_dev.log"
            ]
        ]
    ],
    "Production" => [
        "Database" => [
            "Connection" => "$driver:host=$host;dbname=$db",
            "Username"   => "$user",
            "Password"   => "$pass"
        ],
        "System" => [
            "Locale" => "$locale",
            "Timezone" => "$zone",
            "Debug" => [
                "Enabled" => false
            ],
            "Minify"  => [
                "Html"       => true,
                "Css"        => true,
                "JavaScript" => true
            ],
            "Cache"  => [
                "Handler" => "Continut\\Core\\System\\Cache\\FileCache"
            ],
            "Logger" => [
                "Handler" => "Apix\\Log\\Logger\\File",
                "Target"  => __ROOTCMS__ . "/Var/Log/app_prod.log"
            ]
        ]
    ]
];
HER;

        $config = str_replace(
            ['$driver', '$host', '$db', '$locale', '$zone', '$user', '$pass'],
            [$driver, $host, $db, $locale, $zone, $user, $pass],
            $config
        );

        // Create the Extensions/configuration.php file
        if (!file_exists(__ROOTCMS__ . DS . 'Extensions' . DS . 'configuration.php')) {
            $config = str_replace(array_keys($params), $params, $config);
            $f      = @fopen(__ROOTCMS__ . DS . 'Extensions' . DS . 'configuration.php', 'w');
            if (!$f) {
                $params['error'] = Utility::helper('Localization')
                    ->translate('setup.step5.error.configurationPermission');

                return $this->step5($params);
            }
            fwrite($f, $config);
            fclose($f);
        } else {
            $params['error'] = Utility::helper('Localization')->translate('setup.step5.error.configurationExists');

            return $this->step5($params);
        }

        // create all our starting tables
        $sql = file_get_contents('Sql\\' . $driver . '_clean.sql');
        $dbh->exec($sql);

        // create our admin user
        $backendUser = Utility::createInstance('\Continut\Core\System\Domain\Model\BackendUser');
        $backendUser
            ->setUsername($adminU)
            ->setPassword($adminP)
            ->setLanguage($locale)
            ->setTimezone($zone);
        Utility::createInstance('\Continut\Core\System\Domain\Collection\BackendUserCollection')
            ->add($backendUser)
            ->save();

        return $view->render();
    }
}
