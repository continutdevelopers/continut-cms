--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: sys_backend_usergroups; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_backend_usergroups (
    id integer NOT NULL,
    title character varying(255),
    access text,
    is_deleted smallint DEFAULT 0::smallint NOT NULL
);

--
-- Name: sys_backend_usergroups_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_backend_usergroups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_backend_usergroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_backend_usergroups_id_seq OWNED BY sys_backend_usergroups.id;


--
-- Name: sys_backend_users; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_backend_users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    is_deleted smallint DEFAULT 0::smallint NOT NULL,
    is_active smallint DEFAULT 0::smallint NOT NULL,
    usergroup_id integer NOT NULL,
    name character varying(255) NOT NULL,
    language character varying(100) NOT NULL,
    timezone character varying(100) NOT NULL,
    image character varying(100) NOT NULL,
    attributes text NOT NULL
);

--
-- Name: sys_backend_users_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_backend_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_backend_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_backend_users_id_seq OWNED BY sys_backend_users.id;


--
-- Name: sys_cache; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_cache (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    value text NOT NULL,
    key character varying(255) NOT NULL,
    expires_at integer NOT NULL,
    record_id integer NOT NULL
);

--
-- Name: sys_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_cache_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_cache_id_seq OWNED BY sys_cache.id;


--
-- Name: sys_configuration; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_configuration (
    id integer NOT NULL,
    domain_id integer NOT NULL,
    language_id integer NOT NULL,
    reference character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);

--
-- Name: sys_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_configuration_id_seq OWNED BY sys_configuration.id;


--
-- Name: sys_content; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_content (
    id integer NOT NULL,
    page_id integer NOT NULL,
    original_id integer DEFAULT 0 NOT NULL,
    domain_url_id integer NOT NULL,
    type character varying(100) NOT NULL,
    title character varying(255) NOT NULL,
    column_id integer NOT NULL,
    parent_id integer DEFAULT 0 NOT NULL,
    value text NOT NULL,
    reference_id integer NOT NULL,
    is_visible smallint DEFAULT 1::smallint NOT NULL,
    is_deleted smallint DEFAULT 0::smallint NOT NULL,
    created_at integer NOT NULL,
    modified_at integer NOT NULL,
    sorting integer DEFAULT 0 NOT NULL
);

--
-- Name: sys_content_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_content_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_content_id_seq OWNED BY sys_content.id;


--
-- Name: sys_domain_urls; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_domain_urls (
    id integer NOT NULL,
    parent_id integer NOT NULL,
    url character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    is_alias smallint DEFAULT 0::smallint NOT NULL,
    domain_id integer NOT NULL,
    flag character varying(2) NOT NULL,
    title character varying(255) NOT NULL,
    sorting integer DEFAULT 0 NOT NULL,
    locale character varying(20) NOT NULL,
    is_default smallint DEFAULT 0::smallint NOT NULL
);

--
-- Name: sys_domain_urls_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_domain_urls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_domain_urls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_domain_urls_id_seq OWNED BY sys_domain_urls.id;


--
-- Name: sys_domains; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_domains (
    id integer NOT NULL,
    titile character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    is_visible smallint DEFAULT 1::smallint NOT NULL,
    sorting integer NOT NULL
);

--
-- Name: sys_domains_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_domains_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_domains_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_domains_id_seq OWNED BY sys_domains.id;


--
-- Name: sys_file_mounts; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_file_mounts (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    folder character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    url character varying(255) NOT NULL
);

--
-- Name: sys_file_references; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_file_references (
    id integer NOT NULL,
    file_id integer NOT NULL,
    foreign_id integer NOT NULL,
    is_visible smallint DEFAULT 0::smallint NOT NULL,
    is_deleted smallint DEFAULT 0::smallint NOT NULL,
    tablename character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    alt character varying(255) NOT NULL,
    description text NOT NULL
);

--
-- Name: sys_file_references_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_file_references_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_file_references_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_file_references_id_seq OWNED BY sys_file_references.id;


--
-- Name: sys_files; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_files (
    id integer NOT NULL,
    fullpath character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    filesize integer NOT NULL,
    location character varying(255) NOT NULL,
    mime character varying(255) NOT NULL,
    created_at integer NOT NULL,
    modified_at integer NOT NULL,
    mount_id integer NOT NULL
);

--
-- Name: sys_files_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_files_id_seq OWNED BY sys_files.id;


--
-- Name: sys_frontend_usergroups; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_frontend_usergroups (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    access character varying(255) NOT NULL,
    is_deleted smallint DEFAULT 0::smallint NOT NULL
);

--
-- Name: sys_frontend_usergroups_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_frontend_usergroups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_frontend_usergroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_frontend_usergroups_id_seq OWNED BY sys_frontend_usergroups.id;


--
-- Name: sys_frontend_users; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_frontend_users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    usergroup_id integer NOT NULL,
    is_deleted integer DEFAULT 0 NOT NULL,
    is_active integer DEFAULT 1 NOT NULL,
    attributes text NOT NULL
);

--
-- Name: sys_frontend_users_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_frontend_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_frontend_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_frontend_users_id_seq OWNED BY sys_frontend_users.id;


--
-- Name: sys_history; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_history (
    id integer NOT NULL,
    record_id integer NOT NULL,
    record_table character varying(255) NOT NULL,
    modified_at integer NOT NULL,
    modified_by integer NOT NULL,
    data text NOT NULL
);

--
-- Name: sys_history_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_history_id_seq OWNED BY sys_history.id;


--
-- Name: sys_languages; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_languages (
    id integer NOT NULL,
    domain_id integer NOT NULL,
    language_iso3 character varying(3) NOT NULL,
    title character varying(255) NOT NULL,
    sorting integer DEFAULT 0 NOT NULL,
    flag character varying(2) NOT NULL
);

--
-- Name: sys_languages_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_languages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_languages_id_seq OWNED BY sys_languages.id;


--
-- Name: sys_notifications; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_notifications (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    message text NOT NULL,
    data text NOT NULL,
    link character varying(255) NOT NULL,
    "user" integer NOT NULL,
    created_at integer NOT NULL,
    is_read smallint DEFAULT 0::smallint NOT NULL
);

--
-- Name: sys_pages; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_pages (
    id integer NOT NULL,
    parent_id integer NOT NULL,
    title character varying(255) NOT NULL,
    original_id integer DEFAULT 0 NOT NULL,
    domain_url_id integer NOT NULL,
    is_in_menu smallint DEFAULT 1::smallint NOT NULL,
    is_visible smallint DEFAULT 0::smallint NOT NULL,
    is_deleted smallint DEFAULT 0::smallint NOT NULL,
    layout character varying(255) NOT NULL,
    is_layout_recursive smallint DEFAULT 0::smallint NOT NULL,
    frontend_layout character varying(255) NOT NULL,
    backend_layout character varying(255) NOT NULL,
    cached_path character varying(255) NOT NULL,
    sorting integer DEFAULT 0 NOT NULL,
    slug character varying(255) NOT NULL,
    meta_keywords character varying(255) NOT NULL,
    meta_description text NOT NULL,
    start_date integer NOT NULL,
    end_date integer NOT NULL,
    is_cachable smallint DEFAULT 0::smallint NOT NULL
);

--
-- Name: sys_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_pages_id_seq OWNED BY sys_pages.id;


--
-- Name: sys_routes; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_routes (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    data text NOT NULL
);

--
-- Name: sys_routes_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_routes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_routes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_routes_id_seq OWNED BY sys_routes.id;


--
-- Name: sys_user_sessions; Type: TABLE; Schema: public; Owner: vagrant; Tablespace:
--

CREATE TABLE sys_user_sessions (
    session_id integer NOT NULL,
    session_data text NOT NULL,
    session_expires integer DEFAULT 0 NOT NULL,
    user_id integer NOT NULL,
    user_type character varying(200) NOT NULL
);

--
-- Name: sys_user_sessions_session_id_seq; Type: SEQUENCE; Schema: public; Owner: vagrant
--

CREATE SEQUENCE sys_user_sessions_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: sys_user_sessions_session_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vagrant
--

ALTER SEQUENCE sys_user_sessions_session_id_seq OWNED BY sys_user_sessions.session_id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_backend_usergroups ALTER COLUMN id SET DEFAULT nextval('sys_backend_usergroups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_backend_users ALTER COLUMN id SET DEFAULT nextval('sys_backend_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_cache ALTER COLUMN id SET DEFAULT nextval('sys_cache_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_configuration ALTER COLUMN id SET DEFAULT nextval('sys_configuration_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_content ALTER COLUMN id SET DEFAULT nextval('sys_content_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_domain_urls ALTER COLUMN id SET DEFAULT nextval('sys_domain_urls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_domains ALTER COLUMN id SET DEFAULT nextval('sys_domains_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_file_references ALTER COLUMN id SET DEFAULT nextval('sys_file_references_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_files ALTER COLUMN id SET DEFAULT nextval('sys_files_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_frontend_usergroups ALTER COLUMN id SET DEFAULT nextval('sys_frontend_usergroups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_frontend_users ALTER COLUMN id SET DEFAULT nextval('sys_frontend_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_history ALTER COLUMN id SET DEFAULT nextval('sys_history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_languages ALTER COLUMN id SET DEFAULT nextval('sys_languages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_pages ALTER COLUMN id SET DEFAULT nextval('sys_pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_routes ALTER COLUMN id SET DEFAULT nextval('sys_routes_id_seq'::regclass);


--
-- Name: session_id; Type: DEFAULT; Schema: public; Owner: vagrant
--

ALTER TABLE ONLY sys_user_sessions ALTER COLUMN session_id SET DEFAULT nextval('sys_user_sessions_session_id_seq'::regclass);


--
-- Data for Name: sys_backend_usergroups; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_backend_usergroups (id, title, access, is_deleted) FROM stdin;
1	Administrators	\N	0
\.


--
-- Name: sys_backend_usergroups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_backend_usergroups_id_seq', 1, true);


--
-- Data for Name: sys_backend_users; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_backend_users (id, username, password, is_deleted, is_active, usergroup_id, name, language, timezone, image, attributes) FROM stdin;
\.


--
-- Name: sys_backend_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_backend_users_id_seq', 1, false);


--
-- Data for Name: sys_cache; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_cache (id, type, value, key, expires_at, record_id) FROM stdin;
\.


--
-- Name: sys_cache_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_cache_id_seq', 1, false);


--
-- Data for Name: sys_configuration; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_configuration (id, domain_id, language_id, reference, value) FROM stdin;
\.


--
-- Name: sys_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_configuration_id_seq', 1, false);


--
-- Data for Name: sys_content; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_content (id, page_id, original_id, domain_url_id, type, title, column_id, parent_id, value, reference_id, is_visible, is_deleted, created_at, modified_at, sorting) FROM stdin;
\.


--
-- Name: sys_content_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_content_id_seq', 1, false);


--
-- Data for Name: sys_domain_urls; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_domain_urls (id, parent_id, url, code, is_alias, domain_id, flag, title, sorting, locale, is_default) FROM stdin;
\.


--
-- Name: sys_domain_urls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_domain_urls_id_seq', 1, false);


--
-- Data for Name: sys_domains; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_domains (id, titile, url, is_visible, sorting) FROM stdin;
\.


--
-- Name: sys_domains_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_domains_id_seq', 1, false);


--
-- Data for Name: sys_file_mounts; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_file_mounts (id, title, folder, type, username, password, url) FROM stdin;
\.


--
-- Data for Name: sys_file_references; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_file_references (id, file_id, foreign_id, is_visible, is_deleted, tablename, title, alt, description) FROM stdin;
\.


--
-- Name: sys_file_references_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_file_references_id_seq', 1, false);


--
-- Data for Name: sys_files; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_files (id, fullpath, filename, filesize, location, mime, created_at, modified_at, mount_id) FROM stdin;
\.


--
-- Name: sys_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_files_id_seq', 1, false);


--
-- Data for Name: sys_frontend_usergroups; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_frontend_usergroups (id, title, access, is_deleted) FROM stdin;
\.


--
-- Name: sys_frontend_usergroups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_frontend_usergroups_id_seq', 1, false);


--
-- Data for Name: sys_frontend_users; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_frontend_users (id, name, username, password, usergroup_id, is_deleted, is_active, attributes) FROM stdin;
\.


--
-- Name: sys_frontend_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_frontend_users_id_seq', 1, false);


--
-- Data for Name: sys_history; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_history (id, record_id, record_table, modified_at, modified_by, data) FROM stdin;
\.


--
-- Name: sys_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_history_id_seq', 1, false);


--
-- Data for Name: sys_languages; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_languages (id, domain_id, language_iso3, title, sorting, flag) FROM stdin;
\.


--
-- Name: sys_languages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_languages_id_seq', 1, false);


--
-- Data for Name: sys_notifications; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_notifications (id, title, message, data, link, "user", created_at, is_read) FROM stdin;
\.


--
-- Data for Name: sys_pages; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_pages (id, parent_id, title, original_id, domain_url_id, is_in_menu, is_visible, is_deleted, layout, is_layout_recursive, frontend_layout, backend_layout, cached_path, sorting, slug, meta_keywords, meta_description, start_date, end_date, is_cachable) FROM stdin;
\.


--
-- Name: sys_pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_pages_id_seq', 1, false);


--
-- Data for Name: sys_routes; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_routes (id, name, path, data) FROM stdin;
\.


--
-- Name: sys_routes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_routes_id_seq', 1, false);


--
-- Data for Name: sys_user_sessions; Type: TABLE DATA; Schema: public; Owner: vagrant
--

COPY sys_user_sessions (session_id, session_data, session_expires, user_id, user_type) FROM stdin;
\.


--
-- Name: sys_user_sessions_session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vagrant
--

SELECT pg_catalog.setval('sys_user_sessions_session_id_seq', 1, false);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

