<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 15.05.2017 @ 18:00
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Log;

use Continut\Core\System\Tools\Exception;
use Continut\Core\System\Tools\HttpException;
use Continut\Core\Utility;

class BugSnatcher
{

    const ERROR_FILES_PATH = 'Public';

    /**
     * Register our bug snatch handler
     */
    public function register()
    {
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
        register_shutdown_function([$this, 'handleShutdown']);
    }

    /**
     * Exception handler method
     *
     * @param \Exception $exception The exception that was thrown
     */
    public function handleException($exception)
    {
        // General error template, when in production mode
        $errorTemplate = self::ERROR_FILES_PATH . DS . 'Error.html';

        // Http exceptions have custom html templates, based on the error code
        // They are all stored inside the Public folder
        // Not all HTTP error codes have html files created, so please add the ones you need as you see fit
        // For example, to customize 500 errors you'd create a file called 500.html inside the 'Public' folder
        if ($exception instanceof HttpException) {
            switch ($exception->getCode()) {
                default:
                    $code          = (int)$exception->getCode();
                    $errorTemplate = self::ERROR_FILES_PATH . DS . $code . '.html';
                    break;
            }
        }

        // In Production mode, we log any errors/exceptions and we do not show them in the frontend
        // For all the other environments, 'Test', 'Development' or your custom ones, we show the errors
        if (Utility::getApplicationEnvironment() == 'Production') {
            Utility::getLogger()->critical($exception);
            echo file_get_contents(__ROOTCMS__ . DS . $errorTemplate);
            die();
        } else {
            // in PHP 7 you can receive both an error or an exception in the exception handler function
            if ($exception instanceof \Error) {
                //Utility::debugData($exception->getMessage() . ' ' . $exception->getTraceAsString(), 'error');
                Utility::getLogger()->critical($exception);
            } else {
                Utility::debugData($exception, 'exception');
                Utility::getLogger()->error($exception);
            }
            echo $exception->getMessage();

            Utility::debugAjax();
        }
    }

    /**
     * Error handler method
     *
     * @param int    $errorNumber
     * @param string $errorMessage
     * @param string $errorFile
     * @param int    $errorLine
     *
     * @throws Exception
     */
    public function handleError($errorNumber, $errorMessage, $errorFile = '', $errorLine = 0)
    {
        $errorString = 'Error code: ' . $errorNumber . ' | ' . $errorMessage .
            ' in file ' . $errorFile . ' on line ' . $errorLine;
        Utility::getLogger()->error($errorString);
        if (Utility::getApplicationEnvironment() == 'Production') {
            echo file_get_contents(__ROOTCMS__ . DS . self::ERROR_FILES_PATH . DS . 'Error.html');
            die();
        } else {
            // push it to the DebugBar stack
            Utility::debugData($errorString, 'error');
            Utility::debugAjax();
        }
    }

    /**
     * Handle errors on shutdown
     */
    public function handleShutdown()
    {
        if ($error = error_get_last()) {
            if ($error['type'] === E_ERROR) {
                $this->handleError(1, $error['message'], $error['file'], $error['line']);
            }
        }
    }
}
