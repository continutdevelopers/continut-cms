<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 23.04.2015 @ 08:00
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Cache;

use Continut\Core\System\Tools\ErrorException;
use Continut\Core\Utility;
use Psr\SimpleCache\CacheInterface;

/**
 * Redis cache for Continut CMS
 *
 * @package Continut\Core\System\Cache
 */
class RedisCache extends RegularMethods implements CacheInterface, WildcardCacheInterface
{
    /**
     * @var int Default lifetime for each cache item, in seconds
     */
    protected $defaultTTL = 86400;

    /**
     * @var \Redis
     */
    protected $redisClient = null;

    /**
     * Initialize the Redis client
     *
     * @throws ErrorException
     */
    public function __construct()
    {
        $this->redisClient = new \Redis();
        $socket            = Utility::getConfiguration('System/Cache/Handler/Socket');
        if ($socket) {
            if (!$this->redisClient->connect($socket)) {
                throw new ErrorException('Cannot connect to Redis instance', 4002);
            }
        } else {
            $cacheHost = Utility::getConfiguration('System/Cache/Handler/Host');
            $cachePort = Utility::getConfiguration('System/Cache/Handler/Port');
            $host      = $cacheHost ? $cacheHost : '127.0.0.1';
            $port      = $cachePort ? $cachePort : '6379';
            $timeout   = (int)Utility::getConfiguration('System/Cache/Handler/Timeout');
            if (!$this->redisClient->connect($host, $port, $timeout)) {
                throw new ErrorException('Cannot connect to Redis instance', 4002);
            }
        }
        $password = Utility::getConfiguration('System/Cache/Handler/Password');
        if ($password) {
            if (!$this->redisClient->auth($password)) {
                throw new ErrorException('Cannot authenticate the Redis connection', 4003);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        $value = $this->getMultiple([$key])[ $key ];

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getMultiple($keys, $default = null)
    {
        foreach ($keys as $key) {
            $this->validateKey($key);
        }
        $foundItems = $this->redisClient->getMultiple($keys);
        // note that redis returns a regular array not an associative one, so you will need to remap your keys

        $indexedItems = [];
        foreach ($foundItems as $index => $value) {
            if ($value === false) {
                $foundItems[ $index ] = $default;
            } else {
                $foundItems[ $index ] = unserialize($value);
            }
            $indexedItems[ $keys[ $index ] ] = $foundItems[ $index ];
        }

        return $indexedItems;
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        $this->validateKey($key);

        return $this->redisClient->exists($key);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        return $this->redisClient->flushDb();
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        return $this->deleteMultiple([$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMultiple($keys)
    {
        array_walk($keys, [$this, 'validateKey']);

        // delete returns the number of keys that were actually deleted
        // we need to make sure that we deleted ALL of the keys we had in our array
        return ($this->redisClient->delete($keys) == sizeof($keys));
    }

    /**
     * {@inheritdoc}
     */
    public function deleteWithWildcard($wildcard)
    {
        // Redis "keys" method already supports wildcards, so we are going to use it instead of our own method
        $keysToDelete = $this->redisClient->keys($wildcard);

        return $this->deleteMultiple($keysToDelete);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value, $ttl = null)
    {
        return $this->setMultiple([$key => $value], $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function setMultiple($values, $ttl = null)
    {
        if (is_null($ttl)) {
            $ttl = $this->defaultTTL;
        }
        foreach ($values as $key => $value) {
            $this->validateKey($key);

            // if any of the keys is not saved, exit
            if (!$this->redisClient->setEx($key, $ttl, serialize($value))) {
                return false;
            }
        }

        return true;
    }
}
