<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 23.04.2015 @ 08:00
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Cache;

use Continut\Core\Utility;
use Psr\SimpleCache\CacheInterface;

/**
 * APCu cache for Continut CMS
 *
 * @package Continut\Core\System\Cache
 */
class ApcuCache extends RegularMethods implements CacheInterface, WildcardCacheInterface
{
    /**
     * @var int Default lifetime for each cache item, in seconds
     */
    protected $defaultTTL = 86400;

    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        return $this->getMultiple([$key], $default)[ $key ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMultiple($keys, $default = null)
    {
        $foundItems = [];
        foreach ($keys as $key) {
            $this->validateKey($key);
            $value = apcu_fetch($key);
            if ($value === false) {
                $value = $default;
            } else {
                $value = unserialize($value);
            }
            $foundItems[ $key ] = $value;
        }

        return $foundItems;
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        $this->validateKey($key);

        return apcu_exists($key);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        return apcu_clear_cache();
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        return $this->deleteMultiple([$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMultiple($keys)
    {
        array_walk($keys, [$this, 'validateKey']);

        // true on success or array with failed keys
        if (sizeof($keys) > 0) {
            $deleteStatus = apcu_delete($keys);
            if ($deleteStatus !== true) {
                // There seems to be a bug in APCu that it sometimes returns an empty array, even though it should
                // either return a boolean or an array with the keys that were not deleted, so we do this extra
                // check to make sure everything is in order
                if (is_array($deleteStatus) && sizeof($deleteStatus) > 0) {
                    Utility::getLogger()->alert('Could not remove the following keys: ' . implode(',', $deleteStatus));
                }
            }
        }

        // if the $keys array is empty, return true
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteWithWildcard($wildcard)
    {
        // APCu has PCRE support for it's iterator so we'll try to use it
        $keysToDelete = [];

        foreach (new \APCUIterator('/' . $wildcard . '/') as $cache) {
                $keysToDelete[] = $cache['key'];
        }

        return $this->deleteMultiple($keysToDelete);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value, $ttl = null)
    {
        return $this->setMultiple([$key => $value], $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function setMultiple($values, $ttl = null)
    {
        if (is_null($ttl)) {
            $ttl = $this->defaultTTL;
        }
        foreach ($values as $key => $value) {
            $this->validateKey($key);

            // if any of the keys is not saved, exit
            if (!apcu_store($key, serialize($value), $ttl)) {
                return false;
            }
        }

        return true;
    }
}
