<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 07.04.2015 @ 11:22
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Cache;

/**
 * Contains common used methods by all Cache Handler classes
 */
class RegularMethods
{
    /**
     * @param $key
     *
     * @return bool
     */
    protected function validateKey($key)
    {
        if (!is_string($key) || preg_match("#[{}()/\\\\@:]#", $key)) {
            throw new InvalidArgumentException('You have used an invalid cache key: ' . var_export($key, true));
        }

        return true;
    }

    /**
     * Matches wildcards of a subject based on a defined pattern
     *
     * eg: pattern "sys_pages_*" on a subject "sys_pages_9023" would return true
     *
     * @param string $pattern
     * @param string $subject
     *
     * @return bool
     */
    protected function wildcardMatch($pattern, $subject)
    {
        $pattern = '/^' . preg_quote($pattern) . '$/';
        $pattern = str_replace('\*', '.*', $pattern);
        if (!preg_match($pattern, $subject, $regs)) {
            return false;
        }

        return true;
    }
}
