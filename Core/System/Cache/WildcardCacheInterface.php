<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 10.08.2017 @ 08:00
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Cache;

/**
 * Simple addition to the PSR-16 CacheInterface to allow for wildcard based deletion
 *
 * Interface WildcardCacheInterface
 * @package ContinutCms\Core\System\Cache
 */
interface WildcardCacheInterface
{
    /**
     * Allows you to delete all the keys that contain a certain wildcard.
     * For ex:, if you want to delete all the cache keys starting with "sys_pages_" you would use the value
     * "sys_pages_*"
     *
     * @param string $wildcard Wildcard to use for filtering
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     */
    public function deleteWithWildcard($wildcard);
}
