<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 07.04.2015 @ 11:22
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\Cache;

use Psr\SimpleCache\CacheInterface;

/**
 * Simple file cache system implemented for Continut CMS
 * All cached content is stored inside the folder /Cache/Content/
 *
 * @package Continut\Core\System\Cache
 */
class FileCache extends RegularMethods implements CacheInterface, WildcardCacheInterface
{
    /**
     * Location where our cached file is to be stored
     */
    const CACHE_DIR = DS . 'Cache' . DS . 'Content' . DS;

    /**
     * File extension to be appended to cache filess
     */
    const FILE_EXTENSION = '.cache';

    /**
     * @var int Default lifetime for each cache item, in seconds
     */
    protected $defaultTTL = 86400;

    /**
     * List of cached items
     *
     * @var array
     */
    protected $items;

    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        return $this->getMultiple([$key], $default)[ $key ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMultiple($keys, $default = null)
    {
        $foundItems = [];

        foreach ($keys as $key) {
            $this->validateKey($key);
            if ($this->has($key)) {
                $foundItems[ $key ] = $this->items[ $key ];
            } else {
                $filename = $this->getCacheDir() . $key . self::FILE_EXTENSION;
                if (file_exists($filename)) {
                    $unpackedData = unserialize(file_get_contents($filename));
                    if (!is_array($unpackedData)) {
                        throw new CacheException('Cache packed data should be an array containing a value and a ttl.');
                    }
                    // cache not yet expired so we can return the cached value
                    if (isset($unpackedData['ttl']) && ($unpackedData['ttl'] > time())) {
                        $foundItems[ $key ] = $unpackedData['value'];
                    } else {
                        $foundItems = $default;
                    }
                } else {
                    $foundItems[ $key ] = $default;
                }
                $this->items[ $key ] = $foundItems[ $key ];
            }
        }

        return $foundItems;
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        $this->validateKey($key);

        return isset($this->items[ $key ]);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $removedAll = true;

        // get all the cache files generated in the cache folder and
        // remove them all
        $directoryIterator = new \DirectoryIterator($this->getCacheDir());
        foreach ($directoryIterator as $cacheFile) {
            // we want to keep our empty index.html untouched
            if ($cacheFile->isFile() && $cacheFile->getFilename() != 'index.html') {
                // if at least 1 of the cache files was not removed successfully, then
                // the clear() function should return false
                if (!unlink($cacheFile->getPathname())) {
                    $removedAll = false;
                }
            }
        }

        $this->items = [];

        return $removedAll;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        return $this->deleteMultiple([$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMultiple($keys)
    {
        array_walk($keys, [$this, 'validateKey']);

        foreach ($keys as $key) {
            unset($this->items[ $key ]);
            if (!unlink($this->getCacheDir() . DS . $key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteWithWildcard($wildcard)
    {
        $keysToDelete      = [];
        $directoryIterator = new \DirectoryIterator($this->getCacheDir());
        foreach ($directoryIterator as $cacheFile) {
            if ($cacheFile->isFile() && $cacheFile->getFilename() != 'index.html') {
                $key = $cacheFile->getFilename();
                if ($this->wildcardMatch($wildcard, $key)) {
                    $keysToDelete[] = $key;
                }
            }
        }

        return $this->deleteMultiple($keysToDelete);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value, $ttl = null)
    {
        return $this->setMultiple([$key => $value], $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function setMultiple($values, $ttl = null)
    {
        if (is_null($ttl)) {
            $ttl = $this->defaultTTL;
        }
        foreach ($values as $key => $value) {
            $this->validateKey($key);

            $filename = $this->getCacheDir() . $key;
            $packData = serialize(['ttl' => (time() + $ttl), 'value' => $value]);
            if (file_put_contents($filename . self::FILE_EXTENSION, $packData)) {
                $this->items[ $key ] = $value;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets the path to where the cache files are stored
     *
     * @return string
     */
    private function getCacheDir()
    {
        return __ROOTCMS__ . self::CACHE_DIR;
    }
}
