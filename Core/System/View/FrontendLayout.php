<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 06.04.2015 @ 19:28
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\View;

class FrontendLayout extends BaseLayout
{
    /**
     * @var \Continut\Core\System\Domain\Model\Page
     */
    protected $page;

    /**
     * @return \Continut\Core\System\Domain\Model\Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param \Continut\Core\System\Domain\Model\Page $page
     *
     * @return $this
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }
}
