<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 03.04.2015 @ 18:55
 * Project: Conţinut CMS
 */

namespace Continut\Core\System\View;

use Continut\Core\Utility;

class BaseLayout extends BaseView
{
    const DEBUGBAR_LOCATION = 'Lib/maximebf/debugbar/src/DebugBar/Resources';
    /**
     * Tree of elements to render by this layout
     *
     * @var mixed
     */
    protected $elements;

    /**
     * @var string Page title
     */
    protected $title = '';

    /**
     * @var string Class or classes to be applied to the body tag
     */
    protected $bodyClass;

    /**
     * @var array List of Css/JavaScript assets to include
     */
    protected $assets;

    /**
     * @return mixed
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param $elements
     *
     * @return $this
     */
    public function setElements($elements)
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * Render layout
     *
     * @return string
     */
    public function render()
    {
        $debugPath = str_replace(__ROOTCMS__, '', $this->getTemplate());
        Utility::debugData('Layout rendered ' . $debugPath, 'start');
        Utility::debugData('Layout used: ' . $debugPath, 'message');

        if (!is_file($this->template)) {
            Utility::debugData($this->__('backend.layout.noLayoutSpecified'), 'error');

            $content = $this->__('backend.layout.noLayoutSpecified');
        } else {
            ob_start();
            include_once $this->getTemplate();
            $content = ob_get_clean();
        }

        Utility::debugData('Layout rendered ' . $debugPath, 'stop');

        return $content;
    }

    /**
     * Show all content from a container
     *
     * @param int $id Id if the container to show
     *
     * @return string
     */
    public function showContainerColumn($id)
    {
        if (empty($this->elements)) {
            return '';
        }

        $htmlElements = '';

        Utility::debugData("showContainerColumn ($id) rendering", 'start');
        Utility::debugData('Rendering showContainerColumn(' . $id . ')', 'message');

        foreach ($this->getElements() as $element) {
            $element->setLayout($this);
            if ($element->getColumnId() == $id) {
                $htmlElements .= $element->render($element->children);
            }
        }

        Utility::debugData("Container ($id) rendering", 'stop');

        return $htmlElements;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        if (empty($this->title)) {
            return Utility::getApplicationName();
        }
        return $this->title;
    }

    /**
     * @param string $title Set page title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyClass()
    {
        return $this->bodyClass;
    }

    /**
     * @param string $bodyClass
     *
     * @return $this
     */
    public function setBodyClass($bodyClass)
    {
        $this->bodyClass = $bodyClass;

        return $this;
    }

    public function getUrl()
    {
        $url = $_SERVER['HTTP_HOST'];
        if (Utility::getApplicationScope() == Utility::SCOPE_FRONTEND) {
            $url = Utility::getSite()->getUrl();
        }

        return $url;
    }

    /**
     * Add Javascript asset
     *
     * @param $configuration
     *
     * @return $this
     */
    public function addJsAsset($configuration)
    {
        $this->assets[ 'JavaScript' ][ $configuration['identifier'] ] = $configuration;

        return $this;
    }

    /**
     * Add Css asset
     *
     * @param array $configuration Configuration array with asset details
     *
     * @return $this
     */
    public function addCssAsset($configuration)
    {
        $this->assets[ 'Css' ][ $configuration['identifier'] ] = $configuration;

        return $this;
    }

    /**
     * @return array
     */
    public function getAssets()
    {
        return $this->assets;
    }

    public function renderDebugbarContent()
    {
        $content = '';
        // if the debuger is enabled, show debug data
        if (Utility::getConfiguration('System/Debug/Enabled')) {
            $content = Utility::debug()
                ->getJavascriptRenderer()
                ->setBaseUrl(self::DEBUGBAR_LOCATION)
                ->setEnableJqueryNoConflict(true)
                ->render();
        }

        return $content;
    }

    public function renderDebugbarHeader()
    {
        $script = '';
        if (Utility::getConfiguration('System/Debug/Enabled')) {
            /*Utility::debug()
                ->getJavascriptRenderer()
                ->disableVendor('jquery');*/
            $script = Utility::debug()
                ->getJavascriptRenderer()
                ->setBaseUrl(self::DEBUGBAR_LOCATION)
                ->setEnableJqueryNoConflict(true)
                ->renderHead();
        }

        return $script;
    }

    /**
     * Return html code for the assets
     *
     * @return string
     */
    protected function renderAssets()
    {
        $header = '';
        foreach (['Css', 'JavaScript'] as $type) {
            if (!isset($this->assets[ $type ])) {
                continue;
            }

            // if we need to merge and minify the assets, do it now
            if (Utility::getConfiguration('System/Minify/' . $type)) {
                // require the minify library
                require_once __ROOTCMS__ . DS . 'Cache' . DS . 'Assets' . DS . 'lib.php';

                $assetFiles = [];
                foreach ($this->assets[ $type ] as $identifier => $configuration) {
                    // for the moment we do not merge and minify external resources as they might be using a CDN system
                    // @TODO: see if it makes sense to merge them in the final output
                    if (isset($configuration['external']) && $configuration['external'] == true) {
                        $header .= $this->assetFromType($configuration['file'], $type);
                    } else {
                        $assetFiles[] = Utility::getAssetPath(
                            $type . DS . $configuration['file'],
                            $configuration['extension']
                        );
                    }
                }

                if (!empty($assetFiles)) {
                    $staticUri   = DS . 'Cache' . DS . 'Assets';
                    $query       = 'f=' . implode(',', $assetFiles);
                    $requestType = ($type == 'JavaScript') ? 'js' : 'css';

                    $uri = \Minify\StaticService\build_uri($staticUri, $query, $requestType);

                    $header .= $this->assetFromType($uri, $type);
                }
            } else {
                foreach ($this->assets[ $type ] as $identifier => $configuration) {
                    // if it's an external css/js, just link it directly
                    if (isset($configuration['external']) && $configuration['external'] == true) {
                        $filePath = $configuration['file'];
                    } else {
                        $filePath = Utility::getAssetPath(
                            $type . '/' . $configuration['file'],
                            $configuration['extension']
                        );
                    }
                    $header .= $this->assetFromType($filePath, $type);
                }
            }
        }

        // empty assets array
        $this->assets = [];

        return $header;
    }

    protected function assetFromType($file, $type = 'Css')
    {
        if ($type == 'Css') {
            return "\t" . '<link rel="stylesheet" type="text/css" href="' . $file . '" />' . "\n";
        }
        if ($type == 'JavaScript') {
            return "\t" . '<script type="text/javascript" src="' . $file . '"></script>' . "\n";
        }
    }
}
