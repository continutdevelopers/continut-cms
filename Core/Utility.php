<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.04.2015 @ 22:10
 * Project: Conţinut CMS
 */

namespace Continut\Core;

use Continut\Core\System\Tools\ErrorException;
use Continut\Core\System\Tools\Exception;
use Continut\Core\System\Tools\HttpException;
use Continut\Core\System\Tools\DatabaseException;
use DebugBar\DataCollector\ConfigCollector;
use DebugBar\DataCollector\PDO\PDOCollector;
use DebugBar\DataCollector\PDO\TraceablePDO;
use DebugBar\DebugBarException;
use DebugBar\StandardDebugBar;
use Intervention\Image\ImageManager;

/**
 * Class Utility
 *
 * @package Continut\Core
 */
class Utility
{
    /**
     * @var array List of class mappings defined in extension configuration
     */
    public static $classMappings = [];

    /**
     * @var array Array storing all the extension's configuration
     */
    public static $extensionsConfiguration = [];

    /**
     * @var mixed
     */
    public static $autoloader;

    /**
     * @var \Continut\Core\System\Http\Request Request variable
     */
    protected static $request;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher The event dispatcher
     */
    protected static $dispatcher;

    /**
     * @var \PDO Database handler
     */
    protected static $databaseHandler = null;

    /**
     * @var \Psr\SimpleCache\CacheInterface
     */
    protected static $cacheHandler = null;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected static $loggerHandler = null;

    /**
     * @var \Continut\Core\System\Domain\Model\UserSession Current user session data
     */
    protected static $session = null;

    /**
     * @var array List of helpers loaded
     */
    protected static $helpers = [];

    /**
     * @var string Application scope, Frontend or Backend
     */
    protected static $applicationScope;

    /**
     * @var array List of enabled extensions
     */
    protected static $allowedExtensions;

    /**
     * @var \DebugBar\StandardDebugBar
     */
    protected static $debug;

    /**
     * @var array Configuration array
     */
    public static $configuration;

    /**
     * @var \Continut\Core\System\Domain\Model\Site
     */
    protected static $site;

    /**
     * @var \Intervention\Image\ImageManager ImageManager class
     */
    protected static $imageManager;

    /**
     * @var string Application environment, Development, Test or Production
     */
    protected static $applicationEnvironment;

    /**
     * @var \Continut\Core\System\Domain\Model\Page
     */
    protected static $currentPage;

    /**
     * @var \GuzzleHttp\Client
     */
    protected static $httpClient = null;

    // application running scope
    const SCOPE_BACKEND  = 'Backend';
    const SCOPE_FRONTEND = 'Frontend';

    /**
     * Generate an instance based on the sent class or map to an overwritten class stored in
     * the classMappings variable
     *
     * @param string $classToLoad Full namespace and class name to load
     * @param array  ...$params   Additional parameters to pass to the constructor of the class
     *
     * @throws \Continut\Core\System\Tools\Exception
     *
     * @return mixed
     */
    public static function createInstance($classToLoad, ...$params)
    {
        $class = $classToLoad;

        // @TODO : implement a class mapper so overwriting is easily done in the configuration.json
        if (array_key_exists($classToLoad, static::$classMappings)) {
            $class = static::$classMappings[ $classToLoad ];
        }

        if (class_exists($class)) {
            $classInstance = new $class(...$params);
        } else {
            throw new ErrorException('Class does not exist ' . $classToLoad);
        }

        return $classInstance;
    }

    /**
     * @return string
     */
    public static function getApplicationScope()
    {
        return static::$applicationScope;
    }

    /**
     * @return string
     */
    public static function getApplicationEnvironment()
    {
        return static::$applicationEnvironment;
    }

    /**
     * @param $applicationScope
     * @param $applicationEnvironment
     */
    public static function setApplicationScope($applicationScope, $applicationEnvironment)
    {
        static::$applicationScope       = $applicationScope;
        static::$applicationEnvironment = $applicationEnvironment;

        // load environment configuration
        $configuration = require_once __ROOTCMS__ . '/Extensions/configuration.php';

        // convert the multiarray to a 2d array
        // $config is defined inside configuration.php
        // @TODO: add a check to see if $configuration is defined or if it is valid

        $recursiveArray = new \RecursiveIteratorIterator(
            new \RecursiveArrayIterator($configuration[ $applicationEnvironment ])
        );
        $result         = [];
        // transform the multiarray as a flat "x/y/z" array
        foreach ($recursiveArray as $leaf) {
            $keys = [];
            foreach (range(0, $recursiveArray->getDepth()) as $depth) {
                $keys[] = $recursiveArray->getSubIterator($depth)->key();
            }
            $result[ join('/', $keys) ] = $leaf;
        }
        // store the basic configuration from the file. It will be later on merged with the other configuration
        // options defined in the database
        static::$configuration = $result;
        unset($configuration);

        // create the image manager
        static::$imageManager = new ImageManager();
        // and the http client handler
        static::$httpClient   = new \GuzzleHttp\Client();

        // Set multibyte encoding to utf-8 and use the mb_ functions for proper multilanguage handling
        // see: http://php.net/manual/en/ref.mbstring.php
        mb_internal_encoding('UTF-8');

        // load the list of the CMS extensions
        $extensions = require_once __ROOTCMS__ . '/Extensions/extensions.php';
        foreach ($extensions as $environment => $values) {
            // only enable the extensions configured for the current environment
            if ($environment === $applicationEnvironment) {
                static::$allowedExtensions = $values;
            }
        }
    }

    /**
     * @param string $path Return configuration path
     *
     * @return mixed|null
     */
    public static function getConfiguration($path)
    {
        if (isset(static::$configuration[ $path ])) {
            return static::$configuration[ $path ];
        }

        // @TODO: throw an exception
        return null;
    }

    /**
     * @param string $path
     * @param mixed  $value
     */
    public static function setConfiguration($path, $value)
    {
        static::$configuration[ $path ] = $value;
    }

    /**
     * Create a database handler and connect to the database
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public static function connectToDatabase()
    {
        try {
            $pdo = new \PDO(
                static::getConfiguration('Database/Connection'),
                static::getConfiguration('Database/Username'),
                static::getConfiguration('Database/Password'),
                [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']
            );
            // if debugging is enabled
            if (static::getConfiguration('System/Debug/Enabled')) {
                static::$databaseHandler = new TraceablePDO($pdo);
                static::debug()->addCollector(new PDOCollector(static::$databaseHandler));
            } else {
                static::$databaseHandler = $pdo;
            }
            static::$databaseHandler->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            throw new DatabaseException(
                'Cannot connect to the database. Please check username, password and host',
                20000001
            );
        }
    }

    /**
     * Disconnect from database
     */
    public static function disconnectFromDatabase()
    {
        static::$databaseHandler = null;
    }

    /**
     * @return \PDO
     */
    public static function getDatabase()
    {
        return static::$databaseHandler;
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcher
     */
    public static function getEventDispatcher()
    {
        if (static::$dispatcher === null) {
            static::$dispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
        }

        return static::$dispatcher;
    }

    /**
     * @param string $requestUri Current request uri
     *
     * @throws Exception
     */
    public static function setCurrentWebsite($requestUri = '')
    {
        if (Utility::getApplicationScope() == Utility::SCOPE_FRONTEND) {
            $urlParts = explode('/', $requestUri);
            $code     = (isset($urlParts[1])) ? $urlParts[1] : '';
            $url      = $_SERVER['SERVER_NAME'];
            // Get the first domain with this serverName and language code. Should only have 1 of each anyway
            $domainUrl = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                ->findByUrlAndCode($url, $code)
                ->getFirst();

            if ($domainUrl) {
                static::$site = Utility::createInstance('Continut\Core\System\Domain\Model\Site')
                    ->setDomainUrl($domainUrl);
            } else {
                throw new HttpException(
                    404,
                    'The domain you are currently trying to access is not configured inside the CMS application!'
                );
            }
        } else {
            $domainId   = static::getSession()->get('current_domain');
            $languageId = static::getSession()->get('current_language');
            if ($domainId && $languageId) {
                $domainUrl = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                    ->whereDomainAndLanguage($domainId, $languageId)
                    ->getFirst();

                static::$site = Utility::createInstance('Continut\Core\System\Domain\Model\Site')
                    ->setDomainUrl($domainUrl);
            }
        }
    }

    /**
     * @return \Continut\Core\System\Domain\Model\Site
     */
    public static function getSite()
    {
        return static::$site;
    }

    /**
     * @return System\Domain\Model\Page
     */
    public static function getCurrentPage()
    {
        return static::$currentPage;
    }

    /**
     * @param System\Domain\Model\Page $currentPage
     */
    public static function setCurrentPage($currentPage)
    {
        static::$currentPage = $currentPage;
    }

    /**
     * @return ImageManager
     */
    public static function getImageManager()
    {
        return static::$imageManager;
    }

    /**
     * Fetch an url using our httpClient and return it's content
     *
     * @param string $uri
     * @param string $method
     * @param array  $options
     *
     * @throws \Continut\Core\System\Tools\HttpException
     *
     * @return mixed
     */
    public static function getUrl($uri, $method = 'GET', $options = [])
    {
        // is it an external call? Then use our httpClient
        if (preg_match('/^(?:http|ftp)s?|s(?:ftp|cp):/', $uri)) {
            try {
                $response = static::$httpClient->request($method, $uri, $options);
            } catch (\GuzzleHttp\Exception\RequestException $exception) {
                 throw new HttpException($exception->getCode(), $exception->getMessage());
            }
            // @TODO: see if we also need to return the headers
            $content = $response->getBody();
        } else {
            $content = @file_get_contents($uri);
        }

        return $content;
    }

    /**
     * @return \Continut\Core\System\Http\Request instance
     * @throws \Continut\Core\System\Tools\Exception
     */
    public static function getRequest()
    {
        if (static::$request == null) {
            static::$request = static::createInstance('Continut\Core\System\Http\Request');
        }

        return static::$request;
    }

    /**
     * Return current user session
     *
     * @return System\Domain\Model\UserSession
     */
    public static function getSession()
    {
        return static::$session;
    }

    /**
     * @param System\Domain\Model\UserSession $session
     */
    public static function setSession($session)
    {
        static::$session = $session;
    }

    /**
     * Return subdirectories of a directory as an array
     *
     * @param string $path Parent directory to scan
     *
     * @return array
     */
    public static function getSubdirectories($path)
    {
        $directories = array_diff(scandir($path), ['.', '..']);
        $list        = [];

        foreach ($directories as $value) {
            if (is_dir($path . DS . $value)) {
                $list[ $path . DS . $value ] = $value;
            }
        }

        return $list;
    }

    /**
     * Loads all configuration files found in extensions residing in a certain folder
     *
     * @var string $path Absolute path in which to look for extensions configuration
     * @var string $type Type of extensions to load configuration for (System or Local)
     * @var bool   $filter Should we filter only on enabled extensions or load all that are present?
     *
     * @throws \Continut\Core\System\Tools\Exception
     *
     * @return array
     */
    public static function loadExtensionsConfigurationFromFolder($path, $type = 'Local', $filter = true)
    {
        $extensionFolders  = static::getSubdirectories($path . DS . $type);
        $enabledExtensions = isset(static::getExtensions()[$type]) ? static::getExtensions()[$type] : [];

        $data = [];

        foreach ($extensionFolders as $path => $folderName) {
            // only load enabled extensions
            if (!$filter || (in_array($folderName, $enabledExtensions) || $type === 'System')) {
                if (!file_exists($path . DS . 'configuration.json')) {
                    throw new ErrorException('configuration.json file not found for extension ' . $folderName);
                }
                // load localizations for the extension, if available
                static::helper('Localization')
                    ->loadLabelsFromPath(
                        static::getConfiguration('System/Locale'),
                        $path . DS . 'Resources' . DS . 'Private' . DS . static::$applicationScope . DS . 'Languages'
                    );
                // then load the extension configuration data
                $jsonData = json_decode(file_get_contents($path . DS . 'configuration.json'), true);
                if ($jsonData === null) {
                    throw new ErrorException(
                        $folderName . '/configuration .json file is empty or contains invalid json data. ' .
                        'Please check syntax or remove the file if it is empty. ' .
                        $folderName . ' (' . static::$applicationScope . ' scope)'
                    );
                } else {
                    $data = array_merge($data, $jsonData);
                }
            }
        }

        return $data;
    }

    /**
     * Return the complete configuration of an extension or the entire configuration of all extensions
     *
     * @param string $extensionName
     *
     * @return mixed/null
     */
    public static function getExtensionSettings($extensionName = '')
    {
        // if no extension name is passed, return the entire configuration data
        if ($extensionName == '') {
            return static::$extensionsConfiguration;
        }
        // else return only the configuration for this extension
        if (isset(static::$extensionsConfiguration[ $extensionName ])) {
            return static::$extensionsConfiguration[ $extensionName ];
        }

        return null;
    }

    /**
     * Execute a plugin method and return it's output as a string
     *
     * @param string $contextExtension  Name of the extension to look for
     * @param string $contextController Controller name to execute
     * @param string $contextAction     The action to execute
     * @param mixed  $contextSettings   Additional settings to be passed to the plugin
     *
     * @return mixed
     * @throws \Continut\Core\System\Tools\Exception
     */
    public static function callPlugin($contextExtension, $contextController, $contextAction, $contextSettings = [])
    {
        $controller = static::getController($contextExtension, $contextController, $contextAction, $contextSettings);

        $content = $controller->getRenderOutput();

        return $content;
    }

    /**
     * Call a controller and return it's reference
     *
     * @param string $contextExtension  Name of the extension to look for
     * @param string $contextController Controller name to execute
     * @param string $contextAction     The action to execute
     * @param mixed  $contextSettings   Additional settings to be passed to the plugin
     *
     * @return mixed
     * @throws \Continut\Core\System\Tools\Exception
     */
    public static function getController($contextExtension, $contextController, $contextAction, $contextSettings = [])
    {
        $templateAction     = $contextAction;
        $templateController = $contextController;
        $contextController  .= 'Controller';
        $contextAction      .= 'Action';

        $type        = static::getExtensionSettings($contextExtension)['type'];
        $classToLoad = "Continut\\Extensions\\$type\\$contextExtension\\Classes\\Controller\\$contextController";

        // Instantiate the controller
        $controller = Utility::createInstance($classToLoad);
        $controller
            ->setName($templateController)
            ->setAction($templateAction)
            ->setExtension($contextExtension);
        $controller->data = $contextSettings;

        // and call it's action method, if it exists
        if (!method_exists($controller, $contextAction)) {
            throw new HttpException(
                404,
                'The action you are trying to call does not exist for this controller'
            );
        }

        $contextScope = $controller->getScope();

        $controller
            ->getView()
            ->setTemplate(
                static::getResourcePath(
                    "$templateController/$templateAction",
                    $contextExtension,
                    $contextScope,
                    'Template'
                )
            );

        return $controller;
    }

    /**
     * @param string $resourceName  Name of the resource to load (template filename, container filename, etc)
     * @param string $extensionName Name of the extension that holds the resource
     * @param string $scope         Scope of the resource, either for the Backend or the Frontend
     * @param string $type          Type of resource (Template, Container, Partial, Layout, Helper) - in singular form
     *
     * @throws \Continut\Core\System\Tools\Exception
     *
     * @return string Absolute path to the resource to load
     */
    public static function getResourcePath($resourceName, $extensionName, $scope = 'Frontend', $type = 'Template')
    {
        $extensionType = static::getExtensionSettings($extensionName)['type'];
        $fileExtension = '.' . strtolower($type) . '.php';
        $type          = $type . 's';
        $resourcePath  = __ROOTCMS__ .
            "/Extensions/$extensionType/$extensionName/Resources/Private/$scope/$type/$resourceName$fileExtension";

        return $resourcePath;
    }

    /**
     * Return a Public asset, be it a CSS file, JS file or anything inside Resources/Public
     *
     * @param string $resourceName
     * @param string $contextExtension
     *
     * @return string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public static function getAssetPath($resourceName, $contextExtension)
    {
        $extensionType = static::getExtensionSettings($contextExtension)['type'];

        $resourcePath = "Extensions/$extensionType/$contextExtension/Resources/Public/$resourceName";

        return $resourcePath;
    }

    /**
     * Moves a key before another key
     *
     * @param array  $array
     * @param string $beforeKey
     * @param string $newKey
     * @param array  $newValue
     *
     * @return array
     */
    public static function arrayMoveBefore($array, $beforeKey, $newKey, $newValue)
    {
        if (array_key_exists($beforeKey, $array)) {
            unset($array[ $newKey ]);
            $temp = [];
            foreach ($array as $key => $value) {
                if ($key === $beforeKey) {
                    $temp[ $newKey ] = $newValue;
                }
                $temp[ $key ] = $value;
            }

            return $temp;
        }

        return $array;
    }

    /**
     * Moves a key after another key
     *
     * @param array  $array
     * @param string $afterKey
     * @param string $newKey
     * @param mixed  $newValue
     *
     * @return array
     */
    public static function arrayMoveAfter($array, $afterKey, $newKey, $newValue)
    {
        if (array_key_exists($afterKey, $array)) {
            unset($array[ $newKey ]);
            $temp = [];
            foreach ($array as $key => $value) {
                $temp[ $key ] = $value;
                if ($key === $afterKey) {
                    $temp[ $newKey ] = $newValue;
                }
            }

            return $temp;
        }

        return $array;
    }

    /**
     * Returns the current cache handler
     *
     * @return \Psr\SimpleCache\CacheInterface
     * @throws \Continut\Core\System\Tools\Exception
     */
    public static function getCache()
    {
        if (static::$cacheHandler === null) {
            $cacheHandlerClass = static::getConfiguration('System/Cache/Handler');
            if (empty($cacheHandlerClass)) {
                // if no cache handling class is defined, always fallback to the file cache, since it will work on
                // all systems, as long as write permissions are enabled in the /Cache folder
                $cacheHandlerClass = 'Continut\Core\System\Cache\FileCache';
            }
            static::$cacheHandler = static::createInstance($cacheHandlerClass);
        }

        return static::$cacheHandler;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public static function getLogger()
    {
        if (static::$loggerHandler === null) {
            $handler = static::getConfiguration('System/Logger/Handler');
            // How do we handle our logs?
            if ($handler == null) {
                $handler = 'Apix\Log\Logger\File';
            }
            // where do we store our logs?
            $target = static::getConfiguration('System/Logger/Target');
            if ($target == null) {
                $target = __ROOTCMS__ . '/Var/Log/app_prod.log';
            }
            static::$loggerHandler = static::createInstance(
                $handler,
                $target
            );
            static::$loggerHandler->setCascading(false)->setDeferred(false);
        }

        return static::$loggerHandler;
    }

    /**
     * Return a helper by name
     *
     * @param $helperName
     *
     * @return mixed Helpers class instance
     */
    public static function helper($helperName)
    {
        if (!isset(static::$helpers[ $helperName ])) {
            static::$helpers[ $helperName ] = static::createInstance('Continut\Core\System\Helper\\' . $helperName);
        }

        return static::$helpers[ $helperName ];
    }

    /**
     * List of extensions that are enabled/activated on this system
     *
     * @return array
     */
    public static function getExtensions()
    {
        return static::$allowedExtensions;
    }

    /**
     * Traverses all leaf nodes of this array and if they contain specific values, it pre-processes the data
     * and overwrites them
     *
     * @param array $record Record data, at least the 'id' and 'table'
     * @param array $data
     */
    public static function setFileReferences($record, &$data)
    {
        array_walk_recursive(
            $data,
            function (&$value, $record) {
                if (!((strpos($value, '_cc_file:') === false))) {
                    $files     = explode(',', $value);
                    $newValues = [];
                    foreach ($files as $file) {
                        if (strpos($file, '_cc_file:') === 0) {
                            $parts  = explode('_cc_file:', $file);
                            $fileId = $parts[1];

                            $fileReference = Utility::createInstance('Continut\Core\System\Domain\Model\FileReference');
                            $fileReference
                                ->setTablename($record['table'])
                                ->setFileId($fileId)
                                ->setIsVisible(true)
                                ->setIsDeleted(false)
                                ->setForeignId($record['id']);

                            $fileReferenceId =
                                Utility::createInstance(
                                    'Continut\Core\System\Domain\Collection\FileReferenceCollection'
                                )
                                    ->add($fileReference)
                                    ->save()
                                    ->getFirst()
                                    ->getId();
                            $newValues[]     = $fileReferenceId;
                        } else {
                            $newValues[] = $file;
                        }
                    }
                    $value = implode(',', $newValues);
                }
            },
            $record
        );
    }

    /**
     * Sets a debug message/value for PhpDebugbar, if debugging is enabled
     *
     * @param mixed  $name Name or data to collect
     * @param string $type Type of debug info to set
     */
    public static function debugData($name, $type)
    {
        if (static::getConfiguration('System/Debug/Enabled')) {
            try {
                switch ($type) {
                    case 'config':
                        // @TODO: Move this check into a filter or a custom Collector class
                        if (is_array($name)) {
                            // do not display the database connection parameters
                            $name['Database/Password']   = '*';
                            $name['Database/Username']   = '*';
                            $name['Database/Connection'] = '*';
                        }
                        static::debug()->addCollector(new ConfigCollector($name));
                        break;
                    case 'exception':
                        static::debug()['exceptions']->addException($name);
                        break;
                    case 'start':
                        static::debug()['time']->startMeasure($name);
                        break;
                    case 'stop':
                        static::debug()['time']->stopMeasure($name);
                        break;
                    case 'error':
                        static::debug()['messages']->error($name);
                        break;
                    default: // message
                        static::debug()['messages']->info($name);
                }
            } catch (DebugBarException $e) {
                //throw new Exception("Debugbar");
            }
        }
    }

    /**
     * Returns the debug object
     *
     * @return \DebugBar\StandardDebugBar
     */
    public static function debug()
    {
        if (!static::$debug) {
            static::$debug = new StandardDebugBar();
        }

        return static::$debug;
    }

    /**
     * Debug data for Ajax calls
     */
    public static function debugAjax()
    {
        if (static::$debug) {
            static::$debug->sendDataInHeaders();
        }
    }

    /**
     * Returns the list of configured layouts for the FE and BE
     *
     * @return array
     */
    public static function getLayouts()
    {
        $layouts = [];

        $settings = static::getExtensionSettings('');
        foreach ($settings as $extensionName => $params) {
            if (isset($params['ui']['layout'])) {
                foreach ($params['ui']['layout'] as $layoutId => $layoutData) {
                    $layouts[ $extensionName ][ $extensionName . '.' . $layoutId ] =
                        static::helper('Localization')->translate($layoutData['label']);
                }
            }
        }

        return $layouts;
    }

    /**
     * @param      $string
     * @param bool $capitalizeFirstCharacter
     *
     * @see http://stackoverflow.com/questions/2791998/convert-dashes-to-camelcase-in-php
     *
     * @return mixed
     */
    public static function toCamelCase($string, $capitalizeFirstCharacter = false)
    {

        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }

        return $str;
    }

    /**
     * Convert CamelCase to underscore notation (mySimpleTest => my_simple_test)
     *
     * @param string $input
     *
     * @return string
     */
    public static function toUnderscore($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('_', $ret);
    }

    /**
     * Recursive merge arrays while removing duplicate keys
     *
     * @param array $array1
     * @param array $array2
     *
     * @return array Merged array
     */
    public static function arrayMergeRecursiveUnique($array1, $array2)
    {
        if (empty($array1)) {
            return $array2;
        }

        foreach ($array2 as $key => $value) {
            if (is_array($value) && is_array(@$array1[ $key ])) {
                $value = self::arrayMergeRecursiveUnique($array1[ $key ], $value);
            }
            $array1[ $key ] = $value;
        }

        return $array1;
    }

    /**
     * Generate a slug from a string
     *
     * @param string $string Original string
     *
     * @return string Slug generated from original string
     */
    public static function generateSlug($string)
    {
        // needs the "intl" extension
        $string = transliterator_transliterate(
            'Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();',
            $string
        );

        return str_replace(' ', '-', $string);
    }

    /**
     * Returns a list of allowed HTTP verbs
     *
     * @return array
     */
    public static function validVerbs()
    {
        return [
            'get',
            'post',
            'put',
            'patch',
            'delete',
            'copy',
            'head',
            'options',
            'link',
            'unlink',
            'purge',
            'lock',
            'unlock',
            'propfind',
            'view'
        ];
    }

    /**
     * Minifies HTML code
     *
     * @param string $html
     *
     * @return string
     */
    public static function compressHtml($html)
    {
        return \Minify_HTML::minify($html);
    }

    /**
     * Current software version
     *
     * @return string
     */
    public static function getVersion()
    {
        return '1.0.0';
    }

    /**
     * Software name
     *
     * @return string
     */
    public static function getApplicationName()
    {
        return 'Conținut CMS';
    }
}
