<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 29.03.2015 @ 18:00
 * Project: Conţinut CMS
 */

namespace Continut\Core;

use Continut\Core\System\Tools\ErrorException;

/**
 * Main Class that bootstraps the system
 *
 * @package Continut\Core
 */
final class Bootstrap
{
    const COOKIE_NAME = 'ContinutCMS';

    /**
     * @var string Final content that gets rendered after all controllers/plugins/layouts/views are executed
     */
    protected $renderContent = '';

    /**
     * Set current running environment
     *
     * @param string $applicationScope
     * @param string $environment
     *
     * @return $this
     */
    public function setEnvironment($applicationScope, $environment)
    {
        require_once 'Utility.php';

        // Load the composer autoloader
        Utility::$autoloader = require __ROOTCMS__ . '/Lib/autoload.php';

        // BugSnatcher handles error/exception messages captured during the application's lifetime
        $bugSnatch = Utility::createInstance('Continut\Core\System\Log\BugSnatcher');
        $bugSnatch->register();

        Utility::setApplicationScope($applicationScope, $environment);
        Utility::debugData('Application', 'start');

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeWebsite()
    {
        $request = Utility::getRequest();
        $request->mapRouting();

        // on the frontend we use the locale that is configured inside the domain url, if one is specified of course
        if (Utility::getApplicationScope() == Utility::SCOPE_FRONTEND) {
            if (Utility::getSite()->getDomainUrl()->getLocale()) {
                Utility::setConfiguration('System/Locale', Utility::getSite()->getDomainUrl()->getLocale());
            }

            // get the configuration stored in the database
            $configurationCollection = Utility::createInstance(
                'Continut\Core\System\Domain\Collection\ConfigurationCollection'
            )
                ->whereDomainAndLanguage(
                    Utility::getSite()->getDomain()->getId(),
                    Utility::getSite()->getDomainUrl()->getId()
                );
            // and merge it with the configuration we have in configuration.php
            // this way database config can overwrite the configuration done in the file
            foreach ($configurationCollection->getAll() as $configuration) {
                Utility::setConfiguration($configuration->getReference(), $configuration->getValue());
            }
        } else {
            // @TODO: check if we need to merge the config for the backend too
            // and if so, get the value from the session called 'configurationSite'
        }
        Utility::debugData(Utility::$configuration, 'config');
        setlocale(LC_ALL, Utility::getConfiguration('System/Locale'));
        //var_dump(Utility::$configuration);

        return $this;
    }

    /**
     * Loads all the core configurations, like the class mapper, etc
     *
     * @return $this
     */
    public function loadExtensionsConfiguration()
    {
        // Load Local and System extensions configuration data
        Utility::debugData('Loading extensions configuration', 'start');

        $cacheKey = '_cms_configuration_backend_' . Utility::getConfiguration('System/Locale');
        if (Utility::getApplicationScope() == Utility::SCOPE_FRONTEND) {
            $cacheKey = '_cms_configuration_' . Utility::getSite()->getDomainUrl()->getId();
        }

        $cachedConfiguration = Utility::getCache()->get($cacheKey);
        if ($cachedConfiguration) {
            Utility::$extensionsConfiguration = $cachedConfiguration['configuration'];
            Utility::helper('Localization')->setTranslationLabels($cachedConfiguration['labels']);
        } else {
            Utility::$extensionsConfiguration = array_merge(
                Utility::loadExtensionsConfigurationFromFolder(__ROOTCMS__ . DS . 'Extensions', 'Local'),
                Utility::loadExtensionsConfigurationFromFolder(__ROOTCMS__ . DS . 'Extensions', 'Community'),
                Utility::loadExtensionsConfigurationFromFolder(__ROOTCMS__ . DS . 'Extensions', 'System')
            );
            Utility::getCache()->set(
                $cacheKey,
                [
                    'configuration' => Utility::getExtensionSettings(),
                    'labels'        => Utility::helper('Localization')->getTranslationLabels()
                ]
            );
        }
        Utility::debugData('Loading extensions configuration', 'stop');

        return $this;
    }

    /**
     * Call the current parsed Frontend controller and action using the extension's context
     *
     * @return $this
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function connectFrontendController()
    {
        Utility::debugData('Main frontend controller called', 'start');

        $request = Utility::getRequest();
        // Get request argument values or switch to default values if not defined
        $requestType       = $request->getArgument('_type', '');
        $contextExtension  = $request->getArgument('_extension', 'Frontend');
        $contextController = $request->getArgument('_controller', 'Page');
        $contextAction     = $request->getArgument('_action', 'index');
        // for API calls, the controllers are stored inside an "Controllers/Api" subfolder
        if ($requestType === 'api') {
            $contextController = 'Api\\' . $contextController;
            $httpVerb          = mb_strtolower($_SERVER['REQUEST_METHOD']);
            $contextAction     = (in_array($httpVerb, Utility::validVerbs()) ? $httpVerb : 'get') . $contextAction;
        }

        $this->renderContent = Utility::callPlugin($contextExtension, $contextController, $contextAction);

        return $this;
    }

    /**
     * Call the current parsed Backend controller and action
     *
     * @return $this
     *
     * @throws \Continut\Core\System\Tools\ErrorException
     */
    public function connectBackendController()
    {
        Utility::debugData('Main backend controller called', 'start');

        //try {
            $request = Utility::getRequest();

            // Get request argument values or switch to default values if not defined
            $contextExtension  = $request->getArgument('_extension', 'Backend');
            $contextController = $request->getArgument('_controller', 'Index');
            $contextAction     = $request->getArgument('_action', 'dashboard');

            $controller = Utility::getController($contextExtension, $contextController, $contextAction);

            if (!$controller->getUseLayout() || Utility::getRequest()->isAjax()) {
                $this->renderContent = $controller->getRenderOutput();
                // for ajax requests use the Ajax debugger, if it is enabled
                if (Utility::getRequest()->isAjax()) {
                    Utility::debugAjax();
                }
            } else {
                // If it's not an AJAX request, load layout then render it
                // otherwise just return directly the response
                /* @var \Continut\Core\System\View\BackendLayout $layout */
                $layout = Utility::createInstance('Continut\Core\System\View\BackendLayout');
                $layout->setTemplate('/Extensions/System/Backend/Resources/Private/Backend/Layouts/Default.layout.php');

                // if the layout template is overwritten at the controller level, update it
                if ($controller->getLayoutTemplate()) {
                    $layout->setTemplate($controller->getLayoutTemplate());
                }
                $layout->setContent($controller->getRenderOutput());

                $this->renderContent = $layout->render();
            }
        /*} catch (\Exception $e) {
            throw new ErrorException(
                'Could not finalise execution of the backend controller, reason => ' . $e->getMessage() .
                ' in file ' . $e->getFile()
            );
        }*/

        return $this;
    }

    /**
     * Create a database handler and connect to the database
     *
     * @return $this
     */
    public function connectToDatabase()
    {
        Utility::connectToDatabase();

        return $this;
    }

    /**
     * Disconnect from database
     *
     * @return $this
     */
    public function disconnectFromDatabase()
    {
        Utility::disconnectFromDatabase();

        return $this;
    }

    /**
     * Start User session
     *
     * @return $this
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function startSession()
    {
        // Create our session handler
        $userSession = Utility::createInstance('Continut\Core\System\Domain\Model\UserSession');
        session_name(self::COOKIE_NAME . '_' . Utility::getApplicationScope());
        session_set_save_handler($userSession, true);
        session_save_path(__ROOTCMS__ . DS . 'Tmp' . DS . 'Session');
        session_start();

        // @TODO this only checks Backend user sessions. We should do one for frontend users too, especially after they
        // switch the language, on multilanguage websites
        if ($userSession->get('user_id')) {
            // load details of existing user
            $userId = (int)$userSession->get('user_id');
            $user   = Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserCollection')
                ->findById($userId);
            // or create a new empty session
            if (!$user) {
                $user = Utility::createInstance('Continut\Core\System\Domain\Model\BackendUser');
            }
            $userSession->setUser($user);
            if ($user->getLanguage()) {
                Utility::setConfiguration('System/Locale', $user->getLanguage());
            }
        }

        Utility::setSession($userSession);

        return $this;
    }

    /**
     * Dumps the rendered content to screen
     */
    public function render()
    {
        echo $this->renderContent;
    }
}
