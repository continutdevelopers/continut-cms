<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <base href="http://<?= $url ?>">
    <link rel="shortcut icon" href="/Public/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/Public/favicon.ico" type="image/x-icon">
    <title><?= $pageTitle ?></title>

    <?= $pageHead ?>

    <meta name="description" content=<?= json_encode($this->getPageModel()->getMetaDescription()) ?>/>
    <meta name="keywords" content=<?= json_encode($this->getPageModel()->getMetaKeywords()) ?>/>

</head>
<body>
<?= $pageContent ?>
<?= $pageFooter ?>
</body>
</html>