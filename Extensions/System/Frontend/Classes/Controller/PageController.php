<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 04.04.2015 @ 13:16
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Frontend\Classes\Controller;

use Continut\Core\System\Controller\FrontendController;
use Continut\Core\System\Tools\HttpException;
use Continut\Core\Utility;

class PageController extends FrontendController
{
    public function indexAction()
    {
        //Utility::debugData('page_rendering', 'start', 'Page rendering');
        // get page id request
        $pageId = (int)$this->getRequest()->getArgument('id');
        // or slug, whichever is sent
        $pageSlug = $this->getRequest()->getArgument('slug');

        // Load the page model from the database, by id or slug
        $pageModel = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendPageCollection'
        )->findWithIdOrSlug($pageId, $pageSlug);

        if (!$pageModel) {
            // throw a 404 notFound page
            throw new HttpException(404, $this->__('exception.page.notFound'));
        }
        Utility::setCurrentPage($pageModel);

        // should this page be cached? If so, fetch it's cached data, if it is available
        if ($pageModel->getIsCachable()) {
            $cache = Utility::getCache()->get($pageModel->getCacheKey());
            // display the cached version only if one is found
            // otherwise continue the execution and store the new cached version at the end of the call
            if ($cache) {
                return $cache;
            }
        }
        $pageModel->mergeOriginal();

        // load the frontend layout
        $layout = Utility::createInstance('Continut\Core\System\View\FrontendLayout');

        // get all elements from the database that belong to this page and are not hidden or deleted
        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendContentCollection'
        );
        $contentCollection->findVisibleForPage($pageModel->getId());

        $contentTree = $contentCollection->buildTree();

        $layout
            ->setPage($pageModel)
            ->setTemplate(__ROOTCMS__ . $pageModel->getFrontendLayout())
            ->setElements($contentTree)
            ->setTitle($pageModel->getTitle());

        // dump it all on screen
        $content = $layout->render();
        if (Utility::getConfiguration('System/Minify/Html')) {
            $content = Utility::compressHtml($content);
        }

        if ($pageModel->getIsCachable()) {
            // the html minifier does use a bit of resources, so we only use it so far
            // on content that will be stored in the cache, leaving it untouched for dynamic pages
            // @TODO: check to see if it makes sense+*- for non cached pages too
            Utility::getCache()->set($pageModel->getCacheKey(), $content);
        }

        //Utility::debugData('page_rendering', 'stop');
        return $content;
    }
}
