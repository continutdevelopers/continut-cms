<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 04.04.2015 @ 13:16
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Frontend\Classes\Controller\Api;

use Continut\Core\System\Controller\FrontendController;
use Continut\Core\Utility;

class PageController extends FrontendController
{
    /**
     * Fetch a page details (by id or slug)
     * eg: /api/frontend/page/id/100 or /api/frontend/page/slug/my-sample-slug
     *
     * HTTP Verb : get
     *
     * @return string
     */
    public function getPageAction()
    {
        $pageId = (int)$this->getRequest()->getArgument('id');
        // or slug, whichever is sent
        $pageSlug = $this->getRequest()->getArgument('slug');
        // should we also return the content elements for this page?
        $includeContent = $this->getRequest()->getArgument('include_content', 0);

        $pageModel = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendPageCollection'
        )->findWithIdOrSlug($pageId, $pageSlug);

        if ($pageModel) {
            if ($includeContent) {
                $contentCollection = Utility::createInstance(
                    'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendContentCollection'
                );
                $contentCollection->findVisibleForPage($pageModel->getId());

                $content = $contentCollection->buildJsonTree();

                return $this->json(['page' => $pageModel->dataMapper(), 'content' => $content]);
            }

            return $this->json($pageModel->dataMapper());
        } else {
            return $this->json(['error' => 'missing']);
        }
    }

    /**
     * Fetch all pages of the current domain
     * eg: /api/frontend/pages/
     *
     * HTTP Verb : get
     *
     * @return string
     */
    public function getPagesAction()
    {
        $languageId = Utility::getSite()->getDomainUrl()->getId();

        $pageCollection = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendPageCollection'
        )->whereLanguage($languageId);

        return $this->json($pageCollection->buildJsonTree());
    }
}
