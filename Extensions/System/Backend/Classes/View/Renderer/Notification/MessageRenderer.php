<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 03.01.2016 @ 15:00
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\View\Renderer\Notification;

use Continut\Core\Utility;
use Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer;

class MessageRenderer extends BaseRenderer
{
    /**
     * NotificationRenderer constructor
     */
    public function __construct()
    {
        $this->setTemplate(Utility::getResourcePath('Renderer/notification', 'Backend', 'Backend', 'Template'));
    }
}
