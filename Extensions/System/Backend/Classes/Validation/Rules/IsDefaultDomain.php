<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 25.06.2017 @ 16:00
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Validation\Rules;

use Continut\Core\Utility;
use Respect\Validation\Exceptions\ComponentException;
use Respect\Validation\Rules\AbstractRule;
use Respect\Validation\Rules\Numeric;

class IsDefaultDomain extends AbstractRule
{
    public $domainId;

    public $domainUrlId;

    /**
     * IsDefaultDomain constructor.
     *
     * @param int $domainUrlId
     * @param int $domainId
     *
     * @throws ComponentException
     */
    public function __construct($domainUrlId, $domainId)
    {
        $this->domainId    = $domainId;
        $this->domainUrlId = $domainUrlId;
        $paramValidator    = new Numeric();
        if (!$paramValidator->validate($domainId)) {
            throw new ComponentException(
                sprintf('Domain %s must be a number', $domainId)
            );
        }
        if ($domainUrlId != null && !$paramValidator->validate($domainUrlId)) {
            throw new ComponentException(
                sprintf('DomainUrl %s must be a number', $domainUrlId)
            );
        }
    }

    /**
     * Execute the validation rule
     *
     * @param $input
     *
     * @return bool
     */
    public function validate($input)
    {
        $input = (int)$input;
        if ($input === 0) {
            return true;
        }

        // Get the current number of default languages setup for this domain_id
        // The number should either be 0 or 1, as we cannot have more than 1 default language per domain
        // We can have 0 though, if no default language was already set up
        $languageCollection   = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection');
        $noOfDefaultLanguages = $languageCollection->countExistingDefault($this->domainId);
        $checkedLanguage      = $languageCollection->getFirst();

        return (
            ($noOfDefaultLanguages === 0) ||
            (($checkedLanguage->getId() == $this->domainUrlId) && $noOfDefaultLanguages === 1)
        );
    }
}
