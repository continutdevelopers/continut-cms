<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 25.06.2017 @ 16:00
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Validation\Exceptions;

use \Respect\Validation\Exceptions\ValidationException;

class IsDefaultDomainException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT  => [
            self::STANDARD => 'Each domain can only have 1 default language. ' .
                'Make sure you unset the existing default language before setting a new one',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'Validation message if the negative of MyRule is called and fails validation.',
        ],
    ];
}
