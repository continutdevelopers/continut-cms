<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 11.04.2015 @ 17:50
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Domain\Model;

use Continut\Core\System\Domain\Model\Content;
use Continut\Core\Utility;

class BackendContent extends Content
{
    /**
     * @var bool if rendered from inside a reference, certain menu elements are disabled
     */
    protected $fromReference = false;

    /**
     * Outputs "regular" content, of type "content" in the database
     *
     * @param mixed $elements
     *
     * @return string
     */
    public function render($elements = null)
    {
        $title = $this->getContentTitle();

        $configuration = json_decode($this->getValue(), true);
        $variables     = $configuration['content']['data'];
        $view          = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath(
            $configuration['content']['template'],
            $configuration['content']['extension'],
            'Backend',
            'Content'
        ));
        if (is_array($variables)) {
            $view->assignMultiple($variables);
        }
        $view = parent::prepareView($view);

        $value = $view->render();

        return $this->formatBlock('content', $title, $value);
    }

    /**
     * Returns the title of a content element or a dummy text, if no title is defined
     *
     * @return string
     */
    protected function getContentTitle()
    {
        $title = $this->getTitle();

        if ($title == '') {
            $title = Utility::helper('Localization')->translate('backend.content.noTitle');
        }

        return $title;
    }

    /**
     * Renders the backend editable part of a content element
     *
     * @param string $type          The type of content element we're formating
     * @param string $title         The title of the content element, if any
     * @param string $content       The content of the element
     * @param bool   $fromReference Rendered from inside a reference?
     *
     * @return string
     */
    protected function formatBlock($type, $title, $content)
    {
        $view = Utility::createInstance('Continut\Core\System\View\BaseView');
        $view->setTemplate(Utility::getResourcePath(
            'BackendContent',
            'Backend',
            'Backend',
            'Wrapper'
        ));
        $view->assignMultiple(
            [
                'id'            => $this->getId(),
                'columnId'      => $this->getColumnId(),
                'type'          => $type,
                'visible'       => $this->getIsVisible(),
                'fromReference' => $this->getFromReference(),
                'title'         => $title,
                'content'       => $content,
                'pageId'        => Utility::getRequest()->getArgument('id'),
                'canPaste'      => Utility::getSession()->has('backend.content')
            ]
        );

        return $view->render();
    }

    /**
     * @return bool
     */
    public function getFromReference()
    {
        return $this->fromReference;
    }

    /**
     * @param bool $fromReference
     *
     * @return $this;
     */
    public function setFromReference($fromReference)
    {
        $this->fromReference = $fromReference;

        return $this;
    }
}
