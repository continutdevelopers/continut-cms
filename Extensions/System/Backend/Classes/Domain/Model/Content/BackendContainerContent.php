<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.08.2015 @ 19:48
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Domain\Model\Content;

use Continut\Core\Utility;
use Continut\Extensions\System\Backend\Classes\Domain\Model\BackendContent;

class BackendContainerContent extends BackendContent
{
    /**
     * Outputs "container" content
     *
     * @param mixed $elements Chidren elements to render
     *
     * @return mixed|string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function render($elements = null)
    {
        $title = $this->getContentTitle();

        $configuration = json_decode($this->getValue(), true);
        $variables     = $configuration['container']['data'];
        $container     = Utility::createInstance('Continut\Core\System\View\BackendContainerView');
        $container->setId($this->getId());
        foreach ($elements as $element) {
            $element->setFromReference($this->getFromReference());
        }
        $container->setElements($elements);
        $container->setTemplate(
            Utility::getResourcePath(
                $configuration['container']['template'],
                $configuration['container']['extension'],
                'Backend',
                'Container'
            )
        );
        if (is_array($variables)) {
            $container->assignMultiple($variables);
        }
        $container = parent::prepareView($container);
        $value = $container->render();

        return $this->formatBlock('container', $title, $value);
    }
}
