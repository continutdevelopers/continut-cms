<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.12.2015 @ 14:46
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class FrontendUsersController extends BackendController
{
    /**
     * Frontend users management
     */
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $grid
            ->setFormAction(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'FrontendUsers', '_action' => 'grid'])
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserCollection'))
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'id'          => [
                        'label'    => 'backend.users.grid.field.id',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ]
                    ],
                    'username'    => [
                        'label'    => 'backend.users.grid.field.username',
                        'css'      => 'col-sm-4',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'usergroupId' => [
                        'label'    => 'backend.users.grid.field.usergroupId',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer',
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'isDeleted'   => [
                        'label'    => 'backend.users.grid.field.isDeleted',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer',
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'isActive'    => [
                        'label'    => 'backend.users.grid.field.isActive',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer',
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'actions'     => [
                        'label'    => 'backend.users.grid.field.actions',
                        'css'      => 'col-sm-2 text-right flip',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\System\Backend\Classes\View\Renderer\ActionsRenderer',
                            'parameters' => [
                                'showEdit'   => true,
                                'showDelete' => true,
                                'extension'  => 'Backend',
                                'controller' => 'FrontendUsers'
                            ]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * Edit frontend user action
     */
    public function editAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $user = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserCollection')
            ->findById($id);
        $frontendGroups = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserGroupCollection')
            ->findAll();

        $this->getView()->assignMultiple([
            'user'   => $user,
            'groups' => $frontendGroups->toSelect('id', 'title')
        ]);
    }

    /**
     * New frontend user action
     */
    public function createAction()
    {
        $user           = Utility::createInstance('Continut\Core\System\Domain\Model\FrontendUser');
        $frontendGroups = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserGroupCollection')
            ->findAll();

        $this->getView()->assignMultiple([
            'user'   => $user,
            'groups' => $frontendGroups->toSelect('id', 'title')
        ]);
    }

    /**
     * Save group action
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        $userCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserCollection');

        if ($id == 0) {
            $user = Utility::createInstance('Continut\Core\System\Domain\Model\FrontendUser');
        } else {
            $user = $userCollection->findById($id);
        }
        // if no password is specified it means we do not need to change it
        if (empty($data['password'])) {
            $data['password'] = $user->getPassword();
        }
        $user->update($data);

        if ($user->validate()) {
            $userCollection
                ->reset()
                ->add($user)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('general.flash.recordSaved'));

            $this->redirect(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'FrontendUsers', '_action' => 'grid'])
            );
        }

        $this->getView()->assign('user', $user);
    }
}
