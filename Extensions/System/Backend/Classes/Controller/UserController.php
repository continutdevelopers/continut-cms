<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.12.2015 @ 14:46
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class UserController extends BackendController
{
    /**
     * Show the user's profile form/page
     */
    public function profileAction()
    {
        $backendUsergroups = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\BackendUserGroupCollection'
        )->findAll();

        $this->getView()->assign('groups', $backendUsergroups);
    }

    /**
     * Save the updated user's profile
     */
    public function saveProfileAction()
    {
        $currentUser = $this->getUser();

        $data = $this->getRequest()->getArgument('data');

        Utility::setFileReferences(['id' => $currentUser->getId(), 'table' => 'sys_backend_users'], $data);
        $currentUser
            ->update($data)
            ->save();

        Utility::getSession()->addFlashMessage($this->__('general.flash.recordSaved'));

        $this->redirect(Utility::helper('Url')->linkToPath('admin', ['_controller' => 'User', '_action' => 'profile']));
    }

    /**
     * Hides a certain section from being displayed in the admin pages
     */
    public function hideSectionAction()
    {
        $section = $this->getRequest()->getArgument('section');
        // Save the value in the user's session
        Utility::getSession()
            ->getUser()
            ->setAttribute('showSection.' . $section, '0')
            ->save();

        return $this->json(['success' => 1, 'message' => 'Div hidden']);
    }
}
