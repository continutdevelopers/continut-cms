<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 19.04.2015 @ 21:25
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\System\Tools\ErrorException;
use Continut\Core\Utility;

class ContentController extends BackendController
{
    /**
     * Shows the "add content element" wizard
     */
    public function wizardAction()
    {
        // element id, if it will be added inside another container
        $id            = (int)$this->getRequest()->getArgument('id');
        // the pid of the new element
        $pageId        = (int)$this->getRequest()->getArgument('page_id');
        // column into which it will be added
        $columnId      = (int)$this->getRequest()->getArgument('column_id');
        $placement     = $this->getRequest()->getArgument('placement', 'inside');
        $configuration = Utility::getExtensionSettings();

        $types      = [];
        $extensions = [];
        foreach ($configuration as $extensionName => $values) {
            if (isset($values['elements'])) {
                foreach ($values['elements'] as $type => $elementValues) {
                    $types[ $type ][]             = ['extension' => $extensionName, 'configuration' => $elementValues];
                    $extensions[ $extensionName ] = $extensionName;
                }
            }
        }

        // Load the page model, so that we can retrieve it's current layout, theme, etc
        $page = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
            ->findById($pageId);

        $this->getView()->assignMultiple(
            [
                'types'      => $types,
                'extensions' => $extensions,
                'id'         => $id,
                'pageId'     => $pageId,
                'page'       => $page,
                'columnId'   => $columnId,
                'placement'  => $placement
            ]
        );
    }

    /**
     * "Cut" operation for content elements
     */
    public function cutAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');
        $this->getSession()->set('backend.content', ['operation' => 'cut', 'id' => $id]);

        return $this->json(
            [
                'success'   => '1',
                'operation' => 'cut',
                'message'   => 'Element was cut. You can paste it wherever you want'
            ]
        );
    }

    /**
     * "Copy" operation for content elements
     */
    public function copyAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');
        $this->getSession()->set('backend.content', ['operation' => 'copy', 'id' => $id]);

        return $this->json(
            [
                'success'   => '1',
                'operation' => 'copy',
                'message'   => 'Element was copied. You can paste it wherever you want'
            ]
        );
    }

    /**
     * Paste the current content element in session
     */
    public function pasteAction()
    {
        $targetElementId = (int)$this->getRequest()->getArgument('id');
        $placement       = $this->getRequest()->getArgument('placement');
        $pasteType       = $this->getRequest()->getArgument('type');
        $dataToPaste     = $this->getSession()->get('backend.content');

        $pastedElementId = (int)$dataToPaste['id'];
        if ($pastedElementId > 0 && $targetElementId > 0) {
            // the element we are copying
            $originalElement = Utility::createInstance('Continut\Core\System\Domain\Collection\ContentCollection')
                ->findActiveById($pastedElementId);
            // the element into which or before/after which we paste our new copy
            $targetElement = Utility::createInstance('Continut\Core\System\Domain\Collection\ContentCollection')
                ->findActiveById($targetElementId);
            // grab all the info of the page onto which we're pasting our element
            $page = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                ->findById($targetElement->getPageId());

            $pastedContent = clone $originalElement;
            // references do not have any value and point to the original element
            if ($pasteType == 'reference') {
                $pastedContent->setType('reference');
                $pastedContent->setReferenceId($originalElement->getId());
                $pastedContent->setValue(null);
            }
            // set the id to null so that a new element is created
            $pastedContent->setId(null);
            $pastedContent->setColumnId($targetElement->getColumnId());
            $pastedContent->setPageId($targetElement->getPageId());
            $pastedContent->changePlacement(
                $page->getId(),
                $targetElementId,
                $targetElement->getColumnId(),
                $placement
            );

            // if it's a CUT, delete the original
            if ($dataToPaste['operation'] === 'cut') {
                $originalElement->setIsDeleted(1);

                Utility::createInstance('Continut\Core\System\Domain\Collection\ContentCollection')
                    ->add($originalElement)
                    ->save();
            }
        }

        // remove the copied/cut element from the session
        $this->getSession()->remove('backend.content');

        return $this->json(
            [
                'message'   => 'Element was pasted',
                'operation' => 'paste',
                'success'   => '1'
            ]
        );
    }

    /**
     * Delete a content element in the backend
     *
     * @return string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
        );
        $contentElement    = $contentCollection->where('id = :id AND is_deleted = 0', ['id' => $id])->getFirst();

        $contentElement->setIsDeleted(true);

        $contentCollection
            ->reset()
            ->add($contentElement)
            ->save();

        return $this->json(
            [
                'id'        => $contentElement->getId(),
                'operation' => 'delete'
            ]
        );
    }

    /**
     * Toggles an element's visibility
     */
    public function toggleVisibilityAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
        );
        $content           = $contentCollection->findById($id);

        $content->setIsVisible(!$content->getIsVisible());

        $contentCollection
            ->reset()
            ->add($content)
            ->save();

        return $this->json(
            [
                'id'        => $content->getId(),
                'operation' => 'toggleVisibility',
                'visible'   => (int)$content->getIsVisible(),
                'menuText'  => ($content->getIsVisible()) ?
                    $this->__('backend.content.operation.hide') :
                    $this->__('backend.content.operation.show'),
                'infoText'  => ($content->getIsVisible()) ? '' : $this->__('backend.content.headerIsHidden')
            ]
        );
    }

    /**
     * Adds a content element to the page
     */
    public function addAction()
    {
        $id        = (int)$this->getRequest()->getArgument('id', 0);
        $pageId    = (int)$this->getRequest()->getArgument('page_id', 0);
        $columnId  = (int)$this->getRequest()->getArgument('column_id', 0);
        $settings  = $this->getRequest()->getArgument('settings');
        $placement = $this->getRequest()->getArgument('placement', 'inside');

        $wizard         = Utility::createInstance('Continut\Core\System\View\BaseView');
        $wizardTemplate = ucfirst($settings['type'] . 's/' . $settings['template']);
        $wizard->setTemplate(Utility::getResourcePath($wizardTemplate, $settings['extension'], 'Backend', 'Wizard'));

        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
        );

        $this->getView()->assignMultiple(
            [
                'element'   => $contentCollection->createEmptyFromType($settings['type']),
                'content'   => $wizard->render(),
                'settings'  => $settings,
                'id'        => $id,
                'pageId'    => $pageId,
                'columnId'  => $columnId,
                'placement' => $placement
            ]
        );

        return $this->json(
            [
                'id'        => null,
                'html'      => $this->getView()->render(),
                'operation' => 'add'
            ]
        );
    }

    /**
     * Allows you to edit a content element
     *
     * @return string
     * @throws \Continut\Core\System\Tools\ErrorException
     */
    public function editAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
        );
        $content           = $contentCollection->findActiveById($id);

        $this->getView()->assign('element', $content);

        $data       = json_decode($content->getValue(), true);
        $wizardData = $data[ $content->getType() ];

        $wizard         = Utility::createInstance('Continut\Core\System\View\BaseView');
        $wizardTemplate = ucfirst($content->getType()) . 's/' . $wizardData['template'];
        $wizard->setTemplate(Utility::getResourcePath($wizardTemplate, $wizardData['extension'], 'Backend', 'Wizard'));
        if (!isset($wizardData['data']['title'])) {
            $wizardData['data']['title'] = $content->getTitle();
        }
        $wizard->assignMultiple($wizardData['data']);

        $this->getView()->assignMultiple(
            [
                'id'       => $id,
                'pageId'   => $content->getPageId(),
                'columnId' => $content->getColumnId(),
                'content'  => $wizard->render()
            ]
        );

        return $this->json(
            [
                'id'        => $content->getId(),
                'html'      => $this->getView()->render(),
                'operation' => 'edit'
            ]
        );
    }

    /**
     * Saves the changes made to a content element in the backend
     *
     * @return string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function updateAction()
    {
        $id      = (int)$this->getRequest()->getArgument('id');
        $data    = $this->getRequest()->getArgument('data', null);

        if ($data && $id > 0) {
            // whenever you are dealing with file_references they need a special treatment,
            // so we need to parse all data through the setFileReferences Utility function
            Utility::setFileReferences(['id' => $id, 'table' => 'sys_content'], $data);
            $contentCollection = Utility::createInstance(
                'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
            );
            $content           = $contentCollection->findById($id);
            $values            = json_decode($content->getValue(), true);
            if (isset($data['title'])) {
                $content->setTitle($data['title']);
            }
            $values[ $content->getType() ]['data'] = $data;
            $content->setValue(json_encode($values));
            $contentCollection
                ->reset()
                ->add($content)
                ->save();

            return $this->json(
                [
                    'success' => 1,
                    'content' => $content->dataMapper()
                ]
            );
        } else {
            return $this->json(
                [
                    'success' => 0,
                    'message' => 'Error'
                ]
            );
        }
    }

    /**
     * Adds a content element to the backend page
     *
     * @throws ErrorException
     *
     * @return string
     */
    public function createAction()
    {
        $pageId           = (int)$this->getRequest()->getArgument('page_id');
        $targetId         = (int)$this->getRequest()->getArgument('id');
        $columnId         = (int)$this->getRequest()->getArgument('column_id');
        $data             = $this->getRequest()->getArgument('data');
        $settings         = $this->getRequest()->getArgument('settings');
        $placement        = $this->getRequest()->getArgument('placement', 'inside');
        $settings['data'] = $data;
        $type             = $settings['type'];
        $value            = [$type => $settings];

        $page = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
            ->findById($pageId);

        $content = Utility::createInstance('Continut\Extensions\System\Backend\Classes\Domain\Model\BackendContent');
        $content
            ->setType($type)
            ->setPageId($pageId)
            ->setDomainUrlId($page->getDomainUrlId())
            ->setParentId($targetId)
            ->setIsVisible(true)
            ->setIsDeleted(false)
            ->setSorting(false)
            ->setColumnId($columnId)
            ->setValue(json_encode($value));
        if (isset($data['title'])) {
            $content->setTitle($data['title']);
        }

        $content->changePlacement($pageId, $targetId, $columnId, $placement);

        //Utility::getEventDispatcher()->dispatch('backend.content.afterSave');

        return $this->json(
            [
                'success' => 1,
                'content' => $content->dataMapper(),
                'url'     => Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_controller' => 'Content',
                        '_action'     => 'update',
                        'id'          => $content->getId()
                    ]
                ),
            ]
        );
    }

    /**
     * Update content element's container once it is dropped
     *
     * @return string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function updateContainerAction()
    {
        $newParent = (int)$this->getRequest()->getArgument('parent_id');
        $newColumn = (int)$this->getRequest()->getArgument('column_id');
        $id        = (int)$this->getRequest()->getArgument('id');
        $beforeId  = (int)$this->getRequest()->getArgument('before_id');

        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
        );
        $contentElement    = $contentCollection->where('id = :id AND is_deleted = 0', ['id' => $id])->getFirst();

        // the element was either added before a container or at the very end of a container, in which case the
        //  $beforeId is not present
        if ($beforeId) {
            // if it is added before an element then get it's "sorting" value and set it to our element
            $otherElement = $contentCollection->where('id = :id', ['id' => $beforeId])
                ->getFirst();
            $contentElement->setSorting($otherElement->getSorting());
            // all the other elements AFTER our new element get a "sorting" value + 1
            $elementsToModify = $contentCollection->where(
                'column_id = :column_id AND parent_id = :parent_id AND sorting >= :sorting_value',
                ['column_id' => $newColumn, 'parent_id' => $newParent, 'sorting_value' => $otherElement->getSorting()]
            );
            foreach ($elementsToModify->getAll() as $element) {
                $element->setSorting($element->getSorting() + 1);
            }
            $elementsToModify->save();
        } else {
            // if it is added at the beginning of a container, then it has the "sorting" value 1
            // if it is added at the end then it has the "sorting" value of the last element + 1
            $otherElement = $contentCollection->where(
                'column_id = :column_id AND parent_id = :parent_id ORDER BY sorting DESC',
                ['column_id' => $newColumn, 'parent_id' => $newParent]
            )->getFirst();
            if ($otherElement) {
                $contentElement->setSorting($otherElement->getSorting() + 1);
            } else {
                $contentElement->setSorting(1);
            }
        }

        // Update our element setting it's container parent, column id and the last modified date
        $contentElement
            ->setParentId($newParent)
            // @TODO Change all time manipulations to GMT+0
            ->setModifiedAt(time())
            ->setColumnId($newColumn);

        // The element gets saved by the collection. If the collection was already used to do a select, then we reset it
        // this way it only impacts our element
        $contentCollection
            ->reset()
            ->add($contentElement)
            ->save();

        return $this->json(
            [
                'success' => 1
            ]
        );
    }

    /**
     * Renders repeater content elements
     *
     * @return string
     */
    public function repeaterTemplateAction()
    {
        $index     = (int)$this->getRequest()->getArgument('repeaterElementId');
        $name      = $this->getRequest()->getArgument('repeaterName');
        $template  = $this->getRequest()->getArgument('template');
        $extension = $this->getRequest()->getArgument('extension');
        $values    = $this->getRequest()->getArgument('values', []);

        // This view holds the template with all the fields you want to repeat
        $fieldsView = Utility::createInstance('Continut\Core\System\View\BaseView');
        $fieldsView->setTemplate(Utility::getResourcePath($template, $extension, 'Backend', 'Template'));

        // Enter repeater context
        Utility::helper('Form')->setRepeaterMode(true);
        Utility::helper('Form')->setRepeaterName($name);
        Utility::helper('Form')->setRepeaterElementId($index);

        $fieldsView->assignMultiple($values);
        $html = $fieldsView->render();

        // Exit repeater context
        Utility::helper('Form')->setRepeaterMode(false);

        return $html;
    }

    /**
     * Remove a sys_file_reference from a content element
     *
     * @return string json object with status and informations about the deleted reference
     */
    public function deleteReferenceAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        if ($id > 0) {
            $fileReferenceCollection = Utility::createInstance(
                'Continut\Core\System\Domain\Collection\FileReferenceCollection'
            );
            $element                 = $fileReferenceCollection->findById($id);
            // remove all elements from the collection (we only have 1 since we did a findById)
            $fileReferenceCollection->delete();
        }

        return $this->json(
            [
                'success' => 1,
                'message' => $this->__('backend.content.reference.message.deleted'),
                'element' => $element->dataMapper()
            ]
        );
    }
}
