<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.12.2015 @ 14:46
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class BackendUsersController extends BackendController
{
    /**
     * Backend users management
     */
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $backendUsergroups = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\BackendUserGroupCollection'
        )->findAll();

        $grid
            ->setFormAction(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'BackendUsers', '_action' => 'grid'])
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserCollection'))
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'id'          => [
                        'label'    => 'backend.users.grid.field.id',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ]
                    ],
                    'name'        => [
                        'label'    => 'backend.users.grid.field.name',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'username'    => [
                        'label'    => 'backend.users.grid.field.username',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'usergroupId' => [
                        'label'    => 'backend.users.grid.field.usergroupId',
                        'css'      => 'col-sm-2',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\System\Backend\Classes\View\Renderer\TextRenderer',
                            'parameters' => ['fromValues' => $backendUsergroups->getElements(), 'fromField' => 'title']
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => $backendUsergroups->toSelect('id', 'title', true)
                        ]
                    ],
                    'isActive'    => [
                        'label'    => 'backend.users.grid.field.isActive',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer',
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'actions'     => [
                        'label'    => 'backend.users.grid.field.actions',
                        'css'      => 'col-sm-2 text-right flip',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\System\Backend\Classes\View\Renderer\ActionsRenderer',
                            'parameters' => [
                                'showEdit'   => true,
                                'showDelete' => true,
                                'extension'  => 'Backend',
                                'controller' => 'BackendUsers'
                            ]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * Edit backend user action
     */
    public function editAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $user = Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserCollection')
            ->findById($id);

        $this->setVariables($user);
    }

    /**
     * New backend user action
     */
    public function createAction()
    {
        $user = Utility::createInstance('Continut\Core\System\Domain\Model\BackendUser');

        $this->setVariables($user);
    }

    /**
     * Save user action
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        $userCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserCollection');

        if ($id == 0) {
            $user             = Utility::createInstance('Continut\Core\System\Domain\Model\BackendUser');
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        } else {
            $user = $userCollection->findById($id);
            if (isset($data['change_password'])) {
                $data['password'] = password_hash($data['new_password'], PASSWORD_DEFAULT);
            }
            // we will not pass these 2 fields to the model
            unset($data['change_password']);
            unset($data['new_password']);
        }
        $user->update($data);

        if ($user->validate()) {
            $userCollection
                ->reset()
                ->add($user)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('general.flash.recordSaved'));

            $this->redirect(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'BackendUsers', '_action' => 'grid'])
            );
        }

        $this->setVariables($user);
    }

    private function setVariables($user)
    {
        $backendUsergroups = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\BackendUserGroupCollection'
        )->findAll();

        $this->getView()->assign('user', $user);
        $this->getView()->assign('groups', $backendUsergroups->toSelect('id', 'title'));
    }
}
