<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 10.08.2017 @ 16:30
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class NotificationController extends BackendController
{
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $grid
            ->setFormAction(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'Notification', '_action' => 'grid'])
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(
                Utility::createInstance(
                    'Continut\Extensions\System\Backend\Classes\Domain\Collection\NotificationCollection'
                )
            )
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'id'      => [
                        'label'    => 'backend.notification.grid.field.id',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ]
                    ],
                    'message' => [
                        'label'    => 'backend.notification.grid.field.message',
                        'css'      => 'col-sm-5',
                        'renderer' => [
                            'class' =>
                                'Continut\Extensions\System\Backend\Classes\View\Renderer\Notification\MessageRenderer'
                        ]
                    ],
                    'createdAt'    => [
                        'label'    => 'backend.notification.grid.field.createdAt',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\DateRenderer'
                        ]
                    ],
                    'isRead'  => [
                        'label'    => 'backend.notification.grid.field.isRead',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer',
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'actions' => [
                        'label'    => 'backend.notification.grid.field.actions',
                        'css'      => 'col-sm-2 text-right flip',
                        'renderer' => [
                            'class'      =>
                                'Continut\Extensions\System\Backend\Classes\View\Renderer\Notification\ActionsRenderer',
                            'parameters' => [
                                'showEdit'   => false,
                                'showDelete' => true,
                                'showIsRead' => true,
                                'extension'  => 'Backend',
                                'controller' => 'Notification'
                            ]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * Toggle the read state of a notification
     */
    public function readAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $notificationCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\NotificationCollection'
        );
        $notification = $notificationCollection->findById($id);

        if ($notification) {
            $notification->setIsRead(!$notification->getIsRead());

            $notificationCollection
                ->reset()
                ->add($notification)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('general.flash.recordSaved'));
        } else {
            Utility::getSession()->addFlashMessage($this->__('general.flash.recordNotFound'));
        }

        $this->redirect(
            Utility::helper('Url')->linkToPath('admin', ['_controller' => 'Notification', '_action' => 'grid'])
        );
    }

    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $notificationCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\NotificationCollection'
        );
        $notificationCollection->findById($id);
        $notificationCollection->delete();

        Utility::getSession()->addFlashMessage($this->__('general.flash.recordDeleted'));

        $this->redirect(
            Utility::helper('Url')->linkToPath('admin', ['_controller' => 'Notification', '_action' => 'grid'])
        );
    }
}
