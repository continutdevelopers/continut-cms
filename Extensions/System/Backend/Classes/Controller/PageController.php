<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 25.04.2015 @ 21:18
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class PageController extends BackendController
{
    public function __construct()
    {
        parent::__construct();
        // Set the default Layout to use in all the actions
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Called when the pagetree is shown for the page module
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function indexAction()
    {
        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection');
        $domainsCollection->whereHasLanguages();

        $languagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection');
        $currentDomain       = $this->getSession()->get('current_domain');
        $domainId            = ($currentDomain) ? (int)$currentDomain : $domainsCollection->getFirst()->getId();

        $this->getView()->assignMultiple(
            [
                'domains'   => $domainsCollection,
                'languages' => $languagesCollection->whereDomain($domainId, 'is_default DESC, sorting ASC')
            ]
        );
    }

    /**
     * Called when the pagetree changes or needs to be shown for the first time. Responds with a JSON string of the
     * pagetree
     *
     * @param string $term Search term to use
     *
     * @return string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function treeAction($term = '')
    {
        /** @var \Continut\Core\System\Domain\Collection\PageCollection $pagesCollection */
        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');

        // if a domain/language is already present in the session, then show these ones
        if ($this->getRequest()->hasArgument('domain_id') || $this->getRequest()->hasArgument('domain_url_id')) {
            $domainId   = (int)$this->getRequest()->getArgument('domain_id');
            $languageId = (int)$this->getRequest()->getArgument('domain_url_id', 0);
        } else {
            $domainId   = (int)$this->getSession()->get('current_domain');
            $languageId = (int)$this->getSession()->get('current_language');
        }

        // get the domains collection
        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection');

        if ($domainId == 0) {
            // fetch the first visible domain, ordered by sorting, if no domain_id is sent
            $domain = $domainsCollection->whereVisible()
                ->getFirst();
        } else {
            $domain = $domainsCollection->findById($domainId);
        }

        // then the domains url collection
        $languagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection');
        // see if a domain url was sent, if not get the first one found for this domain
        if ($languageId == 0) {
            $domainUrl = $languagesCollection
                ->whereDomain($domain->getId())
                ->getFirst();
        } else {
            $domainUrl = $languagesCollection
                ->whereDomainAndLanguage($domain->getId(), $languageId)
                ->getFirst();
        }

        // store the selection in the session so that we can show it once they refresh the page
        if ($domain) {
            $this->getSession()->set('current_domain', $domain->getId());
        }
        // store the language (domain_url)
        if ($domainUrl) {
            $this->getSession()->set('current_language', $domainUrl->getId());
        }

        // if no domain url is found, then unfortunately there is nothing that we can show
        if (!$domainUrl) {
            return $this->json(['success' => 0, 'languages' => []]);
        }

        // get all the pages that belong to this domain
        // if the search filter is not empty, filter on page titles
        if (mb_strlen($term) > 0) {
            $pagesCollection->whereLanguageAndTitle($domainUrl->getId(), $term);
        } else {
            $pagesCollection->whereLanguage($domainUrl->getId());
        }

        /** @var \Continut\Core\System\Domain\Collection\DomainUrlCollection $languagesCollection */
        $languagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection');
        $languagesCollection->whereDomain($domain->getId(), 'is_default DESC, sorting ASC');

        $pagesData = [
            'success'   => 1,
            'pages'     => $pagesCollection->buildJsonTree(),
            'languages' => $languagesCollection->toSimplifiedArray()
        ];

        return $this->json($pagesData, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Called when a page's properties are edited in the backend
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function editAction()
    {
        $pageId    = (int)$this->getRequest()->getArgument('page_id');
        $pageModel = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
            ->findById($pageId);
        $pageModel->mergeOriginal();

        $layouts = Utility::getLayouts();

        $this->getView()->assign('page', $pageModel);
        $this->getView()->assign('layouts', $layouts);
    }

    /**
     * Called when the page properties are modified and need to be saved
     */
    public function savePropertiesAction()
    {
        $data = $this->getRequest()->getArgument('data');
        // page id to save properties for
        $id = (int)$data['id'];

        $pageCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageModel      = $pageCollection->findById($id);

        $timezone       = new \DateTimeZone(Utility::getConfiguration('System/Timezone'));
        // convert our date time values to timestamps (integers) and store them as UTC time
        $data['start_date'] = Utility::helper('DateTime')->dateTimeToUTCTimestamp($data['start_date'], $timezone);
        $data['end_date']   = Utility::helper('DateTime')->dateTimeToUTCTimestamp($data['end_date'], $timezone);

        // generate slug if none is present
        if (trim($data['slug']) === '') {
            $data['slug'] = Utility::generateSlug($data['title']);
        }
        // if we switch a Page from "not in cache" to "is in cache"
        // then we need to remove it's already cached version, if one exists
        if (!$pageModel->getIsCachable() && filter_var($data['is_cachable'], FILTER_VALIDATE_BOOLEAN)) {
            Utility::getCache()->delete($pageModel->getCacheKey());
        }
        $pageModel->update($data);

        $result['success'] = 0;
        if ($pageModel->validate()) {
            $pageCollection
                ->reset()
                ->add($pageModel)
                ->save();

            $result['success'] = 1;
            $result['page']    = $pageModel->dataMapper();
            $result['html']    = $this->forward('show')->renderView();
        } else {
            $forward = $this->forward('edit', ['page_id' => $id]);
            $forward->getView()->assign('page', $pageModel);
            $result['html'] = $forward->renderView();
        }

        return $this->json($result);
    }

    /**
     * Called when the user clicks on a page in the page tree. It shows the details of the page on the right side
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function showAction()
    {
        Utility::debugData('Page rendering', 'start');

        // Load the pages collection model
        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageId          = (int)$this->getRequest()->getArgument('id', 0);
        $pageModel       = $pagesCollection->findById($pageId);
        $pageModel->mergeOriginal();

        // The breadcrumb path is cached in the variable "cached_path" as a comma separated list of values
        $breadcrumbs = [];
        if ($pageModel->getCachedPath()) {
            $breadcrumbs = $pagesCollection
                ->whereBreadcrumb($pageModel->getCachedPath())
                ->getAll();
        }

        // Load the content collection model and then find all the content elements that belong to this page_id
        $contentCollection = Utility::createInstance(
            'Continut\Extensions\System\Backend\Classes\Domain\Collection\BackendContentCollection'
        );
        $contentCollection->findAllForPage($pageModel->getId(), $pageModel->getDomainUrlId());

        // Since content elements can have containers and be recursive, we need to build a Tree object to handle them
        $contentTree = $contentCollection->buildTree();

        $layout = Utility::createInstance('Continut\Core\System\View\BackendLayout');
        $layout
            //->setPage($pageModel)
            ->setTemplate(__ROOTCMS__ . $pageModel->getBackendLayout())
            ->setElements($contentTree);

        // Render the Tree elements and save the generated HTML in a variable
        $pageContent = $layout->render();

        // Send all the data to the view
        $this->getView()->assignMultiple(
            [
                'page'        => $pageModel,
                'pageContent' => $pageContent,
                'breadcrumbs' => $breadcrumbs
            ]
        );

        // notify the debugger
        Utility::debugData('Page rendering', 'stop');
    }

    /**
     * Hide or show a page in the frontend (toggle it's is_visible value)
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function toggleVisibilityAction()
    {
        $pageId = (int)$this->getRequest()->getArgument('page_id', 0);

        // Load the pages collection model
        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageModel       = $pagesCollection->findById($pageId);

        $pageModel->setIsVisible(!$pageModel->getIsVisible());

        $pagesCollection
            ->reset()
            ->add($pageModel)
            ->save();

        // since it's an ajax call we return directly the data as json
        return $this->json(
            [
                'visible' => $pageModel->getIsVisible(),
                'pid'     => $pageModel->getId()
            ]
        );
    }

    /**
     * Hide or show a page in any frontend menu (toggle it's is_in_menu value)
     *
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function toggleMenuAction()
    {
        $pageId = (int)$this->getRequest()->getArgument('page_id', 0);

        // Load the pages collection model
        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageModel       = $pagesCollection->findById($pageId);

        // @TODO: Cast the value to an int or check for boolean types in the BaseCollection when saving
        //$pageModel->setIsInMenu(1 - $pageModel->getIsInMenu());
        $pageModel->setIsInMenu(!$pageModel->getIsInMenu());

        $pagesCollection
            ->reset()
            ->add($pageModel)
            ->save();

        //$this->getView()->assign("page", $pageModel);
        return $this->json(
            [
                'isInMenu' => $pageModel->getIsInMenu(),
                'pid'      => $pageModel->getId()
            ]
        );
    }

    /**
     * Search the current page tree
     *
     * @return mixed
     */
    public function searchTreeAction()
    {
        // get the search term
        $term = $this->getRequest()->getArgument('query', '');

        // and filter the page tree on this text
        return $this->treeAction($term);
    }

    /**
     * Saves a page's new parentId once it is moved in the tree
     *
     * @return string
     * @throws \Continut\Core\System\Tools\Exception
     */
    public function treeMoveAction()
    {
        // We get the id of the page that has been drag & dropped
        $pageId = (int)$this->getRequest()->getArgument("movedId");
        // position of node inside the new node, after drop stops
        $newPosition = $this->getRequest()->getArgument("position");
        // new parent to move into, or after
        $newParentId = (int)$this->getRequest()->getArgument("newParentId");
        // get the new sorting for all pages on the same level
        $orders = $this->getRequest()->getArgument("order");

        // Then we load it's Page Model
        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageModel       = $pagesCollection->findById($pageId);

        // If the page is valid, we change it's parentId field and then save the value
        if ($pageModel) {
            $pageModel->setParentId($newParentId);
            foreach ($orders as $sortOrder => $pageId) {
                if ($pageModel->getId() == $pageId) {
                    $pageModel->setSorting($sortOrder);
                }
            }

            // get all pages on the parent level
            $pagesOnSameLevel = $pagesCollection->reset()->where(
                'domain_url_id = :domain_url_id AND parent_id = :parent_id ORDER BY sorting ASC',
                [
                    'domain_url_id' => $pageModel->getDomainUrlId(),
                    'parent_id' => $newParentId
                ]
            );
            if ($pagesOnSameLevel->count()) {
                foreach ($pagesOnSameLevel->getAll() as $page) {
                    foreach ($orders as $sortOrder => $pageId) {
                        if ($page->getId() == $pageId) {
                            $page->setSorting($sortOrder);
                        }
                    }
                }
            }/* else {
                $pageModel->setSorting(0);
            }*/

            $pagesOnSameLevel->save();

            $pagesCollection
                ->reset()
                ->add($pageModel)
                ->save();
        }

        return $this->json(['success' => 1]);
    }

    /**
     * Called when a page is deleted
     */
    public function deleteAction()
    {
        $pageId = (int)$this->getRequest()->getArgument('page_id');

        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageModel       = $pagesCollection->findById($pageId);

        $pageModel->deleteRecursively();

        return $this->json(['success' => 1, 'message' => $this->__('backend.page.deletePage.message')]);
    }

    /**
     * Called when the page creation wizard is displayed
     */
    public function wizardAction()
    {
        $pageId    = (int)$this->getRequest()->getArgument('id');
        $pageModel = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
            ->findById($pageId);

        $layouts = Utility::getLayouts();

        $this->getView()->assign('page', $pageModel);
        $this->getView()->assign('pages', [0 => ['name' => '', 'visibility' => 'visible']]);
        $this->getView()->assign('layouts', $layouts);
    }

    /**
     * Add one or multiple pages to the tree
     */
    public function addAction()
    {
        $pageId        = (int)$this->getRequest()->getArgument('id');
        $pagePlacement = $this->getRequest()->getArgument('page_placement');
        $pages         = $this->getRequest()->getArgument('data')['pages'];
        $domainUrlId   = (int)$this->getRequest()->getArgument('domain_url_id');

        $success = 1;
        $error   = '';

        // if no domainUrlId is set, it means something went wrong. The domainUrlId cannot be zero
        if ($domainUrlId > 0) {
            $languagesCollection = Utility::createInstance(
                'Continut\Core\System\Domain\Collection\DomainUrlCollection'
            );
            $language            = $languagesCollection->findById($domainUrlId);

            // is there any language linked to this domain url id?
            if ($language) {
                $pageCollection     = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
                $savePageCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');

                if ($pageId > 0) {
                    $parentPage   = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                        ->findById($pageId);
                    $sortingValue = (int)$parentPage->getSorting();
                }

                $lastPageSorting = 0;
                if ($pagePlacement === 'inside') {
                    $lastPage = $pageCollection
                        ->findLastForLanguageAndParent($domainUrlId, $pageId);
                    // if we don't have any page so far, just set the new one's sorting order to 1
                    if ($lastPage) {
                        $lastPageSorting = $lastPage->getSorting();
                    }
                }

                $languagesCollection = Utility::createInstance(
                    'Continut\Core\System\Domain\Collection\DomainUrlCollection'
                );
                $language            = $languagesCollection->findById($domainUrlId);

                foreach ($pages as $index => $pageData) {
                    $pageModel = Utility::createInstance('Continut\Core\System\Domain\Model\Page');
                    // added inside a selected page, at the very end of already existing pages
                    if ($pagePlacement === 'inside') {
                        $pageModel->setParentId($pageId);
                        $pageModel->setSorting(++$lastPageSorting);
                    } else {
                        // added directly to the root, at the very end of all existing pages
                        if ($pageId == 0) {
                            $pageModel->setParentId(0);
                            // get the biggest sorting value for all pages already existing on the root level
                            $lastPage = $pageCollection->findByParentId(0, true, true, 'sorting DESC')->getFirst();
                            // if we don't have any page so far, just set the new one's sorting order to 1
                            if ($lastPage) {
                                // and add this new page at the very end of the page tree
                                $pageModel->setSorting((int)$lastPage->getSorting() + 1);
                            } else {
                                $pageModel->setSorting(1);
                            }
                            // added "before" or "after" the selected page
                        } else {
                            $pageModel->setParentId($parentPage->getParentId());
                            if ($pagePlacement === 'before') {
                                // BEFORE
                                $pageModel->setSorting($sortingValue++);
                                // update all other pages AFTER this new one and set their sorting order + 1
                                $otherPages = $pageCollection->findByParentIdAndSorting(
                                    $parentPage->getParentId(),
                                    $parentPage->getSorting(),
                                    '>='
                                )->getAll();
                                $pageCollection->reset();
                                foreach ($otherPages as $sortedPage) {
                                    $sortedPage->setSorting((int)$sortedPage->getSorting() + 1);
                                    $pageCollection->add($sortedPage);
                                }
                                $pageCollection->save();
                            } else {
                                // AFTER
                                $pageModel->setSorting(++$sortingValue);
                                // update all other pages AFTER this new one and set their sorting order + 1
                                $otherPages = $pageCollection->findByParentIdAndSorting(
                                    $parentPage->getParentId(),
                                    $sortingValue,
                                    '>='
                                )->getAll();
                                $pageCollection->reset();
                                foreach ($otherPages as $sortedPage) {
                                    $sortedPage->setSorting((int)$sortedPage->getSorting() + 1);
                                    $pageCollection->add($sortedPage);
                                }
                                $pageCollection->save();
                            }
                        }
                    }
                    $pageModel
                        ->setSlug(Utility::generateSlug($pageData['name']))
                        ->setIsDeleted(false)
                        // @TODO : check if a translation or not
                        ->setOriginalId(0)
                        ->setTitle($pageData['name'])
                        ->setDomainUrlId($language->getId());

                    // set page visibility
                    if ($pageData['visibility']) {
                        switch ($pageData['visibility']) {
                            case 'visible':
                                $pageModel->setIsVisible(true)->setIsInMenu(true);
                                break;
                            case 'hidden_in_menu':
                                $pageModel->setIsVisible(true)->setIsInMenu(false);
                                break;
                            default:
                                $pageModel->setIsVisible(false);
                        }
                    }

                    // set page layout
                    if ($pageData['layout']) {
                        $pageModel->setLayout($pageData['layout']);
                    }

                    $savePageCollection
                        ->add($pageModel);
                }

                $savePageCollection->save();

                // return all the new inserted pages as json objects so that we can update the jsTree data
                $jsonPages = [];
                //$newPosition = ($pagePlacement === 'inside' ? 'last' : 'first');
                foreach ($savePageCollection->getAll() as $page) {
                    $jsonPageData = [
                        'node'     => ['id' => $page->getId(), 'text' => $page->getTitle(), 'icon' => 'fa fa-file'],
                        // jsTree needs '#' for root nodes so parentId == 0 is ignored
                        'parent'   => ((int)$page->getParentId() > 0) ? $page->getParentId() : '#',
                        'position' => $pagePlacement
                    ];
                    $jsonPages[]  = $jsonPageData;
                }

                return $this->json(
                    [
                        'success' => 1,
                        'pages'   => $jsonPages
                    ]
                );
            }
        } else {
            $error   = Utility::helper('Localization')->translate('backend.page.wizard.error.languageMissing');
            $success = 0;
        }

        return $this->json(['success' => $success, 'error' => $error]);
    }

    /**
     * Toggles the touch enhancements for tree and page elements handling
     */
    public function toggleTouchAction()
    {
        $touchEnabled = !$this->getUser()->getAttribute('touchEnabled', 0);
        $this
            ->getUser()
            ->setAttribute('touchEnabled', $touchEnabled)
            ->save();

        return $this->json(['touchEnabled' => $this->getUser()->getAttribute('touchEnabled'), 'success' => 1]);
    }
}
