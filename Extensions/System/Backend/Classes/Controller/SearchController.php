<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 23.09.2017 @ 14:50
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class SearchController extends BackendController
{
    public function allAction()
    {
        $query = $this->getRequest()->getArgument('search');

        $pageCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');
        $pageCollection->whereGlobalTitle($query, 10);

        $pagesData = [];
        foreach ($pageCollection->getAll() as $page) {
            $pagesData[] = ['text' => $page->getTitle(), 'value' => $page->getId()];
        }

        return $this->json($pagesData);
    }
}
