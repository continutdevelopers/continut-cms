<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 27.08.2017 @ 18:20
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\System\Tools\ErrorException;
use Continut\Core\Utility;

class FilterController extends BackendController
{
    /**
     * Returns the filtered results for each $type.
     * The results will be used by the autocomplete component to populate a dropdown with the actual results
     * Can be used to filter on pages, tags, categories, etc
     *
     * @throws ErrorException
     *
     * @return string
     */
    public function filterTagsAction()
    {
        $search     = $this->getRequest()->getArgument('search', '');
        $type       = $this->getRequest()->getArgument('type', '');
        $collection = $this->getRequest()->getArgument('collection', '');

        $values = [];
        $resultCollection = null;
        if ($search && $type) {
            switch ($type) {
                case 'pages':
                    $resultCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                        ->whereLanguageAndTitle(
                            Utility::getSite()->getDomainUrl()->getId(),
                            $search
                        );
                    break;
                case 'pages_translation':
                    // grab the default language for this page's domain
                    $default = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                        ->findDefaultLanguage(Utility::getSite()->getDomain()->getId())->getFirst();
                    // return only pages for the default language
                    $resultCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                        ->whereLanguageAndTitle(
                            $default->getId(),
                            $search
                        );
                    break;
                case 'custom':
                    $resultCollection = Utility::createInstance($collection)->findAll();
                    break;
            }

            if (!$resultCollection) {
                throw new ErrorException('Collection missing for the filters in the linkWizardField field');
            }

            foreach ($resultCollection->getAll() as $record) {
                $values[] = ['text' => $record->formatForFilter(), 'value' => $record->getId()];
            }
        }

        return $this->json($values);
    }

    /**
     * @param string $type
     * @param array  $values
     * @param string $collectionClass
     *
     * @return array|null
     */
    public function getSelected($type, $values, $collectionClass = '')
    {
        $result = null;

        switch ($type) {
            case 'pages':
            case 'pages_translation':
                foreach (Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')
                             ->findByIds($values)->getAll() as $record) {
                    $result[] = ['value' => $record->getId(), 'text' => $record->formatForFilter()];
                }
                break;
            case 'custom':
                foreach (Utility::createInstance($collectionClass)
                             ->findByIds($values)->getAll() as $record) {
                    $result[] = ['value' => $record->getId(), 'text' => $record->formatForFilter()];
                }
                break;
        }

        return $result;
    }
}
