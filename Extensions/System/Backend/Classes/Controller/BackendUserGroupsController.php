<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.12.2015 @ 14:46
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class BackendUserGroupsController extends BackendController
{
    /**
     * Backend Groups management
     */
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $grid
            ->setFormAction(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'BackendUserGroups', '_action' => 'grid'])
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(
                Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserGroupCollection')
            )
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'id'        => [
                        'label'    => 'backend.groups.grid.field.id',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ]
                    ],
                    'title'     => [
                        'label'    => 'backend.groups.grid.field.title',
                        'css'      => 'col-sm-7',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\BaseRenderer'
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'isDeleted' => [
                        'label'    => 'backend.groups.grid.field.isDeleted',
                        'css'      => 'col-sm-2',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer',
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'actions'   => [
                        'label'    => 'backend.groups.grid.field.actions',
                        'css'      => 'col-sm-2 text-right flip',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\System\Backend\Classes\View\Renderer\ActionsRenderer',
                            'parameters' => [
                                'showEdit'   => true,
                                'showDelete' => true,
                                'extension'  => 'Backend',
                                'controller' => 'BackendUserGroups'
                            ]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * New backend usergroup action
     */
    public function createAction()
    {
        $group = Utility::createInstance('Continut\Core\System\Domain\Model\BackendUserGroup');

        $this->getView()->assign('group', $group);
    }

    /**
     * Edit backend usergroup action
     */
    public function editAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $group = Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserGroupCollection')
            ->findById($id);

        $this->getView()->assign('group', $group);
    }

    /**
     * Save group action
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        $groupCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\BackendUserGroupCollection');

        if ($id == 0) {
            $group = Utility::createInstance('Continut\Core\System\Domain\Model\BackendUserGroup');
        } else {
            $group = $groupCollection->findById($id);
        }
        $group->update($data);

        if ($group->validate()) {
            $groupCollection
                ->reset()
                ->add($group)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('general.flash.recordSaved'));

            $this->redirect(
                Utility::helper('Url')->linkToPath('admin', ['_controller' => 'BackendUserGroups', '_action' => 'grid'])
            );
        }

        $this->getView()->assign('group', $group);
    }
}
