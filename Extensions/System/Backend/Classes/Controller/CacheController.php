<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 22.04.2017 @ 10:40
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class CacheController extends BackendController
{
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Clears all the content cache (pages, content elements, plugins, etc)
     * So far the caching system only supports fullpage cache, so only pages are concerned
     *
     * @return string JSON string with cache clearing status
     */
    public function clearContentAction()
    {
        $cacheClearSuccessful = Utility::getCache()->deleteWithWildcard('sys_pages_*');

        if ($cacheClearSuccessful) {
            $message = $this->__('backend.menu.cache.message.cacheCleared');
        } else {
            $message = $this->__('backend.menu.cache.message.cacheNotCleared');
        }

        return $this->json(['success' => $cacheClearSuccessful, 'message' => $message]);
    }

    /**
     * Clears all the configuration cache (websites configuration and cached translation labels)
     *
     * @return string
     */
    public function clearConfigurationAction()
    {
        $cacheClearSuccessful = Utility::getCache()->deleteWithWildcard('_cms_configuration_*');

        if ($cacheClearSuccessful) {
            $message = $this->__('backend.menu.cache.message.cacheCleared');
        } else {
            $message = $this->__('backend.menu.cache.message.cacheNotCleared');
        }

        return $this->json(['success' => $cacheClearSuccessful, 'message' => $message]);
    }
}
