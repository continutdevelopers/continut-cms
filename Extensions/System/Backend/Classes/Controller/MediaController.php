<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 20.04.2015 @ 21:27
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\System\Storage\LocalStorage;
use Continut\Core\System\Storage\UploadHandler;
use Continut\Core\Utility;

class MediaController extends BackendController
{
    // default display mode, allows the browsing of files but no selection
    const HANDLING_TYPE_DISPLAY = 'display';

    // usually displayed in a popup, allows the browsing AND the select/multiselect of files
    const HANDLING_TYPE_SELECT = 'select';

    // Do we show files in square blocks similar to Windows Explorer's Thumbnail mode?
    const LIST_TYPE_THUMBNAILS = 'thumbnails';

    // or more like Explorer's List mode?
    const LIST_TYPE_LIST = 'list';

    /**
     * @var \Continut\Core\System\Storage\StorageInterface
     */
    protected $storage;

    /**
     * Should the files list be displayed in 'thumbnails' mode, or 'list' mode?
     *
     * @var string
     */
    protected $listType;

    /**
     * Are we just displaying the files or are we also able to select files (in a popup)
     * Possible values ('display' or 'select')
     *
     * @var string
     */
    protected $handlingType = self::HANDLING_TYPE_DISPLAY;

    /**
     * Initialize settings from user session data and get a reference to the currently handled storage
     */
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));

        $this->listType = $this->getUser()->getAttribute('media.list', self::LIST_TYPE_THUMBNAILS);
        $this->storage  = Utility::createInstance('Continut\Core\System\Storage\LocalStorage');
    }

    /**
     * Index page, by default shows the entry /Media folder with its files
     */
    public function indexAction()
    {
        $path               = urldecode($this->getRequest()->getArgument('path', $this->storage->getRoot()));
        $this->handlingType = $this->getRequest()->getArgument('handling', self::HANDLING_TYPE_DISPLAY);

        $this->getFileObjects($path);
    }

    /**
     * Returns all files found inside a path
     *
     * @var string $path
     * @var string $searchFor Filenames to filter on
     *
     * @return void
     */
    protected function getFileObjects($path, $searchFor = '')
    {
        // @TODO: Check if it is worth indexing them while in browse mode or if we should only index them while selected
        // We also need to index these files in the sys_files table, if they are not indexed already
        //$fileCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\FileCollection');
        //$fileCollection->where('location LIKE ":location"', ['location' => $path]);

        $this->getView()->assignMultiple(
            [
                'path'         => $path,
                'files'        => $this->storage->getFiles($path, $searchFor),
                'folders'      => $this->storage->getFolders($path),
                'listType'     => $this->listType,
                'handlingType' => $this->handlingType
            ]
        );
    }

    /**
     * Returns the list of files from the selected path, once a folder is clicked on in the Folder Tree
     */
    public function getFilesAction()
    {
        $path               = urldecode($this->getRequest()->getArgument('path', $this->storage->getRoot()));
        $this->listType     = $this->getRequest()->getArgument(
            'list',
            $this->getUser()->getAttribute('media.list', self::LIST_TYPE_THUMBNAILS)
        );
        $searchFor          = $this->getRequest()->getArgument('query', '');
        $this->handlingType = $this->getRequest()->getArgument('handling', self::HANDLING_TYPE_DISPLAY);

        // save the user's preferences
        $this->getUser()
            ->setAttribute('media.list', $this->listType)
            ->save();

        $this->getFileObjects($path, $searchFor);
    }

    /**
     * Create a folder
     */
    public function createFolderAction()
    {
        $folder = $this->getRequest()->getArgument('folder');
        $path   = urldecode($this->getRequest()->getArgument('path', ''));

        $createdFolder = $this->storage->createFolder($folder, $path);
        if (!$createdFolder) {
            Utility::getLogger()->alert($this->__('backend.media.errors.cannotCreateFolder'));
        }

        if ($createdFolder) {
            $message = $this->__('backend.media.folders.message.folderCreated');
        } else {
            $message = $this->__('backend.media.folders.message.cannotCreateFolder');
        }

        return $this->json(['success' => $createdFolder, 'message' => $message]);
    }

    /**
     * Show upload files popup
     */
    public function showUploadFilesAction()
    {
    }

    /**
     * Upload files
     *
     * @see https://github.com/FineUploader/php-traditional-server/blob/master/endpoint.php
     */
    public function uploadFilesAction()
    {
        // @TODO Validate the path with the storage (LocalStorage) and probably move the upload function in the storage
        $path     = $this->getRequest()->getArgument('path', $this->storage->getRoot());
        $uploader = new UploadHandler('uploadFile');
        $result   = $uploader->handleUpload($path, $this->getRequest()->getArgument('overwrite', false));

        if (!$result) {
            return $this->json([
                'success' => false,
                'message' => $uploader->getErrorMsg()
            ]);
        } else {
            // if it's an image return a preview, if not just show a general icon
            $filePreview = '<i class="no-icon"></i>';
            if ($uploader->isWebImage($uploader->getSavedFile())) {
                $cropedImage = Utility::helper('Image')->crop($uploader->getSavedFile(), '64', '64');
                $filePreview = '<img class="media-object" src="' . $cropedImage . '" alt=""/>';
            }

            return $this->json([
                'success' => true,
                'file'    => $uploader->getFileName(),
                'preview' => $filePreview,
                'message' => $this->__('backend.media.upload.message.fileUploaded')
            ]);
        }
    }

    /**
     * Show detailed information about a file, once you click on it
     */
    public function fileInfoAction()
    {
        $fileInfo = $this->storage->getFileInfo($this->getRequest()->getArgument('file'));

        $this->getView()->assign('fileInfo', $fileInfo);
    }

    /**
     * Returns nodes from a path, once a node (folder) in the file explorer is expanded
     *
     * @return string
     */
    public function treeGetNodeAction()
    {
        $path = $this->getRequest()->getArgument('id');
        $path = ($path == '#') ? $this->storage->getRoot() : $path;

        $jsonNodes = [];

        // get list of folders
        foreach ($this->storage->getFolders($path) as $folder) {
            $jsonNodes[] = [
                'text'     => $folder->getName(),
                'id'       => $folder->getRelativePath(),
                'children' => ($folder->getCountFolders() > 0),
                'icon'     => 'fa fa-folder' // we are only displaying folder nodes in this list
            ];
        }

        // We add the storage's getRoot() as a root element to the list of returned nodes and preselect it by default
        if ($path == $this->storage->getRoot()) {
            $jsonNodes = [
                [
                    'text'     => $this->storage->getRoot(),
                    'children' => $jsonNodes,
                    'id'       => $this->storage->getRoot(),
                    'icon'     => 'fa fa-folder',
                    'state'    => ['opened' => true]
                ]
            ];
        }

        return $this->json($jsonNodes);
    }

    /**
     * Index files inside sys_files table
     *
     * @return string
     */
    public function indexFilesAction()
    {
        $selection = $this->getRequest()->getArgument('selection');
        $container = $this->getRequest()->getArgument('container');

        // @TODO: Indexing should probably be done at the Storage level, not directly in the controller
        // so far we only offer LocalStorage support
        $indexedFiles = [];
        $fileIds      = [];
        if ($selection) {
            $files          = explode(',', $selection);
            $fileCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\FileCollection');

            foreach ($files as $filePath) {
                $existingFile = $fileCollection->findByFullpath($filePath)->getFirst();
                if (!$existingFile) {
                    // LocalStorage needs to be replaced by the corresponding storage defined in the config
                    // @TODO: replace it once we have more storage types
                    $storage    = Utility::createInstance('Continut\Core\System\Storage\LocalStorage');
                    $fileObject = $storage->getFileInfo($filePath);
                    $newFile    = Utility::createInstance('Continut\Core\System\Domain\Model\File');
                    $newFile
                        ->setFilename($fileObject->getFullname())
                        ->setFilesize($fileObject->getSize())
                        ->setLocation($fileObject->getRelativePath())
                        ->setFullpath($filePath)
                        ->setCreatedAt(time())
                        ->setModifiedAt(time());

                    $fileCollection
                        ->reset()
                        ->add($newFile)
                        ->save();
                    $indexedFiles[] = $newFile;
                    $fileIds[]      = '_cc_file:' . $newFile->getId();
                } else {
                    $indexedFiles[] = $existingFile;
                    $fileIds[]      = '_cc_file:' . $existingFile->getId();
                }
            }
        }

        // return also the template for all these files
        $template = $this->getView()->assign('files', $indexedFiles)->render();

        /*$filesInfo = [];
        foreach ($indexedFiles as $indexedFile) {
            $filesInfo[] = $indexedFile->dataMapper();
        }*/

        return $this->json(
            [
                'success'   => 1,
                //'filesInfo' => $filesInfo,
                'fileIds'   => implode(',', $fileIds),
                'container' => $container,
                'template'  => $template
            ]
        );
    }
}
