<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 17.09.2017 @ 09:00
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\System\Domain\Model\UserSession;
use Continut\Core\Utility;

class ExtensionController extends BackendController
{
    // default extension repository, if no custom one is used
    const DEFAULT_REPOSITORY = 'https://www.continut.org/repo/extensions.json';

    // where do we temporarily store our downloaded extension archive files
    const DOWNLOAD_PATH = '/Cache/Temp/Downloads/';

    const EXTENSIONS_PATH = '/Extensions/';

    /**
     * Set the layout to be used on this page by default, for all its actions
     */
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Extension management page for the CMS
     */
    public function gridAction()
    {
        // Grab the complete list of available extensions (enabled or not)
        $allExtensions = $this->getAllExtensions();

        // Grab the list of loaded/enabled extensions
        $loadedExtensions = Utility::getExtensions();

        $this->getView()->assignMultiple(
            [
                'loadedExtensions' => $loadedExtensions,
                'allExtensions'    => $allExtensions
            ]
        );
    }

    /**
     * Activates or deactivates an extension
     */
    public function toggleAction()
    {
        $configFile = __ROOTCMS__ . '/Extensions/extensions.php';

        $activate       = (int)$this->getRequest()->getArgument('op');
        $extensionKey   = explode('_', $this->getRequest()->getArgument('key'), 2);
        $extensionScope = Utility::helper('Text')->stripTags($extensionKey[0]);
        $extensionName  = Utility::helper('Text')->stripTags($extensionKey[1]);
        $extensions     = require $configFile;

        // Are we running in Development, Production, Test mode?
        $environment = Utility::getApplicationEnvironment();

        // activate or disable the extension
        if ($activate) {
            if (!in_array($extensionName, $extensions[ $environment ][ $extensionScope ])) {
                $extensions[ $environment ][ $extensionScope ][ $extensionName ] = $extensionName;
            }
        } else {
            foreach ($extensions[ $environment ][ $extensionScope ] as $id => $extension) {
                if ($extensionName == $extension) {
                    unset($extensions[ $environment ][ $extensionScope ][ $id ]);
                }
            }
        }

        // write the updated configuration to file
        $configString = $this->formattedConfig($extensions);
        file_put_contents($configFile, $configString);

        // we need to invalidate the opcache cache on this file, otherwise we get a delayed behaviour
        // seems that opcache checks for file changes every 2 seconds, so we invalidate this one by hand
        // @see : https://bugs.php.net/bug.php?id=65559
        if (function_exists('opcache_invalidate')) {
            opcache_invalidate($configFile, true);
        }

        Utility::getSession()->addFlashMessage(
            $this->__('backend.extension.operation.' . $activate, ['name' => $extensionName])
        );

        // Clear all configuration cache so our changes are taken into account
        Utility::getCache()->deleteWithWildcard('_cms_configuration_*');

        $this->redirect(
            Utility::helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'grid'])
        );
    }

    /**
     * Fetch the latest list of available extensions from the online repository
     */
    public function checkForUpdatesAction()
    {
        $extensionsData = $this->getExtensionsFromRepository();

        // Grab the list of loaded/enabled extensions in our local CMS instance
        $allExtensions = $this->getAllExtensions();

        // Check if we're running the latest version for each installed extension
        $updateResult       = [];
        $extensionsToUpdate = 0;
        if (!empty($extensionsData)) {
            foreach ($extensionsData['extensions'] as $extension) {
                $extensionId = Utility::helper('Text')->stripTags($extension['key']);
                if (isset($allExtensions[ $extensionId ])) {
                    $currentVersion = $allExtensions[ $extensionId ]['version'];
                    $latestVersion  = Utility::helper('Text')->stripTags($extension['version']);
                    $needsUpdate    = version_compare($currentVersion, $latestVersion, '<');
                    $update         = [];
                    if ($needsUpdate) {
                        $extensionsToUpdate++;
                        $update['link']    = Utility::helper('Url')->linkToPath(
                            'admin',
                            [
                                '_controller' => 'Extension',
                                '_action'     => 'update',
                                'key'         => $extensionId,
                                'from'        => $currentVersion,
                                'to'          => $latestVersion
                            ]
                        );
                        $update['version'] = $latestVersion;
                        $update['label']   = Utility::helper('Localization')
                            ->translate('backend.extension.updateAvailable', ['version' => $latestVersion]);
                        $update['button']  = Utility::helper('Localization')->translate('backend.extension.update');
                    }
                    $updateResult[] = [
                        'type'        => $extension['type'],
                        'name'        => $extensionId,
                        'id'          => $extension['type'] . '_' . $extensionId,
                        'needsUpdate' => $needsUpdate,
                        'update'      => $update
                    ];
                }
            }
            $updateResult['success'] = 1;
            if ($extensionsToUpdate > 0) {
                $updateResult['message'] = $this->__(
                    'backend.extension.grid.checkCompletedExcept',
                    ['count' => $extensionsToUpdate]
                );
            } else {
                $updateResult['message'] = $this->__('backend.extension.grid.checkCompleted');
            }
        } else {
            $updateResult['message'] = $this->__('backend.extension.grid.noExtensionsFound');
        }

        // Return a JSON object with all the extensions that need update or are already up-to-date
        return $this->json($updateResult);
    }

    /**
     * Update an existing extension to a newer version
     */
    public function updateAction()
    {
        $from = $this->getRequest()->getArgument('from');
        $to   = $this->getRequest()->getArgument('to');
        $key  = $this->getRequest()->getArgument('key');

        // Recheck if we really need to update them
        // The version we're updating to needs to be newer than the current one, obviously
        $needsUpdate = version_compare($from, $to, '<');

        if ($needsUpdate) {
            // Load all the extensions available in the online repository
            $extensionsData = $this->getExtensionsFromRepository();

            $found = false;
            if (isset($extensionsData['extensions'])) {
                foreach ($extensionsData['extensions'] as $extension) {
                    // Grab our extension
                    if ($extension['key'] == $key) {
                        // See if we're fetching the same version as defined in the repository
                        if ($extension['version'] == $to) {
                            // The 'link' defines where we can download the archive version of the extension
                            if ($extension['link']) {
                                // You should only be dealing with ZIP archives for deploying your extensions
                                // as it's the only supported format so far
                                $tempFile = __ROOTCMS__ . self::DOWNLOAD_PATH . 'ext_' . $key . '_' . $to . '.zip';
                                // Download the extension on our "Temp" folder
                                Utility::getUrl($extension['link'], 'GET', ['sink' => $tempFile]);
                                $md5File = md5_file($tempFile);
                                if (isset($extension['md5']) && $md5File == $extension['md5']) {
                                    // Extract the archived extension (or at least try to)
                                    $zip = new \ZipArchive();
                                    if ($zip->open($tempFile)) {
                                        $extType      = $extension['type'];
                                        $type         = (isset($extType) && in_array($extType, ['Local', 'Community']))
                                            ? $extension['type']
                                            : 'Community';
                                        $pathToFolder = __ROOTCMS__ . self::EXTENSIONS_PATH . $type . DS . $key;
                                        // Delete the existing folder first
                                        Utility::helper('Folder')->delete($pathToFolder);
                                        // Then extract the updated extension
                                        $zip->extractTo($pathToFolder);
                                        $zip->close();
                                        Utility::getSession()->addFlashMessage(
                                            $this->__('backend.extension.update.success', ['name' => $key, 'v' => $to])
                                        );
                                    } else {
                                        Utility::getSession()->addFlashMessage(
                                            $this->__('backend.extension.update.extractError', ['name' => $key]),
                                            UserSession::FLASH_ERROR
                                        );
                                    }
                                } else {
                                    Utility::getSession()->addFlashMessage(
                                        $this->__('backend.extension.update.invalidChecksum'),
                                        UserSession::FLASH_ERROR
                                    );
                                }
                            }
                        } else {
                            Utility::getSession()->addFlashMessage(
                                $this->__('backend.extension.update.invalidVersion', ['v' => $to, 'name' => $key]),
                                UserSession::FLASH_ERROR
                            );
                        }
                        $found = true;
                    }
                }
            }

            if (!$found) {
                Utility::getSession()->addFlashMessage(
                    $this->__('backend.extension.update.invalidRepo', ['name' => $key]),
                    UserSession::FLASH_ERROR
                );
            }

            // @TODO Add extension update code
            // Download from the repo, unzip, replace
        }

        $this->redirect(
            Utility::helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'grid'])
        );
    }

    /**
     * Loads all the extensions present on the system, of all types: System, Local or Community extensions
     *
     * @return array
     */
    protected function getAllExtensions()
    {
        return array_merge(
            Utility::loadExtensionsConfigurationFromFolder(__ROOTCMS__ . DS . 'Extensions', 'Local', false),
            Utility::loadExtensionsConfigurationFromFolder(__ROOTCMS__ . DS . 'Extensions', 'Community', false),
            Utility::loadExtensionsConfigurationFromFolder(__ROOTCMS__ . DS . 'Extensions', 'System', false)
        );
    }

    /**
     * Get the URL to the extensions repository, either the official one or a custom one that you use
     *
     * @return string
     */
    protected function getExtensionsRepository()
    {
        // if a custom repository url is defined, use it, if not fallback to the default one
        // the repository is just the url where it will look for extensions
        $repository = Utility::getConfiguration('System/Repository/Url');
        if (is_null($repository)) {
            $repository = self::DEFAULT_REPOSITORY;
        }

        return $repository;
    }

    /**
     * Connects to the extension repository and returns all the available extension data, as array
     *
     * @return array
     */
    protected function getExtensionsFromRepository()
    {
        // Get the repo url
        $repository = $this->getExtensionsRepository();

        // Fetch the JSON object from the repository with all the extensions data
        $listOfExtensions = Utility::getUrl($repository);
        // Convert it to an array
        $extensionsData = json_decode($listOfExtensions, true);

        return $extensionsData;
    }

    /**
     * Formats the extensions data
     *
     * @param $extensions
     *
     * @return string
     */
    protected function formattedConfig($extensions)
    {
        // we could use var_export but then we'd have no control over the output and formatting
        // so we're going to format the file ourselves
        $configString = "<?php\n\nreturn [\n";
        foreach ($extensions as $scope => $extensionData) {
            $configString .= sprintf('  "%s" => [' . PHP_EOL, $scope);
            foreach ($extensionData as $type => $extension) {
                if (!empty($extension)) {
                    $configString .= sprintf(
                        '    "%s" => [' . PHP_EOL . '      "%s"' . PHP_EOL . '    ],' . PHP_EOL,
                        $type,
                        implode('",' . PHP_EOL . '      "', $extension)
                    );
                } else {
                    $configString .= sprintf('    "%s" => [],' . PHP_EOL, $type);
                }
            }
            $configString .= "  ],\n";
        }
        $configString .= "];\n";

        return $configString;
    }
}
