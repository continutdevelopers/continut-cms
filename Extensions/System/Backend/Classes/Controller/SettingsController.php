<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.12.2015 @ 14:46
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\System\Backend\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\System\Domain\Model\UserSession;
use Continut\Core\Utility;

class SettingsController extends BackendController
{
    /**
     * Set the layout to be used on this page by default, for all actions
     */
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Default action
     */
    public function indexAction()
    {
    }

    /**
     * Show and handle domains and domainUrl settings
     */
    public function domainsAction()
    {
        $allDomains = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
            ->whereDeleted(false);

        $this->getView()->assign('allDomains', $allDomains);
    }

    /**
     * Create a new domain
     */
    public function newDomainAction()
    {
        $domain = Utility::createInstance('Continut\Core\System\Domain\Model\Domain');

        $this->getView()->assign('domain', $domain);
    }

    /**
     * Edit Domain data
     */
    public function editDomainAction()
    {
        $domainId = $this->getRequest()->getArgument('id');

        $domainCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection');
        $domain           = $domainCollection->findById($domainId);

        $this->getView()->assign('domain', $domain);
    }

    /**
     * Save Domain data
     */
    public function saveDomainAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        // reference the collection
        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection');

        // new domain, to add
        if ($id == 0) {
            $domain = Utility::createInstance('Continut\Core\System\Domain\Model\Domain');
        } // edit and save
        else {
            $domain = $domainsCollection->findById($id);
        }
        $domain->update($data);

        if ($domain->validate()) {
            $domainsCollection
                ->reset()
                ->add($domain)
                ->save();

            // redirect to the "domainsAction" since all went well and data is saved
            $this->redirect(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_controller' => 'Settings',
                        '_action'     => 'domains'
                    ]
                )
            );
        }

        $this->getView()->assign('domain', $domain);
    }

    /**
     * Delete selected domain
     */
    public function deleteDomainAction()
    {
        $domainId = (int)$this->getRequest()->getArgument('id');

        $domain = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
            ->findById($domainId);

        if ($domain) {
            $domain->setIsDeleted(true);
            Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
                ->add($domain)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('general.flash.recordDeleted'));
        }

        $this->redirect(
            Utility::helper('Url')->linkToPath(
                'admin',
                [
                    '_controller' => 'Settings',
                    '_action'     => 'domains'
                ]
            )
        );
    }

    /**
     * Edit a language/domainUrl
     */
    public function editDomainUrlAction()
    {
        $domainUrlId = $this->getRequest()->getArgument('id', 0);

        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
            ->findAll();
        $domainUrl         = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
            ->findById($domainUrlId);

        $this->getView()->assignMultiple([
            'domainUrl' => $domainUrl,
            'domains'   => $domainsCollection
        ]);
    }

    public function newDomainUrlAction()
    {
        $domainUrl = Utility::createInstance('Continut\Core\System\Domain\Model\DomainUrl');

        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
            ->findAll();

        $this->getView()->assignMultiple([
            'domainUrl' => $domainUrl,
            'domains'   => $domainsCollection
        ]);
    }

    /**
     * Save domainUrl object
     */
    public function saveDomainUrlAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        $languagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection');

        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection')
            ->findAll();

        // new domainUrl, to add
        if ($id == 0) {
            $domainUrl = Utility::createInstance('Continut\Core\System\Domain\Model\DomainUrl');
        } // edit and save
        else {
            $domainUrl = $languagesCollection->findById($id);
        }
        $domainUrl->update($data);

        if ($domainUrl->validate()) {
            $languagesCollection
                ->reset()
                ->add($domainUrl)
                ->save();

            // redirect to the "domainsAction" since all went well and data is saved
            $this->redirect(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_controller' => 'Settings',
                        '_action'     => 'domains'
                    ]
                )
            );
        }

        $this->getView()->assignMultiple([
            'domainUrl' => $domainUrl,
            'domains'   => $domainsCollection
        ]);
    }

    /**
     * Delete selected domain
     */
    public function deleteDomainUrlAction()
    {
        $domainUrlId = (int)$this->getRequest()->getArgument('id');

        $domainUrl = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
            ->findById($domainUrlId);

        if ($domainUrl) {
            if ($domainUrl->getIsDefault()) {
                Utility::getSession()->addFlashMessage(
                    $this->__('backend.domainUrl.message.cannotDeleteDefault'),
                    UserSession::FLASH_ERROR
                );
            } else {
                $domainUrl->setIsDeleted(true);
                Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                    ->add($domainUrl)
                    ->save();

                Utility::getSession()->addFlashMessage($this->__('general.flash.recordDeleted'));
            }
        }

        $this->redirect(
            Utility::helper('Url')->linkToPath(
                'admin',
                [
                    '_controller' => 'Settings',
                    '_action'     => 'domains'
                ]
            )
        );
    }

    /**
     * Save config data into sys_configuration
     */
    public function saveSettingsAction()
    {
        // get currenty selected domain and language
        $domainId   = 0;
        $languageId = 0;
        if ($this->getSession()->has('configurationSite')) {
            $site  = $this->getSession()->get('configurationSite');
            $parts = explode('_', $site);
            if ($parts[0] == 'domain') {
                $domainId = (int)$parts[1];
            }
            if ($parts[0] == 'url') {
                $languageId = (int)$parts[1];
                $domainUrl  = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainUrlCollection')
                    ->findById($languageId);
                $domainId   = $domainUrl->getDomainId();
            }
        }

        $configurationCollection = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\ConfigurationCollection'
        );
        $deleteCollection = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\ConfigurationCollection'
        );
        $data = $this->getRequest()->getArgument('data');

        // store configuration that should be deleted here
        $keysToDelete = [];
        foreach ($data as $key => $value) {
            if ($key === 'delete' && is_array($data['delete'])) {
                foreach ($data['delete'] as $deleteKey => $deleteValue) {
                    $deleteValue = (int)$deleteValue;
                    if ($deleteValue === 1) {
                        $deleteKey = str_replace('_', '/', $deleteKey);
                        $keysToDelete[]= $deleteKey;
                    }
                }
            } else {
                $reference = str_replace('_', '/', $key);
                $model     = Utility::createInstance('Continut\Core\System\Domain\Collection\ConfigurationCollection')
                    ->findWithReference($domainId, $languageId, $reference);
                // update or create new configuration value
                if ($model) {
                    $model->setValue($value);
                } else {
                    $model = Utility::createInstance('Continut\Core\System\Domain\Model\Configuration');
                    $model
                        ->setValue($value)
                        ->setDomainId($domainId)
                        ->setReference($reference)
                        ->setLanguageId($languageId);
                }
                $configurationCollection
                    ->add($model);
            }
        }
        // save new configuration or update existing one
        $configurationCollection->save();

        // delete keys marked as such
        if (!empty($keysToDelete)) {
            $deleteCollection->findWithReferences($domainId, $languageId, $keysToDelete);
            $deleteCollection->delete();
        }

        Utility::getSession()->addFlashMessage($this->__('general.flash.recordSaved'));

        $this->redirect(
            Utility::helper('Url')->linkToPath(
                'admin',
                [
                    '_controller' => 'Settings',
                    '_action'     => 'configure',
                    'section'     => $this->getRequest()->getArgument('section')
                ]
            )
        );
    }

    /**
     * Show media/files settings
     */
    public function configureAction()
    {
        $domainsCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\DomainCollection');
        $domainsCollection->whereDeleted(false);

        if ($this->getSession()->has('configurationSite')) {
            $this->getSession()->set(
                'configurationSite',
                $this->getRequest()->getArgument('configuration_site', $this->getSession()->get('configurationSite'))
            );
        } else {
            $this->getSession()->set(
                'configurationSite',
                (string)$this->getRequest()->getArgument('configuration_site', 0)
            );
        }

        $configurationCollection = Utility::createInstance(
            'Continut\Core\System\Domain\Collection\ConfigurationCollection'
        );

        $config = [];

        // if we are in global scope, get global config only
        if ($this->getSession()->get('configurationSite') === '0') {
            $configurationCollection->whereGlobalScope();
        }

        // or get domain level
        if (mb_strpos($this->getSession()->get('configurationSite'), 'domain_') === 0) {
            $parts    = explode('_', $this->getSession()->get('configurationSite'));
            $domainId = (int)$parts[1];
            $configurationCollection->whereDomainScope($domainId);
        }

        // or language one
        if (mb_strpos($this->getSession()->get('configurationSite'), 'url_') === 0) {
            $parts      = explode('_', $this->getSession()->get('configurationSite'));
            $languageId = (int)$parts[1];
            $configurationCollection->whereLanguageScope($languageId);
        }

        foreach ($configurationCollection->getAll() as $configuration) {
            $config[ $configuration->getReference() ] = $configuration->getValue();
        }

        // a section is made up of it's parent id and it's actual id
        // eg: system:media refers to the 'media' section inside 'system'
        $selected = $this->getRequest()->getArgument('section');
        $section  = explode('_', $selected, 2);
        $sParent  = $section[0];
        $sChild   = $section[1];

        $extensionsConfig = Utility::getExtensionSettings();

        $settings = [];
        foreach ($extensionsConfig as $extensionKey => $extConfig) {
            if (isset($extConfig['backend']['settings'])) {
                $settings = array_merge($settings, $extConfig['backend']['settings']);
            }
        }

        // Remove all the fields and the panels that should not be displayed on the current scope
        if (isset($settings[$sParent]['sections'][$sChild])) {
            $child = $settings[$sParent]['sections'][$sChild];
            if (isset($child['panels'])) {
                foreach ($child['panels'] as $panelId => $panel) {
                    if (isset($panel['fields'])) {
                        foreach ($panel['fields'] as $fieldId => $field) {
                            $visibility = Utility::helper('FormSettings')->checkVisibility($field['visibility']);
                            if ($visibility['scope'] != 'global' && $visibility['minimal'] == 'global' ||
                                $visibility['scope'] == 'url' && $visibility['minimal'] == 'domain') {
                                unset($settings[$sParent]['sections'][$sChild]['panels'][$panelId]['fields'][$fieldId]);
                            }
                        }
                        if (empty($settings[$sParent]['sections'][$sChild]['panels'][$panelId]['fields'])) {
                            unset($settings[$sParent]['sections'][$sChild]['panels'][$panelId]);
                        }
                    }
                }
            }
        }

        $this->getView()->assignMultiple(
            [
                'domains'           => $domainsCollection,
                'config'            => $config,
                'configurationSite' => $this->getSession()->get('configurationSite'),
                'settings'          => $settings,
                'sectionParent'     => $sParent,
                'sectionChild'      => $sChild,
                'selected'          => $selected
            ]
        );
    }
}
