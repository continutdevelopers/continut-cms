var mediaModal  = null;
var mediaTarget = null;
var isDragging  = false;

$(document).ready(function () {
    $('.date-picker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });

    // mediaField open media popup on click
    $('#container').on('click', '.mediafield-image', function () {
        var $button = $(this);
        var url = $button.data("url");
        mediaTarget = $button.data("target");
        mediaModal = new BootstrapDialog({
            title: 'Media',
            cssClass: 'media-dialog',
            size: BootstrapDialog.SIZE_WIDE,
            closable: true,
            closeByBackdrop: false,
            closeByKeyboard: true,
            message: $('<div></div>').load(url),
            onshown: function (dialogRef) {
                $("#file_content").data("maxFiles", $button.data("maxFiles"));
                $("#file_content").data("container", $button.data("container"));
            }
        });
        mediaModal.open();
    });

    // dynamic menus show loading icons inside them (like the "Clear cache" menu that shows a loading icon while
    // it makes an ajax call to clear the cache
    $('.dynamic-menu').on('click', function (event) {
        event.preventDefault();

        var $target = $($(this).data('target'));
        var oldClasses = $target.find('.fa').first().attr('class');
        $target.find('.fa').first().attr('class', 'fa fa-circle-o-notch fa-spin fa-fw margin-bottom');

        $.getJSON($(this).attr('href')).done(function () {
            $target.find('.fa').first().attr('class', oldClasses);
        });
    });

    // Use it whenever you want to make an ajax call and at the end
    // remove a target div specified in the data-target attribute
    $('.hide-section').on('click', function (event) {
        event.preventDefault();

        var $target = $($(this).data('target'));
        $.getJSON($(this).attr('href')).done(function () {
            $target.remove();
        });
    });

    // settings visibility checboxk [global, domain, url]
    $('.settings-checkbox').on('change', function (event) {
        $($(this).data('for')).attr('disabled', !this.checked);
        $(this).prev().val(this.checked ? '0' : '1');

        $('.selectpicker').selectpicker('refresh');
    });

    // handling of update extension behaviour
    $('.update-extensions').on('click', function (event) {
        event.preventDefault();

        $.getJSON($(this).attr('href')).done(function (data) {
            if (data) {
                $.each(data, function (key, extension) {
                    if (extension.needsUpdate) {
                        var $updateBlock = $('#extension_' + extension.id + ' .update');
                        $updateBlock.empty();
                        $updateBlock.html(extension.update.label +
                            '<br/><a class="btn btn-success" href="' + extension.update.link +
                            ' ">' + extension.update.button + '</a>');
                    }
                })
            }
        });
    });
});

// utility functions
function appendAsset(url, type, callback) {
    if ($('head script[src="' + url + '"]').length > 0) {
        if (callback) {
            callback()
        }
        return;
    }

    if (type === "js") {
        var script  = document.createElement('script');
        script.type = "text/javascript";
        script.src  = url;
    } else {
        var script = document.createElement('link');
        script.type = "text/css";
        script.rel  = "stylesheet";
        script.href = url;
    }
    if (script.readyState) {  //ie
        script.onreadystatechange = function() {
            if (script.readyState === "loaded" || script.readyState === "complete" ) {
                script.onreadystatechange = null;
                if (callback) {
                    callback()
                }
            }
        };
    } else { // other browsers
        script.onload = function() {
            if (callback) {
                callback()
            }
        };
    }
    document.getElementsByTagName("head")[0].appendChild(script);
}

// Whenever an ajax request is completed in the backend, check if the Continut-Redirect headers are set
// and if so, redirect to this page (session expired)
$(document).ajaxSuccess(function (event, xhr, settings) {
    if (xhr.responseJSON && xhr.responseJSON.hasOwnProperty('success')) {
        // if there's no message to display, skip it
        if (!xhr.responseJSON.hasOwnProperty('message')) {
            return;
        }

        var messageType = 'info';
        if (!xhr.responseJSON.success) {
            messageType = 'danger';
        }
        $.notify({
            message: xhr.responseJSON.message
        }, {
            placement: {from: 'bottom'},
            type: messageType
        });
    }
    if (xhr.getResponseHeader('Continut-Redirect')) {
        window.location = xhr.getResponseHeader('Continut-Redirect');
    }
});