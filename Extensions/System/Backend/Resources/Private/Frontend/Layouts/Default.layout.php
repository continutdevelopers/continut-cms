<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://<?= $this->getUrl() ?>">
    <link rel="shortcut icon" href="/Public/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="/Public/apple-touch-icon.png?v=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/Public/favicon-32x32.png?v=1">
    <link rel="icon" type="image/png" sizes="16x16" href="/Public/favicon-16x16.png?v=1">
    <link rel="manifest" href="/Public/manifest.json?v=1">
    <link rel="mask-icon" href="/Public/safari-pinned-tab.svg?v=1" color="#5bbad5">
    <link rel="shortcut icon" href="/Public/favicon.ico?v=1">
    <meta name="apple-mobile-web-app-title" content="Continut CMS">
    <meta name="application-name" content="Continut CMS">
    <meta name="msapplication-config" content="/Public/browserconfig.xml?v=1">
    <meta name="theme-color" content="#ffffff">
    <title><?= $this->getTitle() ?></title>

<?=
    $this
        ->addCssAsset(['identifier' => 'local', 'extension' => 'Backend', 'file' => 'login/login.css'])
        ->renderAssets();
?>

    <meta name="description" content="Conținut CMS"/>
    <meta name="keywords" content=""/>

</head>
<body class="login-page">
    <?= $this->showContent(); ?>
<?=
    $this
    ->addJsAsset(['identifier' => 'jquery', 'extension' => 'Backend', 'file' => 'jquery/jquery-3.1.1.min.js'])
    ->renderAssets();
?>
</body>
</html>
