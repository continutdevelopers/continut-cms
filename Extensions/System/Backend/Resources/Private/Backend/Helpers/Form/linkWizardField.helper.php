<?php
    $maxItemsAllowed = (isset($options['maxAllowed'])) ? (int)$options['maxAllowed'] : 1;
    $filterCollection = (isset($filters['collection'])) ? $filters['collection'] : '';
?>
<div class="form-group">
    <?= $fieldLabel ?>
    <?php if (isset($filters['type'])): ?>
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-link"></i></span>
        <select multiple class="form-control" id="<?= $fieldId ?>" name="<?= $fieldName ?>">
            <?php if ($selectedValues): ?>
                <?php foreach ($selectedValues as $selected): ?>
                    <option selected value="<?= $selected['value'] ?>"><?= $selected['text'] ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
        <span class="input-group-btn">
            <button class="btn btn-danger" type="button" title="<?= $this->__('backend.content.removeLink') ?>">
                <i class="fa fa-trash"></i>
            </button>
        </span>
    </div>
    <?php else: ?>
        <p class="text-danger"><?= $this->__('backend.content.field.linkWizard.typeMissing') ?></p>
    <?php endif; ?>
</div>
<?php if (isset($filters['type'])): ?>
<script type="text/javascript">
    $('#<?= $fieldId ?>').tokenize2({
        placeholder: '<?= $this->__('backend.content.linkMissing') ?>',
        tokensMaxItems: <?= $maxItemsAllowed ?>,
        searchMinLength: 2,
        dropdownMaxItems: 25,
        dataSource: function(term, object){
            $.ajax('<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Filter', '_action' => 'filterTags', 'type' => $filters['type'], 'collection' => $filterCollection]) ?>', {
                data: { search: term, start: 0 },
                dataType: 'json',
                success: function(data){
                    var $items = [];
                    $.each(data, function(k, v){
                        $items.push(v);
                    });
                    object.trigger('tokenize:dropdown:fill', [$items]);
                }
            });
        }
    });
</script>
<?php endif; ?>