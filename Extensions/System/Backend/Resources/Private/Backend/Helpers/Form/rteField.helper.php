<div class="form-group <?= (isset($errors) && ($errors['errorsClass'])) ? $errors['errorsClass'] : '' ?>">
    <?= $fieldLabel ?>
    <textarea id="<?= $fieldId ?>" name="<?= $fieldName ?>" class="form-control rte"><?= $value ?></textarea>
    <?= (isset($errors) && ($errors['errorsBlock'])) ? $errors['errorsBlock'] : '' ?>
</div>
<script type="text/javascript">
    CKEDITOR.replace('<?= $fieldId ?>',
        {
            language: '<?= $this->helper("Locale")->localeToEditor(\Continut\Core\Utility::getConfiguration("System/Locale")) ?>'
        }
    );
</script>