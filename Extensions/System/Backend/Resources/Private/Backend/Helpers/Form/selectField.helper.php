<?php $readonly = (isset($options['readonly']) && $options['readonly'] === true) ? 'readonly="readonly"' : ''; ?>
<?php $disabled = (isset($options['disabled']) && $options['disabled'] === true) ? 'disabled="disabled"' : ''; ?>
<?php $cssPrefix = (isset($options['classPrefix'])) ? $options['classPrefix']: ''; ?>
<div class="form-group">
    <?= $fieldLabel ?>
    <select name="<?= $fieldName ?>" id="<?= $fieldId ?>" <?= $disabled ?> <?= $readonly ?> class="form-control selectpicker">
        <?php foreach ($values as $group => $data): ?>
            <?php if (is_array($data)): ?>
                <?php if (!empty($group)): ?>
                    <optgroup label="<?= $group ?>">
                <?php endif; ?>
                <?php if ($data): ?>
                    <?php foreach ($data as $key => $title): ?>
                        <?php $cssClass = (!empty($cssPrefix)) ? 'data-icon="' . $cssPrefix . '-' . $key . '"' : ''; ?>
                        <?php if ($key == $value): ?>
                            <option selected <?= $cssClass ?> value="<?= $key ?>"><?= $title ?></option>
                        <?php else: ?>
                            <option <?= $cssClass ?> value="<?= $key ?>"><?= $title ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if (!empty($group)): ?>
                    </optgroup>
                <?php endif; ?>
            <?php else: ?>
                <?php $cssClass = (!empty($cssPrefix)) ? 'data-icon="' . $cssPrefix . '-' . $group . '"' : ''; ?>
                <?php if ($group == $value): ?>
                    <option selected <?= $cssClass ?> value="<?= $group ?>"><?= $data ?></option>
                <?php else: ?>
                    <option <?= $cssClass ?> value="<?= $group ?>"><?= $data ?></option>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </select>
</div>

<script type="text/javascript">
    $('.selectpicker').selectpicker({});
</script>