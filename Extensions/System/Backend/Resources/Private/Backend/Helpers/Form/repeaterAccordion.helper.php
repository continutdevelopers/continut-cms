<?php $this->helper('Form')->setRepeaterMode(true); ?>
<?php $this->helper('Form')->setRepeaterName($name); ?>
<div data-type="repeater" id="repeater_container_<?= $name ?>" class="content-type-repeater">
    <div class="form-group">
        <label class="control-label"><?= $label ?></label>
        <div class="panel-group" id="repeater_<?= $name ?>" role="tablist" aria-multiselectable="true">
            <?php $repeaterElementId = 0; ?>
            <?php foreach ($values as $variables): ?>
                <?php $repeaterElementId++; ?>
                <?php $this->helper('Form')->setRepeaterElementId($repeaterElementId); ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading<?= $repeaterElementId ?>">
                        <div class="btn-group pull-right flip" role="group" aria-label="...">
                            <button type="button" class="sort-handler btn btn-default"
                                    title="<?= $this->__('general.move') ?>"><i class="fa fa-fw fa-arrows"></i></button>
                            <button type="button" class="remove-record btn btn-danger"
                                    title="<?= $this->__('general.delete') ?>"><i class="fa fa-fw fa-trash"></i>
                            </button>
                        </div>
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#repeater_<?= $name ?>"
                               href="#collapse_<?= $repeaterElementId ?>" aria-expanded="true"
                               aria-controls="collapse_<?= $repeaterElementId ?>"><i
                                        class="fa fa-fw fa-pencil"></i><?= $this->__('backend.content.repeater.elementLabel', ['id' => $repeaterElementId]) ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_<?= $repeaterElementId ?>" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading<?= $repeaterElementId ?>">
                        <div class="panel-body">
                            <?= $fieldsView->assignMultiple($variables)->render(); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <p>
        <button type="button" class="btn btn-success add-repeater-element"><i
                    class="fa fa-fw fa-plus"></i> <?= $this->__('backend.content.repeater.addNew') ?></button>
    </p>
</div>
<?php $this->helper('Form')->setRepeaterMode(false); ?>

<script type="text/javascript">
    // setup repeater functionality
    $('#repeater_container_<?= $name ?>').each(function () {
        // add the repeater on click element creation
        var $repeater = $(this);

        $(this)
            .attr('data-template', '<?= $template ?>')
            .attr('data-extension', '<?= $extension ?>')
            .attr('data-name', '<?= $name ?>')
            .attr('data-limit', <?= $limit ?>)
            .attr('data-element-label', '<?= $this->__('backend.content.repeater.elementLabel') ?>');

        if (($repeater.find('.collapse').length + 1) >= $repeater.data('limit')) {
            $repeater.find('.add-repeater-element').hide();
        }

        $('.remove-record').on('click', $(this), function () {
            $(this).closest('.panel').remove();
        });

        $(this).find('.add-repeater-element').on('click', function () {
            var limit = $repeater.data('limit');
            var lastIndex = $repeater.find('.collapse').length + 1;

            // hide the button if we try to create more elements than the repeater allows
            if (lastIndex >= limit) {
                $(this).hide();
            }

            var action       = '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'repeaterTemplate']) ?>';
            var template     = $repeater.data('template');
            var extension    = $repeater.data('extension');
            var name         = $repeater.data('name');
            var elementLabel = $repeater.data('element-label');

            // make an ajax call to fetch and render the template, then add it in the accordion
            $.get(action, {
                repeaterElementId: lastIndex,
                repeaterName: name,
                template: template,
                extension: extension
            }, function (data) {
                var accordionId = $repeater.find('.panel-group').first().attr('id');
                elementLabel = elementLabel.replace('%(id)', lastIndex);

                $repeater.find('.panel-group').append('<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading' + lastIndex + '"><div class="btn-group pull-right flip" role="group" aria-label="..."> <button type="button" class="sort-handler btn btn-default" title="<?= $this->__('general.move') ?>"><i class="fa fa-fw fa-arrows"></i></button> <button type="button" class="remove-record btn btn-danger" title="<?= $this->__('general.delete') ?>"><i class="fa fa-fw fa-trash"></i></button> </div><h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#' + accordionId + '" href="#collapse' + lastIndex + '" aria-expanded="false" aria-controls="collapse' + lastIndex + '"><i class="fa fa-fw fa-pencil"></i>' + elementLabel + '</a></h4></div><div id="collapse' + lastIndex + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + lastIndex + '"> <div class="panel-body">' + data + '</div></div></div>');
            });
        });
    });

    // make the list of items sortable
    $('#repeater_<?= $name ?>').sortable({
        handle: '.sort-handler',
        draggable: '.panel-default',
        sort: true
    });
</script>