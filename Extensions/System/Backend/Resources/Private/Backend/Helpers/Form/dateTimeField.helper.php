<div class="form-group <?= $errors['errorsClass'] ?>">
    <?= $fieldLabel ?>
    <input id="<?= $fieldId ?>" type="text" data-field="datetime"
           class="form-control date-picker" value="<?= $value ?>" name="<?= $fieldName ?>"/>
    <?= $errors['errorsBlock'] ?>

    <script type="text/javascript">
        $('#<?= $fieldId ?>').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            locale: {
                format: moment.PHPconvertFormat('<?= $this->__('locale.datetime.format.long') ?>'),
                applyLabel: "<?= $this->__('backend.daterangepicker.applyLabel') ?>",
                cancelLabel: "<?= $this->__('backend.daterangepicker.cancelLabel') ?>",
                fromLabel: "<?= $this->__('backend.daterangepicker.fromLabel') ?>",
                toLabel: "<?= $this->__('backend.daterangepicker.toLabel') ?>",
                customRangeLabel: "<?= $this->__('backend.daterangepicker.customRangeLabel') ?>",
                weekLabel: "<?= $this->__('backend.daterangepicker.weekLabel') ?>",
                daysOfWeek: [
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.Su') ?>",
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.Mo') ?>",
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.Tu') ?>",
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.We') ?>",
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.Th') ?>",
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.Fr') ?>",
                    "<?= $this->__('backend.daterangepicker.daysOfWeek.Sa') ?>",
                ],
                monthNames: [
                    "<?= $this->__('backend.daterangepicker.monthNames.January') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.February') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.March') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.April') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.May') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.June') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.July') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.August') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.September') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.October') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.November') ?>",
                    "<?= $this->__('backend.daterangepicker.monthNames.December') ?>",
                ]
            }
        }, function(chosen_date) {
            $('#<?= $fieldId ?>').val(chosen_date.format(moment.PHPconvertFormat('<?= $this->__('locale.datetime.format.long') ?>')));
        });
    </script>
</div>