<?php $readonly = (isset($options['readonly']) && $options['readonly'] === true) ? 'readonly="readonly"' : ''; ?>
<?php $disabled = (isset($options['disabled']) && $options['disabled'] === true) ? 'disabled="disabled"' : ''; ?>
<div class="form-group <?= (isset($errors) && ($errors['errorsClass'])) ? $errors['errorsClass'] : '' ?>">
    <?= $fieldLabel ?>
    <?php if (isset($options['prefix'])): ?>
        <?php $prefix = $options['prefix']; ?>
        <div class="input-group">
            <span class="input-group-addon"><?= $prefix ?></span>
            <input id="<?= $fieldId ?>" type="text" class="form-control" <?= $disabled ?> <?= $readonly ?>
                   value="<?= $value ?>" name="<?= $fieldName ?>"/>
        </div>
    <?php else: ?>
        <input id="<?= $fieldId ?>" type="text" class="form-control" <?= $disabled ?> <?= $readonly ?>
               value="<?= $value ?>" name="<?= $fieldName ?>"/>
    <?php endif; ?>
    <?= (isset($errors) && ($errors['errorsBlock'])) ? $errors['errorsBlock'] : '' ?>
</div>