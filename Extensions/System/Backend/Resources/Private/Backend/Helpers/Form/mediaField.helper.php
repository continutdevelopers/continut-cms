<?php
$options = isset($options) ? $options : ['maxFiles' => 1];
?>
<div class="form-group field-type-media">
    <?= $fieldLabel ?>
    <input id="<?= $fieldId ?>" type="hidden" name="<?= $fieldName ?>" value="<?= $value ?>"/>
    <div id="<?= $fieldId ?>_container" class="media-images-wrapper">
        <?php foreach ($fileReferences as $fileReference): ?>
            <?= $this->partial('Media/file', 'Backend', 'Backend', ['fileReference' => $fileReference, 'file' => $fileReference->getFile()]) ?>
        <?php endforeach ?>
    </div>
    <p>
        <button type="button"
                style="<?= (sizeof($fileReferences) == 0 || (isset($options['maxFiles']) && sizeof($fileReferences) < $options['maxFiles'])) ? '' : 'display: none'; ?>"
                data-url="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'index', 'handling' => \Continut\Extensions\System\Backend\Classes\Controller\MediaController::HANDLING_TYPE_SELECT]) ?>"
                data-target="#<?= $fieldId ?>"
                data-max-files="<?= $options['maxFiles'] ?>"
                data-container="#<?= $fieldId ?>_container"
                class="btn btn-success mediafield-image">
            <i class="fa fa-fw fa-plus"></i> <?= $this->__('backend.media.wizard.addFiles') ?>
        </button>
    </p>
</div>
<script type="text/javascript">
    $('.remove-reference').on('click', function (event) {
        event.preventDefault();

        var url = $(this).attr('href');
        var $button = $(this);

        $.getJSON(url).done(function (data) {
            // get the hidden input holding all the file references for this element in a CSV list
            var $input = $button.closest('.field-type-media').children('input[type="hidden"]');
            // get the input's value and search for the id we're supposed to remove
            var values = $input.val().split(',');
            var search = values.indexOf(data.element.id)
            values.splice(search, 1);
            // remove media block
            $button.closest('.media').remove();
            // update the input's value, with the removed id
            $input.val(values.toString());

            // once a file is removed, check if we should not redisplay the "add files" button
            var $addButton = $button.closest('.field-type-media').find('.mediafield-image');
            if ($addButton.data('max-files')) {
                var mediaCount = $button.closest('.media-images-wrapper').find('.media').length;
                if ($addButton.data('max-files') <= mediaCount) {
                    $addButton.show();
                }
            }
        });
    });
</script>