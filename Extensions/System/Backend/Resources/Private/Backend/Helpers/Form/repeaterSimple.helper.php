<?php $this->helper('Form')->setRepeaterMode(true); ?>
<?php $this->helper('Form')->setRepeaterName($name); ?>
<div data-type="repeater" id="repeater_container_<?= $name ?>" class="content-type-repeater">
    <label class="control-label"><?= $label ?></label>
    <div class="panel-group" id="repeater_<?= $name ?>" role="tablist" aria-multiselectable="true">
        <?php $repeaterElementId = 0; ?>
        <?php foreach ($values as $variables): ?>
            <?php $repeaterElementId++; ?>
            <?php $this->helper('Form')->setRepeaterElementId($repeaterElementId); ?>
            <div class="repeater-element">
                <?= $fieldsView->assignMultiple($variables)->render(); ?>
            </div>
        <?php endforeach; ?>
    </div>
    <p>
        <button type="button" class="btn btn-success add-repeater-element">
            <i class="fa fa-fw fa-plus"></i> <?= $this->__('backend.content.repeater.addNew') ?>
        </button>
    </p>
</div>
<?php $this->helper('Form')->setRepeaterMode(false); ?>

<script type="text/javascript">
    // setup repeater functionality
    $('#repeater_container_<?= $name ?>').each(function () {
        // add the repeater on click element creation
        var $repeater = $(this);

        $(this)
            .attr('data-template', '<?= $template ?>')
            .attr('data-extension', '<?= $extension ?>')
            .attr('data-name', '<?= $name ?>')
            .attr('data-limit', <?= $limit ?>)
            .attr('data-element-label', '<?= $this->__('backend.content.repeater.elementLabel') ?>');

        if (($repeater.find('.collapse').length + 1) >= $repeater.data('limit')) {
            $repeater.find('.add-repeater-element').hide();
        }

        $('.remove-record').on('click', $(this), function () {
            $(this).closest('.panel').remove();
        });

        $(this).find('.add-repeater-element').on('click', function () {
            var limit = $repeater.data('limit');
            var lastIndex = $repeater.find('.repeater-element').length + 1;

            // hide the button if we try to create more elements than the repeater allows
            if (lastIndex >= limit) {
                $(this).hide();
            }

            var action       = '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'repeaterTemplate']) ?>';
            var template     = $repeater.data('template');
            var extension    = $repeater.data('extension');
            var name         = $repeater.data('name');
            var elementLabel = $repeater.data('element-label');

            // make an ajax call to fetch and render the template, then add it in the accordion
            $.get(action, {
                repeaterElementId: lastIndex,
                repeaterName: name,
                template: template,
                extension: extension
            }, function (data) {
                var accordionId = $repeater.find('.panel-group').first().attr('id');
                elementLabel = elementLabel.replace('%(id)', lastIndex);

                $repeater.find('.panel-group').append('<div class="repeater-element">' + data + "</div>");
            });
        });
    });
</script>