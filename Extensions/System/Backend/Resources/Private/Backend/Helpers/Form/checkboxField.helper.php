<div class="checkbox">
    <label>
        <?php if (is_array($values)): ?>
            <?php foreach ($values as $text => $value): ?>
                <?php $id = 'field_' . $name . '_' . $value; ?>
                <input id="<?= $id ?>" type="text" value="<?= $value ?>" name="<?= $fieldName ?>"/> $text
            <?php endforeach; ?>
        <?php else: ?>
            <input id="field_<?= $name ?>" type="checkbox" value="<?= $value ?>" name="<?= $fieldName ?>"/>
        <?php endif; ?>
        <?= $label ?>
    </label>
</div>
