<div class="col-xs-8">
    <?= $input ?>
</div>
<div class="col-xs-4">
    <div class="form-group">
        <?php if ($visibility['scope'] == 'global'): ?>
            <p><?= $this->__('backend.settings.wrapper.minimalVisibility', ['visibility' => $visibility['minimal']]) ?></p>
        <?php else: ?>
            <p><?= $this->__('backend.settings.wrapper.minimalVisibility', ['visibility' => $visibility['minimal']]) ?></p>
            <div class="input-group">
                <label>
                    <input type="hidden" name="<?= $deleteField ?>" value="<?= ($value == null) ? '1' : '0' ?>" />
                    <input type="checkbox" class="settings-checkbox"
                           name="<?= $fieldId ?>_checkbox" <?= ($value == null) ? '' : 'checked="checked"' ?>
                           value="1"
                           data-for="#<?= $fieldId ?>"/> <?= $this->__('backend.settings.wrapper.scopeOverwrite') ?>
                </label>
            </div>
        <?php endif; ?>
    </div>
</div>