<?php if (\Continut\Core\Utility::getConfiguration('showSection.dashboardVideos') != '1'): ?>
    <div class="row" id="section_dashboardVideos">
        <div class="jumbotron">
            <div class="container-fluid">
                <div>
                    <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'User', '_action' => 'hideSection', 'section' => 'dashboardVideos']) ?>"
                       class="pull-right hide-section"
                       data-target="#section_dashboardVideos">
                        <?= $this->__('backend.dashboard.hideSection') ?> <span class="fa fa-close"></span>
                    </a>
                    <h3><?= $this->__('backend.dashboard.tutorials.title') ?></h3>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-dashboard">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-black-tie"></i> <?= $this->__('backend.dashboard.tutorials.forAdministrators') ?>
                                <a href="" class="pull-right flip btn btn-sm btn-default"><?= $this->__('backend.dashboard.tutorials.forAdministrators.link') ?></a>
                            </div>
                            <div class="panel-body">
                                <p class="text-center">
                                    <?= $this->__('backend.dashboard.tutorials.forAdministrators.summary') ?>
                                </p>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/videoseries?list=PLJJ4JkHam3qt1wTwVSbyoDCWfHtOmYpq7" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-dashboard">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-code"></i> <?= $this->__('backend.dashboard.tutorials.forIntegrators') ?>
                                <a href="" class="pull-right flip btn btn-sm btn-default"><?= $this->__('backend.dashboard.tutorials.forIntegrators.link') ?></a>
                            </div>
                            <div class="panel-body">
                                <p class="text-center">
                                    <?= $this->__('backend.dashboard.tutorials.forIntegrators.summary') ?>
                                </p>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/videoseries?list=PLJJ4JkHam3qu5n3cLEqbrlP5VaZcnw7dM" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="panel panel-dashboard">
                            <div class="panel-heading">
                                <i class="fa fa-fw fa-terminal"></i> <?= $this->__('backend.dashboard.tutorials.forDevelopers') ?>
                                <a href="" class="pull-right flip btn btn-sm btn-default"><?= $this->__('backend.dashboard.tutorials.forDevelopers.link') ?></a>
                            </div>
                            <div class="panel-body">
                                <p class="text-center">
                                    <?= $this->__('backend.dashboard.tutorials.forDevelopers.summary') ?>
                                </p>
                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/videoseries?list=PLJJ4JkHam3qu5n3cLEqbrlP5VaZcnw7dM" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>