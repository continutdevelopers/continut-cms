<form method="post"
      action="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'BackendUserGroups', '_action' => 'save']) ?>">
    <?= $this->helper('FormObject')->hiddenField($group, 'id', $group->getId()); ?>
    <?= $this->partial('General/formValidator', 'Backend', 'Backend', ['model' => $group]) ?>
    <div class="col-sm-12">
        <div class="panel panel-form">
            <div class="panel-heading">
                <div class="panel-title">
                    <?= $this->__('backend.groups.properties.header') ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <?= $this->helper('FormObject')->textField($group, 'title', $this->__('backend.groups.properties.title')) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $this->helper('FormObject')->textField($group, 'access', $this->__('backend.groups.properties.access')) ?>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="<?= $this->__('general.save') ?>"/>
                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'BackendUserGroups', '_action' => 'grid']) ?>"
                   class="close-button btn btn-danger pull-right flip"><?= $this->__('general.cancel') ?></a>
            </div>
        </div>
    </div>
</form>