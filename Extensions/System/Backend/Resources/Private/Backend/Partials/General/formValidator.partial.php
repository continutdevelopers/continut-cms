<?php if ($model->hasValidationErrors()): ?>
    <div class="bg-warning col-xs-12">
        <h4><?= $this->__('backend.validator.errors.paragraph') ?></h4>
        <?php foreach ($model->getValidationErrors() as $field => $errors): ?>
            <ul class="list-unstyled">
                <li><strong><?= $field ?>:</strong></li>
                <ul class="list-unstyled">
                    <?php foreach ($errors as $errorMessage): ?>
                        <li><?= $errorMessage ?></li>
                    <?php endforeach; ?>
                </ul>
            </ul>
        <?php endforeach; ?>
    </div>
<?php endif; ?>