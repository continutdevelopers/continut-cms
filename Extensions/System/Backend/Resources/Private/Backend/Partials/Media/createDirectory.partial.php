<div class="form-group">
    <label for="folder"><?= $this->__('backend.media.folders.specifyName') ?></label>
    <input type="text" class="form-control" id="folder"
           placeholder="<?= $this->__('backend.media.folders.specifyName') ?>" name="folder">
</div>