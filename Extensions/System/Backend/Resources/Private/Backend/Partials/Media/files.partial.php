<p>
    <small><?= $path ?></small>
    <span class="pull-right flip"><?= $this->__('backend.media.files.count', ['count' => count($files)]) ?></span>
</p>
<?php if ($files): ?>
    <form method="post" action="">
        <div class="row">
            <?php if ($listType == \Continut\Extensions\System\Backend\Classes\Controller\MediaController::LIST_TYPE_THUMBNAILS): ?>
                <?php foreach ($files as $file): ?>
                    <div class="col-xs-6 col-md-3 col-lg-2">
                        <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'fileInfo', 'file' => urlencode($file->getRelativeFilename())]) ?>"
                           class="thumbnail media-select-file filetype-<?= $file->getExtension() ?>"
                           data-file="<?= $file->getRelativeFilename() ?>">
                            <span class="extension"><?= $file->getExtension() ?>
                                / <?= $this->helper('Units')->formatBytes($file->getSize()); ?></span>
                            <?php if (in_array($file->getExtension(), ['JPG', 'PNG', 'GIF'])): ?>
                                <img
                                        src="<?= $this->helper('Image')->crop($file->getRelativeFilename(), 300, 300, 'storage') ?>"
                                        alt=""/>
                            <?php elseif ($file->getExtension() == 'SVG'): ?>
                                <img
                                        src="<?= $file->getRelativeFilename() ?>"
                                        alt=""/>
                            <?php else: ?>
                                <i class="fa fa-file" style="font-size: 125px"></i>
                            <?php endif; ?>
                            <div class="caption">
                                <p><?= $file->getFullname(); ?></p>
                            </div>
                        </a>
                    </div>
                <?php endforeach ?>
            <?php else: ?>
                <div class="col-xs-12">
                    <div class="list-group">
                        <div class="list-group-item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <strong><?= $this->__('backend.media.list.header.filename') ?></strong>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <strong><?= $this->__('backend.media.list.header.size') ?></strong>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <strong><?= $this->__('backend.media.list.header.modificationDate') ?></strong>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($files as $file): ?>
                            <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'fileInfo', 'file' => urlencode($file->getRelativeFilename())]) ?>"
                               class="list-group-item media-select-file filetype-<?= $file->getExtension() ?>"
                               data-file="<?= $file->getRelativeFilename() ?>">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <?php if (in_array($file->getExtension(), ['JPG', 'PNG', 'GIF'])): ?>
                                            <img
                                                    src="<?= $this->helper('Image')->crop($file->getRelativeFilename(), 60, 60, 'storage') ?>"
                                                    alt=""/>
                                        <?php elseif ($file->getExtension() == 'SVG'): ?>
                                            <img
                                                    width="60"
                                                    src="<?= $file->getRelativeFilename() ?>"
                                                    alt=""/>
                                        <?php else: ?>
                                            <i class="fa fa-file" style="font-size: 60px; width: 60px"></i>
                                        <?php endif; ?>
                                        <?= $file->getFullname(); ?>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <?= $this->helper('Units')->formatBytes($file->getSize()); ?>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <?= $this->helper('DateTime')->format($file->getModificationDate()) ?>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </form>
<?php else: ?>
    <div class="alert alert-warning" role="alert">
        <p><?= $this->__('backend.media.files.missing') ?></p>
    </div>
<?php endif; ?>
<?php if ($handlingType == \Continut\Extensions\System\Backend\Classes\Controller\MediaController::HANDLING_TYPE_SELECT): ?>
    <script type="text/javascript">
        $('.panel-media .media-select-file').on('click', function (event) {
            event.preventDefault();

            $(this).toggleClass('active');

            totalSelectedFiles = $('.panel-media .list-group-item.active, .panel-media .thumbnail.active').length;

            mediaClearFiles(totalSelectedFiles);
        });
    </script>
<?php else: ?>
    <script type="text/javascript">
        $('.panel-media .media-select-file').on('click', function (event) {
            event.preventDefault();

            BootstrapDialog.show({
                title: <?= json_encode($this->__('backend.dialog.type.information')) ?>,
                message: $('<div></div>').load($(this).attr('href'))
            });
        });
    </script>
<?php endif; ?>
