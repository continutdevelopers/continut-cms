<?php $fileTitle = (isset($fileReference)) ? $fileReference->getTitle() : ''; ?>
<?php $fileAlt = (isset($fileReference)) ? $fileReference->getAlt() : ''; ?>
<?php $fileDescription = (isset($fileReference)) ? $fileReference->getDescription() : ''; ?>
<?php $deleteUrl = (isset($fileReference)) ? $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'deleteReference', 'id' => $fileReference->getId()]) : ''; ?>
<div class="media">
    <div class="media-left">
        <?php if ($this->helper('Image')->isImage($file->getFilename())): ?>
            <img class="media-object"
                 src="<?= $this->helper('Image')->crop($file->getRelativePath(), 80, 80, 'backend_wizard') ?>" alt=""/>
        <?php else: ?>
            <span class="fa fa-file" style="font-size: 80px"></span>
        <?php endif; ?>
    </div>
    <div class="media-body">
        <p class="media-heading"><?= $file->getFilename() ?>
            (<?= $this->helper('Units')->formatBytes($file->getFilesize()) ?>)
            <a title="<?= $this->__('backend.media.wizard.fileReference.delete') ?>" href="<?= $deleteUrl ?>"
               class="btn pull-right flip btn-danger remove-reference"><span class="fa fa-fw fa-trash"></span></a>
        </p>
        <div class="row">
            <div class="col-xs col-sm-6">
                <input class="form-control no-label" type="text" value="<?= $fileTitle ?>"
                       placeholder="<?= $this->__('backend.media.wizard.fileReference.title') ?>"/>
            </div>
            <div class="col-xs col-sm-6">
                <input class="form-control no-label" type="text" value="<?= $fileAlt ?>"
                       placeholder="<?= $this->__('backend.media.wizard.fileReference.alt') ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <textarea rows="1" class="form-control no-label"
                          placeholder="<?= $this->__('backend.media.wizard.fileReference.description') ?>"><?= $fileDescription ?></textarea>
            </div>
        </div>
    </div>
</div>