<form method="post"
      action="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'BackendUsers', '_action' => 'save']) ?>">
    <?= $this->helper('FormObject')->hiddenField($user, 'id', $user->getId()); ?>
    <?= $this->partial('General/formValidator', 'Backend', 'Backend', ['model' => $user]) ?>
    <div class="col-sm-12">
        <div class="panel panel-form">
            <div class="panel-heading">
                <div class="panel-title">
                    <?= $this->__('backend.backendUser.properties.header') ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <?= $this->helper('FormObject')->textField($user, 'name', $this->__('backend.backendUser.properties.name')) ?>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <?= $this->helper('FormObject')->selectField($user, 'is_active',
                                    $this->__('backend.backendUser.properties.isActive'),
                                    [
                                        1 => $this->__('general.yes'),
                                        0 => $this->__('general.no'),
                                    ]
                                ) ?>
                            </div>
                        </div>
                        <?= $this->helper('FormObject')->textField($user, 'username', $this->__('backend.backendUser.properties.username')) ?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <?= $this->helper('FormObject')->selectField($user, 'usergroup_id', $this->__('backend.backendUser.properties.usergroupId'), $groups) ?>
                        <?php if ($mode == 'edit'): ?>
                            <?= $this->helper('FormObject')->checkboxField($user, 'change_password', $this->__('backend.backendUser.properties.changePassword'), '1') ?>
                            <?= $this->helper('FormObject')->textField($user, 'new_password', $this->__('backend.backendUser.properties.newPassword'), '', ['readonly' => true]) ?>
                        <?php else: ?>
                            <?= $this->helper('FormObject')->textField($user, 'password', $this->__('backend.backendUser.properties.password')) ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="<?= $this->__('general.save') ?>"/>
                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'BackendUsers', '_action' => 'grid']) ?>"
                   class="close-button btn btn-danger pull-right flip"><?= $this->__('general.cancel') ?></a>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $('#field_change_password').on('change', function (event) {
        var target = $('#field_data_new_password');
        target.prop('readonly', !target.prop('readonly'));
    });
</script>