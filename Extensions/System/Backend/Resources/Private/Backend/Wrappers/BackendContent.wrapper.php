<div id="panel-backend-content-<?= $id ?>" data-id="<?= $id ?>"
     class="content-type-<?= $type ?> panel panel-backend-content <?= (!$fromReference) ? 'content-drag-sender': ''; ?> <?= ($visible) ? 'panel-visible' : 'panel-hidden' ?>">
    <div class="panel-heading">
        <span class="fa fa-list-alt"></span>
        <strong><?= $title ?></strong>
            <span class="title-append"></span>
            <div class="btn-group btn-group-sm pull-right flip no-pep group-paste <?= ($canPaste) ? '' : 'hide' ?>" role="group" aria-label="Element paste">
                <!--<a class="btn btn-default btn-sm drag-controller" title="<?= $this->__('backend.content.operation.move') ?>"><i class="fa fa-fw fa-arrows"></i></a>-->
                <button title="<?= $this->__('backend.content.operation.paste') ?>"
                        type="button"
                        class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                    <i class="fa fa-paste fa-fw"></i>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a title="<?= $this->__('backend.content.operation.paste') ?>"
                           class="content-operation-link operation-paste"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'paste', 'id' => $id, 'placement' => 'before']) ?>">
                            <i class="fa fa-paste fa-fw"></i>
                            <?= $this->__('backend.content.operation.paste') ?>
                            <i class="fa fa-level-up fa-fw"></i>
                        </a>
                    </li>
                    <?php if ($type === 'container'): ?>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.paste') ?>"
                           class="content-operation-link operation-paste"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'paste', 'id' => $id, 'placement' => 'inside']) ?>">
                            <i class="fa fa-paste fa-fw"></i>
                            <?= $this->__('backend.content.operation.paste') ?>
                            <i class="fa fa-long-arrow-right fa-fw"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.paste') ?>"
                           class="content-operation-link operation-paste"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'paste', 'id' => $id, 'placement' => 'after']) ?>">
                            <i class="fa fa-paste fa-fw"></i>
                            <?= $this->__('backend.content.operation.paste') ?>
                            <i class="fa fa-level-down fa-fw"></i>
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.pasteAsLink') ?>"
                           class="content-operation-link operation-paste"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'paste', 'type' => 'reference', 'id' => $id, 'placement' => 'before']) ?>">
                            <i class="fa fa-link fa-fw"></i>
                            <?= $this->__('backend.content.operation.pasteAsLink') ?>
                            <i class="fa fa-level-up fa-fw"></i>
                        </a>
                    </li>
                    <?php if ($type === 'container'): ?>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.pasteAsLink') ?>"
                           class="content-operation-link operation-paste"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'paste', 'type' => 'reference', 'id' => $id, 'placement' => 'inside']) ?>">
                            <i class="fa fa-link fa-fw"></i>
                            <?= $this->__('backend.content.operation.pasteAsLink') ?>
                            <i class="fa fa-long-arrow-right fa-fw"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.pasteAsLink') ?>"
                           class="content-operation-link operation-paste"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'paste', 'type' => 'reference', 'id' => $id, 'placement' => 'after']) ?>">
                            <i class="fa fa-link fa-fw"></i>
                            <?= $this->__('backend.content.operation.pasteAsLink') ?>
                            <i class="fa fa-level-down fa-fw"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="btn-group btn-group-sm pull-right flip no-pep" role="group" aria-label="Element actions">
                <a title="<?= $this->__('backend.content.operation.edit') ?>"
                   class="btn btn-default content-operation-link operation-edit"
                   href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'edit', 'id' => $id]) ?>">
                    <i class="fa fa-pencil fa-fw"></i>
                </a>
                <?php if (!$fromReference): ?>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a title="<?= $this->__('backend.content.operation.cut') ?>"
                           class="content-operation-link operation-cut"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'cut', 'id' => $id]) ?>">
                            <i class="fa fa-cut fa-fw"></i> <?= $this->__('backend.content.operation.cut') ?>
                        </a>
                    </li>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.copy') ?>"
                           class="content-operation-link operation-copy"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'copy', 'id' => $id]) ?>">
                            <i class="fa fa-copy fa-fw"></i> <?= $this->__('backend.content.operation.copy') ?>
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <?php if ($visible): ?>
                            <a title="<?= $this->__('backend.content.operation.hide') ?>"
                               class="content-operation-link operation-visibility"
                               href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'toggleVisibility', 'id' => $id, 'show' => 0]) ?>">
                                <i class="fa fa-eye-slash fa-fw"></i> <?= $this->__('backend.content.operation.hide') ?>
                            </a>
                        <?php else: ?>
                            <a title="<?= $this->__('backend.content.operation.show') ?>"
                               class="content-operation-link operation-visibility"
                               href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'toggleVisibility', 'id' => $id, 'show' => 1]) ?>">
                                <i class="fa fa-eye fa-fw"></i> <?= $this->helper('Localization')->translate('backend.content.operation.show') ?>
                            </a>
                        <?php endif; ?>
                    </li>
                    <li>
                        <a title="<?= $this->__('backend.content.operation.delete') ?>"
                           class="content-operation-link operation-delete"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'delete', 'id' => $id]) ?>">
                            <i class="fa fa-trash-o fa-fw"></i> <?= $this->__('backend.content.operation.delete') ?>
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a class="content-wizard" title="<?= $this->__('backend.content.addNew.before') ?>"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'wizard', 'placement' => 'before', 'id' => $id, 'column_id' => $columnId, 'page_id' => $pageId]) ?>">
                            <i class="fa fa-plus fa-fw"></i> <?= $this->__('backend.content.addNew.before') ?>
                            <i class="fa fa-level-up fa-fw"></i>
                        </a>
                    </li>
                    <li>
                        <a class="content-wizard" title="<?= $this->__('backend.content.addNew.after') ?>"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'wizard', 'placement' => 'after', 'id' => $id, 'column_id' => $columnId, 'page_id' => $pageId]) ?>">
                            <i class="fa fa-plus fa-fw"></i>
                            <?= $this->__('backend.content.addNew.after') ?>
                            <i class="fa fa-level-down fa-fw"></i>
                        </a>
                    </li>
                </ul>
                <?php endif; ?>
            </div>
    </div>
    <div class="panel-body no-pep"><?= $content ?></div>
</div>