<?php if ($this->helper('Image')->isImage($fileInfo->getFullname())): ?>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#tab-preview" aria-controls="tab-preview" role="tab"
                data-toggle="tab"><?= $this->__('backend.media.info.tab.preview') ?></a>
        </li>
        <li role="presentation">
            <a href="#tab-details" aria-controls="tab-details" role="tab"
                data-toggle="tab"><?= $this->__('backend.media.info.tab.details') ?></a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab-preview">
            <img src="<?= $this->helper('Image')->resize($fileInfo->getRelativeFilename(), 600, 600); ?>"
                 class="img-responsive" alt=""/>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab-details">
            <table class="table table-bordered">
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.filename') ?></th>
                    <td><?= $fileInfo->getFullname() ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.filesize') ?></th>
                    <td><?= $this->helper('Units')->formatBytes($fileInfo->getSize()) ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.dimensions') ?></th>
                    <td><?= $this->__('backend.media.info.tab.details.dimensionsPx', ['w' => $fileInfo->getDimensions()['w'], 'h' => $fileInfo->getDimensions()['h']]) ?>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.filetype') ?></th>
                    <td><?= $fileInfo->getExtension() ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.relativePath') ?></th>
                    <td><?= $fileInfo->getRelativePath() ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.relativeFilename') ?></th>
                    <td><?= $fileInfo->getRelativeFilename() ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.absolutePath') ?></th>
                    <td><?= $fileInfo->getAbsolutePath() ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.absoluteFilename') ?></th>
                    <td><?= $fileInfo->getAbsoluteFilename() ?></td>
                </tr>
                <tr>
                    <th><?= $this->__('backend.media.info.tab.details.creationDate') ?></th>
                    <td><?= $this->helper('DateTime')->format($fileInfo->getCreationDate()) ?>
                        - <?= $fileInfo->getCreationDate() ?></td>
                </tr>
            </table>
        </div>
    </div>
<?php else: ?>
    <table class="table table-bordered">
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.filename') ?></th>
            <td><?= $fileInfo->getFullname() ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.filesize') ?></th>
            <td><?= $this->helper('Units')->formatBytes($fileInfo->getSize()) ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.filetype') ?></th>
            <td><?= $fileInfo->getExtension() ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.relativePath') ?></th>
            <td><?= $fileInfo->getRelativePath() ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.relativeFilename') ?></th>
            <td><?= $fileInfo->getRelativeFilename() ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.absolutePath') ?></th>
            <td><?= $fileInfo->getAbsolutePath() ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.absoluteFilename') ?></th>
            <td><?= $fileInfo->getAbsoluteFilename() ?></td>
        </tr>
        <tr>
            <th><?= $this->__('backend.media.info.tab.details.creationDate') ?></th>
            <td><?= $this->helper('DateTime')->format($fileInfo->getCreationDate()) ?>
                - <?= $fileInfo->getCreationDate() ?></td>
        </tr>
    </table>
<?php endif; ?>
