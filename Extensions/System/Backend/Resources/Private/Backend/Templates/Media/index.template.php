<div class="panel panel-default panel-media">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-3">
                <h2 class="title"><?= $this->__('backend.media.title') ?></h2>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-xs-7">
                        <form id="form_media_search" method="post" action="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'searchFile']) ?>">
                            <div class="input-group">
                                <div class="input-group-addon"><span class="fa fa-fw fa-search"></span></div>
                                <input type="text" name="query" class="form-control" placeholder="<?= $this->__('backend.media.search') ?>">
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-5">
                        <div class="btn-group pull-right flip media-list-type" role="group"
                             aria-label="Thumbnail display">
                            <button type="button" data-list-type="list"
                                    title="<?= $this->__('backend.media.listType.list') ?>"
                                    class="btn btn-default <?= ($listType == 'list') ? 'active' : ''; ?>"><i
                                        class="fa fa-fw fa-bars"></i></button>
                            <button type="button" data-list-type="thumbnails"
                                    title="<?= $this->__('backend.media.listType.thumbnails') ?>"
                                    class="btn btn-default <?= ($listType == 'thumbnails') ? 'active' : ''; ?>"><i
                                        class="fa fa-fw fa-th"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="btn-group-vertical btn-block" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a class="btn btn-success" id="create_folder" href="#">
                            <?= $this->__('backend.media.folders.create') ?> <i class="fa fa-fw fa-plus"></i>
                        </a>
                    </div>
                    <div class="btn-group" role="group">
                        <a class="btn btn-warning" id="upload_files"><?= $this->__('backend.media.files.upload') ?> <i
                                    class="fa fa-fw fa-upload"></i></a>
                    </div>
                </div>
                <div id="file_tree"></div>
            </div>
            <div class="col-sm-9">
                <div id="file_content">
                    <?= $this->partial('Media/files', 'Backend', 'Backend', ['path' => $path, 'files' => $files, 'listType' => $listType, 'handlingType' => $handlingType]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($handlingType == \Continut\Extensions\System\Backend\Classes\Controller\MediaController::HANDLING_TYPE_SELECT): ?>
        <div class="panel-footer stick-to-bottom text-center">
            <button type="button"
                    class="btn btn-primary flip pull-left media-confirm"><?= $this->__('general.select') ?></button>
            <span id="media_selected_count"><?= $this->__('general.selectedXFiles', ['count' => 0]); ?></span>
            <button type="button" aria-label="close"
                    class="close-button btn btn-danger pull-right flip"><?= $this->__('general.close') ?></button>
        </div>
    <?php endif; ?>
</div>

<script>
    var totalSelectedFiles = 0;

    $('#file_tree').jstree({
        'core': {
            'multiple': false,
            'animation': 0,
            'check_callback': true,
            'themes': {
                'variant': 'large',
                'dots': true
            },
            'strings': {
                'Loading ...': '<?= $this->__('backend.tree.loading') ?>',
            },
            'data': {
                'url': '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'treeGetNode']) ?>',
                dataType: 'json',
                'data': function (node) {
                    return {'id': node.id};
                }
            },
            force_text: true
        },
        'dnd': {
            'copy': false, // true allows to make copies while dragging and holding Ctrl. We don't want this
            'always_copy': false,
            'check_while_dragging': false,
            'large_drag_target': true,
            'large_drop_target': true
        },
        'search': {
            'show_only_matches': true,
            'show_only_matches_children': false
        },
        'types': {
            'default': {'icon': 'folder'},
            'file': {'valid_children': [], 'icon': 'file'}
        },
        'plugins': ['dnd', 'search', 'wholerow']
        //'plugins' : ['dnd', 'search', 'wholerow', 'checkbox']
    })
        .on('changed.jstree', function (e, data) {
            if (data && data.selected && data.selected.length) {
                // once a node is clicked, load the corresponding media elements in the right side
                console.log(data);
                var nodeId = data.node.id;
                $.ajax({
                    url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'getFiles', 'handling' => $handlingType]) ?>',
                    data: {path: nodeId},
                    beforeSend: function (xhr) {
                        //$('#' + nodeId + ' > .jstree-anchor').append('<span class="fa fa-spinner fa-pulse"></span>');
                    }
                })
                    .done(function (data) {
                        $('#file_content').html(data);
                        //$('#' + nodeId).find('.fa-spinner').remove();
                        //previousSelectedNode = nodeId;
                        //anchor.data('requestRunning', false);
                        mediaClearFiles(0);
                    });
            }

        });

    // create folder button
    $('#create_folder').on('click', function (event) {
        event.preventDefault();

        BootstrapDialog.confirm({
            message: '<?= preg_replace('/[\r\n]+/', null, $this->partial('Media/createDirectory', 'Backend', 'Backend')) ?>',
            title: '<?= $this->__('backend.media.folders.create') ?>',
            type: BootstrapDialog.TYPE_SUCCESS,
            callback: function (result) {
                // if user confirms, send create folder request
                if (result) {
                    var path = '';
                    var selectedNodes = $('#file_tree').jstree('get_selected');
                    if (selectedNodes.length > 0) {
                        path = selectedNodes[0];
                    }
                    $.getJSON('<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'createFolder']) ?>', {
                        folder: $('#folder').val(),
                        path: path
                    }).done(function (data) {
                        if (data.success) {
                            $('#file_tree').jstree('refresh');
                        }
                    });
                }
            }
        });
    });

    $('#upload_files').on('click', function (event) {
        event.preventDefault();

        BootstrapDialog.show({
            title: '<?= $this->__('backend.media.files.upload') ?>',
            size: BootstrapDialog.SIZE_WIDE,
            message: $('<div></div>').load('<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'showUploadFiles']) ?>')
        });
    });

    // switch list type display
    $('.media-list-type button').on('click', function () {
        var nodeId = $('#file_tree').jstree('get_selected')[0];
        var list = $(this).data('list-type');
        $('.media-list-type button').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'getFiles', 'handling' => $handlingType]) ?>',
            data: {path: nodeId, list: list},
            beforeSend: function (xhr) {
                //$('#' + nodeId + ' > .jstree-anchor').append('<span class="fa fa-spinner fa-pulse"></span>');
            }
        })
            .done(function (data) {
                $('#file_content').html(data);
                mediaClearFiles(0);
            });
    });

    // media search
    $('#form_media_search').on('submit', function (event) {
        // do not submit form
        event.preventDefault();

        var $form = $(this);
        var nodeId = $('#file_tree').jstree('get_selected')[0];
        var query = $form.find('input[name="query"]').val();

        $form.find('.fa-fw').removeClass('fa-search').addClass('fa-spin fa-refresh');

        $.ajax({
            url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'getFiles', 'handling' => $handlingType]) ?>',
            data: {path: nodeId, query: query},
            beforeSend: function (xhr) {
                // @TODO add a loader somewhere
            }
        })
            .done(function (data) {
                $('#file_content').html(data);
                mediaClearFiles(0);
                $form.find('.fa-fw').removeClass('fa-spin fa-refresh').addClass('fa-search');
            });
    });

    // when confirming the file selection, index the selected files
    // and update the value of the hidden input with these new file ids
    $('.panel-media .media-confirm').on('click', function () {
        // get all selected files and return them
        var selection = $.map(
            $('.panel-media #file_content .active'),
            function (element) {
                return $(element).data('file')
            })
            .join(",");
        $.ajax({
            url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'indexFiles']) ?>',
            data: {selection: selection, container: $('#file_content').data('container')},
            dataType: 'json',
            beforeSend: function (xhr) {
            }
        })
            .done(function (data) {
                $(mediaTarget).val(function () {
                    if (this.value != '') {
                        return this.value + ',' + data.fileIds;
                    } else {
                        return data.fileIds;
                    }
                });
                $(data.container).append(data.template);
            });
        mediaModal.close();
    });

    $('.panel-media .close-button').on('click', function () {
        mediaModal.close();
    });

    function mediaClearFiles(count) {
        totalSelectedFiles = count;
        $('#media_selected_count').html('<?= $this->__('general.selectedXFiles'); ?>'.replace('%(count)', totalSelectedFiles));
    }
</script>