<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <button id="uploadBtn" class="btn btn-primary"><i class="fa fa-fw fa-upload"></i>
                <?= $this->__('backend.media.upload.popup.uploadButton') ?>
            </button>
            <div class="pull-right">
                <label for="overwrite">
                    <input type="checkbox" id="media_upload_overwrite" name="media_upload_overwrite" value="1"/>
                    <?= $this->__('backend.media.upload.popup.overwriteFiles') ?>
                </label>
                <br/>
                <small><?= $this->__('backend.media.upload.popup.overwriteDescription') ?></small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" id="media_files_upload_list">
            <div class="dropzone text-center" id="media_file_dropzone">
                <p><?= $this->__('backend.media.upload.popup.dragAndDrop') ?></p>
            </div>
        </div>
    </div>
</div>

<script>
    // generates a valid tag id from a string; useful for generating ids from filenames
    function validId(text) {
        return text.replace(/[\. ,:-]+/g, "-");
    }

    var uploader = new ss.SimpleUpload({
        dropzone: 'media_file_dropzone',
        button: $('#uploadBtn'),
        url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'uploadFiles']); ?>',
        name: 'uploadfile',
        debug: true,
        multipart: false,
        multiple: true,
        multipleSelect: true,
        hoverClass: 'hover',
        focusClass: 'focus',
        responseType: 'json',
        noParams: false,
        onChange: function () {
            $('#media_files_upload_list').html('');
        },
        startXHR: function (filename) {
            this.setProgressBar($('#mful_' + validId(filename) + ' .upload-progress'));
        },
        onSubmit: function (filename) {
            var self = this;
            self.setData({
                path: $('#file_tree').jstree('get_selected')[0],
                overwrite: ($('#media_upload_overwrite').is(':checked') ? '1' : '0')
            });
            $('#media_files_upload_list').append('<div class="media upload-file" id="mful_' + validId(filename) + '"> <div class="media-left upload-preview"></div> <div class="media-body"><h4 class="media-heading">' + filename + '</h4><em class="message-result"></em> </div> <div class="upload-progress" style="width: 0%"></div> </div>');
        },
        onComplete: function (filename, response) {
            var fileId = validId(filename);
            $('#mful_' + fileId + ' .upload-progress').hide();
            var $target = $('#mful_' + fileId);

            if (!response) {
                $target.addClass('bg-danger'); // cannot upload
                return;
            }

            if (response.success === true) {
                $target.addClass('bg-success').find('.media-heading').text(response.file);
                $target.addClass('bg-success').find('.message-result').text(response.message);
                $target.find('.upload-preview').append(response.preview);
            } else {
                if (response.message) {
                    $target.addClass('bg-warning').find('.message-result').text(response.message);
                } else {
                    $target.addClass('bg-danger').find('.message-result').text('An error occurred and the upload failed.');
                }
            }
        },
        onError: function (filename, errorType, status, statusText) {
            var fileId = validId(filename);
            $('#mful_' + fileId + ' .upload-progress').hide();
            var $target = $('#mful_' + fileId);
            $target.addClass('bg-danger').find('.message-result').text(statusText + ' (' + status + ')');
        },
        onAllDone: function () {
            // once all the uploads are completed, refresh the list of files in the background
            var nodeId = $('#file_tree').jstree('get_selected')[0];
            //var list = $(this).data('list-type');
            $.ajax({
                url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Media', '_action' => 'getFiles']) ?>',
                data: {path: nodeId}
            }).done(function (data) {
                $('#file_content').html(data);
                mediaClearFiles(0);
            });
        }
    });
</script>