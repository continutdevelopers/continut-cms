<div class="row">
    <div class="col-sm-3">
        <div class="thumbnail">
            <?php if ($backendUser->getImage()): ?>
                <img src="<?= $this->helper('Image')->resize($backendUser->getImage(), 400, null, 'backend'); ?>" alt=""
                     class="">
            <?php endif; ?>
            <div class="caption">
                <h3><?= $backendUser->getName(); ?></h3>
                <p>Account created on<br/><span class="fa fa-calendar-check-o"></span>
                    <small>13 dec 2016</small>
                </p>
                <p>Last connected on<br/><span class="fa fa-calendar-check-o"></span>
                    <small>15 dec 2016 at 14h30</small>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <?= $this->helper('Session')->showFlashMessages(); ?>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h3><?= $this->__('backend.user.form.title.manageProfile') ?></h3>
                <form method="post" id="save_profile"
                      action="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'User', '_action' => 'saveProfile']); ?>">
                    <?= $this->helper('FormObject')->info($this->__('backend.user.form.id'), $backendUser->getId()) ?>
                    <?= $this->helper('FormObject')->info($this->__('backend.user.form.username'), $backendUser->getUsername()) ?>
                    <?= $this->helper('FormObject')->selectField($backendUser, 'language', $this->__('backend.user.form.language'), $this->helper('Locale')->locales(), $backendUser->getLanguage()) ?>
                    <?= $this->helper('FormObject')->selectField($backendUser, 'timezone', $this->__('backend.user.form.timezone'), $this->helper('Locale')->timezones(), $backendUser->getTimezone()) ?>
                    <?= $this->helper('FormObject')->textField($backendUser, 'name', $this->__('backend.user.form.name'), $backendUser->getName()) ?>
                    <?= $this->helper('FormObject')->selectField($backendUser, 'usergroup_id', $this->__('backend.user.form.usergroup'), $groups->toSelect('id', 'title', false), $backendUser->getUsergroupId()) ?>
                    <?= $this->helper('FormObject')->mediaField($backendUser, 'image', $this->__('backend.user.form.image'), $backendUser->getImage(), ['maxFiles' => 1]) ?>
                    <input type="submit" class="btn btn-primary" value="<?= $this->__('backend.user.form.update') ?>"/>
                </form>
            </div>
            <div class="col-xs-12 col-sm-6">
                <form id="change_password" method="post" action="">
                    <h3><?= $this->__('backend.user.form.title.changePassword') ?></h3>
                    <?= $this->helper('FormObject')->textField($backendUser, 'current_password', $this->__('backend.user.form.currentPassword'), '') ?>
                    <?= $this->helper('FormObject')->textField($backendUser, 'new_password', $this->__('backend.user.form.newPassword'), '') ?>
                    <?= $this->helper('FormObject')->textField($backendUser, 'new_password_confirm', $this->__('backend.user.form.confirmNewPassword'), '') ?>
                    <input type="submit" class="btn btn-primary" value="<?= $this->__('backend.user.form.update') ?>"/>
                </form>
            </div>
        </div>
    </div>
</div>
