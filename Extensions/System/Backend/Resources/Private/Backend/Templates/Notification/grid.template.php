<h2><?= $this->__('backend.notification.grid.title') ?></h2>
<?= $this->helper('Session')->showFlashMessages(); ?>
<div class="panel panel-default panel-grid">
    <div class="panel-heading">
        <div class="panel-title"><?= $this->__('backend.notification.grid.title') ?></div>
    </div>
    <div class="panel-body">
        <?= $grid->render() ?>
    </div>
</div>