<a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'notifications']) ?>"
   class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-fw fa-2x fa-envelope-o <?= ($notifications) ? 'fa-alerted' : ''; ?>"></i> <?= $this->__('backend.menu.notifications') ?>
</a>
<ul class="dropdown-menu media-list">
    <li class="dropdown-header">
        <?= $this->__('backend.notifications.newNotificationsAmount', ['count' => sizeof($notifications)]) ?>
    </li>
    <?php foreach ($notifications as $notification): ?>
        <li role="separator" class="divider"></li>
        <li class="media">
            <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Notification', '_action' => 'grid']) ?>">
                <div class="media-body">
                    <small class="flip time">
                        <i class="fa fa-clock-o"></i>
                        <?= $this->helper('DateTime')->format($notification->getCreatedAt()) ?>
                    </small><br/>
                    <small><?= $this->__($notification->getMessage(), $notification->getData()); ?></small>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
    <li role="separator" class="divider"></li>
    <li>
        <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Notification', '_action' => 'grid']) ?>">
            <?= $this->__('backend.notifications.showAll') ?>
        </a>
    </li>
</ul>