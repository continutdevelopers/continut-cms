<h2><?= $this->__('backend.dashboard') ?></h2>
<p><?= $this->__('backend.dashboard.welcome', ['fullname' => $backendUser->getName()]) ?></p>

<?php if ($backendUser->getAttribute('showSection.dashboardVideos', '1') == '1'): ?>
    <?= $this->partial('Help/videosDashboard', 'Backend', 'Backend') ?>
<?php endif; ?>

<div class="row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-dashboard dashboard-domains">
            <div class="panel-heading">
                <i class="fa fa-fw fa-globe"></i> Content statistics
                <a href="" class="pull-right flip btn btn-sm btn-default">View complete list</a>
            </div>
            <div class="panel-body">
                <?php foreach ($domains->getAll() as $domain): ?>
                    <?php if ($domain->getDomainUrls()): ?>
                        <p><strong><?= $domain->getTitle() ?></strong></p>
                        <?php foreach ($domain->getDomainUrls() as $url): ?>
                            <div class="media">
                                <div class="media-left">
                                    <i class="flag-icon flag-icon-<?= $url->getFlag() ?>"></i>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><?= $url->getCompleteUrl() ?></h4>
                                    <span class="badge"><?= \Continut\Core\Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection')->whereCount('domain_url_id = :domain_url_id', ['domain_url_id' => $url->getId()]) ?></span>
                                    pages
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-dashboard">
            <div class="panel-heading">
                <i class="fa fa-fw fa-thumbs-up"></i> @TODO
                <a href="" class="pull-right flip btn btn-sm btn-default">View complete list</a>
            </div>
            <div class="panel-body">
                <p>Use it for other infos</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-12">
        <div class="panel panel-dashboard">
            <div class="panel-heading">
                <i class="fa fa-fw fa-server"></i> <?= $this->__('backend.dashboard.system.title') ?>
                <a href=""
                   class="pull-right flip btn btn-sm btn-default"><?= $this->__('backend.dashboard.system.viewSpecs') ?></a>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <tr <?= (version_compare(PHP_VERSION, '5.5.0', '<')) ? 'class="danger"' : ''; ?>>
                        <th width="50%"><?= $this->__('backend.dashboard.system.php') ?></th>
                        <td width="50%"><?= PHP_VERSION ?></td>
                    </tr>
                    <tr>
                        <th><?= $this->__('backend.dashboard.system.server') ?></th>
                        <td><?= (isset($_SERVER['SERVER_SOFTWARE'])) ? $_SERVER['SERVER_SOFTWARE'] : $this->__('backend.dashboard.system.server.unknown'); ?></td>
                    </tr>
                </table>
                <p><?= $this->__('backend.dashboard.system.directives') ?></p>
                <table class="table table-striped">
                    <tr <?= ($this->helper('Units')->iniValueToBytes(ini_get('memory_limit')) < 33554432) ? 'class="danger"' : ''; ?>>
                        <th width="50%">
                            <?= $this->__('backend.dashboard.system.directive.memory_limit') ?>
                        </th>
                        <td width="50%">
                            <small>memory_limit</small>
                            <br/>
                            <?= ini_get('memory_limit') ?>
                        </td>
                    </tr>
                    <tr <?= ((int)ini_get('max_execution_time') < 30) ? 'class="danger"' : ''; ?>>
                        <th>
                            <?= $this->__('backend.dashboard.system.directive.max_execution_time') ?>
                        </th>
                        <td>
                            <small>max_execution_time</small>
                            <br/>
                            <?= ini_get('max_execution_time') ?>
                        </td>
                    </tr>
                    <tr <?= ($this->helper('Units')->iniValueToBytes(ini_get('post_max_size')) < 8388608) ? 'class="warning"' : ''; ?>>
                        <th>
                            <?= $this->__('backend.dashboard.system.directive.post_max_size') ?>
                        </th>
                        <td>
                            <small>post_max_size</small>
                            <br/>
                            <?= ini_get('post_max_size') ?>
                        </td>
                    </tr>
                    <tr <?= ($this->helper('Units')->iniValueToBytes(ini_get('upload_max_filesize')) < 8388608) ? 'class="warning"' : ''; ?>>
                        <th>
                            <?= $this->__('backend.dashboard.system.directive.upload_max_filesize') ?>
                        </th>
                        <td>
                            <small>upload_max_filesize</small>
                            <br/>
                            <?= ini_get('upload_max_filesize') ?>
                        </td>
                    </tr>
                    <tr <?= ((int)ini_get('max_input_vars') < 200) ? 'class="danger"' : ''; ?>>
                        <th>
                            <?= $this->__('backend.dashboard.system.directive.max_input_vars') ?>
                        </th>
                        <td>
                            <small>max_input_vars</small>
                            <br/>
                            <?= ini_get('max_input_vars') ?>
                        </td>
                    </tr>
                    <tr <?= ini_get('display_errors') ? 'class="warning"' : ''; ?>>
                        <th>
                            <?= $this->__('backend.dashboard.system.directive.display_errors') ?>
                        </th>
                        <td>
                            <small>display_errors</small>
                            <br/>
                            <?= ini_get('display_errors') ? $this->__('general.yes') : $this->__('general.no'); ?>
                        </td>
                    </tr>
                </table>
                <?php
                $extensionsList    = ['PDO', 'Reflection', 'bcmath', 'ctype', 'date', 'exif', 'fileinfo', 'filter', 'gd', 'iconv', 'intl', 'json', 'mbstring', 'pcre', 'posix', 'session', 'spl', 'standard', 'tokenizer', 'curl', 'dom', 'imagick', 'xml'];
                $missingExtensions = [];
                foreach ($extensionsList as $extension) {
                    if (!extension_loaded($extension)) {
                        $missingExtensions[] = $extension;
                    }
                }
                ?>
                <?php if ($missingExtensions): ?>
                    <p class="text-danger">
                        <?= $this->__('backend.dashboard.system.extensionsMissing', ['count' => sizeof($missingExtensions)]) ?>
                        <?= implode(',', $missingExtensions); ?>
                    </p>
                <?php else: ?>
                    <p class="text-success"><?= $this->__('backend.dashboard.system.extensionsLoaded') ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>