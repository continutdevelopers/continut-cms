<div class="quick-panel pull-right flip">
    <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'BackendUsers', '_action' => 'create']) ?>"
       class="btn btn-success"><span
                class="fa fa-icon fa-plus"></span> <?= $this->__('backend.users.grid.addBackendUser') ?></a>
</div>
<h2><?= $this->__('backend.users.grid.title') ?></h2>
<?= $this->helper('Session')->showFlashMessages(); ?>
<div class="panel panel-default panel-grid">
    <div class="panel-heading">
        <div class="panel-title"><?= $this->__('backend.users.grid.title') ?></div>
    </div>
    <div class="panel-body">
        <?= $grid->render() ?>
    </div>
</div>

<div class="quick-panel">
    <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'BackendUsers', '_action' => 'create']) ?>"
       class="btn btn-success"><span
                class="fa fa-icon fa-plus"></span> <?= $this->__('backend.users.grid.addBackendUser') ?></a>
</div>