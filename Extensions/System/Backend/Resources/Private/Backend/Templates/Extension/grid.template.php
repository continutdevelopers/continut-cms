<h2><?= $this->__('backend.extension.title') ?></h2>
<?= $this->helper('Session')->showFlashMessages(); ?>
<div class="panel panel-default panel-grid">
    <div class="panel-body">
        <div class="grid">
            <div class="row grid-row">
                <div class="col-sm-6">
                    <!--<a href=""
                       class="btn btn-default"
                       id="extensions_update">
                        <span class="fa fa-fw fa-globe"></span> <?= $this->__('backend.extension.grid.searchOnline') ?>
                    </a>-->
                    <!--<form class="form-inline">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search_online" id="search_online" placeholder="">
                        </div>
                        <button type="submit" class="btn btn-default"><?= $this->__('general.search') ?></button>
                    </form>-->
                </div>
                <div class="col-sm-6">
                    <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'checkForUpdates']) ?>"
                       class="pull-right btn btn-primary update-extensions"
                       id="extensions_update">
                        <span class="fa fa-fw fa-refresh"></span> <?= $this->__('backend.extension.grid.checkForUpdates') ?>
                    </a>
                </div>
            </div>
            <div class="row grid-row grid-even">
                <div class="col-sm-2"><?= $this->__('backend.extension.grid.header.name') ?></div>
                <div class="col-sm-2"><?= $this->__('backend.extension.grid.header.version') ?></div>
                <div class="col-sm-1"><?= $this->__('backend.extension.grid.header.scope') ?></div>
                <div class="col-sm-3"><?= $this->__('backend.extension.grid.header.description') ?></div>
                <div class="col-sm-2"><?= $this->__('backend.extension.grid.header.activated') ?></div>
                <div class="col-sm-2"></div>
            </div>
            <?php $index = 0; ?>
        <?php foreach ($allExtensions as $extensionId => $extensionData): ?>
            <?php $extensionKey = $this->helper('Text')->stripTags($extensionData['type']) . '_' . $extensionId; ?>
            <?php $index++; ?>
            <div class="row grid-row <?= ($index % 2 == 0) ? 'grid-even' : ''?>" id="extension_<?= $extensionKey ?>">
                <div class="col-sm-2">
                    <strong><?= $this->helper('Text')->stripTags($extensionData['name']) ?></strong>
                    <?php if (isset($extensionData['author'])): ?>
                        <br/>
                        <em><?= $this->helper('Text')->stripTags($extensionData['author']['name']) ?></em>
                        <small><?= $this->helper('Text')->stripTags($extensionData['author']['email']) ?></small>
                        <?php if (isset($extensionData['author']['web'])): ?>
                            <a href="<?= $extensionData['author']['web']; ?>" alt="" target="_blank">
                                <?= $this->helper('Text')->stripTags($extensionData['author']['web']) ?>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2">
                    <?= $this->__('backend.extension.current', ['version' => $this->helper('Text')->stripTags($extensionData['version'])]); ?>
                    <div class="update"></div>
                </div>
                <div class="col-sm-1"><span class="badge"><?= $this->__('backend.extension.scope.' . $this->helper('Text')->stripTags($extensionData['type'])) ?></span></div>
                <div class="col-sm-3"><?= $this->helper('Text')->stripTags($extensionData['description']) ?></div>
                <?php if ($extensionData['type'] != 'System'): ?>
                    <div class="col-sm-2"><?= (in_array($extensionId, $loadedExtensions[$extensionData['type']])) ? $this->__('general.yes') : $this->__('general.no') ?></div>
                    <div class="col-sm-2">
                        <?php if (in_array($extensionId, $loadedExtensions[$extensionData['type']])): ?>
                            <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'toggle', 'key' => $extensionKey, 'op' => 0]) ?>" class="btn btn-danger"><span class="fa fa-fw fa-power-off"></span> <?= $this->__('backend.extension.grid.operation.deactivate') ?></a>
                        <?php else: ?>
                            <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'toggle', 'key' => $extensionKey, 'op' => 1]) ?>" class="btn btn-success"><span class="fa fa-fw fa-plug"></span> <?= $this->__('backend.extension.grid.operation.activate') ?></a>
                        <?php endif; ?>
                    </div>
                <?php else: ?>
                    <div class="col-sm-2"><?= $this->__('general.yes') ?></div>
                    <div class="col-sm-2"></div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</div>
