<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <?= $this->helper('Session')->showFlashMessages(); ?>
            <div class="col-xs-12">
                <h2><?= $this->__('backend.settings.general.title') ?></h2>
                <p><?= $this->__('backend.settings.general.subtitle') ?></p>
            </div>

            <div class="col-xs-12 col-md-3">
                <div class="panel panel-dashboard">
                    <div class="panel-heading">
                        <i class="fa fa-fw fa-globe"></i> <?= $this->__('backend.settings.domains.title') ?>
                        <a id="link_system_domains"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'domains']) ?>"
                           class="pull-right flip btn btn-sm btn-default"><?= $this->__('general.view') ?></a>
                    </div>
                    <div class="panel-body">
                        <?= $this->__('backend.settings.domains.description') ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-3">
                <div class="panel panel-dashboard">
                    <div class="panel-heading">
                        <i class="fa fa-fw fa-user-secret"></i> <?= $this->__('backend.settings.session.title') ?>
                        <a id="link_system_sessions"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'session']) ?>"
                           class="pull-right flip btn btn-sm btn-default"><?= $this->__('general.view') ?></a>
                    </div>
                    <div class="panel-body">
                        <p>Configure how long a session should last and manage control of resources for Backend and
                            Frontend users</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-3">
                <div class="panel panel-dashboard">
                    <div class="panel-heading">
                        <i class="fa fa-fw fa-cloud"></i> <?= $this->__('backend.settings.media.title') ?>
                        <a id="link_system_media"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'media']) ?>"
                           class="pull-right flip btn btn-sm btn-default"><?= $this->__('general.view') ?></a>
                    </div>
                    <div class="panel-body">
                        <?= $this->__('backend.settings.media.description') ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-3">
                <div class="panel panel-dashboard">
                    <div class="panel-heading">
                        <i class="fa fa-fw fa-puzzle-piece"></i> <?= $this->__('backend.settings.extensions.title') ?>
                        <a id="link_system_media"
                           href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'grid']) ?>"
                           class="pull-right flip btn btn-sm btn-default"><?= $this->__('general.view') ?></a>
                    </div>
                    <div class="panel-body">
                        <?= $this->__('backend.settings.extensions.description') ?>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>