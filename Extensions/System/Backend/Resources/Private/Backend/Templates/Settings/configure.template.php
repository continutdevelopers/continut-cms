<div class="row">
    <!-- left sidebar -->
    <div class="col-sm-12 col-md-3" id="settings_sidebar">
        <h2><?= $this->__('backend.menu.settings') ?></h2>
        <p><?= $this->__('backend.settings.description') ?></p>
        <div class="row">
            <div class="col-md-12">
                <form id="form_scope" method="post"
                      action="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'configure', 'section' => $selected]) ?>">
                    <?= $this->partial('General/domainsSelect', 'Backend', 'Backend', ['configurationSite' => $configurationSite, 'domains' => $domains]) ?>
                </form>
                <script type="text/javascript">
                    $('#configuration_site').on('change', function (event) {
                        $('#form_scope').submit();
                    });
                </script>
            </div>
        </div>

        <div class="row">
            <div class="list-group" id="settings_menu" role="tablist" aria-multiselectable="true">
                <?php if ($settings): ?>
                    <?php foreach ($settings as $key => $setting): ?>
                        <a class="list-group-item collapsed" data-toggle="collapse" data-parent="#settings_menu"
                           href="#settings_menu_<?= $key ?>" aria-expanded="false" aria-controls="settings_menu_<?= $key ?>">
                            <h4 class="list-group-item-heading">
                                <i class="fa fa-fw <?= $setting['icon'] ?>"></i> <?= $this->__($setting['label']) ?>
                            </h4>
                            <?php if (isset($setting['description'])): ?>
                                <p class="list-group-item-text"><?= $this->__($setting['description']) ?></p>
                            <?php endif; ?>
                        </a>
                        <div id="settings_menu_<?= $key ?>" class="list-group collapse" role="tabpanel">
                            <?php foreach ($setting['sections'] as $sectionKey => $section): ?>
                                <a class="<?= ($selected == ($key . '_' . $sectionKey) ? 'active' : '') ?> list-group-item"
                                   href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'configure', 'section' => $key . '_' . $sectionKey]) ?>">
                                    <i class="fa fa-fw <?= $section['icon'] ?>"></i> <?= $this->__($section['label']) ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <!-- main content -->
    <div class="col-sm-12 col-md-9">
        <?php if (isset($settings[$sectionParent]['sections'][$sectionChild])): ?>
            <?php $child = $settings[$sectionParent]['sections'][$sectionChild]; ?>
            <h3><?= $this->__($child['label']) ?></h3>
            <?php if (isset($child['panels'])): ?>
            <form method="post"
                  id="content_edit"
                  action="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'saveSettings', 'section' => $selected]) ?>">
                <?php foreach ($child['panels'] as $panel): ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <?= $this->__($panel['label']) ?>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php if (isset($panel['fields'])): ?>
                            <?php foreach ($panel['fields'] as $field): ?>
                                <?php if ($field['type'] === 'text'): ?>
                                    <?= $this->helper('FormSettings')->textField(
                                        $field['name'],
                                        $field['label'],
                                        (isset($config[$field['name']])) ? $config[$field['name']] : null,
                                        ['visibility' => $field['visibility']]
                                    );
                                    ?>
                                <?php elseif ($field['type'] === 'select'): ?>
                                    <?= $this->helper('FormSettings')->selectField(
                                        $field['name'],
                                        $field['label'],
                                        $field['values'],
                                        (isset($config[$field['name']])) ? $config[$field['name']] : null,
                                        ['visibility' => $field['visibility']]
                                    );
                                    ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endforeach; ?>
                <input type="submit" name="submit" class="btn btn-primary" value="<?= $this->__('general.save') ?>"/>
            </form>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<script>
    $('#settings_menu_<?= $sectionParent ?>').collapse('show');
</script>
