<?php if ($page): ?>
    <p class="lead"><?= $this->__('backend.page.wizard.create.description', ['page' => $page->getTitle()]) ?></p>
<?php else: ?>
    <p class="lead"><?= $this->__('backend.page.wizard.title.root') ?></p>
<?php endif ?>
<form method="post" id="form_page_wizard" class="form"
      action="<?= $this->helper("Url")->linkToPath('admin', ['_controller' => 'Page', '_action' => 'add', 'id' => (($page) ? $page->getId() : 0)]) ?>">
    <input type="hidden" name="domain_url_id" value=""/>
    <div class="row">
        <div class="col-sm-12">
            <p><strong><?= $this->__('backend.page.wizard.placement.title') ?></strong></p>
        </div>
        <?php if ($page): ?>
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                        <input type="radio" value="before" name="page_placement"
                               id="before_page">
                        <?= $this->__('backend.page.wizard.placement.before') ?>
                    </label>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($page): ?>
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                        <input type="radio" value="inside" name="page_placement" id="inside_page">
                        <?= $this->__('backend.page.wizard.placement.inside') ?>
                    </label>
                </div>
            </div>
        <?php endif ?>
        <div class="col-sm-4">
            <div class="radio">
                <label>
                    <input type="radio" value="after" name="page_placement" checked id="after_page">
                    <?php if ($page): ?>
                        <?= $this->__('backend.page.wizard.placement.after') ?>
                    <?php else: ?>
                        <?= $this->__('backend.page.wizard.root.placement.after') ?>
                    <?php endif; ?>
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="wizard_pages">
                <?= $this->helper('Form')->repeaterField('pages', $this->__('backend.page.wizard.pages.list'), $this->valueOrDefault('pages', []), 'Page/pageRepeater', 'Backend', 10, 'simple') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <hr/>
        <div class="col-sm-12">
            <input type="submit" id="btn-save" class="btn btn-primary btn-save"  value="<?= $this->__('backend.page.wizard.button.save') ?>"/>
            <a class="btn btn-default pull-right flip " data-dismiss="modal"><?= $this->__('backend.page.wizard.button.close') ?></a>
        </div>
    </div>
</form>
<script>

    // set the domain_url_id (also known as the "language")
    $('#form_page_wizard input[name=domain_url_id]').val($('#select_language').val());

    // Post form using FormData, if supported
    $('#form_page_wizard').on('submit', function () {
        var form = $(this);
        var formdata = false;
        if (window.FormData) {
            formdata = new FormData(form[0]);
        }
        var formAction = form.attr('action');
        $.ajax({
            url: formAction,
            data: formdata ? formdata : form.serialize(),
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.success) {
                    var index;
                    for (index = 0; index < data.pages.length; index++) {
                        var page = data.pages[index];
                        var inc = (page.position === 'after') ? (1 + index) : 0;
                        var selectedNode = $('#cms_tree').jstree(true).get_selected(true)[0];
                        var newPos = 0;
                        if (selectedNode !== undefined) {
                            if (selectedNode.parent !== '#') {
                                if (page.position === 'inside') {
                                    newPos = $('#' + selectedNode.parent + ' > ul > li').length;
                                } else {
                                    newPos = $('#' + selectedNode.parent + ' li').index($('#' + selectedNode.id)) + inc;
                                }
                            } else {
                                if (page.position === 'inside') {
                                    newPos = $('#' + selectedNode.id + ' > ul > li').length + index;
                                } else {
                                    newPos = $('#cms_tree > ul > li').index($('#' + selectedNode.id)) + inc;
                                }
                            }
                        } else {
                            if ($('#cms_tree li').length > 0) {
                                newPos = $('#cms_tree > ul > li').length;
                            }
                        }

                        if (page.position === 'inside') {
                            page.parent = selectedNode;
                        }
                        $('#cms_tree').jstree('create_node', page.parent, page.node, newPos)
                    }
                    addModal.close();
                } else {
                    // @TODO add error message
                }
            }
        });

        return false;
    });
</script>