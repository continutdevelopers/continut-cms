<div class="row page-line form-group">
    <div class="col-sm-6">
        <?= $this->helper('Form')->textField('name', $this->__('backend.page.wizard.placeholder.title'), $this->valueOrDefault('name', '')) ?>
    </div>
    <div class="col-sm-3">
        <?= $this->helper('Form')->selectField('layout', $this->__('backend.page.properties.pageLayout'), array_merge(['' => $this->__('backend.layout.selectLayout')], \Continut\Core\Utility::getLayouts()), $this->valueOrDefault('layout', '')) ?>
    </div>
    <div class="col-sm-3">
        <?= $this->helper('Form')->selectField('visibility', $this->__('backend.page.properties.visibility'),
            [
                'visible'        => $this->__('backend.page.wizard.state.visible'),
                'hidden'         => $this->__('backend.page.wizard.state.hidden'),
                'hidden_in_menu' => $this->__('backend.page.wizard.state.hiddenInMenu')
            ],
            $this->valueOrDefault('visibility', 'visible')
        ) ?>
    </div>
</div>