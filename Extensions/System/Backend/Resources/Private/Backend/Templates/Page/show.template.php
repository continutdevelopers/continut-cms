<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb breadcrumb-page-tree">
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <li><a class="page-link" href="#"
                       data-page-id="<?= $breadcrumb->getId() ?>"><?= $breadcrumb->getTitle(); ?></a></li>
            <?php endforeach ?>
            <li class="active"><?= $page->getTitle() ?> (<?= $page->getId() ?>)</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-xs-10">
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-warning" id="page-edit">
                <i class="fa fa-fw fa-pencil"></i> <span
                        class="visible-lg-inline"><?= $this->__('backend.page.edit') ?></span>
            </button>
            <a href="<?= $this->helper('Url')->linkToSlug($page, true) ?>" target="_blank" class="btn btn-info" id="page-view">
                <i class="fa fa-fw fa-mouse-pointer"></i> <span
                        class="visible-lg-inline"><?= $this->__('backend.page.view') ?></span>
            </a>
            <button type="button" class="btn btn-default" id="page-visibility-frontend">
                <span class="element-visible <?= $page->getIsVisible() ? '' : 'hide' ?>"><i
                            class="fa fa-fw fa-check"></i> <span
                            class="visible-lg-inline"><?= $this->__('backend.page.visibleInFrontend') ?></span></span>
                <span class="element-hide <?= $page->getIsVisible() ? 'hide' : '' ?>"><i
                            class="fa fa-fw fa-close text-danger"></i> <span
                            class="visible-lg-inline"><?= $this->__('backend.page.notVisibleInFrontend') ?></span></span>
            </button>
            <button type="button" class="btn btn-default" id="page-visibility-menu">
                <span class="element-visible <?= $page->getIsInMenu() ? '' : 'hide' ?>"><i
                            class="fa fa-fw fa-eye"></i> <span
                            class="visible-lg-inline"><?= $this->__('backend.page.visibleInMenu') ?></span></span>
                <span class="element-hide <?= $page->getIsInMenu() ? 'hide' : '' ?>"><i
                            class="fa fa-fw fa-eye-slash text-danger"></i> <span
                            class="visible-lg-inline"><?= $this->__('backend.page.notVisibleInMenu') ?></span></span>
            </button>
        </div>
    </div>
    <div class="col-xs-2">
        <div class="btn-group pull-right flip" role="group">
            <button type="button" class="btn btn-danger" id="page-delete"><i
                        class="fa fa-fw fa-trash"></i> <?= $this->__('backend.page.deletePage') ?></button>
        </div>
    </div>
</div>

<div class="progress-loader text-center loader-page-save-properties">
    <img src="<?= $this->helper('Image')->getPath('Images/spin.svg', 'Backend') ?>" alt=""/>
</div>

<div id="page_edit_block" class="row"></div>
<div id="scroll_top" class="scroll-overlap top">
    <p class="text-center">
        <span class="fa fa-arrow-up fa-3x"></span>
    </p>
</div>
<div class="panel panel-warning page-panel <?= $backendUser->getAttribute('touchEnabled', false) ? 'touch-friendly' : ''; ?>">
    <div class="panel-heading"><i class="fa fa-fw fa-file-o"></i> <span
                class="current-page-title"><?= $page->getTitle() ?></span></div>
    <div class="panel-body">
        <?= $pageContent ?>
    </div>
</div>
<div id="scroll_bottom" class="scroll-overlap bottom">
    <p class="text-center">
        <span class="fa fa-arrow-down fa-3x"></span>
    </p>
</div>
<script type="text/javascript">
    // since we're loading, editing pages/content dynamically, we need to clear the existing CKEditor instances
    // everytime we go from edit content mode to show page mode
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].destroy();
    }

    $('.page-link').on('click', function () {
        $.ajax({
            url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Page', '_action' => 'show']) ?>',
            data: {page_id: $(this).data('page-id')}
        })
            .done(function (data) {
                $('#content').html(data);
            });
    });
    $('#page-visibility-frontend').on('click', function () {
        $.ajax({
            url: '<?= $this->helper('Url')->LinkToPath('admin', ['_controller' => 'Page', '_action' => 'toggleVisibility']) ?>',
            data: {page_id: <?= $page->getId() ?> },
            dataType: 'json'
        })
            .done(function (data) {
                var node = $('#cms_tree').jstree(true).get_node(data.pid);
                var nodeId = '#' + node.a_attr.id;
                if (data.visible) {
                    $('#page-visibility-frontend .element-visible').removeClass("hide");
                    $('#page-visibility-frontend .element-hide').addClass("hide");
                    $(nodeId).find(".jstree-icon").first().removeClass("fa-disabled");
                } else {
                    $('#page-visibility-frontend .element-visible').addClass("hide");
                    $('#page-visibility-frontend .element-hide').removeClass("hide");
                    $(nodeId).find(".jstree-icon").first().addClass("fa-disabled");
                }
            });
    });
    $('#page-visibility-menu').on('click', function () {
        $.ajax({
            url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Page', '_action' => 'toggleMenu']) ?>',
            data: {page_id: <?= $page->getId() ?> },
            dataType: 'json'
        })
            .done(function (data) {
                var node = $('#cms_tree').jstree(true).get_node(data.pid);
                var nodeId = '#' + node.a_attr.id;
                if (data.isInMenu) {
                    $('#page-visibility-menu .element-visible').removeClass("hide");
                    $('#page-visibility-menu .element-hide').addClass("hide");
                    $(nodeId).find(".jstree-icon").first().addClass("fa-file").removeClass("fa-eye-slash text-danger");
                } else {
                    $('#page-visibility-menu .element-visible').addClass("hide");
                    $('#page-visibility-menu .element-hide').removeClass("hide");
                    $(nodeId).find(".jstree-icon").first().removeClass("fa-file").addClass("fa-eye-slash text-danger");
                }
            });
    });
    $('#page-edit').on('click', function () {
        if ($('#page_edit_block').is(':empty')) {
            $.ajax({
                url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Page', '_action' => 'edit']) ?>',
                data: {page_id: <?= $page->getId() ?> }
            }).done(function (data) {
                $('#page_edit_block').html(data);
            });
        }
    });
    $('#page-delete').on('click', function () {
        BootstrapDialog.confirm({
            message: <?= json_encode($this->__('backend.page.deletePage.confirm', ['page' => $page->getTitle()])) ?>,
            title: <?= json_encode($this->__('backend.page.deletePage')) ?>,
            type: BootstrapDialog.TYPE_DANGER,
            btnCancelLabel: '<?= $this->__('general.cancel'); ?>',
            btnOKLabel: '<?= $this->__('general.yes'); ?>',
            callback: function (result) {
                // if user confirms, send delete request
                if (result) {
                    $.getJSON({
                        url: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Page', '_action' => 'delete']) ?>',
                        data: {page_id: <?= $page->getId() ?> }
                    }).done(function (data) {
                        if (data.success) {
                            $('#cms_tree').jstree('delete_node', '#<?= $page->getId() ?>');
                        } else {
                            // @TODO : add error message with details
                        }
                    });
                }
            }
        });
    });

    $('.content-wizard').on('click', function (e) {
        e.preventDefault();
        BootstrapDialog.show({
            title: <?= json_encode($this->__('backend.content.wizard.create.title')) ?>,
            message: $('<div></div>').load($(this).attr('href'))
        })
    });

    // dropdown with operations related to content elements
    $('.content-operation-link').on('click', function (ev) {
        ev.preventDefault();

        var url = $(this).attr('href');

        $.getJSON(url, function (data) {
            if (data.operation === "copy") {
                if (data.success) {
                    $('.panel-backend-content .group-paste').removeClass('hide');
                }
            }
            if (data.operation === "cut") {
                if (data.success) {
                    $('.panel-backend-content .group-paste').removeClass('hide');
                }
            }
            if (data.operation === "paste") {
                if (data.success) {
                    $('.panel-backend-content .group-paste').addClass('hide');
                }
            }
            if (data.operation === "delete") {
                $('#panel-backend-content-' + data.id).remove();
            }
            if (data.operation === "edit") {
                $('#content').html(data.html);
            }
            if (data.operation === "toggleVisibility") {
                var $panel = $('#panel-backend-content-' + data.id);
                if (data.visible) {
                    $panel.removeClass('panel-hidden');
                } else {
                    $panel.addClass('panel-hidden');
                }
                var $visibilityLink = $panel.children('.panel-heading').find('.operation-visibility');
                $visibilityLink.html(
                    $visibilityLink.html().replace($.trim($visibilityLink.text()), data.menuText)
                );
                $panel.children('.panel-heading').find('.title-append').text(data.infoText);
            }
        });
    });

    $('.content-drag-sender').pep({
        useCSSTranslation: false,
        droppable: '.content-drag-receiver',
        elementsWithInteraction: '.no-pep',
        place: false,
        revert: true,
        //debug: true,
        allowDragEventPropagation: false,
        prevParent: null,
        prevBrother: null,
        revertIf: function (ev, obj) {
            return !this.activeDropRegions.length;
        },
        cssEaseDuration: 200,
        stop: function (ev, obj) {
            $('.scroll-overlap').hide();
            $('html, body').stop(true);
            if (this.activeDropRegions.length > 0) {
                // get first drop region and set the element as it's child
                var dropInto = $(this.activeDropRegions[0]);
                var target = dropInto.closest('.container-receiver');
                var beforeId = target.prevObject.next(".panel-backend-content").data('id');
                if (target) {
                    dropInto.addClass('loader');
                    $.getJSON('<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Content', '_action' => 'updateContainer']) ?>',
                        {
                            parent_id: target.data('parent'),
                            column_id: target.data('id'),
                            id: this.$el.data('id'),
                            before_id: beforeId
                        })
                        .always(function (data) {
                            if (data.success) {
                                dropInto.after(obj.$el);
                                obj.$el.removeAttr('style');
                            } else {
                                obj.revert();
                                obj.$el.removeAttr('style');
                                // @TODO : show an error message
                            }
                            $('.content-drag-receiver').remove();
                        });
                } else {
                    $('.content-drag-receiver').remove();
                    obj.reAttach();
                }
            } else {
                $('.content-drag-receiver').remove();
                obj.reAttach();
            }
        },
        start: function (ev, obj) {
            $('.scroll-overlap').show();
        },
        drag: function(ev, obj) {
            // scroll the page if we're overlaping the scrollTop or scrollBottom divs
            var rect1 = obj.el.getBoundingClientRect();
            var rect2 = $('#scroll_bottom')[0].getBoundingClientRect();
            var rect3 = $('#scroll_top')[0].getBoundingClientRect();

            var overlapBottom = !(rect1.right < rect2.left ||
                rect1.left > rect2.right ||
                rect1.bottom < rect2.top ||
                rect1.top > rect2.bottom);

            var overlapTop = !(rect1.right < rect3.left ||
                rect1.left > rect3.right ||
                rect1.bottom < rect3.top ||
                rect1.top > rect3.bottom);

            if (overlapBottom || overlapTop) {
                if (overlapTop) {
                    $('html, body').animate({ scrollTop: 0 }, 3000);
                } else {
                    $('html, body').animate({scrollTop: $(document).height()}, 3000);
                }
            } else {
                $('html, body').stop(true);
            }
        },
        initiate: function (ev, obj) {
            // we need to see if the class is not applied already, as the touch/click events
            // are called twice on tablets/mobiles
            if (!$('.content-drag-receiver').length) {
                this.options.prevParent = obj.$el.parent();
                this.options.prevBrother = obj.$el.prev();
                obj.$el.css({left: obj.$el.offset().left, top: obj.$el.offset().top});
                obj.$el.detach().appendTo('body');
                $('.container-receiver').prepend($('<div class="content-drag-receiver receiver-on"></div>'));
                $('.panel-backend-content').each(function (index, item) {
                    if (!$(item).is(obj.$el)) {
                        $(item).after($('<div class="content-drag-receiver receiver-on"></div>'));
                    }
                });
            }
        }
    });
</script>