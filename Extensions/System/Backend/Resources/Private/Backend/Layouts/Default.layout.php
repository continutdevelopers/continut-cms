<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://<?= $this->getUrl() ?>">
    <link rel="shortcut icon" href="/Public/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="/Public/apple-touch-icon.png?v=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/Public/favicon-32x32.png?v=1">
    <link rel="icon" type="image/png" sizes="16x16" href="/Public/favicon-16x16.png?v=1">
    <link rel="manifest" href="/Public/manifest.json?v=1">
    <link rel="mask-icon" href="/Public/safari-pinned-tab.svg?v=1" color="#5bbad5">
    <link rel="shortcut icon" href="/Public/favicon.ico?v=1">
    <meta name="apple-mobile-web-app-title" content="Continut CMS">
    <meta name="application-name" content="Continut CMS">
    <meta name="msapplication-config" content="/Public/browserconfig.xml?v=1">
    <meta name="theme-color" content="#ffffff">
    <title><?= $this->getTitle() ?></title>

    <?= $this
        // css files
        ->addCssAsset(['identifier' => 'bootstrap', 'extension' => 'Backend', 'file' => 'bootstrap/bootstrap.css'])
        //->addCssAsset(['identifier' => 'bootstrap-rtl',      'extension' => 'Backend', 'file' => 'bootstrap-rtl/bootstrap-rtl.css'])
        //->addCssAsset(['identifier' => 'bootstrap-theme',    'extension' => 'Backend', 'file' => 'bootstrap/bootstrap-theme.css'])
        ->addCssAsset(['identifier' => 'bootstrap-select', 'extension' => 'Backend', 'file' => 'bootstrap-select/bootstrap-select.css'])
        ->addCssAsset(['identifier' => 'bootstrap-dialog', 'extension' => 'Backend', 'file' => 'bootstrap-dialog/bootstrap-dialog.css'])
        ->addCssAsset(['identifier' => 'daterangepicker', 'extension' => 'Backend', 'file' => 'bootstrap-daterangepicker/daterangepicker.css'])
        ->addCssAsset(['identifier' => 'fontawesome', 'extension' => 'Backend', 'file' => 'fontawesome/font-awesome.css'])
        ->addCssAsset(['identifier' => 'flagicons', 'extension' => 'Backend', 'file' => 'flagicons/flag-icon.css'])
        ->addCssAsset(['identifier' => 'tokenize2', 'extension' => 'Backend', 'file' => 'tokenize2/tokenize2.min.css'])
        ->addCssAsset(['identifier' => 'jstree', 'extension' => 'Backend', 'file' => 'jstree/themes/continut/style.css'])
        ->addCssAsset(['identifier' => 'local', 'extension' => 'Backend', 'file' => 'local/themes/standard.css'])
        ->addJsAsset(['identifier' => 'jquery', 'extension' => 'Backend', 'file' => 'jquery/jquery-3.1.1.min.js'])
        ->addJsAsset(['identifier' => 'bootstrap', 'extension' => 'Backend', 'file' => 'bootstrap/bootstrap.min.js'])
        ->addJsAsset(['identifier' => 'bootstrap-select', 'extension' => 'Backend', 'file' => 'bootstrap-select/bootstrap-select.js'])
        ->addJsAsset(['identifier' => 'bootstrap-dialog', 'extension' => 'Backend', 'file' => 'bootstrap-dialog/bootstrap-dialog.js'])
        ->addJsAsset(['identifier' => 'bootstrap-notify', 'extension' => 'Backend', 'file' => 'bootstrap-notify/bootstrap-notify.js'])
        ->addJsAsset(['identifier' => 'ajax-uploader', 'extension' => 'Backend', 'file' => 'ajax-uploader/SimpleAjaxUploader.js'])
        ->addJsAsset(['identifier' => 'moment', 'extension' => 'Backend', 'file' => 'bootstrap-daterangepicker/moment.min.js'])
        ->addJsAsset(['identifier' => 'phpmoment', 'extension' => 'Backend', 'file' => 'bootstrap-daterangepicker/phpmoment.js'])
        ->addJsAsset(['identifier' => 'daterangepicker', 'extension' => 'Backend', 'file' => 'bootstrap-daterangepicker/daterangepicker.js'])
        ->addJsAsset(['identifier' => 'jstree', 'extension' => 'Backend', 'file' => 'jstree/jstree.js'])
        ->addJsAsset(['identifier' => 'pep', 'extension' => 'Backend', 'file' => 'pep/jquery.pep.js'])
        ->addJsAsset(['identifier' => 'ckeditor', 'extension' => 'Backend', 'file' => 'ckeditor/ckeditor.js'])
        ->addJsAsset(['identifier' => 'html5-sortable', 'extension' => 'Backend', 'file' => 'sortable/sortable.min.js'])
        ->addJsAsset(['identifier' => 'html5-sortable-jq', 'extension' => 'Backend', 'file' => 'sortable/jquery.binding.js'])
        ->addJsAsset(['identifier' => 'tokenize2', 'extension' => 'Backend', 'file' => 'tokenize2/tokenize2.min.js'])
        ->addJsAsset(['identifier' => 'local', 'extension' => 'Backend', 'file' => 'local/backend.js'])
        ->renderAssets();
    ?>
    <?= $this->renderDebugbarHeader() ?>

    <meta name="description" content="Conținut CMS"/>
    <meta name="keywords" content=""/>

</head>
<?php if ($this->getBodyClass()) : ?>
<body class="<?= $this->getBodyClass() ?>"><?php else: ?>
<body><?php endif ?>

    <nav class="navbar navbar-default" id="main_toolbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#main_toolbar_collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"
                   href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Index', '_action' => 'dashboard']); ?>">
                    <h1><img src="<?= $this->helper('Image')->getPath('Images/logo_negru.svg', 'Backend'); ?>" height="32"
                             alt="<?= $this->__('product.name') ?>"
                             class="flip pull-left"/> <?= $this->__('product.name') ?> <br/>
                        <small><?= $this->__('product.version', ['version' => \Continut\Core\Utility::getVersion()]) ?></small>
                    </h1>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="main_toolbar_collapse">
                <form class="navbar-form navbar-left" id="search_form">
                    <div class="input-group">
                        <select name="search_term" id="global_search_query" multiple></select>
                        <!--<input type="text" class="form-control" id="global_search_query" placeholder="search">-->
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
                <script type="text/javascript">
                    // global search box
                    $('#global_search_query').tokenize2({
                        dataSource: '<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Search', '_action' => 'all']) ?>',
                        tokensMaxItems: 1,
                        searchMinLength: 2,
                        debounce: 250
                    });
                </script>

                <ul class="nav navbar-nav navbar-right">
                    <!--<li>
                        <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'favorites']) ?>">
                            <i class="fa fa-fw fa-2x fa-star-o"></i> <?= $this->__('backend.menu.favorites') ?>
                        </a>
                    </li>-->
                    <li class="dropdown">
                        <?= \Continut\Core\Utility::callPlugin('Backend', 'Index', 'notifications'); ?>
                    </li>
                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'index']) ?>" role="button" aria-expanded="false" title="<?= $this->__('backend.menu.settings') ?>">
                            <i class="fa fa-fw fa-2x fa-cogs"></i> <?= $this->__('backend.menu.settings') ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'domains']) ?>">
                                    <i class="fa fa-fw fa-globe"></i> <?= $this->__('backend.settings.domains.title') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Extension', '_action' => 'grid']) ?>">
                                    <i class="fa fa-fw fa-puzzle-piece"></i> <?= $this->__('backend.settings.extensions.title') ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Settings', '_action' => 'configure', 'section' => 'system_session']) ?>">
                                    <i class="fa fa-fw fa-cog"></i> <?= $this->__('backend.settings.all') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            <?php if (\Continut\Core\Utility::getSession()->getUser()->getImage()): ?>
                                <img src="<?= $this->helper('Image')->crop(\Continut\Core\Utility::getSession()->getUser()->getImage(), 24, 24, 'backend'); ?>"
                                     alt=""
                                     class="img-circle"> <?= \Continut\Core\Utility::getSession()->getUser()->getName(); ?>
                            <?php else: ?>
                                <img src="<?= $this->helper('Image')->getPath('Images/profile_missing.jpg', 'Backend'); ?>"
                                     height="24" alt=""
                                     class="img-circle"> <?= \Continut\Core\Utility::getSession()->getUser()->getName(); ?>
                            <?php endif; ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'User', '_action' => 'profile']) ?>">
                                    <i class="fa fa-fw fa-user-o"></i> <?= $this->__('backend.menu.user.editProfile') ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= $this->helper('Url')->linkToPath('admin', ['_controller' => 'Login', '_action' => 'logout']) ?>">
                                    <i class="fa fa-fw fa-sign-out"></i> <?= $this->__('backend.menu.user.logout') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-default" id="main_menu">
        <div class="container-fluid">
            <?= \Continut\Core\Utility::callPlugin('Backend', 'Index', 'mainmenu'); ?>
        </div>
    </nav>
    <div id="container" class="container-fluid">
        <?= $this->showContent(); ?>
    </div>
    <?= $this->renderDebugbarContent() ?>
</body>
</html>