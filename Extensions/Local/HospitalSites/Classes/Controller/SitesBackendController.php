<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.05.2015 @ 22:28
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Local\HospitalSites\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class SitesBackendController extends BackendController
{
    /**
     * JobsBackendController constructor - set default layout
     */
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Grid with all the available sites
     */
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $grid
            ->setFormAction(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_extension' => 'HospitalSites',
                        '_controller' => 'SitesBackend',
                        '_action' => 'grid'
                    ]
                )
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(
                Utility::createInstance(
                    'Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection\SiteCollection'
                )
            )
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'name'        => [
                        'label'    => 'backend.ext.hospitalSites.grid.field.name',
                        'css'      => 'col-sm-4',
                        'renderer' => [
                            'parameters' => ['crop' => 200, 'cropAppend' => '...', 'removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'street'      => [
                        'label'    => 'backend.ext.hospitalSites.grid.field.street',
                        'css'      => 'col-sm-5',
                        'renderer' => [
                            'parameters' => ['crop' => 200, 'cropAppend' => '...', 'removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'isVisible'   => [
                        'label'    => 'backend.ext.hospitalSites.grid.field.isVisible',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer'
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'isInPreview' => [
                        'label'    => 'backend.ext.hospitalSites.grid.field.isInPreview',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer'
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'actions'     => [
                        'label'    => 'backend.ext.hospitalSites.grid.field.actions',
                        'css'      => 'col-sm-1 text-right flip',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\System\Backend\Classes\View\Renderer\ActionsRenderer',
                            'parameters' => [
                                'showEdit'   => true,
                                'showDelete' => true,
                                'extension'  => 'HospitalSites',
                                'controller' => 'SitesBackend'
                            ]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * Create a new hospital site - action
     */
    public function createAction()
    {
        $site = Utility::createInstance('Continut\Extensions\Local\HospitalSites\Classes\Domain\Model\Site');

        $this->getView()->assign('site', $site);
    }

    /**
     * Create or Update the Job model
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        // reference the collection
        $siteCollection = Utility::createInstance(
            'Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection\SiteCollection'
        );

        // if no id is present it means we're dealing with a new job that we need to create
        // otherwise it's an existing one that we need to update
        if ($id == 0) {
            $site = Utility::createInstance('Continut\Extensions\Local\HospitalSites\Classes\Domain\Model\Site');
        } else {
            $site = $siteCollection->findById($id);
        }
        $site->update($data);

        if ($site->validate()) {
            $siteCollection
                ->reset()
                ->add($site)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('backend.ext.hospitalSites.flash.siteSaved'));

            // redirect to the list view since all went well and data is saved
            $this->redirect(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_extension' => 'HospitalSites',
                        '_controller' => 'SitesBackend',
                        '_action' => 'grid'
                    ]
                )
            );
        }

        $this->getView()->assign('site', $site);
    }

    /**
     * Edit Site
     */
    public function editAction()
    {
        $id = $this->getRequest()->getArgument('id', 0);

        if ($id > 0) {
            $siteCollection = Utility::createInstance(
                'Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection\SiteCollection'
            );

            $site = $siteCollection->findById($id);

            $this->getView()->assign('site', $site);
        } else {
            throw new ErrorException('The record id you supplied is invalid');
        }
    }
}
