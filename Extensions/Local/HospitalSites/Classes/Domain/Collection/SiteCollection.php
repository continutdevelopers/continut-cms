<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.01.2016 @ 18:04
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class SiteCollection extends BaseCollection
{
    /**
     * Set tablename and each element's class
     */
    public function __construct()
    {
        $this->tablename    = 'ext_hospitalsites_site';
        $this->elementClass = 'Continut\Extensions\Local\HospitalSites\Classes\Domain\Model\Site';
    }

    /**
     * Get all visible and non-deleted jobs
     *
     * @param bool $preview
     *
     * @return $this
     */
    public function findAllVisible($preview = false)
    {
        $where = 'is_deleted = 0 AND is_visible = 1';
        if ($preview) {
            $where .= ' AND is_in_preview = 1';
        }

        return $this->where($where);
    }
}
