<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 21.05.2017 @ 12:50
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Local\HospitalSites\Classes\Domain\Model;

use Continut\Core\System\Domain\Model\BaseModel;
use Respect\Validation\Validator as v;

class Site extends BaseModel
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $isDeleted;

    /**
     * @var bool
     */
    protected $isVisible;

    /**
     * @var bool
     */
    protected $isInPreview;

    /**
     * @var string
     */
    protected $street;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $zip;

    /**
     * @var string
     */
    protected $country;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $fax;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $latitude;

    /**
     * @var string
     */
    protected $longitude;

    /**
     * @var string
     */
    protected $cssPosition;

    public function dataMapper()
    {
        $fields = [
            'name'          => $this->name,
            'is_in_preview' => $this->isInPreview,
            'is_visible'    => $this->isVisible,
            'is_deleted'    => $this->isDeleted,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'phone'         => $this->phone,
            'fax'           => $this->fax,
            'email'         => $this->email,
            'street'        => $this->street,
            'zip'           => $this->zip,
            'country'       => $this->country,
            'city'          => $this->city,
            'css_position'  => $this->cssPosition
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * Define the list of fields to validate and the validators to validate against
     *
     * @return array List of field names and their validators
     */
    public function dataValidation()
    {
        return [
            'name'      => v::length(3, 200),
            //'email'     => v::email(),
            'street'    => v::notEmpty(),
            'latitude'  => v::notEmpty(),
            'longitude' => v::notEmpty()
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     *
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsInPreview()
    {
        return $this->isInPreview;
    }

    /**
     * @param bool $isInPreview
     *
     * @return $this
     */
    public function setIsInPreview($isInPreview)
    {
        $this->isInPreview = $isInPreview;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return $this
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     *
     * @return $this
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     *
     * @return $this
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     *
     * @return $this
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getCssPosition()
    {
        return $this->cssPosition;
    }

    /**
     * @param string $cssPosition
     *
     * @return $this
     */
    public function setCssPosition($cssPosition)
    {
        $this->cssPosition = $cssPosition;

        return $this;
    }

    /**
     * Grab the position on the map as the CSS style 'top' and 'left' values
     *
     * @return string
     */
    public function getMapPositions()
    {
        $position = explode(';', $this->getCssPosition());
        if (is_array($position) && sizeof($position) == 2) {
            return 'left:' . $position[0] . '; top: '. $position[1];
        }
        return '';
    }

    /**
     * @return string
     */
    public function formatForFilter()
    {
        return $this->getName();
    }
}
