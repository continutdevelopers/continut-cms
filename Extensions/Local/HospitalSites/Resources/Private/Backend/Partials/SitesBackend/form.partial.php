<form action="<?= $this->helper('Url')->linkToPath('admin', ['_extension' => 'HospitalSites', '_controller' => 'SitesBackend', '_action' => 'save']) ?>"
      method="post">
    <div class="panel panel-form">
        <div class="panel-heading">
            <div class="panel-title">
                <?= $this->__('backend.ext.hospitalSites.createNew') ?>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <?php if ($site->getId()): ?>
                        <?= $this->helper('FormObject')->hiddenField($site, 'id') ?>
                    <?php endif; ?>
                    <?= $this->helper('FormObject')->textField($site, 'name', $this->__('model.ext_hospitalsites_site.name')); ?>
                    <?= $this->helper('FormObject')->textField($site, 'street', $this->__('model.ext_hospitalsites_site.street')); ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <?= $this->helper('FormObject')->textField($site, 'zip', $this->__('model.ext_hospitalsites_site.zip')); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?= $this->helper('FormObject')->textField($site, 'city', $this->__('model.ext_hospitalsites_site.city')); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?= $this->helper('FormObject')->textField($site, 'country', $this->__('model.ext_hospitalsites_site.country')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <?= $this->helper('FormObject')->selectField($site, 'is_visible',
                                $this->__('model.ext_hospitalsites_site.is_visible'),
                                [
                                    1 => $this->__('general.yes'),
                                    0 => $this->__('general.no'),
                                ]
                            ) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $this->helper('FormObject')->selectField($site, 'is_in_preview',
                                $this->__('model.ext_hospitalsites_site.is_in_preview'),
                                [
                                    1 => $this->__('general.yes'),
                                    0 => $this->__('general.no'),
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <?= $this->helper('FormObject')->textField($site, 'latitude', $this->__('model.ext_hospitalsites_site.latitude')); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $this->helper('FormObject')->textField($site, 'longitude', $this->__('model.ext_hospitalsites_site.longitude')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <?= $this->helper('FormObject')->textField($site, 'email', $this->__('model.ext_hospitalsites_site.email')); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?= $this->helper('FormObject')->textField($site, 'phone', $this->__('model.ext_hospitalsites_site.phone')); ?>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <?= $this->helper('FormObject')->textField($site, 'fax', $this->__('model.ext_hospitalsites_site.fax')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label><?= $this->__('model.ext_hospitalsites_site.css_position') ?></label>
                                <?= $this->helper('FormObject')->hiddenField($site, 'css_position'); ?>
                                <div style="position: relative; overflow: hidden; max-width: 427px;">
                                    <img id="map-valais" class="img-responsive" style="position: relative" src="<?= $this->helper('Image')->getPath('Images/map_valais.svg', 'ThemeHdv'); ?>" width="427" height="290" alt="">
                                    <div id="map-pin" style="position: absolute; width: 10px; height: 10px; border-radius: 50%; background: red; z-index: 25; <?= $site->getMapPositions() ?>"></div>
                                </div>
                                <script type="text/javascript">
                                    $('#map-valais').on('click', function(event) {
                                        var leftPercentage = event.offsetX / $(this).width() * 100; var topPercentage = event.offsetY / $(this).height() * 100;
                                        $('#map-pin').css('left', leftPercentage.toFixed(3) + '%').css('top', topPercentage.toFixed(3) +'%').show();
                                        $('#field_css_position').val(leftPercentage.toFixed(3) + '%;' + topPercentage.toFixed(3) +'%;')
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <input type="submit" name="submit" class="btn btn-primary" value="<?= $this->__('general.save') ?>"/>
            <a href="<?= $this->helper('Url')->linkToPath('admin', ['_extension' => 'HospitalSites', '_controller' => 'SitesBackend', '_action' => 'grid']) ?>"
               class="close-button btn btn-danger pull-right flip"><?= $this->__('general.cancel') ?></a>
        </div>
    </div>
</form>