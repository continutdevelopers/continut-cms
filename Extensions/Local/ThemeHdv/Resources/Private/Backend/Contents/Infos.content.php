<?php if ($items): ?>
    <div class="row">
        <?php foreach ($items as $item): ?>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="text-center" style="margin-bottom: 20px">
                    <span style="font-size: 24px" class="hdvicon-<?= $item['icon'] ?>"></span>
                <?php if (isset($item['title'])): ?>
                    <h2 style="margin: 3px 0; font-size: 24px; font-weight: bold; color: #c31a1f"><?= $item['title'] ?></h2>
                <?php endif; ?>
                <?php if (isset($item['subtitle'])): ?>
                    <?= $item['subtitle'] ?>
                <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <script type="text/javascript">
    appendAsset(
        '<?= \Continut\Core\Utility::getAssetPath('Css/Backend/icons.css', 'ThemeHdv')?>',
        'css',
    );
    </script>
<?php endif; ?>