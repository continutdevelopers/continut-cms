<?php
$sites = \Continut\Core\Utility::callPlugin('ThemeHdv', 'Content', 'sites');
?>
<p><em><?= $this->valueOrDefault('subtitle', '') ?></em></p>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <?= $this->valueOrDefault('description', '') ?>
        <div style="position: relative; overflow: hidden; max-width: 427px;">
            <img id="map-valais" class="img-responsive" style="position: relative" src="<?= $this->helper('Image')->getPath('Images/map_valais.svg', 'ThemeHdv'); ?>" width="427" height="290" alt="">
            <?php if ($sites): ?>
                <?php foreach ($sites as $index => $site): ?>
                    <div class="point" id="h_<?= $site->getId() ?>" data-hospital="<?= $site->getId() ?>" style="position: absolute; width: 10px; height: 10px; border-radius: 50%; background: gray; z-index: 25; <?= $site->getMapPositions() ?>" title="<?= $site->getName() ?>"></div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <img class="img-responsive" src="<?= $this->helper('Image')->getPath('Images/home_temp1.jpg', 'ThemeHdv'); ?>" alt=""/>
    </div>
</div>
