<?php $slides = $this->valueOrDefault('slides', []); ?>
<?php if ($slides): ?>
<div id="carousel_be_preview_<?= $record->getId() ?>" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ($slides as $index => $slide): ?>
            <li data-target="#carousel-example-generic"
                data-slide-to="<?= $index ?>" <?= ($index == 1) ? 'class="active"' : '' ?>></li>
        <?php endforeach; ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php foreach ($slides as $index => $slide): ?>
            <div class="item <?= ($index == 1) ? 'active' : '' ?>">
                <?php if ($slide['image']): ?>
                    <img class="img-responsive"
                         src="<?= $this->helper('Image')->crop($slide['image'], 1024, 400, 'backend') ?>" alt=""/>
                <?php else: ?>
                    <img class="img-responsive"
                         src="<?= $this->helper('Image')->crop(\Continut\Core\Utility::getAssetPath('Images/dummy_slide.jpg', 'ThemeHdv'), 1024, 400, 'backend') ?>" alt=""/>
                <?php endif; ?>
                <div class="carousel-caption">
                    <h3><?= $slide['title'] ?></h3>
                    <p><?= $this->helper('Text')->stripTags($slide['description']) ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel_be_preview_<?= $record->getId() ?>" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel_be_preview_<?= $record->getId() ?>" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php else: ?>
    <p class="text-danger text-center"><?= $this->__('backend.themeHdv.type.content.swiperHeader.noSlidesDefined') ?></p>
<?php endif; ?>