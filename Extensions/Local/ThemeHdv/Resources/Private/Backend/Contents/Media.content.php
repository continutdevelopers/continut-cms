<?php $content = $this->helper('Text')->truncate($this->helper('Text')->stripTags($this->valueOrDefault('content', '')), 500) ?>
<?php $imagePosition = $this->valueOrDefault('imagePosition', 'before'); ?>
<?php $imageRatio = $this->valueOrDefault('imageTextRatio', '1') ?>
<?php $textRatio = 12 - $imageRatio; ?>
<?php $imageColumns = $this->valueOrDefault('imageColumns', '1') ?>
<?php $images = (empty($images)) ? [] : explode(',', $images); ?>
<?php if ($images): ?>
    <?php if ($imagePosition == 'right'): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-<?= $textRatio ?>">
                <p><?= $content ?></p>
            </div>
            <div class="col-xs-12 col-sm-<?= $imageRatio ?>">
                <?= $this->partial('Content/Images', 'ThemeHdv', 'Backend', ['images' => $images, 'columns' => $imageColumns]); ?>
            </div>
        </div>
    <?php elseif ($imagePosition == 'left'): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-<?= $imageRatio ?>">
                <?= $this->partial('Content/Images', 'ThemeHdv', 'Backend', ['images' => $images, 'columns' => $imageColumns]); ?>
            </div>
            <div class="col-xs-12 col-sm-<?= $textRatio ?>">
                <p><?= $content ?></p>
            </div>
        </div>
    <?php elseif ($imagePosition == 'before'): ?>
        <p>
            <?= $this->partial('Content/Images', 'ThemeHdv', 'Backend', ['images' => $images, 'columns' => $imageColumns]); ?>
        </p>
        <p><?= $content ?></p>
    <?php elseif ($imagePosition == 'after'): ?>
        <p><?= $content ?></p>
        <?= $this->partial('Content/Images', 'ThemeHdv', 'Backend', ['images' => $images, 'columns' => $imageColumns]); ?>
    <?php else: ?>
        <p><?= $content ?></p>
    <?php endif; ?>
<?php else: ?>
    <p><?= $content ?></p>
<?php endif; ?>
