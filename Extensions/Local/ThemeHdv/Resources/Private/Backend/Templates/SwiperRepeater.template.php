<div class="row">
    <div class="col-xs-6">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeHdv.type.content.swiperHeader.field.title'), $this->valueOrDefault('title', '')) ?>
    </div>
    <div class="col-xs-6">
        <?= $this->helper('Form')->textField('subtitle', $this->__('backend.themeHdv.type.content.swiperHeader.field.subtitle'), $this->valueOrDefault('subtitle', '')) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $this->helper('Form')->textareaField('description', $this->__('backend.themeHdv.type.content.swiperHeader.field.description'), $this->valueOrDefault('description', '')) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $this->helper('Form')->mediaField('image', $this->__('backend.themeHdv.type.content.swiperHeader.field.image'), $this->valueOrDefault('image', null), ['maxFiles' => 1]) ?>
    </div>
</div>