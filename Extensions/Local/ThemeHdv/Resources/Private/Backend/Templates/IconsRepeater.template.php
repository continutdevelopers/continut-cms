<?php
$icons = [
    ''           => '',
    'hospital'   => 'Hospital',
    'personel'   => 'Personel',
    'bed'        => 'Bed',
    'graph'      => 'Graph',
    'steto'      => 'Steto',
    'heart'      => 'Heart',
    'calculator' => 'Calculator'
];
?>
<div class="row">
    <div class="col-sm-4">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeHdv.type.content.icons.field.title'), $this->valueOrDefault('title', '')) ?>
    </div>
    <div class="col-sm-4">
        <?= $this->helper('Form')->textField('subtitle', $this->__('backend.themeHdv.type.content.icons.field.subtitle'), $this->valueOrDefault('subtitle', '')) ?>
    </div>
    <div class="col-sm-4">
        <?= $this->helper('Form')->selectField('icon', $this->__('backend.themeHdv.type.content.icons.field.icon'), $icons, $this->valueOrDefault('icon', '')) ?>
    </div>
</div>