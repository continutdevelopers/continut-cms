<div class="row">
    <div class="col-xs-6">
        <?= $this->helper('Form')->linkWizardField('site', $this->__('backend.themeHdv.type.content.googleMaps.field.site'), $this->valueOrDefault('site', null), ['type' => 'custom', 'collection' => 'Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection\SiteCollection']) ?>
    </div>
    <div class="col-xs-6">
        <?= $this->helper('Form')->linkWizardField('link', $this->__('backend.themeHdv.type.content.googleMaps.field.link'), $this->valueOrDefault('link', null), ['type' => 'pages']) ?>
    </div>
</div>