<div class="row">
    <div class="col-xs-12">
        <?= $this->helper('Form')->textareaField('description', $this->__('backend.themeHdv.type.content.hospitalsPreview.field.description'), $this->valueOrDefault('description', '')) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-6">
        <?= $this->helper('Form')->linkWizardField('detailPage', $this->__('backend.themeHdv.type.content.hospitalsPreview.field.detailPage'), $this->valueOrDefault('detailPage', null), ['type' => 'pages']) ?>
    </div>
</div>
