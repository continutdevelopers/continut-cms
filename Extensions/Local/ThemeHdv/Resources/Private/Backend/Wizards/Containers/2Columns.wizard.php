<blockquote>
    <p><i class="fa fa-fw fa-info"></i> <?= $this->__('backend.wizard.containers.info', ['count' => '2']) ?></p>
    <footer><?= $this->__('backend.wizard.containers.info.footer') ?></footer>
</blockquote>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeHdv.type.container.2columns.title'), $this->valueOrDefault('title', '')) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
        <?= $this->helper('Form')->textField('subtitle', $this->__('backend.themeHdv.type.container.2columns.subtitle'), $this->valueOrDefault('subtitle', '')) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-6 col-sm-3">
        <?= $this->helper('Form')->selectField(
            'offsetLeft',
            $this->__('backend.themeHdv.type.container.wrapper.offsetLeft'),
            [
                '0' => $this->__('backend.themeHdv.type.container.wrapper.offset.none'),
                '1' => $this->__('backend.themeHdv.type.container.wrapper.offset.1column'),
                '2' => $this->__('backend.themeHdv.type.container.wrapper.offset.2columns'),
            ],
            $this->valueOrDefault('offsetLeft', 0)
        )
        ?>
    </div>
    <div class="col-xs-6 col-sm-3">
        <?= $this->helper('Form')->selectField(
            'offsetRight',
            $this->__('backend.themeHdv.type.container.wrapper.offsetRight'),
            [
                '0' => $this->__('backend.themeHdv.type.container.wrapper.offset.none'),
                '1' => $this->__('backend.themeHdv.type.container.wrapper.offset.1column'),
                '2' => $this->__('backend.themeHdv.type.container.wrapper.offset.2columns'),
            ],
            $this->valueOrDefault('offsetRight', 0)
        )
        ?>
    </div>
</div>