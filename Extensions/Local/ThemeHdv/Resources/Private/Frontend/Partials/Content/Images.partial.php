<?php $imageColumnsClass = ($columns == '1') ? '' : 'col-xs-12 col-sm-' . (12 / $columns); ?>
<?php $index = 1; ?>
<?php foreach ($images as $imageId): ?>
    <?php if ($columns > 1): ?>
        <?php if (($index % $columns == 1) || ($index == 1)): ?>
            <div class="row">
        <?php endif; ?>
        <div class="<?= $imageColumnsClass ?>">
            <img class="responsive"
                 src="<?= $this->helper('Image')->resize($imageId, $this->valueOrDefault('width', 800), $this->valueOrDefault('height', null), 'frontend') ?>"
                 alt=""/>
        </div>
        <?php if (($index % $columns == 0) || ($index == sizeof($images))): ?>
            </div>
        <?php endif; ?>
        <?php $index++ ?>
    <?php else: ?>
        <img class="responsive"
             src="<?= $this->helper('Image')->resize($imageId, $this->valueOrDefault('width', 800), $this->valueOrDefault('height', null), 'frontend') ?>"
             alt=""/>
    <?php endif; ?>
<?php endforeach; ?>