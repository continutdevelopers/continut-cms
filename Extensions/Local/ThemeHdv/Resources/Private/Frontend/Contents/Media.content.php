<?php $content = $this->valueOrDefault('content', '') ?>
<?php $imagePosition = $this->valueOrDefault('imagePosition', 'before'); ?>
<?php $imageRatio = $this->valueOrDefault('imageTextRatio', '1') ?>
<?php $imageColumns = $this->valueOrDefault('imageColumns', '1') ?>
<?php $textRatio = 12 - $imageRatio; ?>
<?php $images = (empty($images)) ? [] : explode(',', $images); ?>
<?php if ($images): ?>
    <?php if ($imagePosition == 'before'): ?>
        <?= $this->partial('Content/Images', 'ThemeHdv', 'Frontend', ['images' => $images, 'columns' => $imageColumns]); ?>
        <?= $content; ?>
    <?php elseif ($imagePosition == 'after'): ?>
        <?= $content; ?>
        <?= $this->partial('Content/Images', 'ThemeHdv', 'Frontend', ['images' => $images, 'columns' => $imageColumns]); ?>
    <?php elseif ($imagePosition == 'right'): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-<?= $textRatio ?>">
                <?= $content; ?>
            </div>
            <div class="col-xs-12 col-sm-<?= $imageRatio ?>">
                <?= $this->partial('Content/Images', 'ThemeHdv', 'Frontend', ['images' => $images, 'columns' => $imageColumns]); ?>
            </div>
        </div>
    <?php elseif ($imagePosition == 'left'): ?>
        <div class="row">
            <div class="col-xs-12 col-sm-<?= $imageRatio ?>">
                <?= $this->partial('Content/Images', 'ThemeHdv', 'Frontend', ['images' => $images, 'columns' => $imageColumns]); ?>
            </div>
            <div class="col-xs-12 col-sm-<?= $textRatio ?>">
                <?= $content; ?>
            </div>
        </div>
    <?php endif; ?>
<?php else: ?>
    <?= $content; ?>
<?php endif; ?>
