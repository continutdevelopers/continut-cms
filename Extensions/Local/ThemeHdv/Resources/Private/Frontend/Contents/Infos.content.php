<?php if ($items): ?>
<div class="row center-xs">
    <?php foreach ($items as $item): ?>
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="tile">
                <span class="icon icon-<?= $item['icon'] ?>"></span>
                <?php if (isset($item['title'])): ?>
                    <h2><?= $item['title'] ?></h2>
                <?php endif; ?>
                <?php if (isset($item['subtitle'])): ?>
                    <h3><?= $item['subtitle'] ?></h3>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>