<?php $locations = $this->valueOrDefault('locations'); ?>
<?php $siteIds = []; ?>
<?php if (is_array($locations)): ?>
    <?php foreach ($locations as $location): ?>
        <?php $siteIds[] = $location['site']; ?>
    <?php endforeach; ?>
<?php endif; ?>
<?php
$sites = \Continut\Core\Utility::callPlugin('ThemeHdv', 'Content', 'sites', ['sites' => $siteIds]);
?>
<div class="row">
    <div class="col-xs-12">
        <div id="map_<?= $record->getId() ?>" style="width: 100%; height: 400px"></div>
        <script>
            function initMap_<?= $record->getId() ?>() {
                var valais = {lat: 46.195846, lng: 7.583165};
                var map = new google.maps.Map(document.getElementById('map_<?= $record->getId() ?>'), {
                    zoom: 9,
                    center: valais,
                    styles:
                        [
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.country",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#4f4f4f"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.province",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#c31a1f"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.province",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#c31a1f"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.province",
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dadada"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#c9c9c9"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            }
                        ]
                });
                <?php foreach ($sites as $site): ?>
                new google.maps.Marker({
                    position: {lat: <?= $site->getLatitude() ?>, lng: <?= $site->getLongitude() ?>},
                    map: map,
                    title: '<?= $site->getName() ?>'
                });
                <?php endforeach; ?>
            }
        </script>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYHpB9cY_uc1zysCgVANGEw6NAlyLw-QQ&callback=initMap_<?= $record->getId() ?>">
        </script>
    </div>
</div>