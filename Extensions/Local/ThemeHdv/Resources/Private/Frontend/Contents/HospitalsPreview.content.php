<?php
$sites = \Continut\Core\Utility::callPlugin('ThemeHdv', 'Content', 'sites');
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
        <p class="lg">
            <?= $description ?>
        </p>
        <div class="control">
            <?php if ($sites): ?>
            <select name="site_selector" class="hdv-select">
                <?php foreach ($sites as $site): ?>
                    <option value="<?= $site->getId() ?>"><?= $site->getName() ?></option>
                <?php endforeach; ?>
            </select>
            <?php endif; ?>
        </div>
        <div class="map-chooser hide-mobile">
            <img src="<?= $this->helper('Image')->getPath('Images/map_valais.svg', 'ThemeHdv'); ?>" class="responsive" alt=""/>
            <?php if ($sites): ?>
                <?php foreach ($sites as $index => $site): ?>
                    <a class="point" id="h_<?= $site->getId() ?>" data-hospital="<?= $site->getId() ?>" href="" style="<?= $site->getMapPositions() ?>" title="<?= $site->getName() ?>"></a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
        <div id="hospital_details_card" class="card-hospital">
            <img class="responsive" src="<?= $this->helper('Image')->getPath('Images/home_temp1.jpg', 'ThemeHdv'); ?>"
                 alt=""/>
            <ul>
                <li><span class="icon icon-pin"></span> Avenue du Grand-Champsec 80, 1951 Sion</li>
                <li><span class="icon icon-phone"></span> 027 603 40 00</li>
                <li><span class="icon icon-gps"></span> <a href="" target="_blank">Itinéraires</a></li>
            </ul>
            <div class="row links">
                <div class="col-xs-6">
                    <h3 class="uppercase">Visiteurs</h3>
                    <ul>
                        <li><a class="arrow-link" href="">Accès et parking</a></li>
                        <li><a class="arrow-link" href="">Heures de visite</a></li>
                    </ul>
                </div>
                <div class="col-xs-6">
                    <h3 class="uppercase">Patients</h3>
                    <ul>
                        <li><a class="arrow-link" href="">Votre admission</a></li>
                        <li><a class="arrow-link" href="">Votre séjour</a></li>
                        <li><a class="arrow-link" href="">Votre sortie</a></li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <p>
                        <a class="btn btn-default btn-plus btn-full" href="">Toutes les infos pratiques</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>