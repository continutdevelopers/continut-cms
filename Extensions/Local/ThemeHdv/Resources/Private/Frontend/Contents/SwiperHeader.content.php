<div class="container-fluid header">
    <!-- Slider main container -->
    <div class="swiper-container swiper-general swiper-header swiper-main">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php foreach ($this->valueOrDefault('slides', []) as $index => $slide): ?>
                <div class="swiper-slide">
                    <?php if ($slide['image']): ?>
                        <div class="bg-image"
                             style="background-image:url('<?= $this->helper('Image')->resize($slide['image'], 1680, 595) ?>');"></div>
                        <div class="bg-image mobile"
                             style="background-image:url('<?= $this->helper('Image')->crop($slide['image'], 640, 470) ?>');"></div>
                    <?php endif; ?>
                    <div class="container container-absolute inside">
                        <div class="row">
                            <div class="col-xs-10 col-sm-6 col-md-5 col-lg-4">
                                <?php if ($slide['title']): ?><h2><?= $slide['title'] ?></h2><?php endif; ?>
                                <?php if ($slide['subtitle']): ?><h1><?= $slide['subtitle'] ?></h1><?php endif; ?>
                                <p><?= $slide['description'] ?></p>
                                <p class="more text-right"><a href="bla.html">Lire la suite</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="container container-absolute footer-pagination">
            <div class="row end-xs">
                <div class="col-xs-6 col-sm-2 buttons">
                    <a href="#" class="hdv-swiper-button-prev"><span class="icon-arrow2-left"></span></a><a href="#"
                                                                                                            class="hdv-swiper-button-next"><span
                                class="icon-arrow2-right"></span></a>
                </div>
            </div>
        </div>

        <!-- If we need scrollbar -->
        <div class="swiper-scrollbar"></div>

    </div>
</div>