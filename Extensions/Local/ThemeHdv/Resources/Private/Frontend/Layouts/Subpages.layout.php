<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://<?= $this->getUrl() ?>">
    <link rel="shortcut icon" href="/Public/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/Public/favicon.ico" type="image/x-icon">
    <title><?= $this->getTitle() ?></title>

<?=
$this
    ->addCssAsset(['identifier' => 'normalize', 'extension' => 'ThemeHdv', 'file' => 'lib/normalize.css'])
    ->addCssAsset(['identifier' => 'flexboxgrid', 'extension' => 'ThemeHdv', 'file' => 'lib/flexboxgrid.css'])
    ->addCssAsset(['identifier' => 'swiper', 'extension' => 'ThemeHdv', 'file' => 'lib/swiper/swiper-3.4.0.css'])
    ->addCssAsset(['identifier' => 'selectize', 'extension' => 'ThemeHdv', 'file' => 'lib/selectize/selectize.css'])
    ->addCssAsset(['identifier' => 'mobile-tabs', 'extension' => 'ThemeHdv', 'file' => 'lib/tabs/responsive-tabs.css'])
    ->addCssAsset(['identifier' => 'main', 'extension' => 'ThemeHdv', 'file' => 'main.css'])
    ->renderAssets()
?>
    <?= $this->renderDebugbarHeader() ?>
    <meta name="description" content="<?= $this->helper('Text')->stripNewlines($this->getPage()->getMetaDescription()) ?>"/>
    <meta name="keywords" content="<?= $this->getPage()->getMetaKeywords() ?>"/>

</head>
<?php if ($this->getBodyClass()) : ?>
<body class="<?= $this->getBodyClass() ?>">
<?php else: ?>
<body>
<?php endif ?>

<?= $this->plugin('ThemeHdv', 'Menu', 'mainmenu'); ?>
<?= $this->showContainerColumn(1); ?>
<?= $this->plugin('ThemeHdv', 'Menu', 'footer'); ?>

<?=
$this
    ->addJsAsset(['identifier' => 'jquery', 'extension' => 'ThemeHdv', 'file' => 'lib/jquery/jquery-3.1.1.min.js'])
    ->addJsAsset(['identifier' => 'swiper', 'extension' => 'ThemeHdv', 'file' => 'lib/swiper/swiper-3.4.1.jquery.js'])
    ->addJsAsset(['identifier' => 'selectize', 'extension' => 'ThemeHdv', 'file' => 'lib/selectize/selectize.js'])
    ->addJsAsset(['identifier' => 'headroom', 'extension' => 'ThemeHdv', 'file' => 'lib/headroom/headroom.js'])
    ->addJsAsset(['identifier' => 'headroom-jq', 'extension' => 'ThemeHdv', 'file' => 'lib/headroom/jquery.headroom.js'])
    ->addJsAsset(['identifier' => 'mobile-tabs', 'extension' => 'ThemeHdv', 'file' => 'lib/tabs/jquery.responsiveTabs.js'])
    ->addJsAsset(['identifier' => 'gsap-plugins', 'extension' => 'ThemeHdv', 'file' => 'lib/greensock/plugins/CSSPlugin.js'])
    ->addJsAsset(['identifier' => 'gsap-tween', 'extension' => 'ThemeHdv', 'file' => 'lib/greensock/TweenLite.js'])
    ->addJsAsset(['identifier' => 'gsap-timeline', 'extension' => 'ThemeHdv', 'file' => 'lib/greensock/TimelineLite.js'])
    ->addJsAsset(['identifier' => 'main', 'extension' => 'ThemeHdv', 'file' => 'main.js'])
    //->addJsAsset(['identifier' => 'gmaps', 'external' => true, 'file' => 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDYHpB9cY_uc1zysCgVANGEw6NAlyLw-QQ'])
    ->renderAssets();
?>
<?= $this->renderDebugbarContent() ?>
</body>
</html>
