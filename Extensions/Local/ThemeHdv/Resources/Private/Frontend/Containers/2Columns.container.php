<?php
$widthRight = 6 - ($offsetRight + $offsetLeft);
?>
<div class="row">
    <div class="col-xs-12 col-sm-offset-<?= $offsetLeft ?> col-sm-<?= $widthRight ?>">
        <?= $this->showContainerColumn(4); ?>
    </div>
    <div class="col-xs-12 col-sm-offset-<?= $offsetLeft ?> col-sm-<?= $widthRight ?>">
        <?= $this->showContainerColumn(5); ?>
    </div>
</div>