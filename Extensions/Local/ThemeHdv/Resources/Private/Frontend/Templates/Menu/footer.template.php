<footer class="container-fluid container-content theme-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>
                    <?= $this->__('hdv.footer.contact.title') ?>
                    <small><?= $this->__('hdv.footer.contact.subtitle') ?></small>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-offset-1 col-sm-11 contains-slider">
                <div class="swiper-container swiper-locations">
                    <div class="swiper-wrapper">
                        <?php foreach ($sites as $site): ?>
                            <div class="swiper-slide">
                                <div class="location" data-location="<?= mb_strtolower($site->getName()) ?>">
                                    <h4><?= $site->getName() ?></h4>
                                    <p>
                                        <?= $site->getStreet() ?><br/>
                                        <?= $site->getZip() ?> <?= $site->getCity() ?><br/>
                                        <?php if ($site->getPhone()): ?>Tél.: <?= $site->getPhone() ?>
                                            <br/><?php endif; ?>
                                        <?php if ($site->getFax()): ?>Fax: <?= $site->getFax() ?><br/><?php endif; ?>
                                    </p>
                                    <p><a class="arrow-link" href="">Plus de détails</a></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="swiper-scrollbar"></div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="outer-pagination">
                    <a href="#" class="hdv-swiper-button-prev"><span class="icon-arrow2-left"></span></a>
                    <span class="icon-line"></span>
                    <a href="#" class="hdv-swiper-button-next"><span class="icon-arrow2-right"></span></a>
                </div>
                <hr/>
                <div class="row mobile-text-center">
                    <div class="col-xs-12 col-sm-6">
                        <p class="uppercase"><strong><?= $this->__('hdv.footer.social.followUs') ?></strong></p>
                        <p class="social-networks">
                            <a href="http://www.twitter.com"><span class="icon-2x icon-twitter"></span></a>
                            <a href="http://www.facebook.com"><span class="icon-2x icon-facebook"></span></a>
                            <a href="http://www.vimeo.com"><span class="icon-2x icon-vimeo"></span></a>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <p class="uppercase"><strong><?= $this->__('hdv.footer.contactByMail.title') ?></strong></p>
                        <p><?= $this->__('hdv.footer.contactByMail.link', ['link' => '@TODO:linkToContactPage']) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<section class="container-fluid emergency">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-1 col-sm-11">
                <div class="row mobile-text-center">
                    <div class="col-xs-12 col-sm-3 hide-mobile">
                        <p><strong><a href=""><?= $this->__('hdv.footer.emergency.title') ?></a></strong></p>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <p><?= $this->__('hdv.footer.emergency.phoneNumber', ['phone' => '144']) ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <p><?= $this->__('hdv.footer.emergency.doctor', ['phone' => '0900144033']) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<aside id="sidebar" class="sidebar">
    <div class="links">
        <a href="#" data-target="shortcut_1"><span class="icon-2x icon-ambulance"></span></a>
        <a href="#" data-target="shortcut_2"><span class="icon-2x icon-pin2"></span></a>
        <a href="#" data-target="shortcut_3"><span class="icon-2x icon-steto2"></span></a>
        <a href="#" data-target="shortcut_4"><span class="icon-2x icon-share"></span></a>
    </div>
    <div class="details-wrapper">
        <div id="shortcut_1" class="details">
            <p><a href=""><span class="icon-ambulance icon-3x"></span></a></p>
            <p><?= $this->__('hdv.footer.emergency.phoneNumber', ['phone' => '144']) ?></p>
            <p><?= $this->__('hdv.footer.emergency.doctor', ['phone' => '0900144033']) ?></p>
        </div>
        <div id="shortcut_2" class="details">
            <p><span class="icon-pin2 icon-3x"></span></p>
            <p><?= $this->__('hdv.sidebar.site.title') ?></p>
            <?php if ($sites): ?>
                <select id="sidebar_site" name="sidebar_site" class="hdv-select general-select control-full"
                        style="width: 80%">
                    <?php foreach ($sites as $site): ?>
                        <option value="<?= $site->getId() ?>"><?= $site->getName() ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
        </div>
        <div id="shortcut_3" class="details">
            <p><span class="icon-steto2 icon-3x"></span></p>
            <p><?= $this->__('hdv.sidebar.consultation.title') ?></p>
            <select id="sidebar_discipline" name="sidebar_site" class="hdv-select general-select control-full"
                    style="width: 80%">
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/anesthesiologie-et-reanimation.html">
                    Anesthésiologie et réanimation
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/angiologie.html">Angiologie</option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/cancerologie-oncologie.html">Cancérologie
                    / Oncologie
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/cardiologie.html">Cardiologie</option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/centre-de-la-memoire.html">Centre de la
                    mémoire
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/centre-du-sein.html">Centre du sein
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/anesthesiologie-et-reanimation/centre-du-traitement-de-la-douleur.html">
                    Centre du traitement de la douleur
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/chimie-clinique.html">Chimie clinique
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/chirurgie-cardiaque.html">Chirurgie
                    cardiaque
                </option>
                <option value="/fr/disciplines-medicales/disciplines-de-a-a-z/chirurgie-generale.html">Chirurgie
                    générale
                </option>
            </select>
        </div>
        <div id="shortcut_4" class="details">
            <p><span class="icon-share icon-3x"></span></p>
            <p><?= $this->__('hdv.sidebar.social.title') ?></p>
            <p class="social-networks">
                <a href=""><span class="icon-2x icon-twitter"></span></a>
                <a href=""><span class="icon-2x icon-facebook"></span></a>
                <a href=""><span class="icon-2x icon-vimeo"></span></a>
            </p>
        </div>
    </div>
</aside>
