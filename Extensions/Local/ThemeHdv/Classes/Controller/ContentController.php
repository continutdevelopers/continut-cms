<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 09.05.2015 @ 16:55
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Local\ThemeHdv\Classes\Controller;

use Continut\Core\System\Controller\FrontendController;
use Continut\Core\Utility;

class ContentController extends FrontendController
{
    /**
     * Return all visible hospital sites or those present in $this->data['sites']
     *
     * @return mixed
     */
    public function sitesAction()
    {
        $sitesCollection = Utility::createInstance(
            'Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection\SiteCollection'
        );
        if (isset($this->data['sites'])) {
            return $sitesCollection->findByIds($this->data['sites'])->getAll();
        } else {
            return $sitesCollection->findAllVisible(true)->getAll();
        }
    }
}
