<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 09.05.2015 @ 16:55
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Local\ThemeHdv\Classes\Controller;

use Continut\Core\System\Controller\FrontendController;
use Continut\Core\Utility;

class MenuController extends FrontendController
{
    /**
     * Mainmenu action
     */
    public function mainmenuAction()
    {
        $pageCollection = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendPageCollection'
        )->whereInMenu(Utility::getSite()->getDomainUrl()->getId());

        $pageTree = $pageCollection->buildTree();

        $this->getView()->assignMultiple(
            [
                'currentPage' => Utility::getCurrentPage(),
                'pageTree'    => $pageTree
            ]
        );
        $this->getPageLanguageMenu();
    }

    /**
     * Get all hospital sites that are displayed on all pages in the footer
     */
    public function footerAction()
    {
        $siteCollection = Utility::createInstance(
            'Continut\Extensions\Local\HospitalSites\Classes\Domain\Collection\SiteCollection'
        )->findAllVisible();

        $this->getView()->assign('sites', $siteCollection->getAll());
    }

    /**
     * Gets the page breadcrumb
     */
    public function showBreadcrumbAction()
    {
        $pagesCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\PageCollection');

        $pageId    = (int)$this->getRequest()->getArgument("id");
        $pageSlug  = $this->getRequest()->getArgument("slug");
        $pageModel = $pagesCollection->findWithIdOrSlug($pageId, $pageSlug);

        $breadcrumbs = [];
        if ($pageModel->getCachedPath()) {
            $breadcrumbs = $pagesCollection
                ->whereBreadcrumb($pageModel->getCachedPath())
                ->getAll();
        }

        $this->getView()->assign("breadcrumbs", $breadcrumbs);
        $this->getView()->assign("page", $pageModel);
    }

    protected function getPageLanguageMenu()
    {
        $currentPage = Utility::getCurrentPage();

        $pageCollection = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendPageCollection'
        );

        // @TODO get default language instead of hardcoding it
        $defaultLanguage = 7;
        if ($currentPage->getDomainUrlId() == $defaultLanguage) {
            // get all the translated pages of the current one
            $pageCollection->where(
                'original_id = :original_id AND is_deleted = 0 AND is_visible = 1',
                [
                    'original_id' => $currentPage->getId()
                ]
            );
        } else {
            // a translated page with original_id = 0 is a page that does not have a source, so it exists in this
            // language only
            if ($currentPage->getOriginalId() != 0) {
                // get all the other translated pages except the current one, also get the original page
                $pageCollection->where(
                    '( (id = :id ) OR (original_id = :original_id ) ) AND ' .
                    '(domain_url_id != :language AND is_deleted = 0 AND is_visible = 1)',
                    [
                        'id'          => $currentPage->getOriginalId(),
                        'original_id' => $currentPage->getOriginalId(),
                        'language'    => $currentPage->getDomainUrlId()
                    ]
                );
            }
        }

        $this->getView()->assignMultiple(
            [
                'currentPage'  => Utility::getCurrentPage(),
                'translations' => $pageCollection->getAll()
            ]
        );
    }
}
