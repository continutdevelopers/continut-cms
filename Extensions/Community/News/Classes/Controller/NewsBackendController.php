<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 31.05.2015 @ 22:28
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Community\News\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\Utility;

class NewsBackendController extends BackendController
{
    /**
     * NewsBackendController constructor - set default layout
     */
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Backend News Grid View
     */
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $grid
            ->setFormAction(
                Utility::helper('Url')
                    ->linkToPath('admin', ['_extension' => 'News', '_controller' => 'NewsBackend', '_action' => 'grid'])
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(
                Utility::createInstance('Continut\Extensions\Community\News\Classes\Domain\Collection\NewsCollection')
            )
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'photo'       => [
                        'label'    => 'backend.news.grid.field.photo',
                        'renderer' => [
                            'class' => 'Continut\Extensions\Community\News\Classes\View\Renderer\PhotoRenderer'
                        ]
                    ],
                    'title'       => [
                        'label'    => 'backend.news.grid.field.title',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'parameters' => ['crop' => 200, 'cropAppend' => '...', 'removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'description' => [
                        'label'    => 'backend.news.grid.field.description',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'parameters' => ['crop' => 300, 'cropAppend' => '...', 'removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'isVisible'   => [
                        'label'    => 'backend.news.grid.field.isVisible',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer'
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'categories'  => [
                        'label'    => 'backend.news.grid.field.categories',
                        'css'      => 'col-sm-2',
                        'renderer' => [
                            'class' => 'Continut\Extensions\Community\News\Classes\View\Renderer\CategoriesRenderer'
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => 'Category 1', '1' => 'Category 2']
                        ]
                    ],
                    'actions'     => [
                        'label'    => 'backend.news.grid.field.actions',
                        'css'      => 'col-sm-2 text-right flip',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\Community\News\Classes\View\Renderer\ActionsRenderer',
                            'parameters' => ['showEdit' => true, 'showDelete' => true]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * Create news
     */
    public function createAction()
    {
        $news            = Utility::createInstance('Continut\Extensions\Community\News\Classes\Domain\Model\News');
        $usersCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserCollection');

        $this->getView()->assign('news', $news);
        $this->getView()->assign('authors', $usersCollection->findAll());
    }

    /**
     * Edit news
     */
    public function editAction()
    {
        $id = (int)$this->getRequest()->getArgument('id');

        $newsCollection  = Utility::createInstance(
            'Continut\Extensions\Community\News\Classes\Domain\Collection\NewsCollection'
        );
        $usersCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserCollection');
        $news            = $newsCollection->findById($id);

        $this->getView()->assign('news', $news);
        $this->getView()->assign('authors', $usersCollection->findAll());
    }

    /**
     * Save news
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        // reference the collection
        $newsCollection = Utility::createInstance(
            'Continut\Extensions\Community\News\Classes\Domain\Collection\NewsCollection'
        );

        if ($id == 0) {
            $news = Utility::createInstance('Continut\Extensions\Community\News\Classes\Domain\Model\News');
        } else {
            $news = $newsCollection->findById($id);
        }
        $news->update($data);

        if ($news->validate()) {
            $newsCollection
                ->reset()
                ->add($news)
                ->save();

            // redirect to the "domainsAction" since all went well and data is saved
            $this->redirect(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_extension'  => 'News',
                        '_controller' => 'NewsBackend',
                        '_action'     => 'grid'
                    ]
                )
            );
        }

        $usersCollection = Utility::createInstance('Continut\Core\System\Domain\Collection\FrontendUserCollection');

        $this->getView()->assign('news', $news);
        $this->getView()->assign('authors', $usersCollection->findAll());
    }
}
