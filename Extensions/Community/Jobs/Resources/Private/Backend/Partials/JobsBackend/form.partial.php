<form action="<?= $this->helper('Url')->linkToPath('admin', ['_extension' => 'Jobs', '_controller' => 'JobsBackend', '_action' => 'save']) ?>"
      method="post">
    <?php if ($job->getId()): ?>
        <?= $this->helper('FormObject')->hiddenField($job, 'id') ?>
    <?php endif; ?>
    <div class="panel panel-form">
        <div class="panel-heading">
            <div class="panel-title">
                <?= $this->__('backend.ext.jobs.createNew') ?>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <?= $this->helper('FormObject')->textField($job, 'title', $this->__('model.ext_jobs_job.title')); ?>
                </div>
                <div class="col-sm-12 col-md-6">
                    <?= $this->helper('FormObject')->rteField($job, 'description', $this->__('model.ext_jobs_job.description')); ?>
                    <?= $this->helper('FormObject')->rteField($job, 'contact', $this->__('model.ext_jobs_job.contact')); ?>
                </div>
                <div class="col-sm-12 col-md-6">
                    <?= $this->helper('FormObject')->rteField($job, 'requirements', $this->__('model.ext_jobs_job.requirements')); ?>
                    <?= $this->helper('FormObject')->textField($job, 'occupation', $this->__('model.ext_jobs_job.occupation')); ?>
                    <?= $this->helper('FormObject')->textField($job, 'location', $this->__('model.ext_jobs_job.location')); ?>
                    <?= $this->helper('FormObject')->textField($job, 'email', $this->__('model.ext_jobs_job.email')); ?>
                    <?= $this->helper('FormObject')->textField($job, 'phone', $this->__('model.ext_jobs_job.phone')); ?>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <input type="submit" name="submit" class="btn btn-primary" value="<?= $this->__('general.save') ?>"/>
            <a href="<?= $this->helper('Url')->linkToPath('admin', ['_extension' => 'Jobs', '_controller' => 'JobsBackend', '_action' => 'grid']) ?>"
               class="close-button btn btn-danger pull-right flip"><?= $this->__('general.cancel') ?></a>
        </div>
    </div>
</form>