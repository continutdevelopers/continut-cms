<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.01.2016 @ 18:04
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Community\Jobs\Classes\Domain\Collection;

use Continut\Core\System\Domain\Model\BaseCollection;

class JobCollection extends BaseCollection
{
    /**
     * Set tablename and each element's class
     */
    public function __construct()
    {
        $this->tablename    = 'ext_jobs_job';
        $this->elementClass = 'Continut\Extensions\Community\Jobs\Classes\Domain\Model\Job';
    }

    /**
     * Get all visible and non-deleted jobs
     *
     * @return $this
     */
    public function findAllVisible()
    {
        return $this->where('is_deleted = 0 AND is_visible = 1');
    }
}
