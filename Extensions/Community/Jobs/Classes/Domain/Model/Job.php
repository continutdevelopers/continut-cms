<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 02.01.2016 @ 17:57
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Community\Jobs\Classes\Domain\Model;

use Continut\Core\System\Domain\Model\BaseModel;
use Respect\Validation\Validator as v;

class Job extends BaseModel
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $occupation;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $isVisible;

    /**
     * @var bool
     */
    protected $isDeleted;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var string
     */
    protected $requirements;

    /**
     * @var string
     */
    protected $contact;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var int
     */
    protected $createdAt;

    /**
     * @var int
     */
    protected $startDate;

    /**
     * Simple datamapper used for the database
     *
     * @return array
     */
    public function dataMapper()
    {
        $fields = [
            'title'        => $this->title,
            'description'  => $this->description,
            'is_visible'   => $this->isVisible,
            'is_deleted'   => $this->isDeleted,
            'requirements' => $this->requirements,
            'contact'      => $this->contact,
            'phone'        => $this->phone,
            'email'        => $this->email,
            'start_date'   => $this->startDate,
            'occupation'   => $this->occupation,
            'created_at'   => $this->createdAt,
            'location'     => $this->location
        ];

        return array_merge($fields, parent::dataMapper());
    }

    /**
     * Define the list of fields to validate and the validators to validate against
     *
     * @return array List of field names and their validators
     */
    public function dataValidation()
    {
        return [
            'title'        => v::length(3, 250),
            'email'        => v::email(),
            'requirements' => v::notEmpty(),
            'description'  => v::notEmpty()
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     *
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     *
     * @return $this
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param string $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @param string $requirements
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param int $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }
}
