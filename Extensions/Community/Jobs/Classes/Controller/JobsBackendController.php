<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 20.05.2017 @ 18:50
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Community\Jobs\Classes\Controller;

use Continut\Core\System\Controller\BackendController;
use Continut\Core\System\Tools\ErrorException;
use Continut\Core\Utility;

class JobsBackendController extends BackendController
{
    /**
     * JobsBackendController constructor - set default layout
     */
    public function __construct()
    {
        parent::__construct();
        $this->setLayoutTemplate(Utility::getResourcePath('Default', 'Backend', 'Backend', 'Layout'));
    }

    /**
     * Generates the grid view for the jobs
     */
    public function gridAction()
    {
        $grid = Utility::createInstance('Continut\Extensions\System\Backend\Classes\View\GridView');

        $grid
            ->setFormAction(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_extension'  => 'Jobs',
                        '_controller' => 'JobsBackend',
                        '_action'     => 'grid'
                    ]
                )
            )
            ->setTemplate(Utility::getResourcePath('Grid/gridView', 'Backend', 'Backend', 'Template'))
            ->setCollection(
                Utility::createInstance('Continut\Extensions\Community\Jobs\Classes\Domain\Collection\JobCollection')
            )
            ->setPager(10, Utility::getRequest()->getArgument('page', 1))
            ->setFields(
                [
                    'title'       => [
                        'label'    => 'backend.ext.jobs.grid.field.title',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'parameters' => ['crop' => 200, 'cropAppend' => '...', 'removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'description' => [
                        'label'    => 'backend.ext.jobs.grid.field.description',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'parameters' => ['crop' => 300, 'cropAppend' => '...', 'removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'location'    => [
                        'label'    => 'backend.ext.jobs.grid.field.location',
                        'css'      => 'col-sm-3',
                        'renderer' => [
                            'parameters' => ['removeHtml' => true]
                        ],
                        'filter'   => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Filter\TextFilter'
                        ]
                    ],
                    'isVisible'   => [
                        'label'    => 'backend.ext.jobs.grid.field.isVisible',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer'
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'isDeleted'   => [
                        'label'    => 'backend.ext.jobs.grid.field.isDeleted',
                        'css'      => 'col-sm-1',
                        'renderer' => [
                            'class' => 'Continut\Extensions\System\Backend\Classes\View\Renderer\YesNoRenderer'
                        ],
                        'filter'   => [
                            'class'  => 'Continut\Extensions\System\Backend\Classes\View\Filter\SelectFilter',
                            'values' => ['' => '', '0' => $this->__('general.no'), '1' => $this->__('general.yes')]
                        ]
                    ],
                    'actions'     => [
                        'label'    => 'backend.ext.jobs.grid.field.actions',
                        'css'      => 'col-sm-1 text-right flip',
                        'renderer' => [
                            'class'      => 'Continut\Extensions\Community\Jobs\Classes\View\Renderer\ActionsRenderer',
                            'parameters' => ['showEdit' => true, 'showDelete' => true]
                        ]
                    ]
                ]
            )
            ->initialize();

        $this->getView()->assign('grid', $grid);
    }

    /**
     * Create a new job - action
     */
    public function createAction()
    {
        $job = Utility::createInstance('Continut\Extensions\Community\Jobs\Classes\Domain\Model\Job');

        $this->getView()->assign('job', $job);
    }

    /**
     * Create or Update the Job model
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getArgument('data');
        $id   = (int)$data['id'];

        // reference the collection
        $jobCollection = Utility::createInstance(
            'Continut\Extensions\Community\Jobs\Classes\Domain\Collection\JobCollection'
        );

        // if no id is present it means we're dealing with a new job that we need to create
        // otherwise it's an existing one that we need to update
        if ($id == 0) {
            $job = Utility::createInstance('Continut\Extensions\Community\Jobs\Classes\Domain\Model\Job');
        } else {
            $job = $jobCollection->findById($id);
        }
        $job->update($data);

        if ($job->validate()) {
            $jobCollection
                ->reset()
                ->add($job)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('backend.ext.jobs.flash.jobSaved'));

            // redirect to the list view since all went well and data is saved
            $this->redirect(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_extension'  => 'Jobs',
                        '_controller' => 'JobsBackend',
                        '_action'     => 'grid'
                    ]
                )
            );
        }

        $this->getView()->assign('job', $job);
    }

    /**
     * Edit job
     */
    public function editAction()
    {
        $id = $this->getRequest()->getArgument('id', 0);

        if ($id > 0) {
            $jobCollection = Utility::createInstance(
                'Continut\Extensions\Community\Jobs\Classes\Domain\Collection\JobCollection'
            );

            $job = $jobCollection->findById($id);

            $this->getView()->assign('job', $job);
        } else {
            throw new ErrorException('The record id you supplied is invalid');
        }
    }

    /**
     * Delete job
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getArgument('id', 0);

        if ($id > 0) {
            $jobCollection = Utility::createInstance(
                'Continut\Extensions\Community\Jobs\Classes\Domain\Collection\JobCollection'
            );

            $job = $jobCollection->findById($id);

            $job->setIsDeleted(true);

            $jobCollection
                ->reset()
                ->add($job)
                ->save();

            Utility::getSession()->addFlashMessage($this->__('backend.ext.jobs.flash.jobDeleted'));

            //@TODO : replace it with an ajax call and remove redirect altogether?
            $this->redirect(
                Utility::helper('Url')->linkToPath(
                    'admin',
                    [
                        '_extension'  => 'Jobs',
                        '_controller' => 'JobsBackend',
                        '_action'     => 'grid'
                    ]
                )
            );
        }
    }
}
