<?php
$title = $this->valueOrDefault('title', '');
?>
<div class="jumbotron">
    <?php if ($title): ?>
        <h1><?= $title ?></h1>
    <?php endif; ?>
    <?php if ($lead): ?>
        <p class="lead"><?= $lead ?></p>
    <?php endif; ?>
</div>