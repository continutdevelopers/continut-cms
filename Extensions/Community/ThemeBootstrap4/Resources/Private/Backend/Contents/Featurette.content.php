<?php $display = $this->valueOrDefault('display', 'left'); ?>
<div class="row">
    <?php if ($display == 'left'): ?>
        <div class="col-sm-4">
            <?php if ($image): ?>
                <img class="img-responsive"
                     src="<?= $this->helper('Image')->resize($image, $this->valueOrDefault('width', 500), $this->valueOrDefault('height', null), 'frontend') ?>"
                     alt=""/>
            <?php endif; ?>
        </div>
        <div class="col-sm-8">
            <?php if ($title): ?>
                <h2>
                    <?= $title ?>
                    <?php if ($subtitle): ?>
                        <span class="text-muted"><?= $this->valueOrDefault('subtitle', '') ?></span>
                    <?php endif; ?>
                </h2>
            <?php endif; ?>
            <p><?= $this->valueOrDefault('lead', '') ?></p>
        </div>
    <?php else: ?>
        <div class="col-sm-8">
            <?php if ($title): ?>
                <h2>
                    <?= $title ?>
                    <?php if ($subtitle): ?>
                        <span class="text-muted"><?= $this->valueOrDefault('subtitle', '') ?></span>
                    <?php endif; ?>
                </h2>
            <?php endif; ?>
            <p><?= $this->valueOrDefault('lead', '') ?></p>
        </div>
        <div class="col-sm-4">
            <?php if ($image): ?>
                <img class="img-responsive"
                     src="<?= $this->helper('Image')->resize($image, $this->valueOrDefault('width', 500), $this->valueOrDefault('height', null), 'frontend') ?>"
                     alt=""/>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>