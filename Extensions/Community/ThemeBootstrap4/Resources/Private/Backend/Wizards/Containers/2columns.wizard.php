<blockquote>
    <p><i class="fa fa-fw fa-info"></i> <?= $this->__('backend.wizard.containers.info', ['count' => '2']) ?></p>
    <footer><?= $this->__('backend.wizard.containers.info.footer') ?></footer>
</blockquote>
<div class="row">
    <div class="col-xs-12">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeBootstrap4.type.container.2columns.title'), $this->valueOrDefault('title', '')) ?>
    </div>
</div>