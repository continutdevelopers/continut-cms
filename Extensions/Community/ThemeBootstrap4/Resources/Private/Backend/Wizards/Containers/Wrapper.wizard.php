<blockquote>
    <p><i class="fa fa-fw fa-info"></i> <?= $this->__('backend.wizard.containers.info', ['count' => '1']) ?></p>
    <footer><?= $this->__('backend.wizard.containers.info.footer') ?></footer>
</blockquote>
<div class="row">
    <div class="col-sm-12">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeBootstrap4.type.container.wrapper.title'), $this->valueOrDefault('title', '')) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <?= $this->helper('Form')->selectField(
            'marginType',
            $this->__('backend.themeBootstrap4.type.container.wrapper.marginType'),
            [
                'my-1' => $this->__('backend.themeBootstrap4.type.container.wrapper.marginType.small'),
                'my-3' => $this->__('backend.themeBootstrap4.type.container.wrapper.marginType.medium'),
                'my-5' => $this->__('backend.themeBootstrap4.type.container.wrapper.marginType.big'),
            ],
            $this->valueOrDefault('marginType', 'my-1')
        )
        ?>
    </div>
    <div class="col-xs-12 col-sm-6">
        <?= $this->helper('Form')->selectField(
            'theme',
            $this->__('backend.themeBootstrap4.type.container.wrapper.theme'),
            [
                'theme-white' => $this->__('backend.themeBootstrap4.type.container.wrapper.theme.white'),
                'theme-gray'  => $this->__('backend.themeBootstrap4.type.container.wrapper.theme.gray'),
            ],
            $this->valueOrDefault('theme', 'theme-white')
        )
        ?>
    </div>
</div>