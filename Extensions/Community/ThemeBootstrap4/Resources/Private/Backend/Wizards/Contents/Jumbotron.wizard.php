<div class="row">
    <div class="col-xs-12">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeBootstrap4.type.content.jumbotron.field.title'), $this->valueOrDefault('title', '')) ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-8">
        <?= $this->helper('Form')->textareaField('lead', $this->__('backend.themeBootstrap4.type.content.jumbotron.field.lead'), $this->valueOrDefault('lead', '')) ?>
    </div>
    <div class="col-xs-12 col-sm-4">
        <?= $this->helper('Form')->linkWizardField('link', $this->__('backend.themeBootstrap4.type.content.jumbotron.field.link'), $this->valueOrDefault('link', null), ['type' => 'pages']) ?>
    </div>
</div>