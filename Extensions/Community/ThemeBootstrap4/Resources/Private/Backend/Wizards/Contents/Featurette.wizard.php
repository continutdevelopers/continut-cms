<div class="row">
    <div class="col-xs-12 col-sm-5">
        <?= $this->helper('Form')->textField('title', $this->__('backend.themeBootstrap4.type.content.featurette.field.title'), $this->valueOrDefault('title', '')) ?>
    </div>
    <div class="col-xs-12 col-sm-5">
        <?= $this->helper('Form')->textField('subtitle', $this->__('backend.themeBootstrap4.type.content.featurette.field.subtitle'), $this->valueOrDefault('subtitle', '')) ?>
    </div>
    <div class="col-xs-12 col-sm-2">
        <?= $this->helper('Form')->selectField(
            'display',
            $this->__('backend.themeBootstrap4.type.content.featurette.field.display'),
            [
                'left'  => $this->__('backend.themeBootstrap4.type.content.featurette.field.display.left'),
                'right' => $this->__('backend.themeBootstrap4.type.content.featurette.field.display.right'),
            ],
            $this->valueOrDefault('display', 'left')
        )
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= $this->helper('Form')->textareaField('lead', $this->__('backend.themeBootstrap4.type.content.featurette.field.lead'), $this->valueOrDefault('lead', '')) ?>
    </div>
    <div class="col-xs-12">
        <?= $this->helper('Form')->mediaField('image', $this->__('backend.themeBootstrap4.type.content.featurette.field.image'), $this->valueOrDefault('image', ''), ['maxFiles' => 1]) ?>
    </div>
</div>