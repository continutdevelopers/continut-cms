<?= $this->helper('Form')->textField('title', $this->__('backend.themeBootstrap4.type.content.media.field.title'), $this->valueOrDefault('title', '')) ?>
<?= $this->helper('Form')->rteField('content', $this->__('backend.themeBootstrap4.type.content.media.field.content'), $this->valueOrDefault('content', '')) ?>
<div class="row">
    <div class="col-xs-12 col-sm-8">
        <?= $this->helper('Form')->mediaField('images', $this->__('backend.themeBootstrap4.type.content.media.field.images'), $this->valueOrDefault('images', ''), ['maxFiles' => 3]) ?>
    </div>
    <div class="col-xs-12 col-sm-4">
        <?= $this->helper('Form')->selectField(
            'imagePosition',
            $this->__('backend.themeBootstrap4.type.content.media.field.imagePosition'),
            [
                'before' => $this->__('backend.themeBootstrap4.type.content.media.imagePosition.before'),
                'after'  => $this->__('backend.themeBootstrap4.type.content.media.imagePosition.after'),
                'right'  => $this->__('backend.themeBootstrap4.type.content.media.imagePosition.right'),
                'left'   => $this->__('backend.themeBootstrap4.type.content.media.imagePosition.left'),
            ],
            $this->valueOrDefault('imagePosition', 'before')
        )
        ?>
        <div id="group_image_ratio">
            <?= $this->helper('Form')->selectField(
                'imageTextRatio',
                $this->__('backend.themeBootstrap4.type.content.media.field.imageTextRatio'),
                [
                    '1'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.1'),
                    '2'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.2'),
                    '3'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.3'),
                    '4'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.4'),
                    '5'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.5'),
                    '6'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.6'),
                    '7'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.7'),
                    '8'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.8'),
                    '9'  => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.9'),
                    '10' => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.10'),
                    '11' => $this->__('backend.themeBootstrap4.type.content.media.imageTextRatio.11')
                ],
                $this->valueOrDefault('imageTextRatio', 'before')
            )
            ?>
        </div>
        <?= $this->helper('Form')->selectField(
            'imageColumns',
            $this->__('backend.themeBootstrap4.type.content.media.field.imageColumns'),
            [
                '1' => $this->__('backend.themeBootstrap4.type.content.media.imageColumns.1'),
                '2' => $this->__('backend.themeBootstrap4.type.content.media.imageColumns.2'),
                '3' => $this->__('backend.themeBootstrap4.type.content.media.imageColumns.3'),
                '4' => $this->__('backend.themeBootstrap4.type.content.media.imageColumns.4'),
            ],
            $this->valueOrDefault('imageColumns', '1')
        )
        ?>
    </div>
</div>

<script type="text/javascript">
    $('#field_data_imagePosition').on('change', function (event) {
        if (this.value == 'before' || this.value == 'after') {
            $('#group_image_ratio').hide();
        } else {
            $('#group_image_ratio').show();
        }
    });
</script>