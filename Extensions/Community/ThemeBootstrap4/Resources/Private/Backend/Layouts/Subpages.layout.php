<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">
                    <?= $this->__('backend.themeBootstrap4.layout.container.main') ?>
                </div>
            </div>
            <div class="panel-body">
                <?= $this->showContainerColumn(1); ?>
            </div>
        </div>
    </div>
</div>