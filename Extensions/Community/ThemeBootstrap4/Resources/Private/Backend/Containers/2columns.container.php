<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="panel-title"><?= $this->__('backend.themeBootstrap4.layout.container.2columns') ?></div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <?= $this->showContainerColumn(4); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $this->showContainerColumn(5); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>