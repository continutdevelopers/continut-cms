<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <h4>Footer menu</h4>
                <ul class="list-unstyled footer-menu">
                    <li><a href="">About us</a></li>
                    <li><a href="">Our products</a></li>
                    <li><a href="">What's new</a></li>
                    <li><a href="">Contact us</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <h4>Second menu</h4>
                <ul class="list-unstyled footer-menu">
                    <li><a href="">Villas for rent</a></li>
                    <li><a href="">Studios for sale</a></li>
                    <li><a href="">Apartments for sale</a></li>
                    <li><a href="">Villas for sale</a></li>
                    <li><a href="">Land for sale</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-6">
                <h4>Contact</h4>
                <p>This is a default clean Bootstrap4 theme for Conținut CMS. Customize it as you see fit with your own content.</p>
            </div>
        </div>
    </div>
</footer>