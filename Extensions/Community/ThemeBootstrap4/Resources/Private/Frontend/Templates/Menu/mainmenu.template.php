<section class="container">

    <div class="masthead">
        <nav class="navbar navbar-expand-md navbar-light bg-light mb-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav text-md-center nav-justified w-100">
                    <?php foreach ($pageTree as $level1): ?>
                        <li class="nav-item <?= ($level1->children) ? 'dropdown': '' ?>">
                            <a class="nav-link <?= ($level1->children) ? 'dropdown-toggle': '' ?>" href="<?= $this->helper('Url')->linkToSlug($level1) ?>" id="page_<?= $level1->getId() ?>" <?php if ($level1->children): ?>data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <?php endif; ?>><?= $level1->getTitle()?> <span class="sr-only">(current)</span></a>
                            <?php if ($level1->children): ?>
                            <div class="dropdown-menu w-100" aria-labelledby="page_<?= $level1->getId() ?>">
                                <?php foreach ($level1->children as $level2): ?>
                                    <a class="dropdown-item" href="<?= $this->helper('Url')->linkToSlug($level2) ?>"><?= $level2->getTitle() ?></a>
                                <?php endforeach; ?>
                            </div>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </nav>
    </div>
</section>