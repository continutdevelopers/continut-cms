<?php $title = $this->valueOrDefault('title', ''); ?>
<?php $link  = $this->valueOrDefault('link', null); ?>
<?php $lead  = $this->valueOrDefault('lead', ''); ?>
<div class="jumbotron">
    <?php if ($title): ?>
        <h1><?= $title ?></h1>
    <?php endif; ?>
    <?php if ($lead): ?>
        <p class="lead"><?= $lead ?></p>
    <?php endif; ?>
    <?php if ($link): ?>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Download CC CMS (v1.0)</a></p>
    <?php endif; ?>
</div>