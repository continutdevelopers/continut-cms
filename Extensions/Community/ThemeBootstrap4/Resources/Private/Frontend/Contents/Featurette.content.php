<?php $title    = $this->valueOrDefault('title', ''); ?>
<?php $subtitle = $this->valueOrDefault('subtitle', ''); ?>
<?php $lead     = $this->valueOrDefault('lead', ''); ?>
<?php $display  = $this->valueOrDefault('display', 'left'); ?>
<div class="row featurette">
    <?php if ($display == 'left'): ?>
        <div class="col-md-5 order-2 order-sm-1">
            <img class="featurette-image img-fluid mx-auto"
                 alt=""
                 src="<?= $this->helper('Image')->crop($image, $this->valueOrDefault('width', 500), $this->valueOrDefault('height', 300), 'frontend') ?>"
                 data-holder-rendered="true">
        </div>
        <div class="col-md-7 order-1 order-sm-2">
            <?php if ($title): ?>
                <h2 class="featurette-heading mt-0 mt-sm-5"><?= $title ?>
                    <?php if ($subtitle): ?>
                        <span class="text-muted"><?= $subtitle ?></span>
                    <?php endif; ?>
                </h2>
            <?php endif; ?>
            <?php if ($lead): ?>
                <p class="lead">
                    <?= $lead ?>
                </p>
            <?php endif; ?>
        </div>
    <?php else: ?>
        <div class="col-md-7 order-1">
            <?php if ($title): ?>
                <h2 class="featurette-heading mt-0 mt-sm-5"><?= $title ?>
                    <?php if ($subtitle): ?>
                        <span class="text-muted"><?= $subtitle ?></span>
                    <?php endif; ?>
                </h2>
            <?php endif; ?>
            <?php if ($lead): ?>
                <p class="lead">
                    <?= $lead ?>
                </p>
            <?php endif; ?>
        </div>
        <div class="col-md-5 order-2">
            <img class="featurette-image img-fluid mx-auto"
                 alt=""
                 src="<?= $this->helper('Image')->crop($image, $this->valueOrDefault('width', 500), $this->valueOrDefault('height', 300), 'frontend') ?>"
                 data-holder-rendered="true">
        </div>
    <?php endif; ?>
</div>