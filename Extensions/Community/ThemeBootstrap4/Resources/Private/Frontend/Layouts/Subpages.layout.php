<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://<?= $this->getUrl() ?>">
    <link rel="shortcut icon" href="/Public/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/Public/favicon.ico" type="image/x-icon">
    <title><?= $this->getTitle() ?></title>

    <?=
    $this
        ->addCssAsset(['identifier' => 'bootstrap4', 'extension' => 'ThemeBootstrap4', 'file' => 'bootstrap.min.css'])
        ->addCssAsset(['identifier' => 'main', 'extension' => 'ThemeBootstrap4', 'file' => 'main.css'])
        ->renderAssets()
    ?>
    <?= $this->renderDebugbarHeader() ?>
    <meta name="description" content="<?= $this->helper('Text')->stripNewlines($this->getPage()->getMetaDescription()) ?>"/>
    <meta name="keywords" content="<?= $this->getPage()->getMetaKeywords() ?>"/>

</head>
<?php if ($this->getBodyClass()) : ?>
<body class="<?= $this->getBodyClass() ?>">
<?php else: ?>
<body>
<?php endif ?>

<?= $this->plugin('ThemeBootstrap4', 'Menu', 'mainmenu'); ?>
<?= $this->showContainerColumn(1); ?>
<?= $this->plugin('ThemeBootstrap4', 'Menu', 'footer'); ?>

<?=
$this
    ->addJsAsset(['identifier' => 'jquery', 'extension' => 'ThemeBootstrap4', 'file' => 'jquery.min.js'])
    ->addJsAsset(['identifier' => 'swiper', 'extension' => 'ThemeBootstrap4', 'file' => 'popper.min.js'])
    ->addJsAsset(['identifier' => 'selectize', 'extension' => 'ThemeBootstrap4', 'file' => 'bootstrap.min.js'])
    ->addJsAsset(['identifier' => 'headroom', 'extension' => 'ThemeBootstrap4', 'file' => 'ie10-viewport-bug-workaround.js'])
    ->renderAssets();
?>
<?= $this->renderDebugbarContent() ?>
</body>
</html>
