<section class="full-section py-2 py-sm-4 <?= $theme ?>">
    <div class="container <?= $marginType ?>">
        <?= $this->showContainerColumn(4); ?>
    </div>
</section>