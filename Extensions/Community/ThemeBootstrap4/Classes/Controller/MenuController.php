<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project
 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 09.05.2015 @ 16:55
 * Project: Conţinut CMS
 */

namespace Continut\Extensions\Community\ThemeBootstrap4\Classes\Controller;

use Continut\Core\System\Controller\FrontendController;
use Continut\Core\Utility;

class MenuController extends FrontendController
{
    /**
     * Mainmenu action
     */
    public function mainmenuAction()
    {
        $pageCollection = Utility::createInstance(
            'Continut\Extensions\System\Frontend\Classes\Domain\Collection\FrontendPageCollection'
        )->whereInMenu(Utility::getSite()->getDomainUrl()->getId());

        $pageTree = $pageCollection->buildTree();

        $this->getView()->assignMultiple(
            [
                'currentPage' => Utility::getCurrentPage(),
                'pageTree'    => $pageTree
            ]
        );
        //$this->getPageLanguageMenu();
    }

    public function footerAction()
    {
    }
}
