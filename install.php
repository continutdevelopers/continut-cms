<?php
/**
 * This file is part of the Conţinut CMS project.
 * Distributed under the GNU General Public License.
 * For more details, consult the LICENSE.txt file supplied with the project

 * Author: Radu Mogoş <radu.mogos@pixelplant.ch>
 * Date: 30.03.2015 @ 20:40
 * Project: Conţinut CMS
 */

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * PLEASE NOTE THAT THIS IS A ONE SHOT SCRIPT
 * ONCE YOU HAVE COMPLETED THE INSTALLATION YOU CAN AND SHOULD! DELETE THIS install.php FILE
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

define('__ROOTCMS__', __DIR__);
define('DS', DIRECTORY_SEPARATOR);

// call the autoloader
require_once __ROOTCMS__ . DS . 'Lib' . DS . 'autoload.php';

/* @var $installer \Continut\Core\System\Setup\InstallController */
$installer = new \Continut\Core\System\Setup\InstallController();
$step = 'step' . ((isset($_GET['step'])) ? (int)$_GET['step'] : 1);
echo $installer->$step();
